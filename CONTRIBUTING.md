# Orientações para uma boa Revisão de Código
Utilize a [Piramide do CodeReview](http://www.dein.fr/2015-02-18-maslows-pyramid-of-code-review.html)
e o [Codacy](https://app.codacy.com/projects) para melhorar a percepção sobre as possíveis inconformidades do código:

Verifique se o código é:
## Correto: Faz o que é previsto na issue?
O código resolve o que é proposto na issue?

Levou-se em conta o desempenho satisfatório da aplicação?

## Seguro: Tem vulnerabilidades?
Os dados são armazenados com segurança? 

As informações críticas são tratadas corretamente? 

Os validações de entrada são suficientemente validadas?

## Legível: É fácil de ler e compreender? 
O código deixa claro as regras do negócio (o código é escrito para ser lido por um humano, não por um computador)?

As variáveis, métodos e classes são nomeadas apropriadamente? 

É Utilizado uma convenção de codificação consistente? Os códigos devem seguir os seguintes Guias de codificação: 

[Guia Básico de codificação PHP](http://confluence.unyleya.com.br/pages/viewpage.action?pageId=16255529)

[Guia Básico de Codificação JS](http://confluence.unyleya.com.br/pages/viewpage.action?pageId=16255527)

## Elegante: Utiliza padrões conhecidos?
O código chega ao objetivo sem sacrificar a simplicidade e a concisão? 

Você ficaria animado para trabalhar neste código? 

Você ficaria orgulhoso deste código?

## Altruísta: Deixa a base de código melhor do que era?
Ele está limpando o código não utilizado, aprimorando a documentação, introduzindo melhores padrões por meio de refatoração em pequena escala?

## Verifique também, se o pull-request foi criado corretamente
Verifique se o [pull-request](http://confluence.unyleya.com.br/pages/viewpage.action?pageId=8363690) foi criado corretamente seguindo o [GitFlow](http://confluence.unyleya.com.br/pages/viewpage.action?pageId=8363690)