const webpack = require('webpack');
const path = require('path');
const config = require('./webpack.config');

config.output = {
  devtool: 'eval',
  eslint: {
    formatter: require('eslint-friendly-formatter'),
    configFile: '.eslintrc',
    emitWarning: true,
    failOnWarning: true,
    failOnError: true,
    emitError: true
  },
  filename: '[name].bundle.js',
  publicPath: '/',
  path: path.resolve(__dirname, 'client'),
  output:{pathinfo: true}
};
// console.log(config.output);
//
// config.output.module.preLoaders.push(
//   {
//     test: /\.js$/,
//     loader: 'eslint-loader',
//     exclude: /node_modules/,
//   }
// );

config.plugins = config.plugins.concat([

  // Adds webpack HMR support. It act's like livereload,
  // reloading page after webpack rebuilt modules.
  // It also updates stylesheets and inline assets without page reloading.
  new webpack.HotModuleReplacementPlugin(),
]);

module.exports = config;
