//import angular from 'angular';
import uiRouter from 'angular-ui-router';
import wrapperComponent from './wrapper.component';
import Services from '../../services/Services';

const wrapperModule = angular.module('wrapper', [
  uiRouter,
  Services.name
])
  .config(($stateProvider, $urlRouterProvider) => {
    "ngInject";

    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('app', {
        url: '',
        template: '<wrapper></wrapper>',
        onEnter ($state, AuthService) {
          if (!AuthService.isAuthenticated()){
            $state.go('login');
          }
        }
      })
      .state('tcc', {
        url: '/enviar-tcc',
        template: '<pw-envio-tcc-form></pw-envio-tcc-form>'
      });
  })
  .component('wrapper', wrapperComponent);

export default wrapperModule;
