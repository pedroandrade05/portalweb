import logo2 from '../../../assets/img/logo-2.png';
import logoP from '../../../assets/img/logo-40x40.png';

class WrapperController {
  constructor(
    /* Úteis */ $state, $scope, $localStorage, $rootScope, $sce, $window,
    /* Services */ CampanhaComercialService, CursoService, CursoAlunoService, LinhaDeNegocioService, PessoaService, VendaService, OcorrenciaService,
    EsquemaConfiguracaoService, DownloadService,
    /* Constantes */ EsquemaConfiguracaoConstante, EntidadeConstante, EvolucaoMatriculaConstante
  ) {
    'ngInject';
    this.name = 'wrapper';

    // Services
    this.LinhaDeNegocioService = LinhaDeNegocioService;
    this.CursoService = CursoService;
    this.CursoAlunoService = CursoAlunoService;
    this.CampanhaComercialService = CampanhaComercialService;
    this.PessoaService = PessoaService;
    this.VendaService = VendaService;
    this.OcorrenciaService = OcorrenciaService;
    this.DownloadService = DownloadService;
    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao($state.params.idMatriculaAtiva);

    // Úteis
    this.$rootScope = $rootScope;
    this.$sce = $sce;
    this.$window = $window;
    this.state = $state;
    this.storage = $localStorage;

    // Constantes
    this.EsquemaConfiguracaoConstante = EsquemaConfiguracaoConstante;
    this.EntidadeConstante = EntidadeConstante;
    this.EvolucaoMatriculaConstante = EvolucaoMatriculaConstante;

    // Imagens
    this.logoUrl = logo2;
    this.logoUrlP = logoP;

    // Outros
    this.wrappertoggled = false;
    this.cursoAtivo = {};
    this.hasProfessor = false;
    this.getDadosCursoAtivo();

    // Verificar cursos de extensão (cursos livres) - Via Login Direto
    this.isCursoLivre = this.CursoService.isCursoLivre(this.cursoAtivo);
    $rootScope.cursoOrigem = angular.fromJson(this.storage.cursoOrigem);

    //Buscar venda da Matricula Atual
    this.venda = this.VendaService.getStorageVendaPorMatricula();

    $rootScope.countPromocoes = 0;
    $rootScope.wrapper = this;

    //define os dados do bot
    this.LinhaDeNegocioService.getLinhaDeNegocio(this.cursoAtivo.id_entidade).then((value) => {
      this.$rootScope.linha_de_negocio = value.linha_de_negocio;
      const stAfiliado = (this.venda ? this.venda.st_afiliado : null) || null;

      this.bot = {
        url_bot: this.cursoAtivo.st_conexao && this.cursoAtivo.st_chaveacesso ? $sce.trustAsResourceUrl(this.cursoAtivo.st_conexao + this.cursoAtivo.st_chaveacesso + '&widget=true&tab=true&top=40&text=Alguma%20dúvida%3F&textcolor=ffffff&bgcolor=F7A723&from=right&widgetType=baloon&iconId=&origem=true') : '',
        bot_permitido: this.cursoAtivo.bl_atendimentovirtual,
        params_bot: '&nome_usuario=' + $localStorage.user_name + '&email=' + $localStorage.user_email + '&cpf=' + $localStorage.user_cpf + '&id_entidade=' + this.cursoAtivo.id_entidade + '&tipo_portal=novo_portal&id_matricula=' + this.state.params.idMatriculaAtiva + '&curso=' + value.linha_de_negocio + '&afiliado=' + stAfiliado + '&interesse=Outros&departamento=unyleya&unidade=unyleya'
      };
    });

    //define o id usuario
    const idUsuario = $localStorage.user_id;
    const idEntidade = this.cursoAtivo.id_entidade;
    this.dadosUsuario = {
      st_nome: $localStorage.user_name,
      st_email: $localStorage.user_email,
      id_usuariooriginal: $localStorage.id_usuariooriginal,
      st_usuariooriginal: $localStorage.st_usuariooriginal,
      id_evolucao: $localStorage.idEvolucao,
      id_matricula: this.state.params.idMatriculaAtiva
    };

    this.buscaDadosUsuario(idUsuario, idEntidade);
    this.mostrarItens();

    this.buscarNumeroOcorrencias();

    /**
     * Evento para fechar o popover
     */
    this.$rootScope.$on('closePopover', () => {
      this.novoAtendimentoPopover.isOpen = false;
    });


    //cria um atributo para armazenar o id da sala de aula
    this.idSalaDeAula = null;


    /*
     * cria o watch para definir o valor da sala de aula quando o parametro existir ou deixar de existir,
     * isso irá definir quando será exibido o container que mostra o nome do professor
     */
    $scope.$watch(() => {
      return this.state.params.idSalaDeAula || this.state.params.idSaladeaula;
    }, (newValue) => {
      this.idSalaDeAula = newValue;
      if (!this.idSalaDeAula) {
        this.hasProfessor = false;
      }
    });

    // Objeto com os dados usado pelo popover
    this.novoAtendimentoPopover = {
      isOpen: false,
      html: true,
      template: 'popover-atendimento.html',
      title: 'Atendimento'
    };

  }

  abrirLink = () => {
    if (parseInt(this.cursoAtivo.id_entidade) === 12 || parseInt(this.cursoAtivo.id_entidade) === 158) {
      this.state.go('app.conteudo', {idMatriculaAtiva: this.state.params.idMatriculaAtiva});
    }
    else {
      this.state.go('app.curso', {idMatriculaAtiva: this.state.params.idMatriculaAtiva});
    }
  };

  /**
   * Verifica se a matricula é mesmo do usuario
   * @param idMatricula
   */
  verificaMatriculaAluno(idMatricula) {
    //chama a funcao da service para verificar a matricula do aluno
    this.CursoAlunoService.verificaMatriculaAluno(idMatricula)
      .then((response) => {
        //verifica se o retorno da promisse foi false e redireciona para a tela de selecao de curso
        if (!response) {
          this.state.go('login.selecionar-curso');
        }
      });
  }

  countPromocoes = () => {
    this.CampanhaComercialService.getResource().count({
      id_matricula: this.state.params.idMatriculaAtiva,
      bl_portaldoaluno: 1
    }, (response) => {
      this.$rootScope.countPromocoes = response.total;
    });
  };

  /**
   * Recupera os dados da matricula ativa
   */
  getDadosCursoAtivo() {
    //recupera o parametro da matricula ativa
    const idMatricula = this.state.params.idMatriculaAtiva;

    //verifica a matricula do aluno
    this.verificaMatriculaAluno(idMatricula);
    const objMatriculaAtiva = this.CursoAlunoService.getStorageCursoAtivo(idMatricula);

    this.countPromocoes();
    this.cursoAtivo = objMatriculaAtiva;

    this.cursoAtivo.bl_graducao = +this.cursoAtivo.id_esquemaconfiguracao === 1;

  }

  onToggle() {
    this.wrappertoggled = !this.wrappertoggled;
  }

  redirecionar(rota) {
    const idMatricula = this.cursoAtivo.id_matricula;
    this.state.go(rota, {
      idMatriculaAtiva: idMatricula
    });
  }

  /**
   * Função que busca dos dados do usuário
   * @param idUsuario
   * @param idEntidade
   */
  buscaDadosUsuario = (idUsuario, idEntidade) => {
    if (idUsuario) {
      this.PessoaService.getDadosPessoa(idUsuario, idEntidade).then((response) => {
        this.dadosUsuario.st_urlavatar = response.st_urlavatar;

        // Download do Avatar do aluno
        this.stUrlAvatar = null;
        const downloadAction = 'download-s3-direto';
        this.DownloadService.getAuth(downloadAction).then((auth) => {
          this.stUrlAvatar = this.DownloadService.geraUrlDocumento(auth, downloadAction, this.dadosUsuario.st_urlavatar, 'usuario');
        });
      }, (error) => {
        this.toaster.notify(error.data);
      });
    }
  };

  mostrarItens = () => {
    this.mostrarItem = !((+this.dadosUsuario.id_evolucao === +this.EvolucaoMatriculaConstante.EGRESSO)
      && ((+this.cursoAtivo.id_entidade === +this.EntidadeConstante.UCAM)
        || (+this.cursoAtivo.id_entidade === +this.EntidadeConstante.FACULDADE_UNYLEYA)));
  };


  buscarNumeroOcorrencias = () => {
    const id_matricula = this.state.params.idMatriculaAtiva ? this.state.params.idMatriculaAtiva : null;
    this.OcorrenciaService.numeroOcorrenciasInteressado(id_matricula).then((response) => {
      this.$rootScope.numeroOcorrenciasInteressado = response.ocorrencias;
    });
  };

  handleVoltarGraduacao = () => {
    const idMatricula = this.$rootScope.cursoOrigem.id_matricula;

    this.storage.cursoOrigem = null;

    this.state.go('login.selecionar-curso', {
      idMatricula,
      idCurso: this.$rootScope.cursoOrigem.id_curso
    });
  };

}

export default WrapperController;
