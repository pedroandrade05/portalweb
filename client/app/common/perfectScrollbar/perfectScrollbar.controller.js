/**
 * @description Classe de directive de scrollbar usada em todo o projeto
 * @constructor
 */
class PerfectScrollbarController {
  constructor() {
    this.name = 'perfectScrollbar';
  }
}

export default PerfectScrollbarController;
