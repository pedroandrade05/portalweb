import PerfectScrollbarModule from './perfectScrollbar';
import PerfectScrollbarController from './perfectScrollbar.controller';
import PerfectScrollbarComponent from './perfectScrollbar.component';
import PerfectScrollbarTemplate from './perfectScrollbar.html';

describe('PerfectScrollbar', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PerfectScrollbarModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PerfectScrollbarController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PerfectScrollbarComponent;

    it('includes the intended template',() => {
        expect(component.template).to.equal(PerfectScrollbarTemplate);
      });

    it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

    it('invokes the right controller', () => {
        expect(component.controller).to.equal(PerfectScrollbarController);
      });
  });
});
