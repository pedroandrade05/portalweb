import template from './menuSideBarMain.html';
import controller from './menuSideBarMain.controller';
import './menuSideBarMain.scss';

const menuSideBarMainComponent = {
  restrict: 'E',
  bindings: {
    wrappertoggled:'='
  },
  template,
  controller,
  controllerAs: 'wrapper'
};

export default menuSideBarMainComponent;
