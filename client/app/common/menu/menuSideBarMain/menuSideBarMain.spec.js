import MenuSideBarModule from './menuSideBarMain';
import MenuSideBarController from './menuSideBarMain.controller';
import MenuSideBarComponent from './menuSideBarMain.component';
import MenuSideBarTemplate from './menuSideBarMain.html';

describe('MenuSideBar', () => {
  let $rootScope, makeController;

  beforeEach(window.module(MenuSideBarModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new MenuSideBarController();
    };
  }));

  describe('Module', () => {

        // top-level specs: i.e., routes, injection, naming
  });


  describe('Template', () => {

  });

  describe('Component', () => {
        // component/directive specs
    const component = MenuSideBarComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(MenuSideBarTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(MenuSideBarController);
    });
  });

});
