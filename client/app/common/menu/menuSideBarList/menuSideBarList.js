import uiRouter from 'angular-ui-router';
import MenuSideBarListComponent from './menuSideBarList.component';
import Services from '../../../services/Services';

const menuSideBarListModule = angular.module('menuSideBarList', [
  uiRouter,
  Services.name
]).config(($stateProvider, $urlRouterProvider) => {
  "ngInject";

  $urlRouterProvider.otherwise('/');

  $stateProvider.state('app.menusidebarlist', {
    url: '',
    views: {
      'menu-side-bar-list-view': {
        template: '<menu-side-bar-list></menu-side-bar-list>'
      }
    }
  });

}).component('menuSideBarList', MenuSideBarListComponent);

export default menuSideBarListModule;