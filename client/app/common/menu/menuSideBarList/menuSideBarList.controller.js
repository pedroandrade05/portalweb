import loadingGif from '../../../../assets/img/loading.gif';
import googlePlay from '../../../../assets/img/google_play.svg';
import appleStore from '../../../../assets/img/apple_store.svg';
import cellphone from '../../../../assets/img/cellphone.svg';
import estrela_transparente from '../../../../assets/img/estrela_transparente.svg';
import iconeCupom from '../../../../assets/img/icone-indique-cupom.png';

class menuSideBarListController {
  constructor(
    /* Úteis */  CONFIG, $state, $rootScope, $window, $localStorage,
    /* Services */ NotifyService, MenuService, ModalService, CursoService, CursoAlunoService, EsquemaConfiguracaoService, AuthService, PortalEgressoService, FinanceiroService,
    /* Constantes */ EvolucaoMatriculaConstante, FuncionalidadeConstante, MeioPagamentoConstante) {
    'ngInject';

    this.name = 'menuSideBarList';
    this.$localStorage = $localStorage;
    this.loadingGif = loadingGif;
    this.cellphone = cellphone;
    this.googlePlay = googlePlay;
    this.appleStore = appleStore;
    this.estrela_transparente = estrela_transparente;
    this.loading = false;
    this.CONFIG = CONFIG;
    this.$state = $state;
    this.$scope = $rootScope;
    this.$window = $window;

    /* Services */
    this.toaster = NotifyService;
    this.CursoService = CursoService;
    this.CursoAlunoService = CursoAlunoService;
    this.EsquemaConfiguracaoService = EsquemaConfiguracaoService;
    this.MenuService = MenuService;
    this.AuthService = AuthService;
    this.ModalService = ModalService;
    this.PortalEgressoService = PortalEgressoService;
    this.FinanceiroService = FinanceiroService;

    /* Constante */
    this.FuncionalidadeConstante = FuncionalidadeConstante;
    this.EvolucaoMatriculaConstante = EvolucaoMatriculaConstante;
    this.MeioPagamentoConstante = MeioPagamentoConstante;


    this.itemover = false;

    this.cursoAtivo = {};
    this.menusidebar = [];
    this.portalEgresso = $localStorage.portalEgresso || {};
    this.idMatricula = this.$state.params.idMatriculaAtiva;
    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao(this.idMatricula);
    this.lancamentosMes = [];
    this.bl_minhabiblioteca = false;
    this.matriculaAtiva = this.CursoAlunoService.getStorageCursoAtivo(this.idMatricula);

    this.getMostrarMinhaBiblioteca(this.matriculaAtiva.id_curso);
    this.getDadosCursoAtivo();
    this.buscarFinanceiroMes();
    this.buscaMenuSideBar();

    this.showTrocarCurso = false;
    this.isCursoLivre = this.CursoService.isCursoLivre(this.cursoAtivo.curso);

    // Aluno egresso?
    this.isAlunoEgresso = $localStorage.isAlunoEgresso;
    this.checkAlunoEgresso().catch(() => null);

    // Curso livre não deve mostrar o botão "Trocar Curso"
    if (!this.isCursoLivre) {
      this.CursoAlunoService.getResource().getAll({
        bl_novoportal: true,
        'id_entidade[]': this.CONFIG.idEntidade
      }, (response) => {
        if (response[0].cursos.length > 1) {
          this.showTrocarCurso = true;
        }
      });
    }
  }

  /**
   * Função que busca dos dados do usuário
   * @param idUsuario
   * @param idEntidade
   */
  buscaMenuSideBar = () => {
    const {id_evolucao} = this.cursoAtivo.curso;

    this.MenuService.getMenuSideBar(this.cursoAtivo.id_entidade, id_evolucao, this.cursoAtivo.id_matricula).then((response) => {
      this.menusidebar = response;

      if (this.isCursoLivre) {
        this.menusidebar.unshift({
          st_funcionalidade: 'Meu Curso',
          st_urlcaminho: 'app.curso',
          st_urlicone: 'icon-icon_livro'
        });
      }

      this.menusidebar.forEach((item) => {
        if (+item.id_funcionalidade === this.FuncionalidadeConstante.PROVAS_ONLINE_ESCOLA_SAUDE) {
          item.st_funcionalidade = 'Provas Online';
        }
        else if (+item.id_funcionalidade === this.FuncionalidadeConstante.BIBLIOTECA_VIRTUAL && this.bl_minhabiblioteca) {
          item.st_funcionalidade = 'Bibliotecas Virtuais';
        }
      });

      this.exibirBibliotecaTemporario();
    }, (error) => {
      this.toaster.notify(error.data);
    });
  };

  /**
   * Esta função é responsável por exibir o html que foi colocado no menu para exibir um novo item de menu que redireciona
   * o usuário do id que esta na função para o link de uma nova biblioteca de livros. Esse item de menu foi colocado
   * assim porque foi decidido entre os gestores que iriamos fazer isso para uma apresentação do portal do aluno para
   * uma equipe do MEC, então em um momento oportuno isso será removido do código junto com seu respectivo html.
   * @returns {boolean}
   */
  exibirBibliotecaTemporario = () => {
    const idUserTemporario = 10166204;

    if (this.cursoAtivo.id_usuario === idUserTemporario) {
      return true;
    }

    return false;
  };

  /**
   * @deprecated
   * @param rota
   */
  redirecionar(rota) {
    const idMatricula = this.$state.params.idMatriculaAtiva;
    this.$state.go(rota, {
      idMatriculaAtiva: idMatricula
    });
  }

  /**
   * Recupera os dados da matricula ativa
   */
  getDadosCursoAtivo() {
    //recupera o parametro da matricula ativa
    const idMatricula = this.$state.params.idMatriculaAtiva;
    this.cursoAtivo.id_matricula = idMatricula;
    this.cursoAtivo.curso = this.CursoAlunoService.getStorageCursoAtivo(idMatricula);
    this.cursoAtivo.id_entidade = this.AuthService.getUserCredential('user_entidade');
    this.cursoAtivo.id_usuario = this.AuthService.getUserCredential('user_id');

  }

  abrirRenovacao() {
    return {idMatriculaAtiva: this.idMatricula};
  }

  onToggle() {
    this.$scope.$root.wrapper.onToggle();
  }

  openGooglePlay() {
    this.$window.open('https://play.google.com/store/apps/details?id=com.unyleya.portalapp&hl=pt_BR');
  }

  openAppleStore() {
    this.$window.open('https://itunes.apple.com/br/app/portal-do-aluno/id1037099343?mt=8');
  }

  checkAlunoEgresso = async () => {
    this.isAlunoEgresso = await this.PortalEgressoService.isAlunoEgresso(this.cursoAtivo.id_matricula);
    this.$localStorage.isAlunoEgresso = this.isAlunoEgresso;

    if (this.isAlunoEgresso) {
      this.getDadosPortalEgresso();
    }
  }

  getDadosPortalEgresso = () => {
    this.PortalEgressoService.getDadosPortal().then((response) => {
      this.portalEgresso = response || {};
      this.$localStorage.portalEgresso = this.portalEgresso;
    }, () => null);
  };

  handleOpenMenu = (e, menu) => {
    const {id_funcionalidade} = menu;
    const {st_urlportal} = this.portalEgresso;

    let bl_bloquear = false;

    let title = 'Atenção!';
    let body = 'Este recurso não está disponível.';
    let confirmButton = 'Fechar';
    let cancelButton = false;
    let callback = null;

    // Se o aluno for egresso, bloquear acesso à algumas funcionalidades
    if (this.isAlunoEgresso) {
      if (+id_funcionalidade === this.FuncionalidadeConstante.INDIQUE_GANHE) {
        bl_bloquear = true;

        const icon = `<img src="${iconeCupom}" style="width: 100px"/>`;

        const isCertificado = [
          this.EvolucaoMatriculaConstante.CERTIFICADO,
          this.EvolucaoMatriculaConstante.DIPLOMADO,
          this.EvolucaoMatriculaConstante.EGRESSO
        ].includes(+this.cursoAtivo.curso.id_evolucao);

        cancelButton = false;

        // Textos para alunos NÃO certificados
        title = 'Indique e Ganhe';
        body = `${icon}<br/><br/>Para você que está na fase final do curso o Indique e Ganhe agora está no Portal do Egresso.`;
        confirmButton = st_urlportal ? 'Acesse e continue indicando' : 'Fechar';

        // Textos para alunos de GRADUAÇÃO ou alunos JÁ certificados
        if (this.isGraduacao || isCertificado) {
          title = 'Indique e Ganhe';
          body = `${icon}<br/><br/>Como aluno formado, agora você deve acessar o Indique e Ganhe a partir do Portal do Egresso.`;
          confirmButton = st_urlportal ? 'Acesse o Portal do Egresso' : 'Fechar';
        }

        callback = async () => {
          const {id_entidade, id_usuario, st_urlacesso} = this.cursoAtivo.curso;

          if (st_urlportal) {
            const url = await this.PortalEgressoService.getUrlAutenticarDireto(id_usuario, id_entidade, st_urlacesso, 'indique-e-ganhe', st_urlportal);
            this.$window.open(url, 'uny-portal-egresso');
          }
        };
      }
    }

    // Bloquear acesso ao menu
    if (bl_bloquear) {
      e.preventDefault();

      this.ModalService.show({
        title,
        body,
        confirmButton,
        cancelButton
      }).then(callback);
    }
  };

  /**
   * Buscando lançamentos do mês atual para alerta
   */
  buscarFinanceiroMes() {
    this.FinanceiroService.getResource().getAll({}, (response) => {
      if (response.length > 1) {
        var dataAtual = new Date();
        this.lancamentosMes = response.filter((lancamento) => {
          return ((new Date(lancamento.dt_vencimento)).getMonth() === dataAtual.getMonth() &&
            (new Date(lancamento.dt_vencimento)).getFullYear() === dataAtual.getFullYear() &&
            !lancamento.bl_quitado && lancamento.id_meiopagamento !== this.MeioPagamentoConstante.CARTAO_CREDITO);
        });
      }
    }, (error) => {
      this.toaster.notify(error);
    });
  }

  /**
   * Verifica se o projeto pedagógico tem permissão de exibir a integração Minha Biblioteca
   */
  getMostrarMinhaBiblioteca(id_projetopedagogico) {
    this.CursoAlunoService.getMostrarMinhaBiblioteca(id_projetopedagogico)
      .then((response) => {
        this.bl_minhabiblioteca = response.bl_minhabiblioteca;
      });
  }
}

export default menuSideBarListController;
