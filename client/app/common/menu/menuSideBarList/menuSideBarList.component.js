import template from './menuSideBarList.html';
import controller from './menuSideBarList.controller';
import './menuSideBarList.scss';

const menuSideBarListComponent = {
  restrict: 'E',
  bindings: { wrappertoggled : '<'},
  template,
  controller,
  controllerAs: 'vm'
};

export default menuSideBarListComponent;
