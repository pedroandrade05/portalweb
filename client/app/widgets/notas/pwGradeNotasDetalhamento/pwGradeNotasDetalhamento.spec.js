import PwGradeNotasDetalhamentoModule from './pwGradeNotasDetalhamento';
import PwGradeNotasDetalhamentoController from './pwGradeNotasDetalhamento.controller';
import PwGradeNotasDetalhamentoComponent from './pwGradeNotasDetalhamento.component';
import PwGradeNotasDetalhamentoTemplate from './pwGradeNotasDetalhamento.html';

describe('PwGradeNotasDetalhamento', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwGradeNotasDetalhamentoModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwGradeNotasDetalhamentoController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PwGradeNotasDetalhamentoComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PwGradeNotasDetalhamentoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwGradeNotasDetalhamentoController);
    });
  });
});
