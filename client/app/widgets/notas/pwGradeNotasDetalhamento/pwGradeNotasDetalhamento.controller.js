import loadingGif from "../../../../assets/img/loading.gif";

const _ = require('lodash');

class PwGradeNotasDetalhamentoController {
  constructor(CONFIG, NotasService, $state, CursoAlunoService, SituacaoConstante, EsquemaConfiguracaoConstante) {
    "ngInject";

    //inicia a variavel de loading
    this.loading = false;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;

    this.name = 'pcGradeNotasForm';
    this.NotasService = NotasService;
    this.$state = $state;
    this.notas = [];
    this.erro = '';
    this.alerta = '';
    this.exibirGrade = {};
    this.stringsLegenda = null;

    this.getGradeNotasDetalhada();

    this.SituacaoConstante = SituacaoConstante;

    this.matriculaAtiva = CursoAlunoService.getStorageCursoAtivo(this.$state.params.idMatriculaAtiva);
    this.exibirGrade.graduacao = +this.matriculaAtiva.id_esquemaconfiguracao === EsquemaConfiguracaoConstante.GRADUACAO;
    this.exibirGrade.pos_graduacao = +this.matriculaAtiva.id_esquemaconfiguracao === EsquemaConfiguracaoConstante.POS_GRADUACAO;
  }

  /** Busca todas as notas da matrícula e formata os dados para exibição
   * correta na grade de notas.
   * @function getGradeNotas
   */

  getGradeNotasDetalhada = () => {
    this.loading = true;

    this.NotasService.getGradeNotasDetalhada(this.$state.params.idMatriculaAtiva, this.$state.params.idDisciplina)
      .then((success) => {
        this.notas = success;

        /* Itera sobre as notas de cada módulo, verificando a existência
         de notas de recuperação / atividade. Valores salvos nas variáveis
         são utilizados para manipular a exibição das respectivas colunas
         no template. */

        _.forEach(this.notas, (value) => {
          _.forEach(value.disciplinas, (disciplina) => {

            this.alerta = null;

            if (angular.isDefined(disciplina.ar_notasmoodle) === false || disciplina.ar_notasmoodle === null) {
              this.alerta = 'A disciplina não foi avaliada. Por favor, tente mais tarde.';
            }

            if (disciplina.st_avaliacaorecuperacao !== null) {
              value.temNotaRecuperacao = 1;
            }
            if (disciplina.st_avaliacaoatividade !== null) {
              value.temNotaAtividade = 1;
            }
            if (disciplina.st_notaead_prr) {
              value.temNotaPrrEad = 1;
            }
            if (disciplina.st_notatcc) {
              value.temNotaTcc = 1;
            }
            if (disciplina.st_notatcc_prr && disciplina.dt_defesatcc) {
              value.temNotaPrrTcc = 1;
            }

            /*
            POR-444
            Trocando a informação aproveitamento de disciplina  para sigual AE(Aproveitamento de Estudo)
            */
            if (disciplina.id_situacao === this.SituacaoConstante.APROVEITAMENTO_DISCIPLINA) {
              // GII-10821 - Disciplina Isenta
              disciplina.st_situacao = this.exibirGrade.graduacao ? 'Isenta' : this.SiglasSistema.APROVEITAMENTO_ESTUDO;
            }
          });
        });

        this.getStringsLegenda();

      }, (error) => {
        this.erro = error.data.message;
      }).finally(() => {
        this.loading = false;
      });
  };

  /**
   * Retorna strings de legenda das cores (utilizadas na tabela de legendas)
   * @function getStringsLegenda
   */

  getStringsLegenda = () => {
    const isGraduacao = this.exibirGrade.graduacao;

    // Graduação
    if (isGraduacao) {
      this.stringsLegenda = {
        Aprovado: {
          label: 'Aprovado',
          classeCor: 'verde',
          classeLegenda: 'verde-legenda'
        },
        'Em Prova Final': {
          label: 'Em Prova Final',
          classeCor: 'azul',
          classeLegenda: 'azul-legenda'
        },
        Reprovado: {
          label: 'Reprovado',
          classeCor: 'vermelho',
          classeLegenda: 'vermelho-legenda'
        }
      };
    }

    //Caso pós graduação.
    else {
      this.stringsLegenda = {
        Aprovado: {
          label: 'Aprovado',
          classeCor: 'verde',
          classeLegenda: 'verde-legenda'
        },
        Satisfatório: {
          label: 'Satisfatório',
          classeCor: 'azul',
          classeLegenda: 'azul-legenda'
        },
        Insatisfatório: {
          label: 'Insatisfatório',
          classeCor: 'vermelho',
          classeLegenda: 'vermelho-legenda'
        }
      };
    }
  };

  /**
   * Retorna um hífen (-) caso o valor da nota seja null.
   * @param {string} nota - Nota a ser formatada.
   * @return {string}
   */
  formataNota = (nota) => {
    return nota !== null ? nota : nota = '-';
  }

  /** Retorna uma variável boolean informando se há ou não nota final
   * PRR. Serve como condição para exibir corretamente as cores do status
   * e a nota final PRR quando existir.
   * @function temNotaFinalPRR
   * @param {object} disciplina - Objeto com os dados da disciplina.
   * @returns boolean
   * */

  temNotaFinalPRR = (disciplina) => {
    return (disciplina.id_disciplina_prr !== '' && disciplina.st_notaead_prr !== null);
  };

  /***
   * Exibe a nota final adequada, seja ela a nota final ou
   * a nota final PRR.
   * @param {object} disciplina - Objeto com os dados da disciplina.
   * @return {string}
   */

  exibeNotaFinal = (disciplina) => {

    const temNotaFinalPRR = this.temNotaFinalPRR(disciplina);

    if (temNotaFinalPRR) {
      if (disciplina.nu_notafinal_prr === 0 || disciplina.nu_notafinal_prr === null) {
        return '-';
      }
      if (disciplina.nu_notafinal_prr !== 0 && disciplina.nu_notafinal_prr !== null) {
        return disciplina.nu_notafinal_prr;
      }
    }

    else {
      if (disciplina.nu_notafinal === 0 || disciplina.nu_notafinal === null) {
        return '-';
      }
      if (disciplina.nu_notafinal !== 0 && disciplina.nu_notafinal !== null) {
        return disciplina.nu_notafinal;
      }
    }
  };

}

export default PwGradeNotasDetalhamentoController;
