import loadingGif from "../../../../assets/img/loading.gif";
import Utils from "../../../classes/utils";
const _ = require('lodash');

class PwGradeNotasController {
  constructor(CONFIG, NotasService, $state, CursoAlunoService, SituacaoConstante, SiglasSistema, EsquemaConfiguracaoConstante) {
    "ngInject";

    //inicia a variavel de loading
    this.loading = false;

    this.CONFIG = CONFIG;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;
    this.name = 'pcGradeNotasForm';
    this.NotasService = NotasService;
    this.$state = $state;
    this.notas = [];
    this.exibirGrade = {};
    this.stringsLegenda = null;
    this.removerAcento = Utils.removerAcento;

    this.getGradeNotas();

    this.SituacaoConstante = SituacaoConstante;
    this.SiglasSistema = SiglasSistema;

    this.matriculaAtiva = CursoAlunoService.getStorageCursoAtivo(this.$state.params.idMatriculaAtiva);
    this.exibirGrade.graduacao = +this.matriculaAtiva.id_esquemaconfiguracao === EsquemaConfiguracaoConstante.GRADUACAO;
    this.exibirGrade.pos_graduacao = +this.matriculaAtiva.id_esquemaconfiguracao === EsquemaConfiguracaoConstante.POS_GRADUACAO;

    this.isEscolaSaude = [
      EsquemaConfiguracaoConstante.ESCOLA_TECNICA_SEMESTRAL,
      EsquemaConfiguracaoConstante.ESCOLA_TECNICA_PADRAO_POS,
      EsquemaConfiguracaoConstante.GRADUACAO_ENFERMAGEM
    ].includes(this.matriculaAtiva.id_esquemaconfiguracao);
  }

  /** Busca todas as notas da matrícula e formata os dados para exibição
   * correta na grade de notas.
   * @function getGradeNotas
   */

  getGradeNotas = () => {
    this.loading = true;

    this.NotasService.getGradeNotas(this.$state.params.idMatriculaAtiva)
      .then((success) => {
        this.notas = success;

        /* Itera sobre as notas de cada módulo, verificando a existência
         de notas de recuperação / atividade. Valores salvos nas variáveis
         são utilizados para manipular a exibição das respectivas colunas
         no template. */

        _.forEach(this.notas, (value) => {
          _.forEach(value.disciplinas, (disciplina) => {
            if (disciplina.st_avaliacaorecuperacao !== null) {
              value.temNotaRecuperacao = 1;
            }
            if (disciplina.st_avaliacaoatividade !== null) {
              value.temNotaAtividade = 1;
            }
            if (disciplina.st_notaead_prr) {
              value.temNotaPrrEad = 1;
            }
            if (disciplina.st_notatcc && disciplina.st_notatcc !== '-') {
              value.temNotaTcc = 1;
            }
            if (disciplina.st_notatcc_prr && disciplina.dt_defesatcc) {
              value.temNotaPrrTcc = 1;
            }

            /*
            POR-444
            Trocando a informação aproveitamento de disciplina  para sigual AE(Aproveitamento de Estudo)
            */
            if (disciplina.id_situacao === this.SituacaoConstante.APROVEITAMENTO_DISCIPLINA) {
              // GII-10821 - Disciplina Isenta
              disciplina.st_situacao = this.exibirGrade.graduacao ? 'Isenta' : this.SiglasSistema.APROVEITAMENTO_ESTUDO;
            }

          });
        });

        this.getStringsLegenda();

      }).finally(() => {
        this.loading = false;
      });
  };

  /** Retorna a string da classe que colore o <td>
   * baseando-se no tipo de curso e status.
   * @function getClasseCor
   * @param {object} disciplina - Objeto com os dados da disciplina.
   * @returns string
   */

  getClasseCor = (disciplina) => {

    /*
      Strings de status são geradas diretamente na vw_gradenota.
      st_status -> pós
      st_statusdisciplina -> graduação
     */

    const isGraduacao = this.exibirGrade.graduacao;

    // Graduação
    if (isGraduacao) {
      switch (disciplina.st_statusdisciplina) {
      case 'Aprovado':
        return 'verde';
      case 'Em Prova Final':
        return 'azul';
      case 'Reprovado':
        return 'vermelho';
      case 'Não Iniciada':
        return 'branco_';
      case '-':
        return 'cinza';
      case 'Concluido':
        return 'verde';
      case 'Concluído':
        return 'verde';
      case 'Não Concluido':
        return 'vermelho';
      case 'Isento':
      case 'Aproveitamento de Disciplina':
        return 'amarelo';
      default:
        return '';
      }
    }

    //Pós Graduação
    else {
      const status = (this.temNotaFinalPRR(disciplina) && +disciplina.st_notaead_prr > +disciplina.st_notaead) ? disciplina.st_status_prr : disciplina.st_status;

      switch (status) {
      case 'Aprovado':
        return 'verde';
      case 'Satisfatório':
        return 'azul';
      case 'Insatisfatório':
        return 'vermelho';
      case 'Isento':
      case 'Aproveitamento de Disciplina':
        return 'amarelo';
      default:
        return '';
      }
    }
  };


  /** Retorna strings de legenda das cores (utilizadas na tabela de legendas)
   * com base no tipo de curso.
   * @function getStringsLegenda
   */

  getStringsLegenda = () => {
    const isGraduacao = this.exibirGrade.graduacao;

    // Graduação
    if (isGraduacao) {
      this.stringsLegenda = {
        Aprovado: {
          label: 'Aprovado',
          classeCor: 'verde',
          classeLegenda: 'verde-legenda'
        },
        'Em Prova Final': {
          label: 'Em Prova Final',
          classeCor: 'azul',
          classeLegenda: 'azul-legenda'
        },
        Reprovado: {
          label: 'Reprovado',
          classeCor: 'vermelho',
          classeLegenda: 'vermelho-legenda'
        },
        'AE- Aproveitamento de Estudos': {
          label: 'Disciplina Isenta',
          classeCor: 'amarelo',
          classeLegenda: 'amarelo-legenda'
        },
        Cursando: {
          label: 'Cursando',
          classeCor: 'cinza',
          classeLegenda: 'cinza-legenda'
        },
        'Disciplina a cursar': {
          label: 'Disciplina a cursar',
          classeCor: 'branco',
          classeLegenda: 'branco-legenda'
        }
      };
    }

    // Pós graduação.
    else {
      this.stringsLegenda = {
        Aprovado: {
          label: 'Aprovado',
          classeCor: 'verde',
          classeLegenda: 'verde-legenda'
        },
        Satisfatório: {
          label: 'Satisfatório',
          classeCor: 'azul',
          classeLegenda: 'azul-legenda'
        },
        Insatisfatório: {
          label: 'Insatisfatório',
          classeCor: 'vermelho',
          classeLegenda: 'vermelho-legenda'
        },
        'AE- Aproveitamento de Estudos': {
          label: 'AE - Aproveitamento de Estudos',
          classeCor: 'amarelo',
          classeLegenda: 'amarelo-legenda'
        },
        Cursando: {
          label: 'Cursando',
          classeCor: 'cinza',
          classeLegenda: 'cinza-legenda'
        }
      };
    }
  };

  /** Retorna uma variável boolean informando se há ou não nota final
   * PRR. Serve como condição para exibir corretamente as cores do status
   * e a nota final PRR quando existir.
   * @function temNotaFinalPRR
   * @param {object} disciplina - Objeto com os dados da disciplina.
   * @returns boolean
   * */

  temNotaFinalPRR = (disciplina) => {
    return (disciplina.id_disciplina_prr !== '' && disciplina.st_notaead_prr !== null);
  };

  /***
   * Exibe a nota final adequada, seja ela a nota final ou
   * a nota final PRR.
   * @param {object} disciplina - Objeto com os dados da disciplina.
   * @return {string}
   */

  exibeNotaFinal = (disciplina) => {

    const temNotaFinalPRR = this.temNotaFinalPRR(disciplina);

    if (temNotaFinalPRR) {
      if (disciplina.nu_notafinal_prr === 0 || disciplina.nu_notafinal_prr === null) {
        return '-';
      }
      if (disciplina.nu_notafinal_prr !== 0 && disciplina.nu_notafinal_prr !== null) {
        return disciplina.nu_notafinal_prr;
      }
    }

    else {
      if (this.verificaNotaConceito(disciplina.st_statusdisciplina)){
        return disciplina.st_statusdisciplina;
      }
      if (disciplina.nu_notafinal === 0 || disciplina.nu_notafinal === null) {
        return '-';
      }
      if (disciplina.id_tiponotatcc === 1 && (disciplina.nu_notafinal === '-' || disciplina.nu_notafinal === null)) {
        return '0';
      }
      return disciplina.nu_notafinal;
    }
  };

  verificaNotaConceito(status){

    var text = this.removerAcento(status);

    switch (text){
    case 'concluido':
      return true;
    case 'nao concluido':
      return true;
    default:
      return false;
    }
  }

  /**
   * Retorna um hífen (-) caso o valor da nota seja null.
   * @param {string} nota - Nota a ser formatada.
   * @return {string}
   */
  formataNota = (nota) => {
    return nota !== null ? nota : nota = '-';
  }

  imprimirGradeNota() {
    return this.CONFIG.urlG2 + 'paper/imprimir-grade-nota?id_matricula=' + this.matriculaAtiva.id_matricula;
  }
}

export default PwGradeNotasController;
