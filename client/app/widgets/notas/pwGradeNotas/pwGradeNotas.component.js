import template from './pwGradeNotas.html';
import controller from './pwGradeNotas.controller';
import './pwGradeNotas.scss';

const pwGradeNotasComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pwGradeNotasComponent;
