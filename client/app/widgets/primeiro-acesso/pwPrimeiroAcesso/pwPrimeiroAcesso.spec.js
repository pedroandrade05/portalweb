import PwPrimeiroAcessoModule from './pwPrimeiroAcesso';
import PwPrimeiroAcessoController from './pwPrimeiroAcesso.controller';
import PwPrimeiroAcessoComponent from './pwPrimeiroAcesso.component';
import PwPrimeiroAcessoTemplate from './pwPrimeiroAcesso.html';

describe('PwPrimeiroAcesso', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwPrimeiroAcessoModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwPrimeiroAcessoController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PwPrimeiroAcessoComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PwPrimeiroAcessoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwPrimeiroAcessoController);
    });
  });
});
