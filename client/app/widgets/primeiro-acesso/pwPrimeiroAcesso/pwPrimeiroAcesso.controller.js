import imgAluno from "../../../../assets/img/primeiroacesso1.png";
import imgAtendente from "../../../../assets/img/primeiroacesso2.png";
class PwPrimeiroAcessoController {

  constructor(CONFIG, $rootScope, $window, $scope, $state, $localStorage, NotifyService, $uibModal, $uibModalInstance, CursoAlunoService, OcorrenciaService, PrimeiroAcessoService, MensagemPadrao) {
    'ngInject';

    this.name = 'pwPrimeiroAcesso';
    this.state = $state;
    this.scope = $scope;
    this.$rootScope = $rootScope;
    this.modal = $uibModal;
    this.toaster = NotifyService;
    this.PrimeiroAcessoService = PrimeiroAcessoService;
    this.modalPrimeiroAcesso = $uibModalInstance;
    this.imgAluno = imgAluno;
    this.imgAtendente = imgAtendente;
    this.OcorrenciaService = OcorrenciaService;
    this.CursoAlunoService = CursoAlunoService;
    this.MensagemPadrao = MensagemPadrao;
    this.storage = $localStorage;

    this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);

    this.dados = {
      stCurso: this.MatriculaAtiva.st_curso,
      stNomeCompleto: $localStorage.user_name,
      stNomeCurto: $localStorage.user_shortname
    };
    this.scrollopts = {
      wheelSpeed: 4,
      minScrollbarLength: 50
    };

    this.dadosContrato = {};
    this.ocorrencia = {
      bl_primeiroacesso: true,
      id_matricula: this.MatriculaAtiva.id_matricula
    };
    this.retornaContrato();
  }

  /**
   * Atualiza Storage do Curso Ativo
   * @constructor
   */
  atualizaStorageCursoAtivo = () => {
    this.$rootScope.$emit('atualizaStorage');
  };

  abrirAtendimento() {
    this.atendimento = true;
  }

  closeAtendimento() {
    this.atendimento = false;
  }

  closePrimeiroAcesso() {
    this.modalPrimeiroAcesso.close();
  }

  termoAceite() {
    this.btntermoaceite = true;
  }

  viewContrato() {
    this.exibircontrato = !this.exibircontrato;
  }

  /**
   * Envia os dados para registrar o aceite do contrato
   */
  aceiteContrato() {
    this.PrimeiroAcessoService.postAceiteContrato(this.MatriculaAtiva.id_matricula).then((response) => {
      if (response.tipo) {
        this.toaster.notify('success', '', response.text);
        this.modalPrimeiroAcesso.close();
        this.atualizaStorageCursoAtivo();
      } 
      

      else {
        this.toaster.notify('error', '', response.text);
      }
    }, (error) => {
      this.toaster.notify(error.data);
    });
  }

  /**
   * Retorna os dados do contrato
   */
  retornaContrato() {
    this.PrimeiroAcessoService.getPrimeiroAcesso(this.MatriculaAtiva.id_matricula).then((response) => {
      this.dadosContrato = response;

      //verifica se ja existe alguma ocorrencia e desabilita alguns itens da DOM
      this.bl_ocorrencia = this.dadosContrato.bl_ocorrencia;
    }, () => {});
  }

  /**
   * Salva ocorrencia, parâmetros e arquivo anexado
   */
  save = () => {
    this.OcorrenciaService.create(this.ocorrencia).success(() => {
      this.toaster.notify('success', '', this.dados.stNomeCurto + this.MensagemPadrao.ARQUIVO_FORA_DO_FORMATO_PERMITIDO);
      this.ocorrencia.st_ocorrencia = '';
      this.ocorrencia.files = '';
      this.bl_ocorrencia = true;
      this.closeAtendimento();
    }).error((error) => {
      this.toaster.notify(error.type, error.message);
    });
  };

    /**
   * Abre a ocorrencia existente
   */
  goToOcorrencia = () => {
    this.state.go("app.chamados.show",{id_ocorrencia: this.dadosContrato.id_ocorrencia});
  };

  /**
   * Recebe um arquivo e faz as verificaçoes necessarias para permitir o upload
   * caso fique fora do permitido limpa o campo para nao enviar o arquivo
   */
  verificacaoArquivo = () => {
    //verifica se existe um arquivo
    if (this.ocorrencia.files) {
      var formatosPermitidos = ["application/msword", "application/pdf", "image/png", "image/jpg", "image/jpeg", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"];

      // verifica o formato, se nao estiver dentro do permitido, retorna mensagem informando-o
      if (formatosPermitidos.indexOf(this.ocorrencia.files.type) === -1) {
        this.toaster.notify('error',  this.storage.user_shortname + this.MensagemPadrao.ARQUIVO_FORA_DO_FORMATO_PERMITIDO);
        this.ocorrencia.files = {};
      }


      if (this.ocorrencia.files.size > 10000000) {
        this.toaster.notify('error',  this.storage.user_shortname + this.MensagemPadrao.ARQUIVO_TAMANHO);
        this.ocorrencia.files = {};
      }
    }
  }

}

export default PwPrimeiroAcessoController;
