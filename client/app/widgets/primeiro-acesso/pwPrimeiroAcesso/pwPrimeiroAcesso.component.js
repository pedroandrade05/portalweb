import template from './pwPrimeiroAcesso.html';
import controller from './pwPrimeiroAcesso.controller';
import './pwPrimeiroAcesso.scss';

const pwPrimeiroAcessoComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'pwpa'
};

export default pwPrimeiroAcessoComponent;
