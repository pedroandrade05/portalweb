import PwDisciplinaProfessorModule from "./pwDisciplinaProfessor";
import PwDisciplinaProfessorController from "./pwDisciplinaProfessor.controller";
import PwDisciplinaProfessorComponent from "./pwDisciplinaProfessor.component";
import PwDisciplinaProfessorTemplate from "./pwDisciplinaProfessor.html";

describe('PwDisciplinaProfessor', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwDisciplinaProfessorModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwDisciplinaProfessorController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PwDisciplinaProfessorComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PwDisciplinaProfessorTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwDisciplinaProfessorController);
    });
  });
});
