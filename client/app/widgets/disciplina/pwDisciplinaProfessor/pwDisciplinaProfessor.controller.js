import loadingGif from "../../../../assets/img/loading.gif";

class pwDisciplinaProfessorController {
  constructor(DisciplinaAlunoService, NotifyService, $state, $scope, PessoaService) {
    'ngInject';
    this.name = 'pwDisciplinaProfessor';
    this.DisciplinaAlunoService = DisciplinaAlunoService;

    this.toaster = NotifyService;

    //inicia a variavel de loading
    this.loading = false;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;

    this.$state = $state;
    this.PessoaService = PessoaService;
    this.professor = {};

    if (this.verificarStateDisciplina()) {
      this.buscarDadosProfessorDaSala();
    }


  }

  /**
   * Verifica se a rota atual é a de disciplina e se o parametro da sala de aula existe
   * @returns {boolean}
   */
  verificarStateDisciplina = () => {

    //verifica se a rota é a da disciplina e se o id da sala de aula existe
    if (!this.idSaladeaula) {
      this.showData = false;
    }
    else {
      this.showData = true;
    }

    return this.showData;

  }

  /**
   * Busca os dados da sala de aula/disciplina e depois os dados do professor
   */
  buscarDadosProfessorDaSala = () => {
    this.loading = true;

    //busca os dados da sala de aula
    this.DisciplinaAlunoService
      .retornarDisciplina(
        this.idSaladeaula,
        this.idMatricula,
        this.idEntidade,
        0,
        0,
        true
      ).then((salaDeAula) => {
        if (salaDeAula.id_professor) {

          this.PessoaService
          .getDadosProfessor(salaDeAula.id_professor, salaDeAula.id_entidadesala)
          .then((pessoa) => {
            this.professor.st_urlavatar = false;

            //verifica se a imagem existe
            if (pessoa.st_urlavatar) {
              const img = new Image();
              img.src = pessoa.st_urlavatar;
              img.onerror = () => {
                pessoa.st_urlavatar = false;
              };
              img.onload = () => {
                pessoa.st_urlavatar = pessoa.st_urlavatar;
              };
            }

            //seta o professor na variavel
            this.professor = pessoa;
            this.hasProfessor = true;

          }, () => {
            this.loading = false;

          }).finally(() => {
            this.loading = false;
          });

        }
        else {
          this.loading = false;
        }
      }, (error) => {
        this.toaster.notify('error', error);
        this.loading = false;

      // falta decidir o que fazer em caso de erro
      });

  }


}

export default pwDisciplinaProfessorController;
