class PwDisciplinaDescricaoController {

  constructor(
    $state, CONFIG,
    CursoAlunoService
  ) {
    'ngInject';
    this.name = 'pwDisciplinaDescricao';

    this.state = $state;
    this.CONFIG = CONFIG;

    this.curso = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);

    this.setImagemDisciplina();

  }

  setImagemDisciplina() {
    this.imagem = this.disciplina.st_imagemdisciplina ? this.disciplina.st_imagemdisciplina : this.curso.st_imagemarea;
  }

}

export default PwDisciplinaDescricaoController;
