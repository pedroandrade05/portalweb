import template from "./pwDisciplinaList.html";
import controller from "./pwDisciplinaList.controller";
import "./pwDisciplinaList.scss";

const pwDisciplinaListComponent = {
  restrict: 'E',
  bindings: {
    matriculaAtiva: '<'
  },
  template,
  controller,
  controllerAs: 'pwDc'
};

export default pwDisciplinaListComponent;
