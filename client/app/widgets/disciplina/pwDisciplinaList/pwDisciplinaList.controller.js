import loadingGif from "../../../../assets/img/loading.gif";
import templateModal from "../../../widgets/primeiro-acesso/pwPrimeiroAcesso/pwPrimeiroAcesso.html";
import modalPrimeiroAcessoController from "../../../widgets/primeiro-acesso/pwPrimeiroAcesso/pwPrimeiroAcesso.controller";

import iconGroup from '../../../../assets/img/icons/group.png';

class pwDisciplinaListController {
  constructor(
    DisciplinaAlunoService,
    $state,
    $sce,
    NotifyService,
    $q,
    $scope,
    $uibModal,
    MensagemPadrao,
    $localStorage,
    CursoAlunoService,
    EvolucaoMatriculaConstante,
    MensagensInfoEvolucaoConstante,
    RenovacaoService,
    EsquemaConfiguracaoService,
    TipoDisciplinaConstante
  ) {
    'ngInject';

    this.iconGroup = iconGroup;

    this.service = DisciplinaAlunoService;
    this.cursoAlunoService = CursoAlunoService;
    this.renovacaoService = RenovacaoService;

    this.name = 'pwDisciplinaList';
    this.$state = $state;
    this.$scope = $scope;
    this.$sce = $sce;
    this.modal = $uibModal;
    this.MensagemPadrao = MensagemPadrao;
    this.toaster = NotifyService;
    this.$q = $q;
    this.mostrarMensagemRenovacao = false;
    this.dadosRenovacao = null;

    this.urlAmbientacao = null;

    this.matriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);
    this.MensagensInfoEvolucaoConstante = MensagensInfoEvolucaoConstante;
    this.EvolucaoMatriculaConstante = EvolucaoMatriculaConstante;

    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao(this.matriculaAtiva.id_matricula);
    this.isConcurso = EsquemaConfiguracaoService.isConcurso(this.matriculaAtiva.id_matricula);

    this.TipoDisciplinaConstante = TipoDisciplinaConstante;

    //inicia a variavel de loading
    this.loading = false;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;
    this.disciplinas = [];

    this.dadosUsuario = {
      stNomeCurto: $localStorage.user_name.split(' ').shift()
    };

    this.retornarDisciplinas();
    this.getMostrarBrinquedoteca(this.matriculaAtiva.id_curso);
    this.getMostrarClubeLivro(this.matriculaAtiva.id_curso);
  }

  //abre a modal de primeiro acesso
  abrirPrimeiroAcesso() {
    this.modal.open({
      template: templateModal,
      controller: modalPrimeiroAcessoController,
      controllerAs: 'pwpa',
      size: 'lg',
      scope: this.$scope,
      resolve: {
        primeiroacesso: () => {
          return this.matriculaAtiva;
        }
      }
    });
  }

  /**
   * Retorna as disciplinas com seus dados do moodle
   */
  retornarDisciplinas = () => {
    //inicia a variavel do loading para mostrar o gif
    this.loading = true;

    this.service.getAll({
      id_entidade: this.matriculaAtiva.id_entidade,
      id_matricula: this.matriculaAtiva.id_matricula,
      id_projetopedagogico: this.matriculaAtiva.id_curso,
      bl_moodle: 0,
      bl_disciplina: 0
    }, true).then((disciplinasResp) => {

      this.disciplinas = this.isGraduacao ?
        disciplinasResp.filter((item) => item.id_tipodisciplina !== this.TipoDisciplinaConstante.AMBIENTACAO) : disciplinasResp;

      // Adicionar mais infos aos dados retornados
      angular.forEach(disciplinasResp, (disciplina) => {
        this.andamentoDisciplina(disciplina);
      });
    }).finally(() => {
      // Matriculas com status trancado só devem exibir disciplinas cursadas.
      if (+this.matriculaAtiva.id_evolucao === this.EvolucaoMatriculaConstante.TRANCADO) {
        this.removeDisciplinasIncompletas();
      }

      this.loading = false;
    });
  };

  andamentoDisciplina = (disciplina) => {
    disciplina.showLoading = false;
    disciplina.showChart = false;
    disciplina.salaCompleta = disciplina.st_statuscomplete === 'Completo';
    disciplina.diasRestantes = disciplina.nu_diassala - disciplina.nu_diaspassados;

    if (disciplina.st_statuscomplete === 'Futuro') {
      return false;
    }

    // Exibir um gráfico estático
    if (disciplina.st_statuscomplete === 'Completo') {
      disciplina.showChart = true;
      return false;
    }

    // Exibir o carregamento do gráfico
    disciplina.showLoading = true;

    // Busca dados para o gráfico da disciplina
    this.service.getAll({
      id_entidade: this.matriculaAtiva.id_entidade,
      id_matricula: this.matriculaAtiva.id_matricula,
      id_projetopedagogico: this.matriculaAtiva.id_curso,
      id_saladeaula: disciplina.id_saladeaula,
      bl_moodle: 1,
      bl_disciplina: 0
    }, true).then((response) => {
      //se tiver retornado relevantes do moodle, a gente adiciona no scope
      if (response && response[0]) {
        angular.extend(disciplina, response[0]);
        disciplina.showLoading = false;
        disciplina.showChart = true;
      }
    });
  };

  /**
   * Redireciona a disciplina para sua tela main
   * @param objDisciplina
   */
  redirecionaDisciplina = (objDisciplina) => {
    try {
      if (objDisciplina.id_status === 3) {
        this.toaster.notify('warning', '', 'Disciplina ainda não iniciada');
      }
      else {

        this.$state.go("app.disciplina", {
          idMatriculaAtiva: this.matriculaAtiva.id_matricula,
          idSalaDeAula: objDisciplina.id_saladeaula
        });
      }
    }
    catch (e) {
      this.toaster.notify('error', 'Ops', 'Erro ao tentar redirecionar para tela de disciplina.');
    }
  };

  /**
   * Remove disciplinas incompletas da lista.
   */
  removeDisciplinasIncompletas = () => {
    this.disciplinas = this.disciplinas.filter((value) => {
      return !!value.salaCompleta;
    });
  };

  /* Funções utilizadas para manipulação do template.
  Evita inconsistências com ng-if. */

  evolucaoEhBloqueada = () => {
    return Number(this.matriculaAtiva.id_evolucao) === Number(this.EvolucaoMatriculaConstante.BLOQUEADO);
  };

  evolucaoEhTrancada = () => {
    return Number(this.matriculaAtiva.id_evolucao) === Number(this.EvolucaoMatriculaConstante.TRANCADO);
  };

  getMostrarBrinquedoteca(id_projetopedagogico) {
    this.cursoAlunoService.getMostrarBrinquedoteca(id_projetopedagogico)
      .then((response) => {
        this.matriculaAtiva.bl_mostrarbrinquedoteca = response.bl_mostrarbrinquedoteca;
      });
  }

  getMostrarClubeLivro(id_projetopedagogico) {
    this.cursoAlunoService.getMostrarClubeLivro(id_projetopedagogico)
      .then((response) => {
        this.matriculaAtiva.bl_clubelivro = response.bl_clubelivro;
      });
  }

  /**
   * Redireciona para a tela da brinquedoteca
   */
  redirecionaBrinquedoteca = () => {
    try {
      this.$state.go("app.brinquedoteca", {
        idMatriculaAtiva: this.matriculaAtiva.id_matricula
      });
    }
    catch (e) {
      this.toaster.notify('error', 'Ops', 'Erro ao tentar redirecionar para tela da brinquedoteca.');
    }
  }

  ocultaModuloDisciplina = () => {
    if ( (Number(this.matriculaAtiva.id_entidade) === 12) || (Number(this.matriculaAtiva.id_entidade) === 158) ) {
      return false;
    }
    return true;
  }

  /**
   * Redireciona para a tela da clube do livro
   */
  redirecionaClubeLivro = () => {
    try {
      this.$state.go("app.clube-do-livro", {
        idMatriculaAtiva: this.matriculaAtiva.id_matricula
      });
    }
    catch (e) {
      this.toaster.notify('error', 'Ops', 'Erro ao tentar redirecionar para tela da clube do livro.');
    }
  }
}

export default pwDisciplinaListController;
