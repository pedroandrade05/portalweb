import PwDisciplinaListModule from "./pwDisciplinaList";
import PwDisciplinaListController from "./pwDisciplinaList.controller";
import PwDisciplinaListComponent from "./pwDisciplinaList.component";
import PwDisciplinaListTemplate from "./pwDisciplinaList.html";

describe('PwDisciplinaList', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwDisciplinaListModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwDisciplinaListController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PwDisciplinaListComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PwDisciplinaListTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwDisciplinaListController);
    });
  });
});
