import loadingGif from "../../../../assets/img/loading.gif";

class pwDisciplinaAndamentoController {
  constructor(DisciplinaAlunoService, EsquemaConfiguracaoService, $state, NotifyService) {
    'ngInject';
    this.name = 'pwDisciplinaAndamento';
    this.service = DisciplinaAlunoService;

    this.toaster = NotifyService;

    //inicia a variavel de loading
    this.loading = false;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;

    this.disciplina = {};
    this.showChart = false;

    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao(this.idMatricula);
    this.isConcurso = EsquemaConfiguracaoService.isConcurso(this.idMatricula);

    // verifica se o objeto da sala de aula nao foi passado e consulta os dados
    if (typeof this.sala === angular.isUndefined) {
      this.getDadosDisciplina(this.idSaladeaula, this.idMatricula, this.idEntidade);
    }
    else {
      //se os dados da sala de aula foi passado busca a cor do status
      this.disciplina = this.sala;
      this.configDados();
    }

    this.disciplinaStatus = this.disciplina.st_status;

    if (this.isEncerrado()) {
      this.disciplinaStatus = 'Prazo encerrado';
    }
    if (this.isNaoIntegrado()) {
      this.disciplinaStatus = 'Sala indisponível';
    }

  }

  configDados() {
    this.showChart = true;
    this.disciplina.st_corstatus = this.getCssClassStatus(this.disciplina.st_statuscomplete);
    this.diasRestantes = this.disciplina.nu_diassala - this.disciplina.nu_diaspassados;
    this.isAtrasado = this.diasRestantes <= 10;
  }

  isEncerrado() {
    return [2, 4].includes(+this.disciplina.id_status);
  }

  isNaoIntegrado() {
    return (+this.disciplina.id_status === 4);
  }

  /**
   * Busca os dados da disciplina para mostrar o andamento
   * @param idSalaDeAula
   * @param idMatricula
   * @param idEntidade
     */
  getDadosDisciplina = (idSalaDeAula, idMatricula, idEntidade) => {
    this.loading = true;

    this.service
      .retornarDisciplina(idSalaDeAula, idMatricula, idEntidade)
      .then((success) => {
        this.disciplina = success;
        this.configDados();
      }, (error) => {
        this.toaster.notify('error', error);
      }).finally(() => {
        this.loading = false;
      });
  };

  /**
   * Define a cor que o nome do status vai ficar de acordo com o st_statuscomplete da disciplina
   * Esse switch usa as mesmas regras que estão definidas na directive do grafico
   * @param status
   * @returns {*}
   */
  getCssClassStatus = (status) => {
    switch (status) {
    case "Adiantado":
      return 'success';
    case "Em dia":
      return 'success';
    case "Completo":
      return 'info';
    case "Indefinido":
      return 'success';
    case "Atrasado":
      return 'danger';
    case "Futuro":
      return 'neutro';
    default:
      return 'neutro';
    }
  }


}

export default pwDisciplinaAndamentoController;
