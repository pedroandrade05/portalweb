import template from "./pwDisciplinaAndamento.html";
import controller from "./pwDisciplinaAndamento.controller";
import "./pwDisciplinaAndamento.scss";

const pwDisciplinaAndamentoComponent = {
  restrict: 'E',
  bindings: {
    idMatricula: '<',
    idSaladeaula: '<',
    idEntidade: '<',
    sala:'<?'
  },
  template,
  controller,
  controllerAs: 'pwCtrl'
};

export default pwDisciplinaAndamentoComponent;
