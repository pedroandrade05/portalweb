import PwEnvioTccFormModule from './pwEnvioTccForm';
import PwEnvioTccFormController from './pwEnvioTccForm.controller';
import PwEnvioTccFormComponent from './pwEnvioTccForm.component';
import PwEnvioTccFormTemplate from './pwEnvioTccForm.html';

describe('PwEnvioTccForm', () => {

  let $rootScope, makeController;

  beforeEach(window.module(PwEnvioTccFormModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwEnvioTccFormController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PwEnvioTccFormComponent;

    it('includes the intended template',() => {
        expect(component.template).to.equal(PwEnvioTccFormTemplate);
      });

    it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

    it('invokes the right controller', () => {
        expect(component.controller).to.equal(PwEnvioTccFormController);
      });
  });
});
