class PwEnvioTccFormController {
  constructor($timeout, TccService, Upload, $uibModal, NotifyService, $q, $state, $rootElement, $location, $localStorage, SituacaoTCCConstante) {
    'ngInject';

    this.timeout = $timeout;
    this.$q = $q;
    this.TccService = TccService;
    this.NotifyService = NotifyService;
    this.Upload = Upload;
    this.modal = $uibModal;
    this.$state = $state;
    this.$location = $location;
    this.SituacaoTCCConstante = SituacaoTCCConstante;
    this.$localStorage = $localStorage;

    this.arquivo_tcc = null;
    this.enviandoArquivo = false;
    this.progressPercentage = 0;
    this.st_tituloavaliacao = null;

    this.moodleParams = this.$location.search();
    this.moodleParams.id_entidade = this.$localStorage.user_entidade;
    this.dadostcc = '';
    this.tccEnviado = false;
    this.dadosTccCarregados = false;
    this.verificarEnvioTcc();
  }

  /**
   * Fecha a janela modal
   */
  close = () => {
    if (this.modalInstance) {
      this.modalInstance.close();
    }
  }

  /**
   * Método aditionado para adicionar um delay de 1 segundo apenas por efeito
   * @param bl
     */
  setLoading = (bl) => {
    return this.$q((resolve) => {
      if (!bl) {
        this.timeout(() => {
          this.enviandoArquivo = bl;
          resolve(bl);
        }, 1000);
      }
      else {
        this.enviandoArquivo = bl;
        resolve(bl);
      }
    });

  }

  /**
   * Faz o Upload do TCC
   */
  enviar = () => {

    if (!this.st_tituloavaliacao) {
      this.NotifyService.notify('error', 'Atenção', 'Informe o Título do TCC!');
      return false;
    }

    this.st_tituloavaliacao = this.st_tituloavaliacao.toUpperCase();

    this.setLoading(true);
    this.dadostcc.file = this.arquivo_tcc;
    this.dadostcc.st_tituloavaliacao = this.st_tituloavaliacao;

    this.TccService.enviarTcc(
      {
        file: this.arquivo_tcc,
        st_tituloavaliacao: this.st_tituloavaliacao,
        id_matricula: this.dadostcc.id_matricula
      }
    ).progress((evt) => {
      const percentage = parseInt(100.0 * evt.loaded / evt.total);
      this.progressPercentage = percentage <= 90 ? percentage : 93;
    }).success(() => {
      this.setLoading(false).then(() => {
        this.NotifyService.notify('success', '', 'Arquivo enviado com sucesso!');
        this.tccEnviado = true;
      });
    }).error((response) => {
      this.NotifyService.notify(response.data);
      this.setLoading(false);
    });

  }

  verificarEnvioTcc = () => {
    this.TccService.verificarEnvioTcc(this.moodleParams)
      .then((success) => {
        this.dadostcc = success;
      },
      (error) => {
        this.dadostcc = error;
      })
      .finally(() => {
        this.dadosTccCarregados = true;
      });
  }

}

export default PwEnvioTccFormController;
