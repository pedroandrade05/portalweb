import template from './pwEnvioTccForm.html';
import controller from './pwEnvioTccForm.controller';
import './pwEnvioTccForm.scss';

const pwEnvioTccFormComponent = {
  restrict: 'E',
  bindings: {
    resolve: "<",
    modalInstance: "<"
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pwEnvioTccFormComponent;
