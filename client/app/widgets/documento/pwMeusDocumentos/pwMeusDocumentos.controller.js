import loadingGif from '../../../../assets/img/loading.gif';

class pwMeusDocumentosController {
  constructor(
    /* Úteis */ $localStorage, $q, $sce, $scope, $state, $window, CONFIG, $http,
    /* Services */ AgendamentoService, ArquivosCompartilhadosService, CursoAlunoService, DownloadService,
    EsquemaConfiguracaoService, FinanceiroService, NotifyService, TabelaPrecoService, VistaDeProvaService,
    /* Constantes */ EntidadeConstante, EvolucaoMatriculaConstante
  ) {
    'ngInject';

    this.FinanceiroService = FinanceiroService;
    this.TabelaPrecoService = TabelaPrecoService;
    this.AgendamentoService = AgendamentoService;
    this.VistaDeProvaService = VistaDeProvaService;
    this.ArquivosCompartilhadosService = ArquivosCompartilhadosService;

    this.name = 'pwMeusDocumentos';
    this.$state = $state;
    this.DownloadService = DownloadService;
    this.storage = $localStorage;
    this.$window = $window;
    this.$scope = $scope;
    this.$q = $q;
    this.$sce = $sce;
    this.$http = $http;
    this.CONFIG = CONFIG;
    this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);
    this.loadingGif = loadingGif;
    this.loading = true;
    this.resultTabelaPreco = false;
    this.toaster = NotifyService;
    this.manualDoAluno = null;
    this.EvolucaoMatriculaConstante = EvolucaoMatriculaConstante;
    this.EntidadeConstante = EntidadeConstante;

    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao($state.params.idMatriculaAtiva);

    this.dadosUsuario = {
      id_evolucao: $localStorage.idEvolucao
    };

    this.retornaTabelaPreco();
    this.retornaContratos();
    this.retornararquivosCompartilhados();
    this.mostrarItens();

    this.getTabelaPreco(this.MatriculaAtiva.id_entidade);
    if (!this.isGraduacao) {
      this.getManualDoAluno(this.MatriculaAtiva.id_entidade);
    }

    //armazena todos os contratos encontrados
    this.contratos = [];
    this.arquivos = [];

    //armazena a url da tabela de preço
    this.tabelapreco = '';
  }

  /**
   * Gera a Url e injeta no iframe para efetuar o download do imposto de renda.
   */
  gerarImpostoDeRenda = () => {
    const idMatricula = this.$state.params.idMatriculaAtiva;
    const idEntidade = this.MatriculaAtiva.id_entidade;
    const downloadAction = 'imposto-renda';

    this.DownloadService.getAuth(downloadAction).then((auth) => {
      const urlDownload = this.DownloadService.getUrlDownloadImpostoRenda(auth, downloadAction, idMatricula, idEntidade);
      this.$scope.detailFrame = this.$sce.trustAsResourceUrl(urlDownload);
    }, () => {

      // Error
    });
  }

  /**
   * Verifica se há plano de pagamento disponível para a matrícula. Se houver,
   * gera e abre a URL para download do arquivo em PDF.
   * Caso contrário, notifica o usuário.
   */
  geraPdfPlanoPagamento = () => {
    const idMatricula = this.$state.params.idMatriculaAtiva;
    const idEntidade = this.MatriculaAtiva.id_entidade;
    const downloadAction = 'plano-pagamento';

    this.FinanceiroService.getStatusPlanoPagamento(idMatricula, idEntidade).then((response) => {
      //Se existir plano de pagamento, gerar url e baixar em PDF.
      if (response.existePlanoPagamento) {
        this.DownloadService.getAuth(downloadAction).then((auth) => {
          const urlDownload = this.DownloadService.geraUrlDownloadPlanoPagamento(auth, downloadAction, idMatricula, idEntidade);
          this.$scope.detailFrame = this.$sce.trustAsResourceUrl(urlDownload);
        });
      }
      else {
        //Se não existir plano de pagamento, exibe erro com detalhes do status.
        this.toaster.notify('error', response.status ? response.status : 'Erro');
      }
    });
  }

  /**
   * Gera a Url e injeta no iframe para efetuar o download da tabela de preço da entidade
   */
  retornaTabelaPreco = () => {
    const idEntidade = this.MatriculaAtiva.id_entidade;
    const downloadAction = 'tabela-preco';

    this.DownloadService.getAuth(downloadAction).then((auth) => {
      const urlTabelaPreco = this.DownloadService.geraUrlDownloadTabelaPreco(auth, downloadAction, idEntidade);

      this.DownloadService.pesquisaTabelaPreco(urlTabelaPreco).then((response) => {
        this.tabelapreco = response.data;
        this.loading = false;
      }, () => {
        this.loading = false;
      });
    });
  }

  /**
   * Gera a URL com o token, apos faz a requisiçao solicitando os contratos
   * caso exista retonar um json com os documentos e o caminho, depois e feito o each na aplicaçao
   * imprimindo os contratos na tela
   */
  retornaContratos = () => {
    const idEntidade = this.MatriculaAtiva.id_entidade;
    const idUsuario = this.storage.user_id;
    const idMatricula = this.$state.params.idMatriculaAtiva;
    const downloadAction = 'contrato';

    this.DownloadService.getAuth(downloadAction).then((auth) => {
      const urlContratos = this.DownloadService.geraUrlDownloadContratos(auth, downloadAction, idEntidade, idUsuario, idMatricula);
      this.DownloadService.pesquisaContratos(urlContratos).then((response) => {
        this.contratos = response.data;
        this.loading = false;
      }, () => {
        this.loading = false;
      });
    });
  }

  /**
   * Pesquisa se existe tabela de preço
   * @param id_entidade
   */
  getTabelaPreco = (id_entidade) => {
    this.TabelaPrecoService.getTabelaPreco(id_entidade).then((response) => {
      if (response[0].type) {
        this.resultTabelaPreco = true;
      }
    }, (error) => {
      this.toaster.notify(error.data);
    });
  }

  /** Verifica se há manual do aluno para a entidade. Caso positivo,
   * retorna id_upload e st_upload do manual do aluno.
   * @param {string} id_entidade - ID da entidade.
   * */

  getManualDoAluno(id_entidade) {
    this.FinanceiroService.getManualDoAluno(id_entidade).then((success) => {
      this.manualDoAluno = success;
    });
  }

  /** Monta a URL de download do manual do aluno, com base no st_upload.
   * @deprecated
   */
  downloadManualDoAluno() {
    const urlDownload = this.manualDoAluno.st_upload;
    this.$scope.detailFrame = this.$sce.trustAsResourceUrl(urlDownload);
  }

  /**
   * @description Abrir em uma nova janela o comprovante de agendamento realizado
   */
  abrirComprovanteAgendamento = () => {
    this.$window.open(this.CONFIG.urlG2 + 'default/textos/comprovante-agendamento?id_usuario=' + this.storage.user_id + '&id_avaliacaoagendamento=' + this.agendamentoComprovante.id_avaliacaoagendamento, '_blank');
  };

  /**
   * Função que vai consultar API, para verificar se o aluno possui alguma prova para revisão.
   * Se possuir, será redirecionado para o sistema da Fábrica de Prova.
   * Se não possuir, receberá um alerta informando.
   */
  vistaDeProva = () => {
    this.VistaDeProvaService.getVistaDeProva(this.storage.user_id).then((response) => {
      if (!response.message) {
        this.toaster.notify('warning', 'Por favor, verique se os pop-ups estão liberados');
        this.$window.open(response.url);
      }
      else {
        this.toaster.notify(response.type, response.message);
      }

    }, (error) => {
      this.toaster.notify(error.type, error.message);
    });
  };

  retornararquivosCompartilhados = () => {
    this.ArquivosCompartilhadosService.getArquivosCompartilhados(this.MatriculaAtiva.id_matricula).then((response) => {
      if (response.message) {
        this.toaster.notify('warning', response.message);
      }
      else {
        this.arquivos = response;
      }
    }, (error) => {
      this.toaster.notify(error.type, error.message);
    });
  };

  mostrarItens = () => {
    this.mostrarItem = !((+this.dadosUsuario.id_evolucao === +this.EvolucaoMatriculaConstante.EGRESSO)
      && ((+this.MatriculaAtiva.id_entidade === +this.EntidadeConstante.UCAM)
        || (+this.MatriculaAtiva.id_entidade === +this.EntidadeConstante.FACULDADE_UNYLEYA)));
  }

  /**
   * Abri o arquivo em geral pelo download-s3-direto
   *
   * @param arquivo
   * @param nomePasta
   */
  abrirArquivo = (arquivo, nomePasta) => {
    const downloadAction = 'download-s3-direto';
    this.DownloadService.getAuth(downloadAction).then((auth) => {

      const urlDocumento = this.DownloadService.geraUrlDocumento(auth, downloadAction, arquivo, nomePasta);

      const win = this.$window.open(urlDocumento, '_blank');

      if (win) {
        win.focus();
      }
      else {
        this.toaster.notify('warning', '', 'É necessário permitir que o navegador abra uma nova aba.');
      }
    });
  }
}

export default pwMeusDocumentosController;
