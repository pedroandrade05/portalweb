import PwMeusDocumentosModule from './pwMeusDocumentos';
import PwMeusDocumentosController from './pwMeusDocumentos.controller';
import PwMeusDocumentosComponent from './pwMeusDocumentos.component';
import PwMeusDocumentosTemplate from './pwMeusDocumentos.html';

describe('PwMeusDocumentos', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwMeusDocumentosModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwMeusDocumentosController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PwMeusDocumentosComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PwMeusDocumentosTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwMeusDocumentosController);
    });
  });
});
