import template from './pwDocumentosPessoaisShow.html';
import controller from './pwDocumentosPessoaisShow.controller';
import './pwDocumentosPessoaisShow.scss';

const pwDocumentosPessoaisShowComponent = {
  restrict: 'E',
  bindings: {
    documentoPessoal: "<"
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pwDocumentosPessoaisShowComponent;
