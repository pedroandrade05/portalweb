import loadingGif from "../../../../assets/img/loading.gif";

class PwDocumentosPessoaisShowController {
  constructor(
    /* Services */NotifyService, $state, EntregaDocumentosService, SituacaoDocumentoCertificacao, EsquemaConfiguracaoService
  ) {
    'ngInject';
    this.name = 'pwDocumentosPessoaisShow';

    this.SituacaoDocumentoCertificacao = SituacaoDocumentoCertificacao;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;
    this.toaster = NotifyService;
    this.idMatricula = $state.params.idMatriculaAtiva;
    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao(this.idMatricula);
    this.EntregaDocumentosService = EntregaDocumentosService;
    this.loading = true;
    this.historicoDocumento = [];

    this.buscarHistorico();

  }

  buscarHistorico = () => {
    if (this.documentoPessoal && this.documentoPessoal.id_entregadocumentos) {
      this.EntregaDocumentosService.getHistoricoEnvio(this.idMatricula, this.documentoPessoal.id_entregadocumentos).then((response) => {
        if (response && response.length) {
          this.historicoDocumento = response;
        }
      }).finally(() => {
        this.loading = false;
      });
    }
    else {
      this.loading = false;
      this.historicoDocumento = [];
    }
  }
}

export default PwDocumentosPessoaisShowController;
