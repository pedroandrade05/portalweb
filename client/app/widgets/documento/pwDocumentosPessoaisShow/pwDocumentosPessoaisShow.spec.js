import PwDocumentosPessoaisShowModule from './pwDocumentosPessoaisShow'
import PwDocumentosPessoaisShowController from './pwDocumentosPessoaisShow.controller';
import PwDocumentosPessoaisShowComponent from './pwDocumentosPessoaisShow.component';
import PwDocumentosPessoaisShowTemplate from './pwDocumentosPessoaisShow.html';

describe('PwDocumentosPessoaisShow', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwDocumentosPessoaisShowModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwDocumentosPessoaisShowController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
      let component = PwDocumentosPessoaisShowComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(PwDocumentosPessoaisShowTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(PwDocumentosPessoaisShowController);
      });
  });
});
