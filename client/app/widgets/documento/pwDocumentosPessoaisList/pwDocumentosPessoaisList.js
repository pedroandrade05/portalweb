/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pwDocumentosPessoaisListComponent from './pwDocumentosPessoaisList.component';

//Import do modulo de services do portal
import Services from '../../../services/Services'; // Talvez você precise alterar este destino

//Modulo principal do componente
const pwDocumentosPessoaisListModule = angular.module('pwDocumentosPessoaisList', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])

  .component('pwDocumentosPessoaisList', pwDocumentosPessoaisListComponent);

export default pwDocumentosPessoaisListModule;
