import loadingGif from "../../../../assets/img/loading.gif";

const formatosPermitidos = [
  "application/pdf",
  "image/png",
  "image/jpg", "image/jpeg"
];
const LIMITE_UPLOAD = 5; //Limite de tamanho do arquivo para upload (MB)

class PwDocumentosPessoaisListController {
  constructor(
    /* Úteis */ $timeout,
    /* Services */NotifyService, $localStorage, $state, EntregaDocumentosService, $rootScope, ModalService,
    /* Constantes */MensagemPadrao, SituacaoDocumentoCertificacao, SituacaoDocumentoDiplomacao, EsquemaConfiguracaoService
  ) {
    'ngInject';
    this.$timeout = $timeout;
    this.toaster = NotifyService;
    this.MensagemPadrao = MensagemPadrao;
    this.storage = $localStorage;
    this.idMatricula = $state.params.idMatriculaAtiva;
    this.EntregaDocumentosService = EntregaDocumentosService;
    this.SituacaoDocumentoCertificacao = SituacaoDocumentoCertificacao;
    this.SituacaoDocumentoDiplomacao = SituacaoDocumentoDiplomacao;
    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao(this.idMatricula);
    this.$rootScope = $rootScope;
    this.ModalService = ModalService;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;
    this.name = 'pwDocumentosPessoaisList';
    this.documentosPessoais = [];
    this.loading = true;
    this.documentoEntregaHistorico = {};

    this.buscarDocumentos();
  }

  /**
   * Retorna os documentos obrigatórios
   */
  buscarDocumentos() {
    this.EntregaDocumentosService.getDocumentosPessoais(this.idMatricula).then((response) => {
      if (response && response.length) {
        this.documentosPessoais = response;
      }
      this.loading = false;
    }, () => {
      this.toaster.notify('error', 'Houve um erro ao buscar os dados.');
      this.loading = false;
    });

  }

  /**
   * Recebe um arquivo e faz as verificaçoes necessarias para permitir o upload
   * caso fique fora do permitido limpa o campo para nao enviar o arquivo
   */
  verificacaoArquivo = (indx) => {
    if (this.documentosPessoais[indx].files && this.documentosPessoais[indx].files.length > 0) {
      const arquivosAux = [...this.documentosPessoais[indx].files];
      const arrayNomes = [];
      arquivosAux.map((arquivo, indice) => {

        if (formatosPermitidos.indexOf(arquivo.type) === -1) {
          this.toaster.notify('error', this.storage.user_shortname + this.MensagemPadrao.ARQUIVO_FORA_DO_FORMATO_PERMITIDO_IMAGEM_E_PDF +
            'Verifique seu arquivo '+ arquivo.name, '', 10);
          this.documentosPessoais[indx].files.splice(indice, 1);
        }
        else if (arquivo.size > (LIMITE_UPLOAD * 1024 * 1024)) {
          this.toaster.notify('error', this.storage.user_shortname + this.MensagemPadrao.ARQUIVO_TAMANHO_5MB +
            'Verifique seu arquivo '+ arquivo.name, '', 10);
          this.documentosPessoais[indx].files.splice(indice, 1);
        }
        else {
          arrayNomes.push(arquivo.name);
        }
      });

      //Se houver ao menos um arquivo, ajusta o valor para nome na tela
      this.documentosPessoais[indx].tituloArquivosMultiplos = arrayNomes.join(' + ');

    }
  }

  /**
   * Realiza o envio do documento
   * @param indx
   */
  enviarDocumento = (indx) => {
    if (this.documentosPessoais[indx].files.length) {
      const {
        id_entregadocumentos,
        id_documentos,
        id_usuarioaluno,
        id_entidade,
        id_situacao,
        id_contratoresponsavel
      } = this.documentosPessoais[indx];
      this.documentosPessoais[indx].loading = true;

      const objEnvio = {
        id_matricula: this.idMatricula,
        id_entregadocumentos,
        id_documentos,
        id_usuarioaluno,
        id_entidade,
        id_situacao,
        id_contratoresponsavel,
        files: this.documentosPessoais[indx].files
      };

      if (this.documentosPessoais[indx].files && this.documentosPessoais[indx].files.length > 0) {
        this.EntregaDocumentosService.entregarDocumento(
          objEnvio
        )
          .success((response) => {
            if (response && response.id_entregadocumentos) {
              this.documentosPessoais[indx] = {
                ...this.documentosPessoais[indx],
                id_entregadocumentos: response.id_entregadocumentos,
                st_nomearquivoenvio: response.st_nomearquivoenvio,
                dt_cadastro: response.dt_cadastro,
                dt_ultimasituacao: response.dt_ultimasituacao,
                id_situacao: response.id_situacao || this.SituacaoDocumentoCertificacao.ENTREGUE_VIA_PORTAL,
                st_situacaodocumentoportal: response.st_situacaodocumentoportal
              };
              this.toaster.notify('success', 'Sucesso', 'Seu documento foi enviado para análise.');
            }
            this.documentosPessoais[indx].loading = false;
            this.documentosPessoais[indx].exibirUpload = false;
          }).error((error) => {
            this.documentosPessoais[indx].loading = false;
            this.toaster.notify(error);
          });
      }
    }
  }

  verHistoricoEnvio = (index) => {
    this.documentoEntregaHistorico = this.documentosPessoais[index];
    this.$rootScope.$emit('toggleTopoDescricaoDocumentos');
  }

  voltarListaDocumentos = () => {
    this.documentoEntregaHistorico = {};
    this.$rootScope.$emit('toggleTopoDescricaoDocumentos');
  }

  /**
   * Realiza a confirmação do envio do documento
   * @param indx
   */
  confirmarEnvio = (indx) => {
    this.ModalService.show({
      title: 'Envio de documentos',
      body: 'Atenção! Todos os anexos (frente e verso) devem ser enviados de uma só vez. Após a confirmação do envio não será possível enviar o documento novamente.',
      confirmButton: 'Confirmar',
      cancelButton: 'Cancelar'
    }).then(() => {
      this.enviarDocumento(indx);
    }).catch(() => {
      this.ModalService.close();
    });
  }
}

export default PwDocumentosPessoaisListController;
