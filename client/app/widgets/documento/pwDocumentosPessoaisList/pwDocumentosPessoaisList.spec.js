import PwDocumentosPessoaisListModule from './pwDocumentosPessoaisList'
import PwDocumentosPessoaisListController from './pwDocumentosPessoaisList.controller';
import PwDocumentosPessoaisListComponent from './pwDocumentosPessoaisList.component';
import PwDocumentosPessoaisListTemplate from './pwDocumentosPessoaisList.html';

describe('PwDocumentosPessoaisList', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwDocumentosPessoaisListModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwDocumentosPessoaisListController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
      let component = PwDocumentosPessoaisListComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(PwDocumentosPessoaisListTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(PwDocumentosPessoaisListController);
      });
  });
});
