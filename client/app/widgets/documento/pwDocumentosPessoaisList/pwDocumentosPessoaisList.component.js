import template from './pwDocumentosPessoaisList.html';
import controller from './pwDocumentosPessoaisList.controller';
import './pwDocumentosPessoaisList.scss';

const pwDocumentosPessoaisListComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pwDocumentosPessoaisListComponent;
