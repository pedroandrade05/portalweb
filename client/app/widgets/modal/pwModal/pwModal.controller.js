class PwModalController {

  constructor($sce, ModalService) {
    'ngInject';

    this.name = 'pwModal';
    this.$sce = $sce;
    this.$modal = this.modalInstance;

    this.state = {
      ...ModalService.defaultData,
      ...this.resolve.data
    };

    this.cancel = () => {
      this.$modal.dismiss(false);
    };

    this.confirm = () => {
      this.$modal.close(true);
    };
  }

}

export default PwModalController;
