import template from './pwModal.html';
import controller from './pwModal.controller';
import './pwModal.scss';

const pwModalComponent = {
  restrict: 'E',
  bindings: {
    modalInstance: "<",
    resolve: "<"
  },
  template,
  controller,
  controllerAs: '$ctrl'
};

export default pwModalComponent;
