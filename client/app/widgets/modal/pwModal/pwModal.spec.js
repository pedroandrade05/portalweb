import PwModalModule from './pwModal';
import PwModalController from './pwModal.controller';
import PwModalComponent from './pwModal.component';
import PwModalTemplate from './pwModal.html';

describe('PwModal', () => {
  let $rootScope,
    makeController,
    $state = {params: {idMatriculaAtiva: null}};

  beforeEach(window.module(PwModalModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwModalController($state);
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PwModalComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PwModalTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwModalController);
    });
  });
});
