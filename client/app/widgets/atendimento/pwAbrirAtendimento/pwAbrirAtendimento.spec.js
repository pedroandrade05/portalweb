import PwAbrirAtendimentoModule from './pwAbrirAtendimento';
import PwAbrirAtendimentoController from './pwAbrirAtendimento.controller';
import PwAbrirAtendimentoComponent from './pwAbrirAtendimento.component';
import PwAbrirAtendimentoTemplate from './pwAbrirAtendimento.html';

describe('PwAbrirAtendimento', () => {
  let $rootScope,
    makeController,
    $state = { params: { idMatriculaAtiva: 12 } };

  beforeEach(window.module(PwAbrirAtendimentoModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwAbrirAtendimentoController($state);
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PwAbrirAtendimentoComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PwAbrirAtendimentoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwAbrirAtendimentoController);
    });
  });
});
