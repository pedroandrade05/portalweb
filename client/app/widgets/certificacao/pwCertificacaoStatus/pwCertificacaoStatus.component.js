import template from './pwCertificacaoStatus.html';
import controller from './pwCertificacaoStatus.controller';
import './pwCertificacaoStatus.scss';

const pwCertificacaoStatusComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pwCertificacaoStatusComponent;
