import PwCertificacaoStatusModule from './pwCertificacaoStatus'
import PwCertificacaoStatusController from './pwCertificacaoStatus.controller';
import PwCertificacaoStatusComponent from './pwCertificacaoStatus.component';
import PwCertificacaoStatusTemplate from './pwCertificacaoStatus.html';

describe('PwCertificacaoStatus', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwCertificacaoStatusModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwCertificacaoStatusController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
      let component = PwCertificacaoStatusComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(PwCertificacaoStatusTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(PwCertificacaoStatusController);
      });
  });
});
