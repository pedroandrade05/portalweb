import loadingGif from '../../../../assets/img/loading.gif';

class PwCertificacaoStatusController {
  constructor(
    /* Úteis */ $state, $window,
    /* Services */ CursoAlunoService, NotifyService, CertificacaoService, DownloadService,
    /* Constantes */ EvolucaoMatriculaConstante) {
    'ngInject';
    this.name = 'pwCertificacaoStatus';
    this.state = $state;
    this.situacoes = [];
    this.certificacao = {};

    //cria uma variavel no escopo para a matricula ativa
    this.idMatriculaAtiva = $state.params.idMatriculaAtiva;
    this.CursoAlunoService = CursoAlunoService;
    this.CertificacaoService = CertificacaoService;
    this.NotifyService = NotifyService;
    this.DownloadService = DownloadService;
    this.loadingGif = loadingGif;
    this.$window = $window;

    // Dados Matrícula
    this.matriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);
    this.EvolucaoMatriculaConstante = EvolucaoMatriculaConstante;

    this.buscarSituacoes();
  }

  retornaDadosTimeline(status) {
    return [
      {
        tipo: 'envio-documentos',
        bl_concluido: status.envio,
        st_titulo: 'Envio de documentos obrigatórios',
        st_descricao: (status.envio) ? 'O primeiro passo para a emissão do seu certificado é ter documentos atualizados. Você está com todos os documentos aprovados.'
          : 'O primeiro passo para a emissão do seu certificado é ter documentos atualizados. Verifique quais documentos você precisa enviar.'
      },
      {
        tipo: 'conclusao-curso',
        bl_concluido: status.curso,
        st_titulo: 'Conclusão do curso',
        st_descricao: (status.curso) ? 'Você concluiu todas as disciplinas.' : 'Para a emissão do seu certificado é necessário concluir o curso, de acordo com as regras de aprovação.'
      },
      {
        tipo: 'certificado-digital',
        bl_concluido: status.certificado,
        st_titulo: 'Certificado',
        st_descricao: 'Aqui você vai conferir os dados do seu certificado e, quando ele estiver disponível, você poderá baixá-lo.',
        st_descricaoconfirme: 'Verifique se os dados estão corretos ou se necessitam de algum ajuste (nome, endereço, etc).',
        st_urldocumento: status.st_urldocumento,
        id_documentodigitalmatricula: status.id_documentodigitalmatricula,
        dt_cadastrodigital: status.dt_cadastrodigital,
        bl_confirmadadoscertificado: status.bl_confirmadadoscertificado,
        dt_cadastroimpressa: status.dt_cadastroimpressa,
        dt_envioalunodigital: status.dt_envioalunodigital
      }
    ];
  }

  buscarSituacoes() {
    this.loading = true;
    this.CertificacaoService.getInformacoesCertificacao(this.idMatriculaAtiva)
      .then((response) => {
        if (response) {
          this.statusConclusao = {
            envio: response.bl_documentacao,
            curso: (+response.id_evolucao === this.EvolucaoMatriculaConstante.CONCLUINTE
              || +response.id_evolucao === this.EvolucaoMatriculaConstante.CERTIFICADO),
            certificado: response.id_evolucao === this.EvolucaoMatriculaConstante.CERTIFICADO
          };

          const auxDadosTimeline = {
            ...this.statusConclusao,
            bl_confirmadadoscertificado: response.bl_confirmadadoscertificado,
            st_urldocumento: response.st_urldocumento || null,
            id_documentodigitalmatricula: response.id_documentodigitalmatricula || null,
            dt_cadastrodigital: response.dt_cadastrodigital,
            dt_cadastroimpressa: response.dt_cadastroimpressa,
            dt_envioalunodigital: response.dt_envioalunodigital
          };

          this.situacoes = this.retornaDadosTimeline(auxDadosTimeline);
        }
      }, (error) => {
        this.NotifyService.notify(error.data);
      }).finally(() => {
        this.loading = false;
      });
  }

  baixarCertificado(idDocumentoDigitalMatricula) {
    this.DownloadService
      .geraUrlDownloadCertificadoDigital(
        this.idMatriculaAtiva,
        idDocumentoDigitalMatricula
      )
      .then((response) => {
        const $win = this.$window.open(response);

        if (!$win) {
          this.NotifyService.notify('warning', '', 'O seu navegador bloqueou o download do documento.');
        }
      }, (error) => {
        const type = error && error.status === 404 ? 'warning' : 'error';
        const message = error && error.data && error.data.message ? error.data.message : 'Não foi possível baixar a sua certificação.';
        this.NotifyService.notify(type, '', message);
      });
  }
}

export default PwCertificacaoStatusController;
