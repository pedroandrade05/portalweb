import PwCertificacaoConferirModule from './pwCertificacaoConferir'
import PwCertificacaoConferirController from './pwCertificacaoConferir.controller';
import PwCertificacaoConferirComponent from './pwCertificacaoConferir.component';
import PwCertificacaoConferirTemplate from './pwCertificacaoConferir.html';

describe('PwCertificacaoConferir', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwCertificacaoConferirModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwCertificacaoConferirController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
      let component = PwCertificacaoConferirComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(PwCertificacaoConferirTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(PwCertificacaoConferirController);
      });
  });
});
