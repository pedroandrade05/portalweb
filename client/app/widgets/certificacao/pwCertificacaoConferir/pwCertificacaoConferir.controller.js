import iconClose from "../../../../assets/img/icons/icon-close.png";
import loadingGif from '../../../../assets/img/loading.gif';

class PwCertificacaoConferirController {
  constructor(
    /* Services */PessoaService, NotifyService, CursoAlunoService, OcorrenciaService, CertificacaoService,
    /* Utils */ $state, $localStorage,
    /* Constantes */ AssuntoOcorrenciaConstante, SubAssuntoOcorrenciaConstante, TbSistema) {
    'ngInject';
    this.name = 'pwCertificacaoConferir';
    this.PessoaService = PessoaService;
    this.NotifyService = NotifyService;
    this.CursoAlunoService = CursoAlunoService;
    this.OcorrenciaService = OcorrenciaService;
    this.CertificacaoService = CertificacaoService;
    this.iconClose = iconClose;
    this.state = $state;

    this.AssuntoOcorrenciaConstante = AssuntoOcorrenciaConstante;
    this.SubAssuntoOcorrenciaConstante = SubAssuntoOcorrenciaConstante;
    this.TbSistema = TbSistema;

    this.loading = false;
    this.loadingInteracoes = false;
    this.loadingGif = loadingGif;

    this.dadosAluno = {};
    this.informacoesCertificacao = {};
    this.textoAjuste = '';
    this.dadosConfirmados = false;
    this.atendimentoAberto = false;
    this.ocorrencia = {};

    this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo(this.state.params.idMatriculaAtiva);
    this.exibirCampoAjustes = false;

    const idUsuario = $localStorage.user_id;
    const idEntidade = this.MatriculaAtiva.id_entidade;
    this.buscaDadosUsuario(idUsuario, idEntidade);
    this.buscarStatusCertificacao();
  }

  /**
   * Função para busca dos dados pessoais do usuário
   * @param idUsuario
   * @param idEntidade
   */
  buscaDadosUsuario = (idUsuario, idEntidade) => {
    this.loading = true;
    if (idUsuario) {
      this.PessoaService.getDadosPessoa(idUsuario, idEntidade).then((response) => {
        if (response) {
          this.dadosAluno = response;
        }
      }, (error) => {
        this.NotifyService.notify('error', '', error.data);
      }).finally(() => {
        this.loading = false;
      });
    }
  }

  /**
   * Busca o status atual da certificação
   */
  buscarStatusCertificacao() {
    this.loadingInteracoes = true;
    this.CertificacaoService.buscarCertificacao(this.MatriculaAtiva.id_matricula).then((response) => {
      if (response) {
        this.atendimentoOcorrencia = (response.ocorrencia && response.ocorrencia.id_ocorrencia) ? response.ocorrencia : false;
        this.dadosConfirmados = !!(response.bl_confirmadadoscertificado);
        this.informacoesCertificacao = response;
      }
    }, (error) => {
      this.NotifyService.notify('error', '', error.message);
    }).finally(() => {
      this.loadingInteracoes = false;
    });
  }

  /**
   * Realiza o envio da mensagem para ajuste
   */
  enviarMensagemAjuste() {
    if (this.textoAjuste) {
      this.loadingInteracoes = true;
      this.ocorrencia = {
        id_entidade: this.MatriculaAtiva.id_entidade,
        id_matricula: this.MatriculaAtiva.id_matricula,
        st_titulo: 'Ajustes nos dados para emissão do certificado',
        id_assuntopai: this.AssuntoOcorrenciaConstante.DOCUMENTACAO,
        id_assuntoco: this.SubAssuntoOcorrenciaConstante.AJUSTES_DADOS_CERTIFICACAO,
        st_ocorrencia: this.textoAjuste,
        dt_ultimainteracao: new Date(),
        id_sistema: this.TbSistema.PortalAluno,
        bl_lido: true
      };
      this.OcorrenciaService.create(this.ocorrencia).success((ocorrencia) => {
        this.NotifyService.notify('success', '', 'A ocorrência foi aberta e em breve você receberá uma resposta.');
        this.atendimentoOcorrencia = (ocorrencia && ocorrencia.id_ocorrencia) ? ocorrencia : false;
        this.exibirCampoAjustes = false;
      }).error((error) => {
        this.NotifyService.notify('error', '', error.message);

      }).finally(() => {
        this.loadingInteracoes = false;
      });
    }
  }

  /**
   * Marca os dados da certificação como corretos
   */
  confirmarDados() {
    this.loadingInteracoes = true;
    this.CertificacaoService.confirmarCertificacao(this.MatriculaAtiva.id_matricula).then((response) => {
      if (response) {
        this.dadosConfirmados = true;
      }
    }, (error) => {
      this.NotifyService.notify('error', '', error.message);
    }).finally(() => {
      this.loadingInteracoes = false;
    });
  }

  /**
   * Redireciona o usuário para a área de ocorrências
   * @param id_ocorrencia
   */
  visualizarChamado(id_ocorrencia) {
    this.state.go("app.chamados.show", {
      idMatriculaAtiva: this.MatriculaAtiva.id_matricula,
      id_ocorrencia: id_ocorrencia
    });
  }

}

export default PwCertificacaoConferirController;
