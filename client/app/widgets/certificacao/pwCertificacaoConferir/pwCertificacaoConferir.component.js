import template from './pwCertificacaoConferir.html';
import controller from './pwCertificacaoConferir.controller';
import './pwCertificacaoConferir.scss';

const pwCertificacaoConferirComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pwCertificacaoConferirComponent;
