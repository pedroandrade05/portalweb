import loadingGif from '../../../../assets/img/loading.gif';

class PwCursosLivresController {

  constructor(
    /* Úteis */ $sce, $state, $localStorage,
    /* Services */ CursoService, CursosLivresService, ModalService,
    /* Constantes */ EsquemaConfiguracaoConstante, MensagemPadraoConstante
  ) {
    'ngInject';
    this.name = 'pwCursosLivres';

    // Úteis
    this.$sce = $sce;
    this.$state = $state;
    this.storage = $localStorage;

    // Services
    this.CursoService = CursoService;
    this.CursosLivresService = CursosLivresService;
    this.ModalService = ModalService;

    // Constants
    this.EsquemaConfiguracaoConstante = EsquemaConfiguracaoConstante;
    this.MensagemPadraoConstante = MensagemPadraoConstante;

    // Outros
    this.loadingGif = loadingGif;
    this.matricula = CursoService.getStorageMatriculaAtivo($state.params.idMatriculaAtiva) || {};
    this.cursoOrigem = angular.fromJson(this.storage.cursoOrigem);

    if (!$localStorage.CursosLivresState) {
      $localStorage.CursosLivresState = {};
    }

    this.state = {
      loading: false,
      curso: null,
      cursos: null,
      nuTotal: 0,
      nuCursando: 0,
      textoInformacoes: $localStorage.CursosLivresState.textoInformacoes,
      textoDuvidasFrequentes: $localStorage.CursosLivresState.textoDuvidasFrequentes
    };

    this.getInformacoes();
    this.getDuvidasFrequentes();
    this.getCursos();
  }

  async getInformacoes() {
    const id_entidade = this.cursoOrigem
      ? this.cursoOrigem.id_entidade
      : this.matricula.id_entidade;

    return this.CursosLivresService
      .getMensagemPadrao(
        this.MensagemPadraoConstante.CURSOS_LIVRES_INFORMACOES,
        id_entidade
      )
      .then((response) => {
        const json = response.toJSON();
        this.state.textoInformacoes = json;
        this.storage.CursosLivresState.textoInformacoes = json;
      });
  }

  async getDuvidasFrequentes() {
    const id_entidade = this.cursoOrigem
      ? this.cursoOrigem.id_entidade
      : this.matricula.id_entidade;

    return this.CursosLivresService
      .getMensagemPadrao(
        this.MensagemPadraoConstante.CURSOS_LIVRES_DUVIDAS_FREQUENTES,
        id_entidade
      )
      .then((response) => {
        const json = response.toJSON();
        this.state.textoDuvidasFrequentes = json;
        this.storage.CursosLivresState.textoDuvidasFrequentes = json;
      });
  }

  async getCursos() {
    this.state.cursos = null;

    const params = {
      id_esquemaconfiguracao: this.cursoOrigem
        ? this.cursoOrigem.id_esquemaconfiguracao
        : this.matricula.id_esquemaconfiguracao
    };

    return this.CursosLivresService.getAll(params).then((response) => {
      this.state.cursos = response;
      this.state.nuTotal = response.length;
      this.state.nuCursando = response.filter((item) => item.bl_cursando).length;
      return response;
    });
  }

  handleClickCurso = (curso) => {
    const isCursoLivre = this.CursoService.isCursoLivre(this.matricula);

    const {bl_cursando, id_projetopedagogico} = curso;

    // Realizar login direto
    if (bl_cursando) {
      const {id_usuario, id_entidade, id_matricula, st_urlacesso} = curso.matricula;

      if (this.matricula && this.matricula.id_matricula && !isCursoLivre) {
        this.storage.cursoOrigem = angular.toJson(this.matricula);
      }

      this.$state.go('login.direto', {
        blGravaLog: 1,
        idUsuarioOriginal: id_usuario,
        idEntidade: id_entidade,

        idUsuario: id_usuario,
        stUrlAcesso: st_urlacesso,
        idCurso: id_projetopedagogico,
        idSalaDeAula: 0, // Não abrir sala de aula
        idMatricula: id_matricula
      });

      return;
    }

    this.handleClickConfirmar(curso);
  };

  handleClickConfirmar = async (curso) => {
    // Modal para confirmar nova matrícula
    await this.ModalService.show({
      body: `Você confirma que deseja fazer matrícula no curso livre <b>${curso.st_tituloexibicao}</b>?`,
      confirmButton: 'Confirmar',
      cancelButton: 'Cancelar'
    }).then(async () => {
      this.state.loading = true;

      try {
        // SERVICE PARA MATRICULAR ALUNO
        const {matricula} = await this.CursosLivresService.matricular(this.matricula.id_usuario, curso.id_projetopedagogico);

        // Atualizar cursos
        const cursos = await this.getCursos();

        this.state.loading = false;

        // Modal de sucesso
        await this.ModalService.show({
          title: 'Matrícula efetuada com sucesso!',
          body: `Você foi matriculado no curso <b>${curso.st_tituloexibicao}. Bons estudos!</b>`,
          confirmButton: 'Ir para o curso',
          cancelButton: 'Fechar'
        }).then(() => {
          const cursoNovo = cursos.find((curso) => curso.matricula && +curso.matricula.id_matricula === +matricula.id_matricula);

          if (cursoNovo) {
            this.handleClickCurso(cursoNovo);
          }
        }).catch(() => null);
      } // Catch error
      catch (e) {
        this.state.loading = false;

        // Modal de erro
        await this.ModalService.show({
          title: 'Oops!',
          body: 'Tivemos um problema ao realizar a sua matrícula. Entre em contato com o serviço de atenção.',
          confirmButton: 'Fechar'
        }).catch(() => null);
      }

      this.state.loading = false;
    }).catch(() => null);
  };

}

export default PwCursosLivresController;
