import PwCursosLivresModule from './pwCursosLivres';
import PwCursosLivresController from './pwCursosLivres.controller';
import PwCursosLivresComponent from './pwCursosLivres.component';
import PwCursosLivresTemplate from './pwCursosLivres.html';

describe('PwCursosLivres', () => {
  let $rootScope,
    makeController,
    $state = {params: {idMatriculaAtiva: null}};

  beforeEach(window.module(PwCursosLivresModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwCursosLivresController($state);
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PwCursosLivresComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PwCursosLivresTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwCursosLivresController);
    });
  });
});
