import template from './pwCursosLivres.html';
import controller from './pwCursosLivres.controller';
import './pwCursosLivres.scss';

const pwCursosLivresComponent = {
  restrict: 'E',
  bindings: {
    resolve: "<"
  },
  template,
  controller,
  controllerAs: '$ctrl'
};

export default pwCursosLivresComponent;
