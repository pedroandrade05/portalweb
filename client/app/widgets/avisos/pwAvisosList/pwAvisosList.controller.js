import loadingGif from "../../../../assets/img/loading.gif";
import estrela from '../../../../assets/img/estrela_amarela.svg';
import idCard from '../../../../assets/img/id-card.svg';

class PwAvisosListController {
  constructor(
    /* Úteis */ $scope, $state, $compile, $window, $sce,
    /* Services */ AvisosService, CursoAlunoService, AgendamentoService, CampanhaPublicitariaService, AplicacaoProvaOnlineService,
    RenovacaoService, NotifyService,
    /* Constantes */ EsquemaConfiguracaoConstante, TipoAvaliacaoConstante, $localStorage, EvolucaoMatriculaConstante, EntidadeConstante
  ) {
    'ngInject';
    this.name = 'pwAvisosList';

    // Úteis
    this.compile = $compile;
    this.scope = $scope;
    this.window = $window;
    this.state = $state;
    this.$sce = $sce;
    this.$localStorage = $localStorage;

    //inicia a variável vazia dos avisos
    this.mensagens = [];
    this.campanhas = [];

    // Services
    this.AvisosService = AvisosService;
    this.AgendamentoService = AgendamentoService;
    this.AplicacaoProvaOnlineService = AplicacaoProvaOnlineService;
    this.CampanhaPublicitariaService = CampanhaPublicitariaService;
    this.renovacaoService = RenovacaoService;
    this.NotifyService = NotifyService;

    // Constants
    this.TipoAvaliacaoConstante = TipoAvaliacaoConstante;
    this.EvolucaoMatriculaConstante = EvolucaoMatriculaConstante;
    this.EntidadeConstante = EntidadeConstante;

    // Outros
    this.loading = false;
    this.loadingGif = loadingGif;
    this.estrela = estrela;
    this.idCard = idCard;

    //cria uma variavel no escopo para a matricula ativa
    this.idMatriculaAtiva = $state.params.idMatriculaAtiva;

    // Dados da matrícula
    this.matricula = CursoAlunoService.getStorageCursoAtivo(this.idMatriculaAtiva);
    this.alunoConcluinte = (this.matricula && +this.matricula.id_evolucao === this.EvolucaoMatriculaConstante.CONCLUINTE);
    this.isFaculdadeUnyleya = +this.matricula.id_entidade === +this.EntidadeConstante.FACULDADE_UNYLEYA;

    // Novo aviso de agendamento para a GRADUAÇÃO
    if (+this.matricula.id_esquemaconfiguracao === +EsquemaConfiguracaoConstante.GRADUACAO) {
      this.verificarAgendamentos();
    }

    // Busca Prova Online para a PÓS
    if (+this.matricula.id_esquemaconfiguracao === +EsquemaConfiguracaoConstante.POS_GRADUACAO) {
      this.verificarAplicacaoProvaOnline();
    }

    this.carregarMensagens();
    this.verificarRenovacaoAutomatica();
    this.mostrarAvisoCarteirinha();

  }

  /**
   * Buscar o último agendamento ativo, para mostrar o a mensagem na home do portal
   */
  verificarAgendamentos() {
    this.AgendamentoService
      .getProximoAgendamento(this.idMatriculaAtiva, {
        'id_tipoavaliacao[0]': this.TipoAvaliacaoConstante.ATIVIDADE_PRESENCIAL,
        'id_tipoavaliacao[1]': this.TipoAvaliacaoConstante.AVALIACAO_RECUPERACAO,
        'id_tipoavaliacao[2]': this.TipoAvaliacaoConstante.AVALIACAO_PRESENCIAL,
        'id_tipoavaliacao[3]': this.TipoAvaliacaoConstante.ATIVIDADE_EAD,
        'id_tipoavaliacao[4]': this.TipoAvaliacaoConstante.AVALIACAO_SEMESTRAL,
        'id_tipoavaliacao[5]': this.TipoAvaliacaoConstante.AVALIACAO_SEMESTRAL_RECUPERACAO
      })
      .then((response) => {
        this.agendamento = response.toJSON();
      });
  }

  /**
   * Busca Aplicação de Prova Online ainda não realizada, com a menor data de inicio
   */
  verificarAplicacaoProvaOnline() {
    this.AplicacaoProvaOnlineService
      .getProximaAplicacao(this.idMatriculaAtiva)
      .then((response) => {
        this.aplicacao = response;
      });
  }

  /**
   * Carrega os avisos e campanhas unindo no array de mensagens
   */
  async carregarMensagens() {
    var mensagens = [];
    this.loading = true;

    await this.CampanhaPublicitariaService.getCampanhaPublicitaria(this.matricula.id_entidade, true)
      .then((response) => {
        angular.forEach(response, (campanha) => {
          campanha.date = new Date(campanha.dt_inicio.date);
          campanha.send_date = campanha.dt_inicio.date;
          campanha.icon = 'icon-icon_chat';
          campanha.title = campanha.st_campanhapublicitaria;
          campanha.text = campanha.st_conteudo;
          campanha.bl_fixarportal = 0;
          mensagens.push(campanha);
        });
      }, () => {

      });

    await this.AvisosService.getAll({id_matricula: this.idMatriculaAtiva})
      .then((response) => {
        angular.forEach(response, (aviso, key) => {
          aviso.icon = this.AvisosService.getIconAviso(aviso.type);
          aviso.index = key;
          aviso.date = new Date(aviso.send_date);
          aviso.bl_fixarportal = aviso.object.bl_fixarportal;
          mensagens.push(aviso);
        });
      }, () => {

      });
    this.mensagens = mensagens.sort((a, b) => {
      var e = a.date > b.date ? -1 : 0;
      e -= (a.bl_fixarportal ? 1 : 0) - (b.bl_fixarportal ? 1 : 0);
      return e;
    }
    );
    this.loading = false;
    return true;
  }

  redirecionarMensagem(mensagem) {
    if (!mensagem.type) {
      this.redirecionarCampanha(mensagem);
      return;
    }
    this.redirecionarAviso(mensagem, mensagem.index);
  }

  /**
   * Método que verifica se o aluno está apto a realizar a renovação automática.
   */
  verificarRenovacaoAutomatica = () => {
    this.mostrarMensagemRenovacao = false;
    this.renovacaoService.verificarRenovacao(this.idMatriculaAtiva).then(
      (response) => {
        if (angular.isDefined(response.bl_aptorenovacao)) {
          this.dadosRenovacao = response;
          this.mostrarMensagemRenovacao = response.bl_aptorenovacao && +response.id_evolucaovenda === 9;
        }
      },
      () => {
        this.NotifyService.notify('error', 'Houve um erro de comunicação ao tentar recuperar os dados de renovação');
      });
  };

  renovarMatricula = () => {
    this.state.go('app.renovacao', {
      idMatriculaAtiva: this.state.params.idMatriculaAtiva,
      dados: angular.toJson(this.dadosRenovacao)
    });
  };

  /**
   * Método que mostra a mensagem de carteirinha.
   */
  mostrarAvisoCarteirinha = () => {
    this.mostrarMensagemCarteirinha = false;
    this.AvisosService.getAvisoCarteirinha(this.idMatriculaAtiva).then(
      (response) => {
        if (angular.isDefined(response.st_texto)) {
          this.dadosCarteirinha = response;
          this.mostrarMensagemCarteirinha = true;
        }
      });
  };

  /**
   * Define os parametros e rota para redirecionar o aviso
   * @param aviso
   * @param index
   */
  redirecionarAviso = (aviso, index) => {
    const functionAviso = this.AvisosService.getFunctionAviso(aviso, this.idMatriculaAtiva, this);

    if (aviso.type === 'envioTcc') {
      functionAviso().result.then((result) => {

        if (result.type === "success") {
          this.mensagens.splice(index, 1);
        }

      });
    }
    else {
      functionAviso();
    }

    this.selected = index;

    if (aviso.object && aviso.object.id_enviomensagem && aviso.object.id_matricula && aviso.object.id_evolucao) {
      this.AvisosService.getResource().updateEvolucaoAvisos({
        id_enviomensagem: aviso.object.id_enviomensagem,
        id_enviodestinatario: aviso.object.id_enviodestinatario,
        id_matricula: aviso.object.id_matricula,
        id_evolucao: 43
      });
    }
  };

  /**
   * Funcao para alterar a varaivel que muda o state
   */
  changeState = () => {
    this.statusState = !this.statusState;
  };

  /**
   * Define os parametros e rota para redirecionar o aviso
   * @param campanha
   */
  redirecionarCampanha = (campanha) => {
    this.state.go('app.promocoes.show', {
      idMatriculaAtiva: this.state.params.idMatriculaAtiva,
      tipoCamanha: 'publicitaria',
      idCampanha: campanha.id_campanhapublicitaria
    });
  };

}

export default PwAvisosListController;
