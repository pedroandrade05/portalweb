import template from './pwAvisosList.html';
import controller from './pwAvisosList.controller';
import './pwAvisosList.scss';

const pwAvisosListComponent = {
  restrict: 'E',
  bindings: {
    statusState:'='
  },
  template,
  controller,
  controllerAs: 'avisoCtrl'
};

export default pwAvisosListComponent;
