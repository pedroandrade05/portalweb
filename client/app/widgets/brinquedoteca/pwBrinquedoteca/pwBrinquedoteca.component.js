import template from './pwBrinquedoteca.html';
import controller from './pwBrinquedoteca.controller.js';
import './pwBrinquedoteca.scss';

const pwBrinquedotecaComponent = {
  restrict: 'E',
  bindings: {
    disciplina: '='
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pwBrinquedotecaComponent;
