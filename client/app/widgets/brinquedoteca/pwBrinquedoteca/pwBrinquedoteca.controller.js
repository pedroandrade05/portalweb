import loadingGif from "../../../../assets/img/loading.gif";

class PwBrinquedotecaController {

  constructor(
    $sce,
    $http,
    $state,
    NotifyService,
    BrinquedotecaService,
    LSService,
    CursoAlunoService
  ) {
    'ngInject';

    this.$sce = $sce;
    this.$http = $http;
    this.state = $state;

    this.NotifyService = NotifyService;
    this.BrinquedotecaService = BrinquedotecaService;
    this.cursoAlunoService = CursoAlunoService;
    this.LSService = LSService;

    this.matriculaAtiva = this.cursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);
    this.urlBrinquedoteca = null;

    //inicia a variavel de loading
    this.loading = false;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;

    this.getMostrarBrinquedoteca();

  }

  /**
   * Verifica se mostrará a brinquedoteca
   */
  getMostrarBrinquedoteca() {
    this.loading = true;
    this.cursoAlunoService.getMostrarBrinquedoteca(this.matriculaAtiva.id_curso)
      .then((response) => {
        this.matriculaAtiva.bl_mostrarbrinquedoteca = response.bl_mostrarbrinquedoteca;
        this.st_urlBrinquedoteca = response.st_urlBrinquedoteca;
        if (this.matriculaAtiva.bl_mostrarbrinquedoteca) {
          this.abrirBrinquedoteca();
        }
      }, (error) => {
        this.NotifyService.notify(error.data);
      })
      .finally(() => {
        this.loading = false;
      });
  }

  /**
   * Chamar a pagina da brinquedoteca buscando a url do back end
   */
  abrirBrinquedoteca() {
    this.BrinquedotecaService.getTokenMoodle(this.matriculaAtiva.id_matricula, this.matriculaAtiva.id_entidade)
      .then((success) => {
        const urlBrinquedoteca = success.loginurl + '&wantsurl=' + this.st_urlBrinquedoteca;
        this.urlBrinquedoteca = this.$sce.trustAsResourceUrl(urlBrinquedoteca);
      }, (error) => {
        this.NotifyService.notify(error.data);
      });
  }
}

export default PwBrinquedotecaController;
