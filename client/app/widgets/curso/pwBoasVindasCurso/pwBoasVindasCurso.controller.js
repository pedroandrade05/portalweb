import loadingGif from "../../../../assets/img/loading.gif";
import imgAmbientacao from '../../../../assets/img/ambientacao.png';
import imgManualAluno from '../../../../assets/img/manual_do_aluno.png';
import imgCalendario from '../../../../assets/img/calendario_academico.png';
import imgConhecaCurso from '../../../../assets/img/conheca_curso.png';

class PwBoasVindasCursoController {
  constructor(
    /* Úteis */ CONFIG, $state, $uibModal, $sce, $window,
    /* Services */ CursoService, CursoAlunoService, EsquemaConfiguracaoService, FinanceiroService, NotifyService, DisciplinaAlunoService,
    /* Constantes */ EvolucaoMatriculaConstante, TipoDisciplinaConstante
  ) {
    'ngInject';
    this.name = 'pwBoasVindasCurso';

    /* Uteis  */
    this.$state = $state;
    this.CONFIG = CONFIG;
    this.$sce = $sce;
    this.modalVideo = $uibModal;
    this.$window = $window;

    /* Services */
    this.CursoAlunoService = CursoAlunoService;
    this.CursoService = CursoService;
    this.FinanceiroService = FinanceiroService;
    this.toaster = NotifyService;
    this.DisciplinaAlunoService = DisciplinaAlunoService;

    this.EvolucaoMatriculaConstante = EvolucaoMatriculaConstante;
    this.TipoDisciplinaConstante = TipoDisciplinaConstante;

    //inicia a variavel de loading
    this.loading = false;

    //carrega a imagem
    this.loadingGif = loadingGif;
    this.imgAmbientacao = imgAmbientacao;
    this.imgManualAluno = imgManualAluno;
    this.imgCalendario = imgCalendario;
    this.imgConhecaCurso = imgConhecaCurso;

    this.detalhesCoordenadorFetched = 0;
    this.boasVindasCursoFetched = 0;
    this.avatarExists = false;

    this.disciplinaAmbientacao = null;

    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao(this.matricula.id_matricula);
    if (!this.evolucaoEhBloqueada() && !this.matricula.bl_primeiroacesso && this.isGraduacao) {
      this.retornarDisciplinaAmbientacao();
    }

    this.isManualAluno = !this.evolucaoEhEgresso() && this.isGraduacao;

    this.manualDoAluno = null;
    this.calendarioAcademico = null;

    this.resumoBoasVindasObj = {
      idUsuarioCoordenador: '',
      textoBoasVindasCurso: '',
      nomeCoordenador: '',
      descricaoCoordenador: '',
      embedVideoApresentacao: '',
      urlAvatar: ''
    };
    this.getResumoBoasVindasCurso(this.matricula.id_curso);
    if (this.isGraduacao) {
      this.getManualDoAluno(this.matricula.id_entidade);
      this.getCalendarioAcademico(this.matricula.id_entidade);
    }
  }

  checkIfAvatarExists = () => {
    try {
      const imgUrl = this.resumoBoasVindasObj.urlAvatar;
      const request = new XMLHttpRequest();
      request.open('GET', imgUrl, false);
      request.send();
      return request.status === 200;
    }
    catch (e) {
      return false;
    }
  };

  openModalVideo = () => {
    this.modalVideo.open({
      template: '<div class="embed-responsive embed-responsive-16by9">' + this.resumoBoasVindasObj.embedVideoApresentacao + '</div>'
    });
  };

  /* Preenche o resumoBoasVindasObj com os dados
   relacionados à tela de boas vindas. */

  getResumoBoasVindasCurso = (idCursoAtivo) => {
    this.loading = true;
    this.CursoService.getCoordenadorCurso(idCursoAtivo)
      .then((success) => {
        const idCoordenadorCurso = success.id_usuario;
        const idEntidade = this.matricula.id_entidade;

        this.resumoBoasVindasObj.idUsuarioCoordenador = idCoordenadorCurso;

        //Busca e seta no objeto descrição e nome do coordenador.
        this.CursoService.getDetalhesCoordenador(idCoordenadorCurso, idEntidade)
          .then((success) => {
            this.resumoBoasVindasObj.descricaoCoordenador = success.st_descricaocoordenador;
            this.resumoBoasVindasObj.nomeCoordenador = success.st_nomeexibicao;

            //.substr(1) necessário para remover o primeiro '/' da string com a URL.
            if (success.st_urlavatar) {
              this.resumoBoasVindasObj.urlAvatar = success.st_urlavatar;
            }

            this.detalhesCoordenadorFetched = true;
            this.avatarExists = this.checkIfAvatarExists();
          }, (() => {
          }));

      }, (() => {
      }));

    //Busca e seta no objeto o texto de boas vindas e o vídeo de apresentação.
    this.CursoService.getBoasVindasCurso(this.matricula.id_curso)
      .then((success) => {
        this.resumoBoasVindasObj.textoBoasVindasCurso = success.st_boasvindascurso;

        if (success.st_valorapresentacao && success.st_valorapresentacao !== "") {
          // Garantindo atributos de fullscreen no vídeo
          var myEl = angular.element(success.st_valorapresentacao);
          myEl.attr('allowfullscreen', "true");
          myEl.attr("webkitallowfullscreen", "true");
          myEl.attr("mozallowfullscreen", "true");
          this.resumoBoasVindasObj.embedVideoApresentacao = this.$sce.trustAsHtml(myEl[0].outerHTML);
        }

        this.boasVindasCursoFetched = true;

      }, () => {
      })
      .finally(() => {
        this.loading = false;
      });
  };

  evolucaoEhBloqueada = () => {
    return Number(this.matricula.id_evolucao) === Number(this.EvolucaoMatriculaConstante.BLOQUEADO);
  };

  evolucaoEhEgresso = () => {
    return Number(this.matricula.id_evolucao) === Number(this.EvolucaoMatriculaConstante.EGRESSO);
  };

  /**
   * Redireciona para a tela de Ambientacao
   */
  redirecionaAmbientacao = () => {
    try {
      this.$state.go("app.disciplina", {
        idMatriculaAtiva: this.matricula.id_matricula,
        idSalaDeAula: this.disciplinaAmbientacao.id_saladeaula
      });
    }
    catch (e) {
      this.toaster.notify('error', 'Ops', 'Erro ao tentar redirecionar para tela de Ambientacao.');
    }
  }

  /** Verifica se há manual do aluno para a entidade. Caso positivo,
   * retorna id_upload e st_upload do manual do aluno.
   * @param {string} id_entidade - ID da entidade.
   * */

  getManualDoAluno(id_entidade) {
    this.FinanceiroService.getManualDoAluno(id_entidade).then((success) => {
      this.manualDoAluno = success;
    });
  }

  /** Verifica se há calendario Academico para a entidade.
   * @param {string} id_entidade - ID da entidade.
   * */

  getCalendarioAcademico(id_entidade) {
    this.CursoService.getCalendarioAcademico(id_entidade).then((success) => {
      this.calendarioAcademico = success;
    });
  }

  /**
   * Redireciona para a tela de Calendário Acadêmico
   */
  redirecionaCalendarioAcademico = () => {
    try {
      if (this.calendarioAcademico.length > 1) {
        this.$state.go("app.calendario-academico", {
          idMatriculaAtiva: this.matricula.id_matricula
        });
      }
      else {
        this.$window.open(this.calendarioAcademico[0].st_urlupload);
      }

    }
    catch (e) {
      this.toaster.notify('error', 'Ops', 'Erro ao tentar redirecionar para tela de Calendário Acadêmico.');
      console.error(e);
    }
  }

  /**
   * Retorna as disciplinas de Ambientação
   */
  retornarDisciplinaAmbientacao = () => {

    this.DisciplinaAlunoService.getAll({
      id_entidade: this.matricula.id_entidade,
      id_matricula: this.matricula.id_matricula,
      id_projetopedagogico: this.matricula.id_curso,
      bl_moodle: 0,
      bl_disciplina: 0
    }, true).then((disciplinasResp) => {
      angular.forEach(disciplinasResp, (disciplina) => {
        if (+disciplina.id_tipodisciplina === this.TipoDisciplinaConstante.AMBIENTACAO){
          this.disciplinaAmbientacao = disciplina;
          this.isMostraAmbientacao = true;
        }
      });
    });
  };
}


export default PwBoasVindasCursoController;
