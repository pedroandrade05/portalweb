import PwBoasVindasCursoModule from './pwBoasVindasCurso';
import PwBoasVindasCursoController from './pwBoasVindasCurso.controller';
import PwBoasVindasCursoComponent from './pwBoasVindasCurso.component';
import PwBoasVindasCursoTemplate from './pwBoasVindasCurso.html';

describe('PwBoasVindasCurso', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwBoasVindasCursoModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwBoasVindasCursoController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PwBoasVindasCursoComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PwBoasVindasCursoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwBoasVindasCursoController);
    });
  });
});
