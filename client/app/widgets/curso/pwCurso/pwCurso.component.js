import template from './pwCurso.html';
import controller from './pwCurso.controller.js';
import './pwCurso.scss';

const pwCursoComponent = {
  restrict: 'E',
  bindings: {
    statusState: "<",
    matriculaAluno: '<'
  },
  template,
  controller,
  controllerAs: 'pwcur'
};

export default pwCursoComponent;
