import PwCursoModule from './pwCurso';
import PwCursoController from './pwCurso.controller.js';
import PwCursoComponent from './pwCurso.component.js';
import PwCursoTemplate from './pwCurso.html';

describe('PwCurso', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwCursoModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwCursoController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PwCursoComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PwCursoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwCursoController);
    });
  });
});
