class PwCursoController {
  constructor(
    /* Utils */ $http, $state, $localStorage, CONFIG, $window,
    /* Services */ CursoService, CursosLivresService, EsquemaConfiguracaoService, ModalService, PortalEgressoService,
    /* Constants */ EvolucaoMatriculaConstante, EsquemaConfiguracaoConstante,
  ) {
    'ngInject';
    this.name = 'pwCurso';

    // Uteis
    this.state = $state;
    this.config = CONFIG;
    this.storage = $localStorage;
    this.$http = $http;
    this.$window = $window;

    // Services
    this.CursosLivresService = CursosLivresService;
    this.EsquemaConfiguracaoService = EsquemaConfiguracaoService;
    this.ModalService = ModalService;
    this.PortalEgressoService = PortalEgressoService;

    // Constants
    this.EvolucaoMatriculaConstante = EvolucaoMatriculaConstante;
    this.EsquemaConfiguracaoConstante = EsquemaConfiguracaoConstante;

    // Outros
    this.portalEgresso = {};
    this.isCursoLivre = CursoService.isCursoLivre(this.matriculaAluno);

    // Banner de DESTAQUE sobrepondo imagem do curso
    // O setDestaque é utilizado abaixo para preenchimento
    // Exemplo de uso em: this.getDadosPortalEgresso()
    this.destaque = null;

    // Aluno egresso?
    this.isAlunoEgresso = false;
    this.checkAlunoEgresso().catch(() => null);

    if (this.isCursoLivre) {
      this.getCursoLivreInfo();
    }

    this.imgCover = this.matriculaAluno.st_imagem || this.matriculaAluno.st_imagem_padrao;

    this.matriculaAluno.bl_graduacao = +this.matriculaAluno.id_esquemaconfiguracao === EsquemaConfiguracaoConstante.GRADUACAO
      || +this.matriculaAluno.id_esquemaconfiguracao === EsquemaConfiguracaoConstante.POS_GRADUACAO;

  }

  /**
   * Definir banner em destaque
   * @param {null|Object} props
   */
  setDestaque = (props = {}) => {
    this.destaque = {
      center: true,
      title: null,
      body: null,
      buttons: [],
      ...props
    };
  }

  checkAlunoEgresso = async () => {
    this.isAlunoEgresso = await this.PortalEgressoService.isAlunoEgresso(this.matriculaAluno.id_matricula);

    if (this.isAlunoEgresso) {
      this.getDadosPortalEgresso();
    }
  }

  getDadosPortalEgresso = () => {
    this.PortalEgressoService.getDadosPortal().then((response) => {
      this.portalEgresso = response || {};

      if (this.portalEgresso.st_urlportal) {
        const isGraduacao = this.EsquemaConfiguracaoService.isGraduacao(this.matriculaAluno.id_matricula);

        const isCertificado = [
          this.EvolucaoMatriculaConstante.CERTIFICADO,
          this.EvolucaoMatriculaConstante.DIPLOMADO,
          this.EvolucaoMatriculaConstante.EGRESSO
        ].includes(+this.matriculaAluno.id_evolucao);

        // Texto PADRÃO
        let title = 'Conheça nosso Portal do Egresso!';
        let body = 'Você que está na fase final do seu curso já pode acessar o portal do egresso para conhecer e usufruir do que preparamos para nossa comunidade de ex-alunos. Lá você encontrará materiais exclusivos sobre mercado de trabalho e oportunidades únicas para você continuar investindo na sua carreira.';

        // Textos para alunos certificados
        if (isCertificado) {
          title = 'Benefícios e oportunidades exclusivos para você, aluno formado!';
          body = 'Acesse o Portal do Egresso para usufruir dos benefícios exclusivos que reservamos para você que já concluiu o seu curso.';
        }

        // Textos para GRA
        if (isGraduacao) {
          title = 'Parabéns pela colação de grau!';
          body = 'Desejamos muito sucesso em sua trajetória profissional! Como você está formado, já pode acessar o Portal do Egresso para conhecer e usufruir do que preparamos para nossa comunidade de ex-alunos. Lá você encontrará materiais exclusivos sobre mercado de trabalho e oportunidades únicas para continuar investindo na sua carreira.';
        }

        this.setDestaque({
          title,
          body,
          center: true,
          buttons: [{
            className: 'btn-outline',
            title: 'Ir para o Portal do Egresso',
            onClick: this.abrirPortalEgressso
          }]
        });
      }

    }, () => null);
  };

  getCursoLivreInfo = () => {
    this.cursoLivre = this.storage.cursoLivre;

    return this.CursosLivresService.getAll({
      id_matricula: this.matriculaAluno.id_matricula,
      id_projetopedagogico: this.matriculaAluno.id_curso
    }).then((response) => {
      if (response && response[0] && response[0].bl_cursando) {
        this.cursoLivre = response[0];
        this.storage.cursoLivre = response[0];
      }
    });
  };

  abrirPortalEgressso = async () => {
    try {
      const {st_urlportal} = this.portalEgresso;
      const {id_usuario, id_entidade, st_urlacesso} = this.matriculaAluno;

      if (st_urlportal) {
        const url = await this.PortalEgressoService.getUrlAutenticarDireto(id_usuario, id_entidade, st_urlacesso, null, st_urlportal);
        this.$window.open(url, 'uny-portal-egresso');
      }
    }
    catch (err) {
      this.ModalService.show({
        title: 'Oops!',
        body: 'Não foi possível redirecionar para o Portal do Egresso.<br/>Tente novamente em alguns instantes.',
        confirmButton: 'Ok, Fechar',
        cancelButton: false
      });
    }
  };
}

export default PwCursoController;
