import PwAgendamentoFormModule from './pwAgendamentoForm';
import PwAgendamentoFormController from './pwAgendamentoForm.controller';
import PwAgendamentoFormComponent from './pwAgendamentoForm.component';
import PwAgendamentoFormTemplate from './pwAgendamentoForm.html';

describe('PwAgendamentoForm', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwAgendamentoFormModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwAgendamentoFormController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PwAgendamentoFormComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PwAgendamentoFormTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwAgendamentoFormController);
    });
  });
});
