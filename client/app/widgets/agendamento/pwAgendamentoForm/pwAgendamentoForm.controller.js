import loadingGif from '../../../../assets/img/loading.gif';
import templateEventsCalendar from './eventosTemplate.html';

class PwAgendamentoFormController {

  constructor(
    CONFIG,
    $q,
    NotifyService,
    PessoaService,
    AgendamentoService,
    EnderecoService,
    LinhaDeNegocioService,
    TipoEnderecoConstante,
    SituacaoConstante,
    PaisConstante,
    EntidadeConstante,
    TextoSistemaConstante,
    $sce,
    $state,
    $localStorage,
    calendarConfig,
    $templateCache,
    $window
  ) {

    'ngInject';

    this.name = 'pwAgendamentoForm';

    this.CONFIG = CONFIG;
    this.$q = $q;
    this.NotifyService = NotifyService;
    this.PessoaService = PessoaService;
    this.AgendamentoService = AgendamentoService;
    this.EnderecoService = EnderecoService;
    this.TipoEnderecoConstante = TipoEnderecoConstante;
    this.SituacaoConstante = SituacaoConstante;
    this.PaisConstante = PaisConstante;
    this.EntidadeConstante = EntidadeConstante;
    this.TextoSistemaConstante = TextoSistemaConstante;

    this.$sce = $sce;
    this.$state = $state;
    this.$localStorage = $localStorage;
    this.$window = $window;

    this.loadingGif = loadingGif;
    this.idMatricula = $state.params.idMatriculaAtiva;

    // Definir se a etapa é clicável
    this.etapas = {
      avaliacao: false,
      endereco: true,
      agendamento: false
    };

    this.loading = false;
    this.etapaAtual = null;
    this.isProvaPorDisciplina = false;

    this.avisoCovid = false;

    if (+this.$localStorage.user_entidade === +this.EntidadeConstante.FACULDADE_UNYLEYA ||
      +this.$localStorage.user_entidade === +this.EntidadeConstante.ESTRATEGIA ||
      +this.$localStorage.user_entidade === +this.EntidadeConstante.POS_GRADUACAO) {
      this.avisoCovid = true;
      AgendamentoService.getAvisoCovid(this.TextoSistemaConstante.AVISO_COVID).then((response) => {
        this.mensagemAvisoCovid = response.st_texto;
      });
    }

    this.agendamento = {};
    this.endereco = {
      id_tipoendereco: this.TipoEnderecoConstante.CORRESPONDENCIA,
      bl_padrao: 1,
      bl_confirmado: false
    };

    this.disciplina = null;
    this.agendamentos = [];
    this.avaliacoes = [];
    this.aplicacoes = [];

    this.formReagendar = false;
    this.disciplinas = [];

    this.paises = [];
    this.ufs = [];
    this.municipios = [];
    this.ufsAgendamento = [];
    this.municipiosAgendamento = [];

    this.legendas = [
      {
        label: 'Disponível',
        class: 'azul'
      },
      {
        label: 'Agendado',
        class: 'vermelho'
      },
      {
        label: 'Não disponível',
        class: 'cinza'
      }
    ];

    $templateCache.put('calendarEventsTemplate.html', templateEventsCalendar);
    calendarConfig.templates.calendarSlideBox = 'calendarEventsTemplate.html';

    // Cofigurações do calendário
    this.calendario = {
      calendarView: 'month',
      date: new Date(),
      isOpen: false,
      events: [],
      viewChangeClicked: (nextView) => {
        return nextView === 'year' || nextView === 'month';
      }
    };

    LinhaDeNegocioService.getLinhaDeNegocio(this.$localStorage.user_entidade).then((response) => {
      this.isProvaPorDisciplina = response.bl_provapordisciplina;

      this.prepararCalendario();
      this.alterarEtapa('avaliacao');
    });
  }

  /**
   * Alternar entre as etapas do agendamento
   * @param etapa
   */
  alterarEtapa = (etapa) => {
    this.etapaAtual = etapa || 'endereco';

    this.etapas[etapa] = true;

    switch (etapa) {
    case 'endereco':
      this.loading = true;

      this.buscarEnderecoCorrespondencia().finally(() => {
        this.$q.all([
          this.buscarPaises(),
          this.buscarUfs(),
          this.buscarUfsAgendamento(),
          this.buscarMunicipios(this.endereco.sg_uf)
        ]).finally(() => {
          this.loading = false;
        });
      });

      break;
    case 'avaliacao':
      // Usar por padrão os locais do endereço de correspondência para pesquisar os polos de aplicação
      angular.extend(this.agendamento, {
        sg_uf: this.endereco.sg_uf,
        id_municipio: this.endereco.id_municipio
      });

      // Na graduação é necessário buscar as disciplinas que o aluno pode realizar o agendamento
      if (this.isProvaPorDisciplina) {
        if (!this.disciplinas.length) {
          this.buscarDisciplinasApto();
        }
      }
      else {
        if (!this.avaliacoes.length) {
          this.buscarAvaliacoes();
        }
      }

      break;

    case 'agendamento':
      if (+this.$localStorage.user_entidade === +this.EntidadeConstante.FACULDADE_UNYLEYA) {
        this.municipios = [];
        angular.extend(this.endereco, {
          sg_uf: '',
          id_municipio: ''
        });
      }
      this.buscarAvaliacaoAplicacoes(this.agendamento.id_avaliacao);
      break;
    }
  };

  getClassEtapa = (etapa) => {
    if (this.etapaAtual === etapa) {
      return 'active';
    }

    const keys = Object.keys(this.etapas);

    const indexAtual = keys.indexOf(this.etapaAtual);
    const indexParam = keys.indexOf(etapa);

    return indexAtual > indexParam ? 'complete' : '';
  };

  /**
   * Salvar o endereço de correspondência
   * @param $event
   * @returns {boolean}
   */
  salvarEnderecoCorrespondencia = ($event) => {
    const form = $event.target;
    const is_valid = form.checkValidity();

    if (!is_valid) {
      return false;
    }

    this.loading = true;

    this.EnderecoService.getResource().update(this.endereco.id_endereco, this.endereco).$promise.then(
      (response) => {
        this.loading = false;

        // Mensagem de confirmação
        this.NotifyService.notify('success', '', 'Seu endereço de correspondência foi salvo!');

        angular.extend(this.endereco, response);

        // Definir que o endereço já foi salvo
        this.endereco.bl_confirmado = true;

        // Habilitar o click na etapa
        this.etapas.agendamento = true;

        // Após a confirmação dos dados deve ocorrer o direcionamento automático
        this.alterarEtapa('agendamento');
      },
      (error) => {
        this.loading = false;
        this.NotifyService.notify('error', '', error.data.message);
      }
    );
  };

  abrirAgendamento = (avaliacao) => {
    this.agendamento = avaliacao.ultimo_agendamento;
    angular.extend(this.agendamento, avaliacao);

    delete this.agendamento.agendamentos;
    delete this.agendamento.ultimo_agendamento;
    delete this.agendamento.agendamento_ativo;

    let etapa = 'endereco';

    // Habilitar o click na etapa
    this.etapas.endereco = true;

    if (this.endereco.bl_confirmado) {
      etapa = 'agendamento';

      // Habilitar o click na etapa
      this.etapas.agendamento = true;
    }

    this.alterarEtapa(etapa);
  };

  confirmarSalvarAgendamento = (aplicacao) => {
    let message = '<b>Avaliação:</b><br/>';
    message += this.agendamento.st_avaliacao;

    message += '<br/><br/>';

    message += '<b>Disciplinas:</b><br/>';
    message += this.getAgendamentoDisciplinas(this.agendamento);

    message += '<br/><br/>';
    message += this.getAgendamentoLocal(aplicacao, this.agendamento.bl_provaonline);
    message += '<br/><br/>';

    message += '<b>Data:</b><br/>';
    message += this.getAgendamentoData(aplicacao, this.agendamento.bl_provaonline);

    this.NotifyService.confirm('Resumo do Agendamento', message, () => {
      this.salvarAgendamento();
    });
  };

  /**
   * Salvar agendamento
   * @returns {boolean}
   */
  salvarAgendamento = () => {
    if (!this.agendamento.id_avaliacaoaplicacao) {
      this.NotifyService.notify('warning', '', 'Por favor, selecione um local de prova.');
      return false;
    }

    this.formReagendar = false;
    this.loading = true;

    // Se for o primeiro agendamento do aluno, setar o valor padrão do parametro para PROVA FINAL (não recuperação)
    if (!this.agendamento.id_avaliacao) {
      this.agendamento.id_tipodeavaliacao = 0;
    }

    this.agendamento.id_matricula = this.idMatricula;

    // Desabilitar o click na etapa
    this.etapas.agendamento = false;

    this.AgendamentoService.getResource().create(this.agendamento).$promise.then(
      () => {
        this.loading = false;

        this.prepararCalendario();
        this.buscarAvaliacoes();

        this.NotifyService.notify('success', '', 'Sua avaliação foi agendada!');

        this.alterarEtapa('avaliacao');
      },
      (error) => {
        this.loading = false;
        this.NotifyService.notify('error', '', error.data.message);
      }
    );
  };

  /**
   * Buscar os agendamentos de uma matrícula
   */
  prepararCalendario = () => {
    this.calendario.events = [];
    this.buscarAgendamentos().finally(() => {
      this.agendamentos.forEach((agendamento) => {
        this.adicionarEvento(agendamento);
      });
    });
  };

  /**
   * Adicionar um evento ao calendário
   * @param item
   */
  adicionarEvento = (item) => {
    // Se não tiver data, não adicionar ao calendário
    if (!item.dt_aplicacao) {
      return false;
    }

    // A lib do calendário coloca os eventos em TODOS os anos, então foi necessário filtrá-los aqui.
    if (+item.year !== +this.calendario.date.getFullYear()) {
      return false;
    }

    item.hr_inicio = item.hr_inicio || '00:00';
    item.hr_fim = item.hr_fim || '00:00';

    const dtAplicacaoInicio = (item.dt_aplicacaoinicio ? item.dt_aplicacaoinicio : item.dt_aplicacao).split('-');
    const dtAplicacaoTermino = item.dt_aplicacao.split('-');

    const dateStart = new Date(dtAplicacaoInicio[0], +dtAplicacaoInicio[1] - 1, dtAplicacaoInicio[2], item.hr_inicio.substr(0, 2), item.hr_inicio.substr(3, 2));
    const dateEnd = new Date(dtAplicacaoTermino[0], +dtAplicacaoTermino[1] - 1, dtAplicacaoTermino[2], item.hr_fim.substr(0, 2), item.hr_fim.substr(3, 2));

    this.calendario.events.push({
      title: item.st_aplicadorprova,
      type: 'info',
      data: item,
      startsAt: dateStart,
      endsAt: dateEnd,
      incrementsBadgeTotal: true,
      recursOn: 'year',
      cssClass: 'agendamento-status-agendado',
      allDay: false,
      color: {
        primary: '#e3bc08',
        secondary: '#fdf1ba'
      }
    });
  };

  /**
   * Buscar o endereço de correspondência do aluno
   */
  buscarEnderecoCorrespondencia = () => {
    return this.PessoaService.getVwPessoaEndereco(this.$localStorage.user_id, this.TipoEnderecoConstante.CORRESPONDENCIA).then(
      (response) => {
        response.id_pais = response.id_pais || this.PaisConstante.BRASIL;
        angular.extend(this.endereco, response);
      }
    );
  };

  /**
   * Buscar a lista de disciplinas em que o aluno está apto para realizar o agendamento
   */
  buscarDisciplinasApto = () => {
    this.loading = true;

    return this.AgendamentoService.getDisciplinasApto(this.idMatricula).then(
      (response) => {
        this.loading = false;
        this.disciplinas = response;
      },
      () => {
        this.loading = false;
      }
    );
  };

  buscarAvaliacoes = () => {
    this.agendamento.id_avaliacao = null;
    this.agendamento.id_avaliacaoaplicacao = null;

    let idDisciplina = null;

    if (this.isProvaPorDisciplina && this.disciplina) {
      idDisciplina = this.disciplina.id_disciplina;
    }

    this.loading = true;

    return this.AgendamentoService.getAvaliacoes(this.idMatricula, idDisciplina).then(
      (response) => {
        this.loading = false;
        this.avaliacoes = response.map((item) => {
          item.ultimo_agendamento = item.agendamentos[item.agendamentos.length - 1];

          item.agendamento_ativo = item.agendamentos.filter((item) => {
            return item.id_avaliacaoagendamento;
          }).pop();

          if (!item.agendamento_ativo) {
            item.agendamento_ativo = item.ultimo_agendamento;
          }

          return item;
        });
      },
      (error) => {
        this.loading = false;

        let type = 'error';
        let message = error.data.message;

        if (error.status === 404) {
          type = 'warning';
          message = 'Nenhuma avaliação disponível para agendamento.';
        }

        this.NotifyService.notify(type, '', message);
      }
    );
  };

  buscarAvaliacaoAplicacoes = (idAvaliacao) => {
    let sgUf = null;
    let idMunicipio = null;

    // Resetar o array
    this.aplicacoes = [];

    if (!idAvaliacao) {
      // Desabilitar o click na etapa
      this.etapas.agendamento = false;

      this.alterarEtapa('avaliacao');
    }

    if (!idAvaliacao) {
      return false;
    }

    if (!this.agendamento.bl_provaonline) {
      sgUf = this.endereco.sg_uf;
      idMunicipio = this.endereco.id_municipio;

      // Quando a prova não for ONLINE, é necessário informar o local
      if (!sgUf || !idMunicipio) {
        return false;
      }
    }

    this.loading = true;

    return this.AgendamentoService.getAvaliacaoAplicacoes(this.idMatricula, idAvaliacao, sgUf, idMunicipio).then(
      (response) => {
        this.loading = false;
        this.aplicacoes = response;
      },
      (error) => {
        this.aplicacoes = [];
        this.loading = false;

        const type = error.status === 404 ? 'warning' : 'error';

        this.NotifyService.notify(type, '', error.data.message);
      }
    );
  };

  /**
   * Buscar o histórico de agendamento do aluno
   */
  buscarAgendamentos = () => {
    this.agendamentos = [];

    return this.AgendamentoService.getAll({
      id_matricula: this.idMatricula
    }).then((response) => {
      this.agendamentos = response;
    });
  };

  /**
   * Buscar a lista de países
   */
  buscarPaises = () => {
    return this.EnderecoService.getPaises().then(
      (response) => {
        this.paises = response;
      },
      (error) => {
        this.NotifyService.notify('error', '', error.data.message);
      }
    );
  };

  /**
   * Buscar a lista de ufs
   */
  buscarUfs = () => {
    return this.EnderecoService.getUfs().then(
      (response) => {
        this.ufs = response;
      },
      (error) => {
        this.NotifyService.notify('error', '', error.data.message);
      }
    );
  };

  /**
   * Buscar a lista de municípios baseado no uf
   * @param sgUf
   */
  buscarMunicipios = (sgUf) => {
    if (sgUf) {
      return this.EnderecoService.getMunicipios(sgUf).then(
        (response) => {
          this.municipios = response;
        },
        (error) => {
          this.NotifyService.notify('error', '', error.data.message);
        }
      );
    }
  };

  /**
   * Buscar os dados de endereço baseado no CEP
   */
  buscarEnderecoCep = () => {
    if (this.endereco.st_cep && this.endereco.st_cep.replace(/\D/g).length === 8) {
      this.loading = true;
      this.EnderecoService.getByCep(this.endereco.st_cep).then(
        (response) => {
          response.id_pais = response.id_pais || this.PaisConstante.BRASIL;
          angular.extend(this.endereco, response);
          this.buscarMunicipios(this.endereco.sg_uf);
        },
        () => {
          this.NotifyService.notify('warning', '', 'CEP inválido, por favor tente novamente.');
        }
      ).finally(() => {
        this.loading = false;
      });
    }
  };

  /**
   * Abre em uma nova janela o comprovante de agendamento realizado
   * @param idAvaliacaoAgendamento
   */
  abrirComprovanteAgendamento = (idAvaliacaoAgendamento) => {
    this.$window.open(this.CONFIG.urlG2 + 'default/textos/comprovante-agendamento?id_usuario=' + this.$localStorage.user_id + '&id_avaliacaoagendamento=' + idAvaliacaoAgendamento, '_blank');
  };

  toggleBotaoImprimirComprovante = (avaliacao) => {
    // Se for prova online, exibir o comprovante independente das datas porque a prova online não possui reagendamento
    if (avaliacao.bl_provaonline) {
      return true;
    }

    // Para GRA, o comprovante só deve ser exibido se o prazo de alteração do agendamento não estiver mais disponível
    if (+avaliacao.id_linhadenegocio === 2) {
      return !avaliacao.ultimo_agendamento.bl_dataativa;
    }

    return true;
  };

  /**
   * Define se o botão "Agendar" deve ser mostrado
   * @returns {boolean}
   */
  toggleBotaoAgendamento = (avaliacao) => {
    // A situação não pode ser AGENDADO ou REAGENDADO
    if (+avaliacao.ultimo_agendamento.id_situacao === this.SituacaoConstante.AGENDAMENTO_AGENDADO
      || +avaliacao.ultimo_agendamento.id_situacao === this.SituacaoConstante.AGENDAMENTO_REAGENDADO) {
      return false;
    }

    return true;
  };

  /**
   * Define se o botão "Reagendar" deve ser mostrado
   * @returns {boolean}
   */
  toggleBotaoReagendamento = (avaliacao) => {
    // Não pode ser prova do tipo ONLINE
    if (avaliacao.bl_provaonline) {
      return false;
    }

    // Não pode ter presença lançada
    if (avaliacao.ultimo_agendamento.nu_presenca !== null) {
      return false;
    }

    // A situação precisa ser AGENDADO ou REAGENDADO
    if (+avaliacao.ultimo_agendamento.id_situacao !== this.SituacaoConstante.AGENDAMENTO_AGENDADO
      && +avaliacao.ultimo_agendamento.id_situacao !== this.SituacaoConstante.AGENDAMENTO_REAGENDADO) {
      return false;
    }

    // É necessário que o agendamento esteja em uma data corrente
    if (!avaliacao.ultimo_agendamento.bl_dataativa) {
      return false;
    }

    return true;
  };

  toggleBotaoRealizarProva = (avaliacao) => {
    // Precisa ser PROVA ONLINE
    if (!avaliacao.bl_provaonline) {
      return false;
    }

    // É necessário possuir integração com a Maximize
    if (!avaliacao.st_linkexternoavaliacao) {
      return false;
    }

    // A situação precisa ser AGENDADO ou REAGENDADO
    if (+avaliacao.ultimo_agendamento.id_situacao !== this.SituacaoConstante.AGENDAMENTO_AGENDADO
      && +avaliacao.ultimo_agendamento.id_situacao !== this.SituacaoConstante.AGENDAMENTO_REAGENDADO) {
      return false;
    }

    // Só deve ser exibido se o período de aplicação de prova já iniciou
    const dtInicio = avaliacao.ultimo_agendamento.dt_aplicacaoinicio.split('/').reverse().join('');
    const dtAtual = (new Date()).toISOString().substr(0, 10).split('-').join('');
    if (dtAtual < dtInicio) {
      return false;
    }

    return true;
  };

  alterarDataCalendario = (incMonth = 0, incYear = 0) => {
    const day = 1;
    const month = Number(this.calendario.date.getMonth()) + Number(incMonth);
    const year = Number(this.calendario.date.getFullYear()) + Number(incYear);

    this.calendario.date = new Date(year, month, day);

    this.calendario.events = [];

    this.agendamentos.forEach((agendamento) => {
      this.adicionarEvento(agendamento);
    });
  };

  getAgendamentoDisciplinas = (agendamento, isProvaGlobal = false) => {
    if (isProvaGlobal) {

      return this.$sce.trustAsHtml('<b class="text-info">Prova Global</b>');
    }

    const str = agendamento.disciplinas.map((disciplina) => {
      return '● ' + disciplina.st_disciplina;
    }).join('<br/>');

    return this.$sce.trustAsHtml(str) || '-';
  };

  getAgendamentoLocal = (agendamento, isProvaOnline = false) => {
    if (isProvaOnline) {
      return this.$sce.trustAsHtml('<b class="text-info">Prova Online</b>');
    }

    let str = '';

    if (agendamento.st_endereco && agendamento.st_cidade) {
      str += '<b>Endereço:</b> ' + agendamento.st_endereco + '<br/>' +
        '<b>Cidade:</b> ' + agendamento.st_cidade + (agendamento.sg_uf ? ' - ' + agendamento.sg_uf : '');
    }

    if (agendamento.st_aplicadorprova) {
      str += '<br/><b>Aplicador:</b> ' + agendamento.st_aplicadorprova;
    }

    return this.$sce.trustAsHtml(str) || '-';
  };

  getAgendamentoData = (agendamento, isProvaOnline = false) => {
    let str = '';

    if (isProvaOnline) {
      str += 'de ' + (agendamento.dt_aplicacaoinicio || '').split('-').reverse().join('/');

      if (agendamento.dt_aplicacao) {
        str += '<br/>até ';
      }
    }

    str += (agendamento.dt_aplicacao || '').split('-').reverse().join('/');

    if (!isProvaOnline) {
      if (agendamento.hr_inicio) {
        str += '<br/>' + agendamento.hr_inicio;

        if (agendamento.hr_fim) {
          str += ' as ' + agendamento.hr_fim;
        }
      }
    }

    return this.$sce.trustAsHtml(str) || '-';
  };

  getStringAvaliacaoMaximize = (dtFim) => {
    dtFim = (dtFim || '').split('/').reverse().join('');
    const dtAtual = (new Date()).toISOString().substr(0, 10).split('-').join('');

    return dtAtual > dtFim ? 'Visualizar Prova' : 'Realizar Prova';
  };

  abrirAvaliacaoMaximize = (urlAvaliacao) => {
    const $win = this.$window.open(
      this.CONFIG.urlG2 + 'default/redirecionar/autenticar-maximize?id_usuario=' + this.$localStorage.user_id
    );

    if (!$win) {
      this.NotifyService.notify('warning', '', 'O carregamento da avaliação em uma nova aba foi bloqueada pelo navegador.');
    }

    setTimeout(() => {
      $win.location.href = urlAvaliacao;
    }, 3000);
  };

  /**
   * Buscar a lista de ufs
   */
  buscarUfsAgendamento = () => {
    if (+this.$localStorage.user_entidade === this.EntidadeConstante.FACULDADE_UNYLEYA) {
      return this.AgendamentoService.getUfMunicipio().then(
        (response) => {
          this.ufsAgendamento = response;
        },
        (error) => {
          this.NotifyService.notify('error', '', error.data.message);
        }
      );
    }
    this.ufsAgendamento = this.ufs;
  };

  buscarMunicipiosAgendamento = (sg_uf) => {
    if (+this.$localStorage.user_entidade === this.EntidadeConstante.FACULDADE_UNYLEYA) {
      return this.AgendamentoService.getUfMunicipio(sg_uf).then(
        (response) => {
          this.municipios = response;
        },
        (error) => {
          this.NotifyService.notify('error', '', error.data.message);
        }
      );
    }
    this.buscarMunicipios(sg_uf);
  }

}

export default PwAgendamentoFormController;
