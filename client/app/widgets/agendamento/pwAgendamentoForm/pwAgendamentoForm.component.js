import template from './pwAgendamentoForm.html';
import controller from './pwAgendamentoForm.controller';
import './pwAgendamentoForm.scss';

const pwAgendamentoFormComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pwAgendamentoFormComponent;
