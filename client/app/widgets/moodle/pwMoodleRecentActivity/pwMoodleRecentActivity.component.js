import template from './pwMoodleRecentActivity.html';
import controller from './pwMoodleRecentActivity.controller';
import './pwMoodleRecentActivity.scss';

const pwMoodleRecentActivityComponent = {
  restrict: 'E',
  bindings: {
    idMatricula: '<',
    idSaladeaula: '<',
    idCurso: '<'
  },
  template,
  controller,
  controllerAs: 'pwra'
};

export default pwMoodleRecentActivityComponent;
