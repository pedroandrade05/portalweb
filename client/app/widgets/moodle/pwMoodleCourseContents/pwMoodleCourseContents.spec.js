import PwMoodleCourseContentsModule from './pwMoodleCourseContents';
import PwMoodleCourseContentsController from './pwMoodleCourseContents.controller';
import PwMoodleCourseContentsComponent from './pwMoodleCourseContents.component';
import PwMoodleCourseContentsTemplate from './pwMoodleCourseContents.html';

describe('PwMoodleCourseContents', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwMoodleCourseContentsModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwMoodleCourseContentsController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PwMoodleCourseContentsComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PwMoodleCourseContentsTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwMoodleCourseContentsController);
    });
  });
});
