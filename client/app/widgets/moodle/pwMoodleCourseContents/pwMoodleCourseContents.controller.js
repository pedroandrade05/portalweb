import loadingGif from "../../../../assets/img/loading.gif";
import moment from "moment";

class PwMoodleCourseContentsController {

  constructor(MoodleService, EsquemaConfiguracaoService, UnyleyaMoodleService, $state, NotifyService, $q,
    /* Constante */ TipoSalaDeAulaConstante) {
    'ngInject';
    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao($state.params.idMatriculaAtiva);
    this.isPosGraduacao = EsquemaConfiguracaoService.isPosGraduacao($state.params.idMatriculaAtiva);
    this.UnyleyaMoodleService = UnyleyaMoodleService;
    this.$q = $q;
    this.name = 'pwMoodleCourseContents';
    this.MoodleService = MoodleService;
    this.$state = $state;
    this.toaster = NotifyService;

    /* Constante */
    this.TipoSalaDeAulaConstante = TipoSalaDeAulaConstante;

    //inicia a variavel de loading
    this.loading = false;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;

    this.sala = this.sala || $state.params.sala;

    this.salaIndisponivel = "A disciplina não está disponível. Favor entrar em contato através do Serviço de Atenção para maiores informações.";
    this.tokenInvalido = "Falha na autenticação, tente novamente mais tarde. Se o problema persistir, favor entrar em contato através do Serviço de Atenção para maiores informações.";

    /**
     * Os parâmetros idMatricula e idCurso são passados via parâmetro para o widget, o idMatricula corresponde à Matrícula do Aluno no G2,
     * já o idCurso corresponde ao courseId do Moodle, pode ser pego na tb_saladeaulaintegracao campo st_codsistemacurso
     */
    if (this.idMatricula && this.idCurso && this.idSaladeaula) {

      /* Garante que a função que retorna a classe (colorida) e o texto do
      tooltip só será chamada quando as promises this.ActivitiesComplet() e
      this.CourseContents() já tenham retornado seus resultados.
      */
      let promises = null;

      if (this.isGraduacao && !this.isEncerrado()) {
        promises = [
          this.ActivitiesComplet(this.idMatricula, this.idCurso, this.idSaladeaula),
          this.CourseGetContents(this.idMatricula, this.idCurso, this.idSaladeaula)
        ];
      }
      else {
        promises = [
          this.ActivitiesComplet(this.idMatricula, this.idCurso, this.idSaladeaula),
          this.CourseContents(this.idMatricula, this.idCurso, this.idSaladeaula)
        ];
      }

      $q.all(promises).then(() => {
        this.getCompletionStatus();
      }, (error) => {
        this.errormessage = this.salaIndisponivel;
        if (error.errorcode && error.errorcode === 'invalidtoken') {
          this.errormessage = this.tokenInvalido;
        }
      }).finally(() => {
        this.loading = false;
      });

    }

    this.contents = [];
    this.statusActivities = [];
    this.statusCourse = [];
    this.errormessage = '';
    this.nu_diasbloqueio = 3;

  }

  /**
   * Retorna os status das atividades do aluno no moodle
   * @param idMatricula
   * @param idCurso
   * @param idSaladeaula
   * @constructor
   */
  ActivitiesComplet = (idMatricula, idCurso, idSaladeaula) => {
    return this.MoodleService.getActivitiesCompletionStatus(idMatricula, idCurso, idSaladeaula).then((status) => {
      this.statusActivities = status;
    });
  }

  /**
   * Retorna o conteúdo do curso do Moodle
   * @param idMatricula
   * @param idCurso
   * @param idSaladeaula
   * @constructor
   */
  CourseContents = (idMatricula, idCurso, idSaladeaula) => {
    this.loading = true;
    return this.MoodleService.getCourseContents(idMatricula, idCurso, idSaladeaula).then((contents) => {

      // Define qual conteudo e módulo estão selecionados
      if (this.$state.params.idModulo) {
        contents.forEach((content) => {
          const moduloAtivo = content.modules.find((module) => {
            return Number(module.id) === Number(this.$state.params.idModulo);
          });

          if (moduloAtivo) {
            moduloAtivo.selected = true;
            content.selected = true;
          }
        });
      }

      this.contents = contents;
      this.errormessage = '';
    }, (error) => {
      this.errormessage = this.salaIndisponivel;
      if (error.errorcode && error.errorcode === 'invalidtoken') {
        this.errormessage = this.tokenInvalido;
      }
    });
  }

  CourseGetContents = (idMatricula, idCurso, idSaladeaula) => {
    this.loading = true;

    return this.UnyleyaMoodleService.courseGetContents(idMatricula, idCurso, idSaladeaula).then((contents) => {

      // Define qual conteudo e módulo estão selecionados
      if (this.$state.params.idModulo) {
        contents.forEach((content) => {
          const moduloAtivo = content.modules.find((module) => {
            return Number(module.id) === Number(this.$state.params.idModulo);
          });

          if (moduloAtivo) {
            moduloAtivo.selected = true;
            content.selected = true;
          }
        });
      }

      this.contents = contents;
      this.errormessage = '';
    }, (error) => {
      this.errormessage = this.salaIndisponivel;
      if (error.errorcode && error.errorcode === 'invalidtoken') {
        this.errormessage = this.tokenInvalido;
      }
    });
  };

  /**
   * Retorna o status do curso se no Moodle tiver sido definido algum critétio para isso
   * @param idMatricula
   * @param idCurso
   * @param idSaladeaula
   * @constructor
   */
  CourseCompletionStatus = (idMatricula, idCurso, idSaladeaula) => {
    this.MoodleService.getCourseCompletionStatus(idMatricula, idCurso, idSaladeaula).then((contents) => {
      this.contentsStatus = contents;
    }, () => {

      // falta definir o que mostrar ao ter alguma mensagem de erro
    });
  }

  isEncerrado() {
    return [2, 4].includes(+this.statusSala);
  }

  isAtividadeBloqueada(dtEncerramento) {
    return this.isEncerrado() && moment().diff(
      moment(dtEncerramento, 'YYYY-MM-DD'),
      'days'
    ) <= this.nu_diasbloqueio;
  }

  isProvaFinalBloqueada() {
    return (+this.sala.nu_diasliberarprovafinal) &&
      (moment().diff(moment(this.sala.dt_encerramento, 'YYYY-MM-DD'),
        'days'
      ) <= (-this.sala.nu_diasliberarprovafinal));
  }

  getTempoDesbloqueio() {
    return (24 * this.nu_diasbloqueio) + 24 - moment().diff(
      moment(this.sala.dt_encerramento, 'YYYY-MM-DD'),
      'hours'
    );
  }

  /**
   * Retorna o icone para as atividade co moodle
   * @param  {string} modname Tipo de atividade do moodle
   * @return {string}         Class do icone
   */
  getModuleIcon = (module) => {
    const {modname} = module;

    // Labels não devem exibir ícone
    if (modname === 'label') {
      return null;
    }

    const iconName = {
      forum: 'icon-icon_forum',
      resource: 'icon-icon_livro',
      folder: 'icon-icon_livro',
      quiz: 'icon-icon_disc',
      assign: 'icon-icon_disc'
    }[modname] || 'icon-icon_aula';

    return iconName;
  };

  /**
   * Redireciona a disciplina para sua tela main
   * @param objDisciplina
   */
  redirecionaDisciplina = () => {
    try {
      this.$state.go("app.disciplina", {
        idMatriculaAtiva: this.idMatricula,
        idSalaDeAula: this.idSaladeaula
      });
    }
    catch (e) {
      this.toaster.notify('error', 'Ops', 'Erro ao tentar redirecionar para tela de disciplina.');
    }
  }

  /* Adiciona no objeto content as propriedades
  moduleStatusCompletionClass (módulo), activityStatusCompletionClass e
  activityStatusCompletionText (ativiades). Utilizado para definir a classe
  com cor indicando o progresso e o texto exibido no tooltip.
  */
  getCompletionStatus = () => {
    const {contents} = this;

    contents.forEach((topico) => {
      // Definir status de completo para cada módulo (atividade da disciplina)
      if (this.statusActivities && this.statusActivities.statuses) {
        topico.modules.forEach((module) => {
          module.progress = false;

          this.statusActivities.statuses.forEach((atividade) => {
            if (+atividade.cmid === +module.id) {
              module.progress = true;
              module.visible = true;
              module.completed = atividade.timecompleted > 0;
            }
          });

          if (module.availability && module.modname !== 'lti') {
            module.progress = true;
          }
        });
      }

      // Definir status e quantidade de itens completos para cada tópico
      topico.totalProgress = topico.modules.filter((module) => module.progress).length;
      topico.totalCompleted = topico.modules.filter((module) => module.progress && module.completed).length;
      topico.totalPending = topico.modules.filter((module) => module.progress && !module.completed).length;
    });

    //Módulo
    {
      angular.forEach(this.contents, (contentValue) => {
        if (contentValue.modules.length > 0) {
          angular.forEach(contentValue.modules, (modulo) => {
            angular.forEach(this.statusActivities.statuses, (status) => {
              if ((status.cmid === modulo.id) && status.timecompleted === 0) {
                contentValue.moduleStatusCompletionClass = 'curso-incompleto';
              }
            });
          });
        }
        else {
          contentValue.moduleStatusCompletionClass = 'curso-indisponivel';
        }
      });
    }

    //Atividades
    {
      let preRequisito = null;
      const isBloqueado = this.isAtividadeBloqueada(this.sala.dt_encerramento);
      const nuTempoDesbloqueio = isBloqueado ? this.getTempoDesbloqueio() : 0;

      angular.forEach(this.contents, (modulo) => {
        modulo.contabilizado = true;
        angular.forEach(modulo.modules, (atividade) => {
          let temAcompanhamento = false;
          atividade.warningMessage = 'Este item ainda não está disponível.';

          if (this.isGraduacao) {

            if (atividade.modname === 'lti') {
              if (atividade.progress) {
                preRequisito = atividade.name;
              }
              else {
                modulo.totalProgress++;
                atividade.progress = true;
                atividade.warningMessage = 'Conclua a ' + preRequisito + ' para acessar este ítem.';
                preRequisito = atividade.name;
              }
            }

            if (atividade.name.match(/prova/i) && (atividade.name.match(/final/i) || atividade.name.match(/recuper/i))) {
              if (+this.sala.id_tiposaladeaula === this.TipoSalaDeAulaConstante.PERENE && this.isProvaFinalBloqueada()) {
                atividade.visible = false;
              }

              atividade.visible ? modulo.contabilizado = false : modulo.visible = false;
            }

          }
          if (this.isPosGraduacao) {
            if (['lti', 'assign', 'quiz', 'forum'].includes(atividade.modname) && isBloqueado) {
              atividade.visible = false;
              atividade.waitNotify = 0;
              atividade.warningMessage =
                'Chegou a data de encerramento desta disciplina e as notas de suas atividades estão sendo processadas. ' +
                `Em aproximadamente ${nuTempoDesbloqueio}h você poderá consultá-las novamente.`;
            }
          }

          angular.forEach(this.statusActivities.statuses, (status) => {
            if (status.cmid === atividade.id) {
              temAcompanhamento = true;
              if (status.timecompleted) {
                atividade.activityStatusCompletionClass = 'atividade-completa';
                atividade.activityStatusCompletionText = 'Atividade Concluída';
              }
              else {
                atividade.activityStatusCompletionClass = 'atividade-incompleta';
                atividade.activityStatusCompletionText = 'Atividade Pendente';
              }
            }
          });
          if (!temAcompanhamento) {
            atividade.activityStatusCompletionClass = 'atividade-sem-acompanhamento';
            atividade.activityStatusCompletionText = 'Atividade não acompanhada';
          }
        });
        if (modulo.totalCompleted > 0 && modulo.totalCompleted === modulo.totalProgress) {
          modulo.moduleStatusCompletionClass = 'curso-completo';
        }
      });
    }
  }

}

export default PwMoodleCourseContentsController;
