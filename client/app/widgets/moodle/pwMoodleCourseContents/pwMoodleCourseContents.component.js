import template from './pwMoodleCourseContents.html';
import controller from './pwMoodleCourseContents.controller';
import './pwMoodleCourseContents.scss';

const pwMoodleCourseContentsComponent = {
  restrict: 'E',

  /**
   * Os parâmetros idMatricula e idCurso são passados via parâmetro para o widget, o idMatricula corresponde à Matrícula do Aluno no G2,
   * já o idCurso corresponde ao courseId do Moodle, pode ser pego na tb_saladeaulaintegracao campo st_codsistemacurso
   */
  bindings: {
    idMatricula: '=',
    idSaladeaula: '=',
    idCurso: '=',
    showVoltar: '=',
    statusSala: '=',
    sala: '='
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pwMoodleCourseContentsComponent;
