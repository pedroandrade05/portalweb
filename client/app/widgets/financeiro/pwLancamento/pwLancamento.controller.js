class PwCursoController {
  constructor(/* Úteis */$http, $state, CONFIG, $window,
    /* Services */ FinanceiroService, ModalService, NotifyService,
    /* Constantes */ MeioPagamentoConstante, OrigemBoleto,StatusLancamentoConstante) {
    'ngInject';
    this.name = 'pwLancamento';

    /* Úteis */
    this.state = $state;
    this.CONFIG = CONFIG;
    this.$http = $http;
    this.$window = $window;

    /* Services */
    this.ModalService = ModalService;
    this.FinanceiroService = FinanceiroService;
    this.toaster = NotifyService;

    /* Constantes */
    this.MeioPagamentoConstante = MeioPagamentoConstante;
    this.OrigemBoleto = OrigemBoleto;
    this.StatusLancamentoConstante = StatusLancamentoConstante;

    this.bl_taxa = +this.bltaxa;
    this.showMsgPopup = null;
    this.labelBtGerarBoleto = 'Gerar Boleto';

  }

  /**
   * Retorna os dados do boleto
   * @param id_lancamento
   */
  getDadosLancamento = (id_lancamento) => {
    this.loading = true;
    this.labelBtGerarBoleto = 'Gerar Boleto';
    this.FinanceiroService.getOneById(id_lancamento).then((response) => {
      this.lancamento = response;

      //Defini nome exibido no produto contratado
      let st_produto = "";
      this.lancamento.produtos.map((produto) => {

        let st_textoNomeCurso;
        st_textoNomeCurso = produto.st_tituloexibicao;
        if (produto.st_tituloexibicao === null || produto.st_tituloexibicao === "") {
          st_textoNomeCurso = produto.st_produto;
        }

        st_produto = st_produto !== "" ? st_produto + ", " + st_textoNomeCurso : st_textoNomeCurso;

      });
      this.lancamento.st_produto = st_produto;

      //verifica se o meio de pagamento é boleto.
      if (this.lancamento.id_meiopagamento === this.MeioPagamentoConstante.BOLETO) {
        this.boleto = response;
        if (this.boleto.st_codigobarras && this.boleto.st_situacao === 'Pendente') {
          this.labelBtGerarBoleto = 'Emitir Boleto';
        }

      }
    }, (error) => {
      this.toaster.notify(error.data);
    }).finally(() => {
      this.loading = false;
    });
  };

  emitirDadosBoleto = (id_lancamento, funcao = 'getbarcode', id_venda) => {

    this.ModalService.show({
      title: 'Emissão de Boleto',
      body: 'Deseja realmente solicitar o boleto?',
      confirmButton: 'Confirmar',
      cancelButton: 'Cancelar'
    }).then((result) => {
      if (result) {
        this.getDadosBoleto(id_lancamento, funcao, id_venda);
      }
    });

  }

  /**
   * Retorna os dados do lançamento solicitado dia id_lançamento
   * @param id_lancamento
   * @param funcao
   */
  getDadosBoleto = (id_lancamento, funcao = 'getbarcode', id_venda, primeiraVez = true) => {

    this.loading = true;
    this.showMsgPopup = null;

    if (funcao !== 'emitirboleto') {

      this.FinanceiroService.getDadosBoleto(id_lancamento, funcao).then((response) => {
        this.boleto = response;
      }, (error) => {
        this.toaster.notify(error.data);
      }).finally(() => {
        this.loading = false;
      });

    }
    else {

      this.FinanceiroService.getUrlBoleto(id_lancamento, id_venda).then((response) => {

        if (!response.st_codigobarras && primeiraVez) {
          setTimeout(() => {
            this.getDadosBoleto(id_lancamento, funcao, false);
          }, 7000);
        }
        else {

          this.ModalService.show({
            title: 'Emissão de Boleto',
            body: response.st_mensagem,
            confirmButton: 'Entendi',
            cancelButton: false
          });

          if (response.st_urlboleto) {
            const popupWin = this.$window.open(`${this.CONFIG.urlG2}/loja/recebimento/?venda=${id_venda}&lancamento=${id_lancamento}&id_origem=` + this.OrigemBoleto.NOVO_PORTAL, '_blank', 'width=700,height=600');
            if (!popupWin) {
              this.showMsgPopup = 'Você precisa liberar os popups para emitir boletos';
            }
          }

          this.getDadosLancamento(id_lancamento);

        }

      }, () => {
        this.loading = false;
        this.toaster.notify('error', '', 'Não foi possível emitir o boleto!');
      }).finally(() => {
        if (!primeiraVez) {
          this.loading = false;
        }
      });
    }
  }

  /**
   * Efetua o download do recibo no formato PDF
   * @param lancamento
   */
  getRecibo = (lancamento) => {
    if (!lancamento.st_link) {
      this.toaster.notify('error', '', 'Recibo indisponível');
    }
    else {
      const popupWin = this.$window.open(this.CONFIG.urlG2 + lancamento.st_link.replace('=html', '=pdf'));
      if (!popupWin) {
        this.showMsgPopup = 'Você precisa liberar os popups para emitir boletos';
      }
    }
  }

  /**
   * Efetua o download do NFe
   * @param lancamento
   */
  getNfe = (lancamento) => {
    if (!lancamento.st_nfe) {
      this.toaster.notify('error', '', 'Nfe Não disponivel');
    }
    else {
      const popupWin = this.$window.open(lancamento.st_nfe);
      if (!popupWin) {
        this.showMsgPopup = 'Você precisa liberar os popups';
      }
    }
  }
}

export default PwCursoController;
