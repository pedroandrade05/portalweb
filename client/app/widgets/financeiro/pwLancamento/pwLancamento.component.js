import template from './pwLancamento.html';
import controller from './pwLancamento.controller.js';
import './pwLancamento.scss';

const pwLancamentoComponent = {
  restrict: 'E',
  bindings: {
    lancamento: "<",
    bltaxa: "<"
  },
  template,
  controller,
  controllerAs: 'pw'
};

export default pwLancamentoComponent;
