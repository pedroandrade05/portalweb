import pwLancamentoModule from './pwLancamento';
import pwLancamentoController from './pwLancamento.controller.js';
import pwLancamentoComponent from './pwLancamento.component.js';
import pwLancamentoTemplate from './pwLancamento.html';

describe('pwLancamento', () => {
  let $rootScope, makeController;

  beforeEach(window.module(pwLancamentoModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new pwLancamentoController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = pwLancamentoComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(pwLancamentoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(pwLancamentoController);
    });
  });
});
