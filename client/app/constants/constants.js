export default angular.module('app.portalconstante', [])

  .constant('EsquemaConfiguracaoConstante', {
    GRADUACAO: 1,
    POS_GRADUACAO: 2,
    CONCURSO: 3,
    TDC: 4,
    PADRAO: 5,
    POS_PRESENCIAL: 6,
    POS_GRADUACAO_UCAM: 7,
    CORPORATIVO: 8,
    ESCOLA_TECNICA_SEMESTRAL: 9,
    ESCOLA_TECNICA_PADRAO_POS: 10,
    GRADUACAO_ENFERMAGEM: 11,
    EXTENSAO: 12,
    CURSOS_EXTENSAO: 13
  })

  //constants da tb_tipoendereco
  .constant('TipoEnderecoConstante', {
    CORRESPONDENCIA: 5,
    RESIDENCIAL: 1,
    COMERCIAL: 2,
    MATRIZ: 3,
    FILIAL: 4,
    APLICADOR_DE_PROVA: 6
  })
  .constant('MeioPagamentoConstante', {
    CARTAO_CREDITO: 1,
    BOLETO: 2,
    PIX: 17,
    CARTAO_RECORRENTE: 11
  })
  .constant('SituacaoConstante', {
    AGENDAMENTO_AGENDADO: 68,
    AGENDAMENTO_REAGENDADO: 69,
    AGENDAMENTO_CANCELADO: 70,
    AGENDAMENTO_LIBERADO: 129,
    AGENDAMENTO_ABONADO: 130,
    APROVEITAMENTO_DISCIPLINA: 65,

    APLICACAO_PROVA_ONLINE_AGUARDANDO_SINCRONIZACAO: 232,
    APLICACAO_PROVA_ONLINE_AGUARDANDO_ALUNO: 233,
    APLICACAO_PROVA_ONLINE_ALUNO_AUSENTE: 234,
    APLICACAO_PROVA_ONLINE_NOTA_LANCADA: 235,

    AFILIADO_INDICACAO_PENDENTE: 239,
    AFILIADO_INDICACAO_CONFIRMADO: 240,
    AFILIADO_INDICACAO_RECUSADO: 241,

    AFILIADO_INDICACAO_CUPOM_DISPONIVEL: 242,
    AFILIADO_INDICACAO_CUPOM_RESGATADO_RENOVACAO: 243,
    AFILIADO_INDICACAO_CUPOM_RESGATADO_VALE: 244
  })
  .constant('EvolucaoDisciplinaConstante', {
    NAO_ALOCADO: 11,
    APROVADO: 12,
    CURSANDO: 13,
    INSATISFATORIO: 19,
    TRANCADO: 70
  })
  .constant('StatusLancamentoConstante', {
    PAGO: 1,
    APAGAR: 2,
    ATRASADO: 3
  })
  .constant('MensagemPadraoConstante', {
    CURSOS_LIVRES_INFORMACOES: 87,
    CURSOS_LIVRES_DUVIDAS_FREQUENTES: 88,
    BIBLIOTECAS_VIRTUAIS: 95,
    NUCLEO_PRATICAS_JURIDICAS_INFORMACOES: 96
  })
  .constant('MensagemPadrao', {
    ARQUIVO_FORA_DO_FORMATO_PERMITIDO: ', você só pode fazer upload de arquivos nos formatos JPEG, PNG, PDF, DOC, DOCX, XLS e XLSX.',
    ARQUIVO_FORA_DO_FORMATO_PERMITIDO_IMAGEM: ', você só pode fazer upload de arquivos nos formatos JPG, JPEG, PNG.',
    ARQUIVO_FORA_DO_FORMATO_PERMITIDO_IMAGEM_E_PDF: ', você só pode fazer upload de arquivos nos formatos JPG, JPEG, PNG ou PDF.',
    ARQUIVO_TAMANHO: ', você só pode fazer upload de arquivos com tamanho inferior a 15MB.',
    ARQUIVO_TAMANHO_5MB: ', você só pode fazer upload de arquivos com tamanho inferior a 5MB.',
    ARQUIVO_TAMANHO_IMAGEM_500KB: ' Você só pode fazer upload de arquivos com tamanho inferior a 500KB.',
    OCORRENCIA_CADASTRADA_SUCESSO: ', sua ocorrência foi cadastrada com sucesso.',
    INTERACAO_CADASTRADA_SUCESSO: ', sua interação foi cadastrada com sucesso.5',
    OCORRENCIA_ENCERRADA_SUCESSO: ', sua ocorrência foi encerrada com sucesso.',
    REABRIR_OCORRENCIA_SUCESSO: ', sua ocorrência foi reaberta com sucesso.',
    ACEITE_CONTRATO_DISCIPLINAS: ', você precisa aceitar o contrato para ter acesso às disciplinas.',
    USUARIO_INCORRETO: 'Usuário, e-mail ou senha incorretos. Por favor, verifique as informações e preencha os campos novamente.',
    SENHA_ALTERADA_SUCESSO: ', sua senha foi alterada com sucesso!',
    RECUPERA_SENHA_SUCESSO: 'Um e-mail com instruções para a recuperação da sua senha foi enviado com sucesso.',
    ERRO_REQUISICAO: 'Houve algum problema na requisição, favor tentar mais tarde!',
    DADOS_CADASTRAIS_SUCESSO: 'Dados alterados com sucesso.',
    ALERTA_ACESSAR_COMO: 'As ações realizadas a partir da funcionalidade "ACESSAR COMO" são contabilizadas no portal como ações realizadas pelo próprio aluno!'
  })

  //Constantes de evolução para tb_matricula.
  .constant('EvolucaoMatriculaConstante', {
    AGUARDANDO_CONFIRMACAO: 5,
    CURSANDO: 6,
    CERTIFICADO: 16,
    CONCLUINTE: 15,
    TRANSFERIDO: 20,
    TRANCADO: 21,
    SEM_RENOVACAO: 25,
    RETORNADO: 26,
    CANCELADO: 27,
    BLOQUEADO: 40,
    ANULADO: 41,
    TRANSFERIDO_DE_ENTIDADE: 47,
    DECURSO_DE_PRAZO: 69,
    FORMADO: 81,
    DIPLOMADO: 82,
    EGRESSO: 89
  })

  .constant('PaisConstante', {
    BRASIL: 22
  })

  .constant('MensagensInfoEvolucaoConstante', {
    PERFIL_DESATIVADO: 'Sua matrícula não está mais ativa no portal do aluno.',
    ANULADO: 'Não existe matrícula ativa no portal do aluno.',
    BLOQUEADO: 'Seu acesso ao curso está temporariamente suspenso. Para saber mais, acesse o Serviço de Atenção.',
    TRANCADO: 'Sua matrícula está trancada. Para retornar seus estudos acesse o Serviço de Atenção.',
    CERTIFICADO_APOS_30_DIAS: 'Seu perfil não está mais ativo no Portal do Aluno porque você já concluiu seu curso há' +
      ' mais de 30 dias e seu certificado já foi emitido. Parabéns pela sua conquista e aproveite para consultar as ' +
      'condições especiais para fazer outro curso.'
  })

  .constant('SituacaoTCCConstante', {
    PENDENTE: 203,
    EM_ANALISE: 204,
    DEVOLVIDO_AO_ALUNO: 205,
    AVALIADO_PELO_ORIENTADOR: 206,
    DEVOLVIDO_AO_TUTOR: 207
  })

  .constant('OrigemBoleto', {
    PORTAL: 1,
    NOVO_PORTAL: 2,
    APLICATIVO: 3,
    TEXTO_SISTEMA: 4,
    TEXTO_SISTEMA_COBRANCA: 5
  })

  .constant('TbSistema', {
    PortalAluno: 4,
    SistemaFaculdade: 96
  })

  // tb_tipoavaliacao
  .constant('TipoAvaliacaoConstante', {
    ATIVIDADE_PRESENCIAL: 1,
    AVALIACAO_RECUPERACAO: 2,
    AVALIACAO_SIMULADA: 3,
    AVALIACAO_PRESENCIAL: 4,
    ATIVIDADE_EAD: 5,
    TCC: 6,
    CONCEITUAL: 7,
    AVALIACAO_ONLINE: 8,
    AVALIACAO_ONLINE_RECUPERACAO: 9,
    AVALIACAO_SEMESTRAL: 10,
    AVALIACAO_SEMESTRAL_RECUPERACAO: 11,
    AVALIACAO_SEMESTRAL_ONLINE: 13,
    AVALIACAO_SEMESTRAL_ONLINE_RECUPERACAO: 14
  })

  .constant('TipoOfertaConstante', {
    PADRAO: 1,
    ESTENDIDA: 2,
    PERENE: 3
  })

  .constant('TipoSalaDeAulaConstante', {
    PERMANENTE: 1,
    PERIODICA: 2,
    PERENE: 3
  })

  .constant('SiglasSistema', {
    APROVEITAMENTO_ESTUDO: 'AE'
  })

  .constant('TipoProduto', {
    PROJETO_PEDAGOGICO: 1,
    TAXA: 2,
    DECLARACOES: 3,
    MATERIAL: 4,
    AVALIACAO: 5,
    LIVRO: 6,
    COMBO: 7,
    PRR: 8,
    SIMULADO: 9,
    VIDEO: 10
  })

  .constant('Afiliado', {
    QUERO_PAGO: 2359
  })

  //Constantes de evolução para tb_entidade.
  .constant('EntidadeConstante', {
    FACULDADE_UNYLEYA: 14,
    UCAM: 262,
    GRADUACAO: 352,
    ESTRATEGIA: 1295,
    IMP_ONLINE: 12,
    IMP_ONLINE_VIA_SITE: 158,
    UNYLEYA: 2,
    POS_GRADUACAO: 16
  })

  //Constantes de evoluções para tb_ocorrencia.
  .constant('EvolucaoOcorrencia', {
    AGUARDANDO_ATENDIMENTO: 28,
    SENDO_ATENDIDO: 29,
    AGUARDANDO_INTERESSADO: 30,
    AGUARDANDO_ATENDENTE: 31,
    INTERESSADO_INTERAGIU: 32,
    AGUARDANDO_DOCUMENTACAO: 33,
    AGUARDANDO_PAGAMENTO: 34,
    AGUARDANDO_OUTROS_SETORES: 35,
    ENCERRAMENTO_REALIZADO_INTERESSADO: 36,
    REABERTURA_REALIZADA_INTERESSADO: 37,
    ENCERRAMENTO_REALIZADO_ATENDENTE: 38,
    DEVOLVIDO_DISTRIBUICAO: 39,
    PENDENCIA_SOLUCIONADA: 87,
    PAGAMENTO_REALIZADO: 88
  })

  //Constantes de situações para tb_ocorrencia.
  .constant('SituacaoOcorrencia', {
    PENDENTE: 97,
    EM_ANDAMENTO: 98,
    ENCERRADA: 99,
    AGUARDANDO_ENCERRAMENTO: 112
  })

  .constant('FuncionalidadeConstante', {
    INDIQUE_GANHE: 906,
    PROVAS_ONLINE_ESCOLA_SAUDE: 924,
    DOCUMENTOS_PESSOAIS: 964,
    MENU_FINANCEIRO:767,
    BIBLIOTECA_VIRTUAL:775
  })

  .constant('TextoSistemaConstante', {

    //Usado provisóriamente, apenas enquanto for necessário
    // informar ao aluno sobre a suspensão das atividades devido ao COVID-19
    AVISO_COVID: 3706,
    MODELO_DOCUMENTO_DRAFT: 1,
    MODELO_DOCUMENTO_PAPEL: 1
  })

  .constant('EtapasIntegracaoOffice365',{
    CRIAR_USUARIO:1,
    PRIMEIRO_ACESSO:2,
    RECUPERAR_DADOS_USUARIO:3,
    EXIBIR_DADOS_USUARIO:4,
    CRIAR_USUARIO_UNYLAB: 5,
    PRIMEIRO_ACESSO_UNYLAB: 6
  })

  .constant('TipoDisciplinaConstante', {
    AMBIENTACAO: 3
  })

  .constant('SituacaoDocumentoCertificacao', {
    DEFERIDO: 59,
    ENTREGUE_NA_MATRICULA: 248,
    ENTREGUE_VIA_PORTAL: 249
  })

  .constant('AssuntoOcorrenciaConstante', {
    DOCUMENTACAO: 309
  })

  .constant('SubAssuntoOcorrenciaConstante', {
    AJUSTES_DADOS_CERTIFICACAO: 4115 // Ajustes nos dados para emissão do certificado
  })

  .constant('SituacaoDocumentoDiplomacao', {
    PENDENTE: 57,
    SOB_ANALISE: 58,
    DEFERIDO: 59
  })

  .constant('StatusLancamentoFinanceiro', {
    LANCAMENTOSMES: 1,
    LANCAMENTOSVENCIDAS: 2,
    LANCAMENTOSFUTUROS: 3,
    LANCAMENTOSPAGOS: 4
  })
;
