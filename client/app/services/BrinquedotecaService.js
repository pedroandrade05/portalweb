import RestService from './RestService';

class BrinquedotecaService extends RestService {

  constructor(
    CONFIG,
    $resource,
    $http,
    $q,
    LSService,
    CursoAlunoService,
    $window
  ) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'brinquedoteca';
    this.$window = $window;

    this.addCustomService('getTokenMoodle', CONFIG.urlApi + '/brinquedoteca/get-token-moodle', 'GET', {});
  }

  /**
   * Busca o token do Moodle
   * @param idMatricula
   * @param idEntidade
   */
  getTokenMoodle(idMatricula, idEntidade) {
    const defer = this.q.defer();

    this.getResource().getTokenMoodle({
      id_matricula: idMatricula,
      id_entidade: idEntidade
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

}

export default BrinquedotecaService;
