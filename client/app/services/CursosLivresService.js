import RestService from './RestService';

class CursosLivresService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'cursos-livres';

    this.addCustomService('getMensagemPadrao', CONFIG.urlApi + '/cursos-livres/mensagem-padrao', 'GET', {}, false);
    this.addCustomService('matricular', CONFIG.urlApi + '/cursos-livres/matricular', 'POST', {}, false);
  }

  getMensagemPadrao(id_mensagempadrao, id_entidade) {
    const defer = this.q.defer();

    this.getResource()
      .getMensagemPadrao({
        id_mensagempadrao,
        id_entidade
      }, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });

    return defer.promise;
  }

  matricular(id_usuario, id_projetopedagogico) {
    const defer = this.q.defer();

    this.getResource().matricular({
      id_usuario,
      id_projetopedagogico
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

}

export default CursosLivresService;
