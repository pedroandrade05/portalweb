class ModalService {

  defaultData = {
    title: '',
    body: '',
    confirmButton: 'Ok',
    cancelButton: 'Fechar'
  };

  defaultOptions = {
    animation: true,
    backdrop: 'static',
    component: 'pwModal',
    keyboard: false,
    size: 'md',
    windowClass: 'default-modal-brand'
  };

  //Metodo construtor
  constructor($q, $uibModal) {
    'ngInject';
    this.$q = $q;
    this.$modal = $uibModal;
  }

  show(data = {}, options = {}) {
    return this.$modal.open({
      ...this.defaultOptions,
      ...options,
      resolve: {
        data: () => ({
          ...this.defaultOptions,
          ...data
        })
      }
    }).result;
  }

}

export default ModalService;
