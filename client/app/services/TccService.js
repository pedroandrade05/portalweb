import RestService from './RestService';

/**
 * Classe TccService
 * @author Elcio Guimarães <elcioguimaraes@gmailcom>
 */

class TccService extends RestService {
  constructor(CONFIG, $resource, $http, $q, LSService, Upload) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'curso';
    this.CONFIG = CONFIG;
    this.Upload = Upload;

    this.addCustomService('verificarEnvioTcc', this.CONFIG.urlApi + '/tcc/verificar-envio-tcc', 'GET', {});
  }

  /**
   * Verifica se o TCC pode ser enviado
   * @param params
   * @returns {Function}
      */
  verificarEnvioTcc(params) {
    const defer = this.q.defer();
    this.getResource()
      .verificarEnvioTcc(params,
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }

  /**
   * Faz o upload do TCC
   * @param params
   * @returns {*}
      */
  enviarTcc(params){

    return this.Upload.upload({
      url: this.CONFIG.urlApi +  '/tcc/enviar-tcc',
      data: params
    });

  }

}

export default TccService;
