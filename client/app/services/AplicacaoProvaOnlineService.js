import RestService from './RestService';

/**
 * Classe para tratar Aplicações de Provas Online
 */
class AplicaocaProvaOnlineService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, LSService);
    this.api = CONFIG.urlApi + '/aplicacao-prova-online';

    this.addCustomService('getAplicacoes', this.api, 'GET', {}, true);
    this.addCustomService('getProximaAplicacao', this.api + '/proxima-aplicacao', 'GET', {});
  }

  /**
   * Busca aplicações já sincronizadas para a matrícula
   * @param idMatricula
   * @returns {*}
   */
  getAplicacoes(idMatricula) {
    const defer = this.q.defer();

    const data = {
      id_matricula: idMatricula
    };

    this.getResource().getAplicacoes(data, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Busca a próxima aplicação ainda não realizada por um aluno
   * @param idMatricula
   * @returns {*}
   */
  getProximaAplicacao(idMatricula) {
    const defer = this.q.defer();

    const data = {
      id_matricula: idMatricula
    };

    this.getResource().getProximaAplicacao(data, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

}

export default AplicaocaProvaOnlineService;
