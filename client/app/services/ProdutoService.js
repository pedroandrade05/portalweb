import RestService from './RestService';

class ProdutoService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'produto';
  }

}

export default ProdutoService;
