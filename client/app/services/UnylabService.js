import RestService from './RestService';

/**
 *
 * Classe para integrar o sistema Unylab *
 *
 */
class UnylabService extends RestService {

  constructor(CONFIG, $resource, $http, $q, $localStorage) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, $localStorage);
    this.api = 'unylab';

    this.addCustomService('postTokenUnylab', `${CONFIG.urlApi}/${this.api}/retornar-token`, 'POST', {}, false);
    this.addCustomService('postConexoesGrupos', `${CONFIG.urlApi}/${this.api}/retornar-conexoes-grupos`, 'POST', {}, false);
    this.addCustomService('postCriarUsuarioUnylab', `${CONFIG.urlApi}/${this.api}/criar-usuario`, 'POST', {}, false);
    this.addCustomService('postAtribuirUsuarioGrupo', `${CONFIG.urlApi}/${this.api}/atribuir-usuario-grupo`, 'POST', {}, false);
  }

  /**
   * Retorna os grupos/conexões da Unylab
   * @params stLogin
   * @returns {*}
   */
  postTokenUnylab(stLogin) {
    const defer = this.q.defer();
    this.getResource().postTokenUnylab({
      st_login: stLogin
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Retorna os grupo/conexao total e disponível da Unylab
   * @param idEntidade
   * @returns {*}
   */
  postConexoesGrupos(idEntidade) {
    const defer = this.q.defer();
    this.getResource().postConexoesGrupos({
      id_entidade: idEntidade
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Cria o usuario na Unylab
   * @returns {*}
   * @param idEntidade
   * @param stLoginAzure
   */
  postCriarUsuarioUnylab(idEntidade, stLoginAzure) {
    const defer = this.q.defer();
    this.getResource().postCriarUsuarioUnylab({
      id_entidade: idEntidade,
      username : stLoginAzure
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Atribuir o usuario a um grupo da Unylab
   * @returns {*}
   * @param idEntidade
   * @param stUsername
   */
  postAtribuirUsuarioGrupo(idEntidade, stUsername) {
    const defer = this.q.defer();
    this.getResource().postAtribuirUsuarioGrupo({
      id_entidade : idEntidade,
      username : stUsername
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Altera o status da licença Unylab
   * @param idUsuario
   * @param blLicencaUnylab
   * @returns {*}
   */
  postAlterarLicencaUnylab(idUsuario, blLicencaUnylab) {
    return this.q((resolve, reject) => {
      const data =
        {
          id_usuario: idUsuario,
          bl_licencaunylab: blLicencaUnylab
        };
      this.http(this.prepareRequestApi(
        'POST',
        this.CONFIG.urlApiUsuario + 'api/gu/alterarLicencaUnylab',
        data,
        null
      )).then((response) => {
        const data = response.data;
        data.errorcode ? reject(data) : resolve(data);
      }, (error) => {
        reject(error);
      });
    });
  }

  /**
   * Método padrão para preparar a requisição
   * @param method
   * @param URL
   * @param data
   * @param params
   * @returns {{headers: {Authorization: string, "Content-type": string}, method: *, data: *, params: *, url: *}}
   */
  prepareRequestApi(method, URL, data, params) {
    return {
      method,
      url: URL,
      headers: {
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + this.storage.user_token},
      data: data,
      params: params
    };
  }
}

export default UnylabService;
