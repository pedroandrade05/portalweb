import RestService from './RestService';

/**
 *
 * Classe para integrar o sistema ao Azure AD e Office 365 *
 *
 */
class Office365Service extends RestService {

  constructor(CONFIG, $resource, $http, $q, $localStorage, $stateParams, CursoAlunoService) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, $localStorage, $stateParams, CursoAlunoService);
    this.api = 'office365';

    this.curso = CursoAlunoService.getStorageCursoAtivo($stateParams.idMatriculaAtiva);
  }

  getUrlAutenticaUsuario() {
    return this.curso.st_office365_url;
  }

  getUrlDominioAzureAD() {
    return this.curso.st_office365_domain;
  }

  /**
   * Método padrão para preparar a requisição
   * @param method
   * @param URL
   * @param data
   * @param params
   * @returns {{headers: {Authorization: string, "Content-type": string}, method: *, data: *, params: *, url: *}}
   */
  prepareRequest(method, URL, data, params) {
    return {
      method,
      url: URL,
      headers: {
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + this.storage.user_token},
      data: data,
      params: params
    };
  }

  /**
   * Cria o usuário no Azure AD
   * @param idUsuario
   * @returns {*}
   */
  postCriarUsuario(idUsuario) {
    return this.q((resolve, reject) => {
      const data =
        {
          id_usuario: idUsuario,
          st_dominio: this.curso.st_office365_domain
        }
      ;
      this.http(this.prepareRequest(
        'POST',
        this.CONFIG.urlApiUsuario + 'api/gu/criarUsuario',
        data,
        null
      )).then((response) => {
        const data = response.data;
        data.errorcode ? reject(data) : resolve(data);
      }, (error) => {
        reject(error);
      });

    });
  }

  /**
   * Atribui o usuário ao grupo no Azure AD
   * @param idUsuario
   * @param idEntidade
   * @returns {*}
   */
  postAtribuirGrupoUsuario(idUsuario, idEntidade) {
    return this.q((resolve, reject) => {
      const data =
        {
          id_usuario: idUsuario,
          id_grupoazure: this.curso.st_office365_group,
          id_entidade: idEntidade,
          st_dominio: this.curso.st_office365_domain
        }
      ;
      this.http(this.prepareRequest(
        'POST',
        this.CONFIG.urlApiUsuario + 'api/gu/atribuirGrupoParaUsuario',
        data,
        null
      )).then((response) => {
        const data = response.data;
        data.errorcode ? reject(data) : resolve(data);
      }, (error) => {
        reject(error);
      });

    });
  }

  /**
   * Localiza usuário na base de dados da Unyleya
   * @param idUsuario
   * @returns {*}
   */
  getLocalizaUsuario(idUsuario){
    return this.q((resolve, reject) => {
      this.http(this.prepareRequest(
        'GET',
        this.CONFIG.urlApiUsuario + 'api/gu/recuperarUsuario/'+idUsuario,
        null,
        null
      )).then((response) => {
        const data = response.data.mensagem;
        data.errorcode ? reject(data) : resolve(data);
      }, (error) => {
        reject(error);
      });
    });
  }

  /**
   * Redefinir a senha do usuário no Azure AD
   * @param idUsuario
   * @param idEntidade
   * @returns {*}
   */
  redefinirSenha(idUsuario, idEntidade){
    return this.q((resolve, reject) => {
      this.http(this.prepareRequest(
        'POST',
        this.CONFIG.urlApiUsuario + 'api/gu/redefinirSenhaUsuario',
        {id_usuario: idUsuario,id_entidade: idEntidade,st_dominio: this.curso.st_office365_domain},
        null
      )).then((response) => {
        const data = response.data.mensagem;
        this.loading = false;
        data.errorcode ? reject(data) : resolve(data);
      }, (error) => {
        reject(error);
      });
    });
  }

}

export default Office365Service;
