import RestService from './RestService';

/**
 *
 * Classe para tratar os dados do Moodle via Webservice
 *
 * Aqui você vai encontrar e se possível evoluí-la com métodos que nos auxilie à ter acesso ao Moodle, colher dados e/ou
 * executar outras tarefaz na plataforma
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 *
 */
class MoodleService extends RestService {
  promise = null;

  /**
   *
   * @param $http
   * @param $sce
   * @param $q
   * @param LSService
   * @param CONFIG
   * @param $resource
   */
  constructor($http, $sce, $q, LSService, CONFIG, $resource) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    this.$http = $http;
    this.CONFIG = CONFIG;
    this.$resource = $resource;

    this.$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
    delete this.$http.defaults.headers.common['X-Requested-With'];
    this.$sce = $sce;
    this.$q = $q;
    this.LSService = LSService;
    this.wsurl = 'webservice/rest/server.php?moodlewsrestformat=json';
    this.$sce.trustAsResourceUrl(this.siteurl + '/**');
    this.api = 'log';
    this.addCustomService('gravaLogErroToken', CONFIG.urlApi + '/log/grava-log-erro-token', 'GET', {});

  }


  /**
   * Faz uma chamada ao Webservice do Moodle
   * @param wsfunction
   * @param params
   * @param siteurl
   * @param method
   * @returns {*}
   */
  doWsCall = (wsfunction, params = {}, siteurl, method = 'GET') => {
    return this.$q((resolve, reject) => {
      const request = {
        method,
        url: siteurl + this.wsurl,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      };

      params.wsfunction = wsfunction;
      method === 'GET' ? request.params = params : request.data = params;

      this.$http(request).then((response) => {
        const data = response.data;
        data.errorcode ? reject(data) : resolve(data);
      }, (error) => {
        reject(error);
      });
    });

  }

  /**
   * Obtém o token de acesso do Usuário no Moodle
   * @param id_matricula
   * @param id_saladeaula
   * @param clearCache
   * @returns {*}
   */
  getUserToken = (id_matricula, id_saladeaula, clearCache = false) => {
    const token_name = 'moodle_data.'  + id_saladeaula + '.' + id_matricula.toString();
    const loginurl = this.CONFIG.urlApi + '/moodle/get-token?id_matricula=' + id_matricula + '&id_saladeaula=' + id_saladeaula;

    return this.$q((resolve, reject) => {
      const exist_token = this.LSService.get(token_name, true);
      if (exist_token && !clearCache) {
        return resolve(exist_token);
      }

      this.$http.get(loginurl).then((response) => {
        if (typeof response.data.token !==  angular.isUndefined) {
          this.LSService.set(token_name, response.data, 1440);
          resolve(response.data);
        }
        else {
          reject({error: response});
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  /**
   * Retorna uma URL que da acesso à plataforma Moodle
   * @param id_matricula
   * @param id_saladeaula
   * @param clearCache
   * @returns {*}
   */
  getTokenAcesso = (id_matricula, id_saladeaula, clearCache = false) => {
    return this.$q((resolve, reject) => {
      const cacheServiceId = 'moodle_token.' + id_saladeaula + '.' + id_matricula.toString();

      this.getUserToken(id_matricula, id_saladeaula, clearCache).then((success) => {
        const token = this.LSService.get(cacheServiceId, true);

        if (!clearCache && token) {
          return resolve(token);
        }

        const request = {
          method: 'GET',
          url: this.CONFIG.urlApi + '/moodle/get-token-acesso',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          params: { st_loginintegrado: success.st_loginintegrado, id_saladeaula }
        };

        this.$http(request).then((response) => {
          this.LSService.set(cacheServiceId, response, 720);
          resolve(response);
        }, (error) => {
          reject(error);
        });
      }, (error) => {
        reject(error);
      });
    });
  }

  /**
   * Retorna os Cursos do usuário
   * @param id_matricula
   * @param id_saladeaula
   * @param clearCache
   * @param tryAgain
   * @returns {*}
   */
  getUserCourses = (id_matricula, id_saladeaula, clearCache = false, tryAgain = true) => {
    const service = 'core_enrol_get_users_courses';
    const cacheServiceId = service + '.' + id_saladeaula + '.' + id_matricula.toString();

    return this.$q((resolve, reject) => {
      const courses = this.LSService.get(cacheServiceId, true);

      if (!clearCache && courses) {
        return resolve(courses);
      }

      this.getUserToken(id_matricula, id_saladeaula, clearCache).then((success) => {
        this.doWsCall(service, {
          userid: success.userid,
          wstoken: success.token
        }, success.siteurl).then((response) => {
          this.LSService.set(cacheServiceId, response,2);
          resolve(response);
        }, (error) => {
          // Se for erro de token inválido, requisitar novamente, limpando cache
          // {"exception":"moodle_exception","errorcode":"invalidtoken","message":"Token inv\u00e1lido - token n\u00e3o encontrado"}
          if (tryAgain && error && error.errorcode && error.errorcode === 'invalidtoken') {
            return resolve(this.getUserCourses(id_matricula, id_saladeaula, true, false));
          }

          if (error.errorcode && error.errorcode === 'invalidtoken') {
            this.gravaLogErroToken(error, id_saladeaula ,id_matricula);
          }

          reject(error);
        });
      }, (error) => {
        reject(error);
      });
    });
  }

  /**
   * Retorna o conteúdo de um Curso
   * @param id_matricula
   * @param courseid
   * @param id_saladeaula
   * @param clearCache
   * @param tryAgain
   * @returns {*}
   */
  getCourseContents = (id_matricula, courseid, id_saladeaula, clearCache = false, tryAgain = true) => {
    const service = 'core_course_get_contents';
    const cacheServiceId = service + '.' + id_saladeaula + '.' + id_matricula.toString() + '.' + courseid.toString();

    return this.$q((resolve, reject) => {
      const courses = this.LSService.get(cacheServiceId, true);

      if (!clearCache && courses) {
        return resolve(courses);
      }

      this.getUserToken(id_matricula, id_saladeaula, clearCache).then((success) => {
        this.doWsCall(service, {
          courseid,
          wstoken: success.token
        }, success.siteurl).then((response) => {
          this.LSService.set(cacheServiceId, response,2);
          resolve(response);
        }, (error) => {
          // Se for erro de token inválido, requisitar novamente, limpando cache
          // {"exception":"moodle_exception","errorcode":"invalidtoken","message":"Token inv\u00e1lido - token n\u00e3o encontrado"}
          if (tryAgain && error && error.errorcode && error.errorcode === 'invalidtoken') {
            return resolve(this.getCourseContents(id_matricula, courseid, id_saladeaula, true, false));
          }

          if (error.errorcode && error.errorcode === 'invalidtoken') {
            this.gravaLogErroToken(error, id_saladeaula ,id_matricula);
          }

          reject(error);
        });
      }, (error) => {
        reject(error);
      });
    });
  }

  /**
   * Retorna o Status das atividades do Aluno em cada atividade
   * @param id_matricula
   * @param courseid
   * @param id_saladeaula
   * @param clearCache
   * @param tryAgain
   * @returns {*}
   */
  getActivitiesCompletionStatus = (id_matricula, courseid, id_saladeaula, clearCache = false, tryAgain = true) => {
    const service = 'core_completion_get_activities_completion_status';
    const cacheServiceId = service + '.' + id_saladeaula + '.' + id_matricula.toString() + '.' + courseid.toString();

    return this.$q((resolve, reject) => {
      const courses = this.LSService.get(cacheServiceId, true);

      if (!clearCache && courses) {
        return resolve(courses);
      }

      this.getUserToken(id_matricula, id_saladeaula, clearCache).then((success) => {
        this.doWsCall(service, {
          userid: success.userid,
          courseid,
          wstoken: success.token
        }, success.siteurl).then((response) => {
          this.LSService.set(cacheServiceId, response, 2);
          resolve(response);
        }, (error) => {
          // Se for erro de token inválido, requisitar novamente, limpando cache
          // {"exception":"moodle_exception","errorcode":"invalidtoken","message":"Token inv\u00e1lido - token n\u00e3o encontrado"}
          if (tryAgain && error && error.errorcode && error.errorcode === 'invalidtoken') {
            return resolve(this.getActivitiesCompletionStatus(id_matricula, courseid, id_saladeaula, true, false));
          }

          if (error.errorcode && error.errorcode === 'invalidtoken') {
            this.gravaLogErroToken(error, id_saladeaula ,id_matricula);
          }

          reject(error);
        });
      }, (error) => {
        reject(error);
      });
    });
  }

  /**
   * Retorna o Status de conclusão do Aluno no Curso
   * @param id_matricula
   * @param courseid
   * @param id_saladeaula
   * @param clearCache
   * @param tryAgain
   * @returns {*}
   */
  getCourseCompletionStatus = (id_matricula, courseid, id_saladeaula, clearCache = false, tryAgain = true) => {
    const service = 'core_completion_get_course_completion_status';
    const cacheServiceId = service + '.' + id_saladeaula + '.' + id_matricula.toString() + '.' + courseid.toString();

    return this.$q((resolve, reject) => {
      const courses = this.LSService.get(cacheServiceId, true);

      if (!clearCache && courses) {
        return resolve(courses);
      }

      this.getUserToken(id_matricula, id_saladeaula, clearCache).then((success) => {
        this.doWsCall(service, {
          userid: success.userid,
          courseid,
          wstoken: success.token
        }, success.siteurl).then((response) => {
          this.LSService.set(cacheServiceId, response,2);
          resolve(response);
        }, (error) => {
          // Se for erro de token inválido, requisitar novamente, limpando cache
          // {"exception":"moodle_exception","errorcode":"invalidtoken","message":"Token inv\u00e1lido - token n\u00e3o encontrado"}
          if (tryAgain && error && error.errorcode && error.errorcode === 'invalidtoken') {
            return resolve(this.getCourseCompletionStatus(id_matricula, courseid, id_saladeaula, true, false));
          }

          if (error.errorcode && error.errorcode === 'invalidtoken') {
            this.gravaLogErroToken(error, id_saladeaula ,id_matricula);
          }

          reject(error);
        });
      }, (error) => {
        reject(error);
      });
    });
  }

  gravaLogErroToken(errorToken, idSala, idMatricula ) {

    const  defer = this.q.defer();

    if (typeof error !== angular.isUndefined && !this.storage.bl_acessarcomo) {
      this.getResource().gravaLogErroToken({
        error: errorToken,
        idSala: idSala,
        idMatricula: idMatricula,
        idUsuario: this.storage.storage.user_id,
        entidade: this.storage.storage.user_entidade
      });
      return defer.promise;
    }
  }

}

export default MoodleService;
