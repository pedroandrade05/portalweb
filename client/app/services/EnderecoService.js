import RestService from './RestService';

/**
 *
 * Classe para tratar Endereços, UFs e Municípios
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 *
 */
class EnderecoService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'endereco';

    this.addCustomService('getPaises', CONFIG.urlApi + '/endereco/pais', 'GET', {}, true);
    this.addCustomService('getUfs', CONFIG.urlApi + '/endereco/uf', 'GET', {}, true);
    this.addCustomService('getMunicipios', CONFIG.urlApi + '/endereco/municipio', 'GET', {}, true);
    this.addCustomService('getByCep', CONFIG.urlApi + '/endereco/by-cep', 'GET', {});
    this.addCustomService('getTipoTelefone', CONFIG.urlApi + '/endereco/tipo-telefone', 'GET', {}, true);
  }

  /**
   * Retorna todos os Países
   * @returns {Function}
   */
  getPaises() {
    const defer = this.q.defer();

    this.getResource().getPaises({}, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Retorna todos os UFs
   * @returns {Function}
   */
  getUfs() {
    const defer = this.q.defer();

    this.getResource().getUfs({}, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Retorna todos os Municipios baseado no sg_uf
   * @param sgUf
   * @returns {Function}
   */
  getMunicipios(sgUf) {
    const defer = this.q.defer();

    this.getResource().getMunicipios({
      sg_uf: sgUf
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Retorna o endereço baseado no CEP
   * @param stCep
   * @returns {Function}
   */
  getByCep(stCep) {
    const defer = this.q.defer();

    this.getResource().getByCep({
      st_cep: stCep
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Retorna todos os tipos Telefone
   * @returns {Function}
   */
  getTipoTelefone() {
    const defer = this.q.defer();
    this.getResource().getTipoTelefone({}, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

}

export default EnderecoService;
