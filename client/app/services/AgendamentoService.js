import RestService from './RestService';

/**
 *
 * Classe para tratar o Agendamento de Avaliação
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 *
 */
class AgendamentoService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'agendamento';

    this.addCustomService('getDisciplinasApto', CONFIG.urlApi + '/agendamento/disciplinas-apto', 'GET', {}, true);
    this.addCustomService('getAvaliacoes', CONFIG.urlApi + '/agendamento/avaliacoes', 'GET', {}, true);
    this.addCustomService('getAvaliacaoAplicacoes', CONFIG.urlApi + '/agendamento/avaliacao-aplicacoes', 'GET', {}, true);
    this.addCustomService('getProximoAgendamento', CONFIG.urlApi + '/agendamento/get-proximo-agendamento', 'GET', {}, false);
    this.addCustomService('getInformacoes', CONFIG.urlApi + '/agendamento/get-informacoes', 'GET', {}, false);
    this.addCustomService('getUfMunicipio', CONFIG.urlApi + '/agendamento/avaliacao-aplicacoes-pos', 'GET', {}, true);
    this.addCustomService('getAvisoCovid', CONFIG.urlApi + '/agendamento/get-aviso-covid', 'GET', {}, false);
  }

  getAvaliacoes(idMatricula, idDisciplina = null) {
    const defer = this.q.defer();

    const data = {
      id_matricula: idMatricula
    };

    if (idDisciplina) {
      data.id_disciplina = idDisciplina;
    }

    this.getResource().getAvaliacoes(data, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  getAvaliacaoAplicacoes(idMatricula, idAvaliacao, sgUf, idMunicipio, idDisciplina = null) {
    const defer = this.q.defer();

    this.getResource().getAvaliacaoAplicacoes({
      id_matricula: idMatricula,
      id_avaliacao: idAvaliacao,
      sg_uf: sgUf,
      id_municipio: idMunicipio,
      id_disciplina: idDisciplina
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Retorna as disciplinas em que o aluno está apto para fazer o agendamento
   * @param idMatricula
   * @returns {*}
   */
  getDisciplinasApto(idMatricula) {
    const defer = this.q.defer();

    this.getResource().getDisciplinasApto({
      id_matricula: idMatricula
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Retornar o agendamento que possui a data de aplicação futura mais próxima
   * @param idMatricula
   * @param data
   */
  getProximoAgendamento(idMatricula, data = {}) {
    const defer = this.q.defer();

    this.getResource().getProximoAgendamento({
      id_matricula: idMatricula,
      ...data
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Retornar as informações de agendamento sobre a entidade da matrícula
   * @param idMatricula
   */
  getInformacoes(idMatricula) {
    const defer = this.q.defer();

    this.getResource().getInformacoes({
      id_matricula: idMatricula
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  getUfMunicipio(sgUf = null) {
    const defer = this.q.defer();

    this.getResource().getUfMunicipio({
      sg_uf: sgUf
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  getAvisoCovid(id_textosistema) {
    const defer = this.q.defer();

    this.getResource().getAvisoCovid({
      id_textosistema: id_textosistema
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

}

export default AgendamentoService;
