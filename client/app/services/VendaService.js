import RestService from './RestService';

class VendaService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService, $localStorage) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'venda';
    this.storage = $localStorage;
    this.CONFIG = CONFIG;

    this.addCustomService('getVendaPorMatricula', CONFIG.urlApi + '/venda/get-venda-por-matricula', 'GET', {});

  }

  /**
   * Retorna venda da matrícula atual
   * @param id_matricula
   */
  getVendaPorMatricula(id_matricula) {

    const defer = this.q.defer();

    this.getResource()
      .getVendaPorMatricula({id_matricula: id_matricula}, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });
    return defer.promise;
  }

  /**
   * Seta venda da matrícula atual
   * @param venda
   */
  setStorageVendaPorMatricula(venda) {
    this.storage.venda =  venda;
  }

  /**
   * Retorna venda da matrícula atual
   * @returns {*}
   */
  getStorageVendaPorMatricula() {
    return this.storage.venda;
  }


}

export default VendaService;
