import RestService from './RestService';

/**
 * Classe para tratar os dados do WS Unyleya Moodle
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class UnyleyaMoodleService extends RestService{

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);

    this.api = 'moodle';
    this.LSService = LSService;

    // POR-912 - Retirando o módulo de atividades recentes
    // this.addCustomService('listRecentActivity', CONFIG.urlApi + '/moodle/list-recent-activity', 'GET', {}, true);
    this.addCustomService('courseGetContents', CONFIG.urlApi + '/moodle/course-get-contents', 'GET', {}, true);

  }

  /**
   * Retorna os dados de atividades recentes do moodle
   * @param id_matricula
   * @param courseid
   * @param id_saladeaula
   * @returns {Function}
   */
  listRecentActivity = (id_matricula, courseid, id_saladeaula) => {

    const defer = this.q.defer();

    var params = {
      idMatricula: id_matricula,
      idSaladeaula: id_saladeaula,
      stCodsistemasala: courseid
    };

    this.getResource().listRecentActivity(params
      ,
      (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });
    return defer.promise;

  }

  /**
   * Retorna os dados das atividades do moodle
   * @param id_matricula
   * @param courseid
   * @param id_saladeaula
   * @param clearCache
   * @returns {Function}
   */
  courseGetContents = (id_matricula, courseid, id_saladeaula, clearCache = false) => {

    const service = 'core_course_get_contents';
    const cacheServiceId = service + '.' + id_saladeaula + '.' + id_matricula.toString() + '.' + courseid.toString();

    return this.q((resolve, reject) => {
      const courses = this.LSService.get(cacheServiceId, true);
      if (!clearCache && courses) {
        resolve(courses);
      }
      else {
        var params = {
          id_matricula,
          id_saladeaula,
          st_codsistemasala: courseid
        };

        this.getResource().courseGetContents(params
          ,
          (response) => {
            this.LSService.set(cacheServiceId, response,2);
            resolve(response);
          }, (error) => {
            reject(error);
          });
      }
    });
  }

}

export default UnyleyaMoodleService;
