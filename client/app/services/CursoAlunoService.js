import RestService from "./RestService";

// import "angular";

/**
 * Classe CursoAlunoService
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 */
class CursoAlunoService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
   * @author Elcio Guimarães <elcioguimaraes@gmail.com>
   */
  constructor(CONFIG, $resource, $http, $q, LSService,$localStorage) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller
    this.$q = $q;

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'curso-aluno';
    this.storage = $localStorage;

    this.addCustomService('checkMatriculaAluno', CONFIG.urlApi + '/curso-aluno/verificar-matricula-aluno', 'GET', {});
    this.addCustomService('updateEntidade', CONFIG.urlApi + '/curso-aluno/update-entidade', 'GET', {});
    this.addCustomService('getMostrarBrinquedoteca', CONFIG.urlApi + '/curso-aluno/get-mostrar-brinquedoteca', 'GET', {});
    this.addCustomService('getMostrarClubeLivro', CONFIG.urlApi + '/curso-aluno/get-clube-livro', 'GET', {});
    this.addCustomService('getMostrarMinhaBiblioteca', CONFIG.urlApi + '/curso-aluno/get-minha-biblioteca', 'GET', {});
    this.addCustomService('getMensagemPadrao', CONFIG.urlApi + '/curso-aluno/mensagem-padrao', 'GET', {}, false);
  }

  /**
   * Verifica se a matricula logada esta vinculada ao usuario
   * @param matricula
   * @returns {Function}
   */
  verificaMatriculaAluno(idMatricula) {
    const defer = this.$q.defer();
    this.getResource().checkMatriculaAluno({id_matricula: idMatricula}, (response) => {
      this.setStorageCursoAtivo(response);
      defer.resolve(true);
    }, () => {
      defer.resolve(false);
    });
    return defer.promise;
  }

  /**
   *Atualiza token com a entidade do curso em que ele esta logado
   * @param idEntidade
   * @returns {Function}
   */
  updateEntidade(idEntidade) {
    const defer = this.$q.defer();
    this.getResource().updateEntidade(idEntidade, (response) => {

      this.setStorageCursoAtivo(response);
      defer.resolve(response);
    }, () => {
      defer.resolve(false);
    });
    return defer.promise;
  }

  /**
   * Retorna se o curso deve mostrar a brinquedoteca de acordo com o projeto pedagogico
   * @param idProjetoPedagogico
   */
  getMostrarBrinquedoteca(idProjetoPedagogico) {
    const defer = this.q.defer();
    this.getResource()
      .getMostrarBrinquedoteca({id_projetopedagogico: idProjetoPedagogico},
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }

  /**
   * Seta a matricula no localStorage
   * @param curso
   * @param entidadeId
   */
  setStorageCursoAtivo(curso) {
    this.storage["matricula." + curso.id_matricula] =  curso;
    this.storage.user_entidade =  curso.id_entidade;
    this.storage.user_matricula = curso.id_matricula;
  }

  /**
   * Retorna os dados da matricula que estão no localStorage
   * @param matriculaId
   * @param entidadeId
   */
  getStorageCursoAtivo(idMatricula) {
    const matricula = this.storage["matricula." + idMatricula];
    return matricula;
  }

  /**
   * Remove a matricula do localStorage
   * @param matriculaId
   * @param entidadeId
   */
  removeStorageCursoAtivo(idMatricula) {
    delete this.storage["matricula." + idMatricula];
  }

  /**
   * Retorna se o curso deve mostrar a clube do livro de acordo com o projeto pedagogico
   * @param idProjetoPedagogico
   */
  getMostrarClubeLivro(idProjetoPedagogico) {
    const defer = this.q.defer();
    this.getResource()
      .getMostrarClubeLivro({id_projetopedagogico: idProjetoPedagogico},
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }

  /**
   * Retorna se o curso deve mostrar a opção Minha Biblioteca de acordo com o projeto pedagogico
   * @param idProjetoPedagogico
   */
  getMostrarMinhaBiblioteca(idProjetoPedagogico) {
    const defer = this.q.defer();
    this.getResource()
      .getMostrarMinhaBiblioteca({id_projetopedagogico: idProjetoPedagogico},
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }

  getMensagemPadrao(id_mensagempadrao, id_entidade) {
    const defer = this.q.defer();

    this.getResource()
      .getMensagemPadrao({
        id_mensagempadrao,
        id_entidade
      }, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });

    return defer.promise;
  }
}

export default CursoAlunoService;
