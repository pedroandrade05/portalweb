import RestService from './RestService';

class ClubeLivroService extends RestService {

  constructor(
    CONFIG,
    $resource,
    $http,
    $q,
    LSService,
    $window,
    $localStorage
  ) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'clube-livro';
    this.$window = $window;
    this.storage = $localStorage;
    this.addCustomService('getTokenMoodle', CONFIG.urlApi + '/clube-livro/get-token-moodle', 'GET', {});
    this.addCustomService('getUrl', CONFIG.urlApi + '/clube-livro/get-url', 'GET', {});
  }

  /**
   * Retorna os dados da matricula que estão no localStorage
   * @param matriculaId
   * @param entidadeId
   */
  getStorageMatricula(idMatricula) {
    const matricula = this.storage["matricula." + idMatricula];
    return matricula;
  }

  /**
   * Busca o token do Moodle
   * @param idMatricula
   * @param idEntidade
   */
  getTokenMoodle(idMatricula, idEntidade) {
    const defer = this.q.defer();

    this.getResource().getTokenMoodle({
      id_matricula: idMatricula,
      id_entidade: idEntidade
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  getUrl() {
    const defer = this.q.defer();

    this.getResource().getUrl({}, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

}

export default ClubeLivroService;
