import RestService from './RestService';

/**
 * @author Fabio Carvalho <fabio.carvalho@unyleya.com.br>
 */
class PlannerService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService, CursoAlunoService, $localStorage) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, LSService, CursoAlunoService);
    this.api = 'planner';
    this.LSService = LSService;
    this.$q = $q;

    this.CursoAlunoService = CursoAlunoService;
    this.name = $localStorage.user_name;

    this.addCustomService('returnUrlPlanner',
      CONFIG.urlApi + '/planner/return-url-planner', 'GET', {});
  }

  getUrlPlanner(idMatricula) {
    const defer = this.q.defer();
    const matriculaAtiva = this.CursoAlunoService.getStorageCursoAtivo(idMatricula);
    this.getResource().returnUrlPlanner({id_entidade: matriculaAtiva.id_entidade}, (response) => {
      response.st_caminhocompleto = response.st_caminho + '?concurso=' + matriculaAtiva.id_curso + '&authKey=' + response.st_codchave + '&cpf=' + matriculaAtiva.id_matricula + '&nome=' + this.name;
      defer.resolve(response);
    }, () => {
      defer.resolver(false);
    });
    return defer.promise;
  }
}

export default PlannerService;
