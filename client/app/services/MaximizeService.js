import RestService from './RestService';

/**
 * @author Ilson Nóbrega <ilson.nobrega@unyleya.com.br>
 */
class MaximizeService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService, CursoAlunoService, $localStorage) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'ambientacao';

    // URL setada dessa forma para atender a um MVP decidido em reunião
    this.URL = this.CONFIG.urlMaximize;
    this.token = this.CONFIG.tokenMaximize;

    this.localStorage = $localStorage;

    this.CursoAlunoService = CursoAlunoService;

  }
  getFinalUrl(url) {
    return this.q((resolve, reject) => {
      this.http({
        method: 'GET',
        url: url,
        headers: {
          'Content-type': 'multipart/form-data; charset=utf-8'
        }
      }).then((response) => {
        (response.status === 200 || response.status === 302) ? resolve(response) : reject(response);
      }, (error) => {
        reject(error);
      });

    });
  }
  prepareUserLogin() {
    return this.q((resolve, reject) => {
      this.sendUserData()
        .then(() => {
          this.getAccessKey()
            .then((res) => {
              resolve(this.URL + '/?chaveDeAcesso=' + res.chaveDeAcesso + '#/busca');
            })
            .catch((err) => {
              reject(err);
            });
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  prepareRequest(method, URL, data, params) {
    return {
      method,
      url: URL,
      headers: {
        'Token-Autenticacao': this.token,
        'Content-type': 'application/json; charset=utf-8'
      },
      data: data,
      params: params
    };
  }

  sendUserData() {
    return this.q((resolve, reject) => {
      const data = [
        {
          codigoSistemaOrigem: this.localStorage.user_id,
          nome: this.localStorage.user_name,
          login: this.localStorage.user_id
        }
      ];
      this.http(this.prepareRequest(
        'POST',
        this.URL + '/c/usuario/salvar',
        data,
        null
      )).then((response) => {
        const data = response.data;
        data.errorcode ? reject(data) : resolve(data);
      }, (error) => {
        reject(error);
      });

    });
  }

  getAccessKey() {
    return this.q((resolve, reject) => {
      this.http(this.prepareRequest(
        'POST',
        this.URL + '/c/login/gerarChaveDeAcesso',
        null,
        {codigoAluno: this.localStorage.user_id}
      )).then((response) => {
        const data = response.data;
        data.errorcode ? reject(data) : resolve(data);
      }, (error) => {
        reject(error);
      });

    });
  }

}

export default MaximizeService;
