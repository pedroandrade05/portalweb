import RestService from './RestService';

class DocumentoDigitalService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'documento-digital';

    this.addCustomService('getDocumentosDisponiveis', `${CONFIG.urlApi}/${this.api}/get-documentos-disponiveis`, 'GET', {}, true);
    this.addCustomService('getTipos', `${CONFIG.urlApi}/${this.api}/get-tipos`, 'GET', {}, true);
  }

  getDocumentosDisponiveis(idEntidade, idMatricula) {
    const defer = this.q.defer();

    this.getResource().getDocumentosDisponiveis({
      id_entidade: idEntidade,
      id_matricula: idMatricula
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  getTipos() {
    const defer = this.q.defer();

    this.getResource().getTipos({}, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

}

export default DocumentoDigitalService;
