import RestService from './RestService';

/**
 *
 * Classe para tratas a biblioteca do polo Pergamum
 * @author Janilson Silva <janilson.silva@unyleya.com.br>
 *
 */
class PergamumService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'pergamum';
  }
}

export default PergamumService;
