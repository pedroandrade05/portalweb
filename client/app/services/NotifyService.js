import alertify from "alertifyjs/build/alertify.min.js";

/**
* Service principal para notificações, pode ser evoluída para termos confirms e outros tipos
* @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
*/
class NotifyService {

  //Metodo construtor
  constructor($log) {
    'ngInject';
    this.log = $log;
    this.alertify = alertify;

    alertify.set('notifier','position', 'top-right');
  }

  /**
  * Envia uma notificação
  * @param type
  * @param title
  * @param message
  * @param wait
  * @returns {*|T}
  */
  notify(type = 'success', title = 'OK', message = '', wait = 5) {

    try {
      if (angular.isString(type)) {
        switch (type) {
        case 'success':
          return  alertify.success(title + " " +message, wait).dismissOthers();
        case 'error':
          return  alertify.error(title + " " +message, wait).dismissOthers();
        case 'warning':
          return  alertify.warning(title + " " +message, wait).dismissOthers();
        default:
          return  alertify.success(title + " " +message, wait).dismissOthers();
        }
      }
      if (angular.isObject(type)) {
        type.type || (type.type = 'success');
        type.title || (type.title = 'Sucesso');
        type.message || (type.message = '');
        switch (type.type) {
        case 'success':
          return  alertify.success(type.title + " " +type.message, wait).dismissOthers();
        case 'error':
          return  alertify.error(type.title + " " +type.message, wait).dismissOthers();
        case 'warning':
          return  alertify.warning(type.title + " " +type.message, wait).dismissOthers();
        default:
          return  alertify.success(type.title + " " +type.message, wait).dismissOthers();
        }
      }
    }
    catch (err) {
      this.log.error(err);
    }
  }

  confirm(title = 'Confirmação', message = 'Deseja continuar?', fnConfirm = function() {}, fnCancel = function() {}) {
    return alertify
      .confirm(title, message, fnConfirm, fnCancel)
      .set('labels', {
        ok: 'Confirmar',
        cancel: 'Cancelar'
      });
  }

}

export default NotifyService;
