import RestService from "./RestService";

/**
 * Classe DisciplinaAlunoService
 * @author Ilson Nóbrega <ilson.nobrega@unyleya.com.br>
 */
class ConteudoService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
   * @author Ilson Nóbrega <ilson.nobrega@unyleya.com.br>
   */
  constructor(CONFIG, $resource, $http, $q, LSService, CursoAlunoService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService, CursoAlunoService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'player';
    this.LSService = LSService;
    this.$q = $q;


    this.CursoAlunoService = CursoAlunoService;

    this.addCustomService('returnUrlPlayer',
      CONFIG.urlApi + '/player/return-url-player', 'GET', {});

  }

  /**
   * Retorna a URL do Player que foi configurado no G2
   * @param entidade
   * @returns {Function}
   */
  retornaUrlPlayerConteudo(idMatricula) {
    const defer = this.$q.defer();
    const matriculaAtiva = this.CursoAlunoService.getStorageCursoAtivo(idMatricula);
    this.getResource().returnUrlPlayer({id_entidade: matriculaAtiva.id_entidade}, (response) => {
      response.st_caminhocompleto = response.st_caminho + '/' + matriculaAtiva.id_curso;
      defer.resolve(response);
    }, () => {
      defer.resolve(false);
    });
    return defer.promise;
  }
}

export default ConteudoService;
