import RestService from './RestService';

/**
 * @author Ilson Nóbrega <ilson.nobrega@unyleya.com.br>
 */
class AmbientacaoService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService, CursoAlunoService, $window) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'ambientacao';
    this.$window = $window;
    this.CursoAlunoService = CursoAlunoService;
  }

  getUrlAmbientacao(idMatricula) {
    const defer = this.q.defer();

    const matriculaAtiva = this.CursoAlunoService.getStorageCursoAtivo(idMatricula);

    let st_urlambientacao = null;

    switch (matriculaAtiva.id_esquemaconfiguracao) {
    case 1:
      st_urlambientacao = 'http://ambientacao.unyleya.edu.br/course/view.php?id=4';
      break;
    }

    if (st_urlambientacao && this.$window.location.protocol === "https:") {
      st_urlambientacao = st_urlambientacao.replace("http://", "https://");
    }

    defer.resolve({
      st_urlambientacao: st_urlambientacao
    });

    // Retornar promise para que no futuro caso seja implementado BACKEND, já estar preparado no FRONT
    return defer.promise;
  }

}

export default AmbientacaoService;
