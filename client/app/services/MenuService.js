import RestService from './RestService';

/**
 * Classe MenuService
 * @author Gabriel Resende <gabriel.resende@unyleya.com.br>
 */
class MenuService extends RestService {

  promise = null;

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
   * @author Gabriel Resende <gabriel.resende@unyleya.com.br>
   */
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'menu';
    this.CONFIG = CONFIG;

    this.addCustomService('getMenuSideBar', CONFIG.urlApi + '/menu/menu-side-bar', 'GET', {}, true);

  }

  /**
   * @param idEntidade
   * @param idEvolucao
   * @param idMatricula
   */
  getMenuSideBar(idEntidade, idEvolucao, idMatricula) {
    const defer = this.q.defer();

    this.getResource().getMenuSideBar({
      id_entidade: idEntidade,
      id_evolucao: idEvolucao,
      id_matricula: idMatricula
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    this.promise = defer.promise;

    return defer.promise;
  }

  hasPermission(idFuncionalidade) {
    return this.promise.then((response) => {
      const menu = response.find((menu) => {
        return +menu.id_funcionalidade === +idFuncionalidade;
      });

      if (!menu) {
        throw `Funcionalidade ${idFuncionalidade} não habilitada para o perfil/entidade.`;
      }

      return menu;
    });
  }

}

export default MenuService;
