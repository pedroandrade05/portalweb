import RestService from './RestService';

/**
 * Classe CronogramaService
 */
class CronogramaService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
   */
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services

    super(CONFIG, $resource, $http, $q, LSService);

    this.api = 'cronograma';
    this.CONFIG = CONFIG;

    this.addCustomService('buscarCronograma', CONFIG.urlApi + '/cronograma/buscar-cronograma', 'POST', {}, true);
  }

  /**
   * Busca o cronograma
   * @param idMatricula
   */
  buscarCronograma(idMatricula) {
    const defer = this.q.defer();

    this.getResource().buscarCronograma({
      id_matricula: idMatricula
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }
}

export default CronogramaService;
