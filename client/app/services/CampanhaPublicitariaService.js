import RestService from './RestService';

class CampanhaPublicitariaService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'campanha-publicitaria';
    this.CONFIG = CONFIG;
    this.addCustomService('getCampanhaPublicitaria', CONFIG.urlApi + '/' + this.api, 'GET', {}, true);
  }

  getCampanhaPublicitaria(idEntidade, blDestaque) {
    const defer = this.q.defer();
    const data = {bl_portalweb: 1, id_entidade: idEntidade};

    if (angular.isDefined(blDestaque)) {
      data.bl_destaque = +blDestaque;
    }

    this.getResource()
      .getCampanhaPublicitaria(data,
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }
}

export default CampanhaPublicitariaService;
