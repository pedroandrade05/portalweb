import RestService from './RestService';

class PortalEgressoService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService, CursoAlunoService, EsquemaConfiguracaoService, EvolucaoMatriculaConstante) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'portal-egresso';

    this.CursoAlunoService = CursoAlunoService;
    this.EsquemaConfiguracaoService = EsquemaConfiguracaoService;
    this.EvolucaoMatriculaConstante = EvolucaoMatriculaConstante;

    this.addCustomService('getDadosPortal', `${CONFIG.urlApi}/${this.api}`, 'GET', {});
    this.addCustomService('getEntidadesAcesso', `${CONFIG.urlApi}/${this.api}/entidades`, 'GET', {}, true);
  }

  async isAlunoEgresso(idMatricula) {
    const curso = this.CursoAlunoService.getStorageCursoAtivo(idMatricula);
    const isPosGraduacao = this.EsquemaConfiguracaoService.isPosGraduacao(idMatricula);

    const {id_entidade, id_evolucao} = curso;

    // Evoluções permitidas de acessar o portal do egresso
    const evolucoes = [
      this.EvolucaoMatriculaConstante.FORMADO,
      this.EvolucaoMatriculaConstante.CERTIFICADO,
      this.EvolucaoMatriculaConstante.DIPLOMADO,
      this.EvolucaoMatriculaConstante.EGRESSO
    ];

    // Para a POS, considerar CONCLUINTES
    if (isPosGraduacao) {
      evolucoes.push(this.EvolucaoMatriculaConstante.CONCLUINTE);
    }

    // Verificar se a evolução da matrícula encaixa em alguma aceita
    if (!evolucoes.includes(+id_evolucao)) {
      return false;
    }

    // Buscar entidades com acesso ao portal do egresso
    const entidades = await this.getEntidadesAcesso();

    // Verificar se a entidade da matrícula possui acesso
    if (!entidades.includes(+id_entidade)) {
      return false;
    }

    // Padrão retornar true
    return true;
  }

  getEntidadesAcesso() {
    const defer = this.q.defer();

    this.getResource()
      .getEntidadesAcesso({}, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });

    return defer.promise;
  }

  getDadosPortal() {
    const defer = this.q.defer();

    this.getResource()
      .getDadosPortal({}, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });

    return defer.promise;
  }

  async getUrlAutenticarDireto(idUsuario, idEntidade, stUrlacesso, redirectUri, urlPortal = null) {
    // Se não informar o URL do Portal do Egresso, buscar informações
    if (!urlPortal) {
      const portal = await this.getDadosPortal();

      if (!portal || !portal.st_urlportal) {
        throw new Error('Configurações do Portal do Egresso não encontradas.');
      }

      urlPortal = portal.st_urlportal;
    }

    let url = `${urlPortal}/login/direto/${idEntidade}/${idUsuario}/${stUrlacesso}`;

    if (redirectUri) {
      url += `?redirect=/${redirectUri}`;
    }

    return url;
  }

}

export default PortalEgressoService;
