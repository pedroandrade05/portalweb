import RestService from './RestService';

/**
 * Classe ArquivosCompartilhadosService
 * @author Bruno Maciel Duarte <bruno.duarte@unyelya.com.br>
 */

class ArquivosCompartilhadosService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'arquivos-compartilhados';
    this.CONFIG = CONFIG;

    this.addCustomService('getArquivosCompartilhados', CONFIG.urlApi + '/arquivos-compartilhados', 'GET', {}, true);
  }

  getArquivosCompartilhados(idMatricula) {
    const defer = this.q.defer();
    this.getResource()
      .getArquivosCompartilhados({id_matricula: idMatricula},
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }
}

export default ArquivosCompartilhadosService;
