import RestService from './RestService';

/**
 * Classe EntregaDocumentoService
 * @author Guilherme Gomes <guilherme.noronha@unyleya.com.br>
 */
class EntregaDocumentosService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
   * @param $q
   * @param LSService
   * @param Upload
   * @param $localStorage
   */
  constructor(CONFIG, $resource, $http, $q, LSService, Upload, $localStorage) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    this.api = 'entrega-documentos';
    this.Upload = Upload;
    this.storage = $localStorage;

    this.addCustomService('getDocumentosPessoais', CONFIG.urlApi + '/' + this.api + '/get-documentos-pessoais', 'GET', {}, true);
    this.addCustomService('getHistoricoEnvio', CONFIG.urlApi + '/' + this.api + '/get-historico-envio', 'GET', {}, true);
  }

  /**
   * Retorna os documentos para envio
   * @returns {Function}
   */
  getDocumentosPessoais(id_matricula) {
    const defer = this.q.defer();

    this.getResource().getDocumentosPessoais({id_matricula}, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   *
   * @returns {*}
   * @param documento
   */
  entregarDocumento(documento) {
    return this.Upload.upload({
      url: this.CONFIG.urlApi + '/' + this.api + '/enviar',
      data: documento
    });
  }

  /**
   * Retorna o histórico de envio do documento
   * @param id_matricula
   * @param id_entregadocumentos
   * @returns {*}
   */
  getHistoricoEnvio(id_matricula, id_entregadocumentos) {
    const defer = this.q.defer();

    this.getResource().getHistoricoEnvio({id_matricula, id_entregadocumentos}, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

}

export default EntregaDocumentosService;
