import RestService from './RestService';

/**
 * Classe CursoService
 * @author Marcus Pereira <pereiramarcus93@gmail.com>
 */

class CursoService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService, $localStorage, EntidadeConstante) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'curso';
    this.CONFIG = CONFIG;
    this.storage = $localStorage;
    this.EntidadeConstante = EntidadeConstante;

    this.addCustomService('getDetalhesCoordenador', CONFIG.urlApi + '/curso/get-detalhes-coordenador', 'GET', {});
    this.addCustomService('getCoordenadorCurso', CONFIG.urlApi + '/curso/get-coordenador-curso', 'GET', {});
    this.addCustomService('getBoasVindasCurso', CONFIG.urlApi + '/curso/get-boas-vindas-curso', 'GET', {});
    this.addCustomService('getCalendarioAcademico', CONFIG.urlApi + '/calendario-academico', 'GET', {}, true);
    this.addCustomService('getMensagemPadrao', CONFIG.urlApi + '/curso/get-mensagem-padrao', 'GET', {}, false);
    this.addCustomService('getNPJDigital', CONFIG.urlApi + '/praticas-juridicas/verificar-npj-digital', 'GET', {});
  }

  isCursoLivre(curso) {
    // Por enquanto, os cursos livres são considerados APENAS NA GRADUAÇÃO
    return curso && curso.bl_cursolivre && this.CONFIG.idEntidade.includes(this.EntidadeConstante.GRADUACAO);
  }

  /**
   * Retorna os dados da matricula que estão no localStorage
   * @param matriculaId
   */
  getStorageMatriculaAtivo(idMatricula) {
    const matricula = this.storage["matricula." + idMatricula];
    return matricula;
  }

  getCoordenadorCurso(idProjetoPedagogico) {
    const defer = this.q.defer();
    this.getResource()
      .getCoordenadorCurso({id_projetopedagogico: idProjetoPedagogico},
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }

  getDetalhesCoordenador(idUsuario, idEntidade) {
    const defer = this.q.defer();

    this.getResource()
      .getDetalhesCoordenador({id_usuario: idUsuario, id_entidade: idEntidade},
      (success) => {
        defer.resolve(success);
      },
      (error) => {
        defer.reject(error);
      });
    return defer.promise;
  }

  getBoasVindasCurso(idProjetoPedagogico) {
    const defer = this.q.defer();

    this.getResource()
      .getBoasVindasCurso({id_projetopedagogico: idProjetoPedagogico},
      (success) => {
        defer.resolve(success);
      }, (error) => {
        defer.reject(error);
      });
    return defer.promise;
  }

  getCalendarioAcademico(idEntidade) {
    const defer = this.q.defer();
    this.getResource()
      .getCalendarioAcademico({id_entidade: idEntidade},
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }

  getMensagemPadrao(id_mensagempadrao, id_entidade) {
    const defer = this.q.defer();

    this.getResource()
      .getMensagemPadrao({
        id_mensagempadrao,
        id_entidade
      }, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });

    return defer.promise;
  }

  getNPJDigital(idMatricula) {
    const defer = this.q.defer();
    this.getResource()
      .getNPJDigital({id_matricula: idMatricula},
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }

}

export default CursoService;
