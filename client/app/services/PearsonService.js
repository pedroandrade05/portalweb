import RestService from './RestService';

/**
 *
 * Classe para tratas a biblioteca virtual Pearson
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 *
 */
class PearsonService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'pearson';
  }

}

export default PearsonService;
