import RestService from './RestService';

/**
 * Classe OcorrenciaService
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class OcorrenciaService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
   * @param $q
   * @param LSService
   * @param Upload
   * @param $localStorage
  */
  constructor(CONFIG, $resource, $http, $q, LSService, Upload,$localStorage) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    this.api = 'ocorrencia';
    this.Upload = Upload;
    this.storage = $localStorage;

    this.addCustomService('encerrarChamado', CONFIG.urlApi + '/ocorrencia/encerrar' , 'POST', {});
    this.addCustomService('assuntos', CONFIG.urlApi + '/ocorrencia/assuntos' , 'GET', {}, true);
    this.addCustomService('disciplinas', CONFIG.urlApi + '/ocorrencia/disciplinas' , 'GET', {}, false);
    this.addCustomService('numeroOcorrenciasInteressado', CONFIG.urlApi + '/ocorrencia/numero-ocorrencias-interessado' , 'GET', {});
    this.addCustomService('registraCienciaOcorrencia', CONFIG.urlApi + '/ocorrencia/registra-ciencia-ocorrencia' , 'GET', {});
  }

  /**
   * Reabre um chamado
   * @param id_ocorrencia (integer )
   * @returns {Function|promise}
   * @author Denise Xavier <denise.xavier@unyleya.com.br>
   */
  reabrirChamado(tramite) {
    return this.Upload.upload({
      url: this.CONFIG.urlApi + '/ocorrencia/reabrir',
      data: tramite
    });
  }

  /**
   * Encerra um chamado
   * @param id_ocorrencia (integer )
   * @returns {Function|promise}
   * @author Denise Xavier <denise.xavier@unyleya.com.br>
   */
  encerrarChamado(tramite) {
    const defer = this.q.defer();

    this.getResource().encerrarChamado(tramite, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }


 /**
  * Retorna os assuntos
  * @param array param
  * @returns {Function|promise}
  * @author Denise Xavier <denise.xavier@unyleya.com.br>
  */
  assuntos (param) {
    const defer = this.q.defer();

    this.getResource().assuntos( param , (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

  /**
   * Cria uma nova ocorrencia, enviando o arquivo anexado
   * @param ocorrencia
   * @returns {*}
   * @author Denise Xavier <denise.xavier@unyleya.com.br>
   * @author UPDATE Neemias Santos <neemias.santos@unyleya.com.br>
   */
  create (ocorrencia) {
    return this.Upload.upload({
      url: this.CONFIG.urlApi +  '/ocorrencia/create',
      data: ocorrencia
    });
  }

  /**
   * Retorna as disciplinas
   * @param int matricula
   * @returns {Function|promise}
   */
  disciplinas (matricula) {
    const defer = this.q.defer();

    this.getResource().disciplinas( matricula , (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

  /**
   * Retorna o número de ocorrências com evolução "Aguardando interessado"
   * @param id_matricula (integer)
   * @returns {Function|promise}
   * @author Guilherme Gomes <guilherme.noronha@unyleya.com.br>
   */
  numeroOcorrenciasInteressado(id_matricula) {
    const defer = this.q.defer();

    this.getResource().numeroOcorrenciasInteressado({id_matricula}, (response) => {
      this.setStorageNumeroOcorrencias(response.ocorrencias);
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

  /**
   * Seta o número de ocorrências no localStorage
   * @param numOcorrencias
   */
  setStorageNumeroOcorrencias(numOcorrencias) {
    if (numOcorrencias) {
      this.storage["num-ocorrencias-interessado"] =  numOcorrencias;
    }
  }

  /**
   * Retorna o número de ocorrências do localStorage
   */
  getStorageNumeroOcorrencias() {
    return +this.storage["num-ocorrencias-interessado"];
  }

  /**
   * Realiza o registro do tramite de ciencia relacionado as ocorrencias
   * @param id_matricula (integer)
   * @returns {Function|promise}
   * @author Guilherme Gomes <guilherme.noronha@unyleya.com.br>
   */
  registraCienciaOcorrencia(id_matricula) {
    const defer = this.q.defer();

    this.getResource().registraCienciaOcorrencia({id_matricula}, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

}

export default OcorrenciaService;
