import RestService from './RestService';

/**
 * Classe TabelaPrecoService
 * @author Neemias Santos <neemiassantos.16@gmail.com>
 */
class TabelaPrecoService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
   * @param $q
   * @param LSService
   * @author Neemias Santos <neemiassantos.16@gmail.com>
   */
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'tabela-preco';
    this.addCustomService('getTabelaPreco', CONFIG.urlApi + '/tabela-preco', 'GET', {}, true);
  }

  /**
   * Retorna a tabela de preco da entidade
   * @param idEntidade
   * @returns {Function}
   */
  getTabelaPreco(idEntidade) {
    const defer = this.q.defer();
    this.getResource()
        .getTabelaPreco({id_entidade: idEntidade}, (response) => { //funcao login
          defer.resolve(response);
        }, (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }
}

export default TabelaPrecoService;
