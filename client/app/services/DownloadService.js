/**
 * Created by Unyleya06 on 23/09/2016.
 */

import RestService from './RestService';

class DownloadService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Necessário para que o Angular faça injeções nas Factories e Services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as Factories e Services injetados na classe Controller.

    this.api = 'download';
    this.CONFIG = CONFIG;
    this.$http = $http;

    this.addCustomService('getAuth', CONFIG.urlApi + '/download/auth', 'GET', {});
  }

  /***
   * Retorna o token de autenticação de acordo com a Action de download correspondente.
   * @param downloadAction {string} - action a ser acessada para o download
   * (ex.: para acessar a planoPagamentoAction, passe 'plano-pagamento')
   * @returns {object}
   */

  getAuth(downloadAction) {
    const defer = this.q.defer();

    this.getResource()
      .getAuth({download_action: downloadAction},
        (response) => {
          defer.resolve(response);
        },
        (response) => {
          defer.reject(response);
        });
    return defer.promise;
  }

  /***
   * Gera a string base a ser utilizada como parâmetro no window.open para efetuar os downloads.
   * @param response - response do getAuth()
   * @param downloadAction - downloadAction passado como parâmetro no getAuth()
   * @returns {string}
   */
  geraUrlDownload(response, downloadAction) {
    return this.CONFIG.urlG2 +
      'api-v2/download/'
      + downloadAction
      + '?'
      + 'st_action='
      + response.st_action
      + '&st_token='
      + response.st_token;
  }

  /***
   * Gera a string com a URL para download do plano de pagamento.
   * @param response - response do getAuth()
   * @param downloadAction - downloadAction passado como parâmetro no getAuth()
   * @param idMatricula - ID de matrícula do aluno
   * @param idEntidade - ID da entidade da matrícula
   * @returns {string}
   */
  geraUrlDownloadPlanoPagamento(response, downloadAction, idMatricula, idEntidade) {
    let urlDonwload = this.geraUrlDownload(response, downloadAction);
    urlDonwload += '&id_entidade=' + idEntidade + '&id_matricula=' + idMatricula;
    return urlDonwload;
  }

  /**
   * Gera a string com a URL para download do imposto de renda.
   *
   * @param response
   * @param downloadAction
   * @param idMatricula
   * @param idEntidade
   * @returns {string}
   */
  getUrlDownloadImpostoRenda(response, downloadAction, idMatricula, idEntidade) {
    let urlDonwload = this.geraUrlDownload(response, downloadAction);
    urlDonwload += '&id_entidade=' + idEntidade + '&id_matricula=' + idMatricula;
    return urlDonwload;
  }

  /**
   * Gera a string com a URL para download da tabela de preço
   *
   * @param response
   * @param downloadAction
   * @param idEntidade
   * @returns {string}
   */
  geraUrlDownloadTabelaPreco(response, downloadAction, idEntidade) {
    let urlDownloadTabelaPreco = this.geraUrlDownload(response, downloadAction);
    urlDownloadTabelaPreco += '&id_entidade=' + idEntidade;
    return urlDownloadTabelaPreco;
  }

  /**
   * Gera a string com a URL para download dos contratos
   *
   * @param response
   * @param downloadAction
   * @param idEntidade
   * @param idUsuario
   * @param idMatricula
   * @returns {string}
   */
  geraUrlDownloadContratos(response, downloadAction, idEntidade, idUsuario, idMatricula) {
    let urlDownloadContrato = this.geraUrlDownload(response, downloadAction);
    urlDownloadContrato += '&id_entidade=' + idEntidade + '&id_matricula=' + idMatricula + '&id_usuario=' + idUsuario;
    return urlDownloadContrato;
  }

  /**
   * Retornar uma promise com response igual a string da URL direta do download
   * @param idMatricula
   * @param idDocumentoDigital
   * @returns {Promise}
   */
  geraUrlDownloadDocumentoDigital(idMatricula, idDocumentoDigital) {
    return this.getAuth('documento-digital')
      .then((response) => {
        return this.geraUrlDownload(response, 'documento-digital')
          + '&id_matricula=' + idMatricula
          + '&id_documentodigital=' + idDocumentoDigital;
      });
  }

  /**
   * Gera a string com a URL para download da Relação de Disciplinas
   *
   * @param downloadAction
   * @param idMatricula
   * @param idEntidade
   * @returns {string}
   */
  geraUrlDownloadRelacaoDisciplinas(downloadAction, idMatricula, idEntidade) {
    let urlDownloadRelacaoDisciplinas =
      this.CONFIG.urlG2 +
      'download/' + downloadAction+'?';
    urlDownloadRelacaoDisciplinas += 'id_entidade=' + idEntidade;
    urlDownloadRelacaoDisciplinas += '&id_matricula=' + idMatricula;
    return urlDownloadRelacaoDisciplinas;
  }

  /**
   * Pesquisa a tabela de preço cadastrada e retona para a controllers um json com as informaçoes
   *
   * @param urlTabelaPreco
   * @returns {Function}
   */
  pesquisaTabelaPreco(urlTabelaPreco) {
    const defer = this.q.defer();

    this.addCustomService('urlDownloadTabelaPreco', urlTabelaPreco, 'GET', {});

    this.$http.get(urlTabelaPreco).then((response) => {
      defer.resolve(response);
    },
    (response) => {
      defer.reject(response);
    });
    return defer.promise;
  }

  /**
   * Pesquisa os contratos cadastrados e retona para a controllers um json com as informaçoes
   *
   * @param urlContrato
   * @returns {Function}
   */
  pesquisaContratos(urlContrato) {
    const defer = this.q.defer();

    this.addCustomService('urlDownloadContrato', urlContrato, 'GET', {});

    this.$http.get(urlContrato).then((response) => {
      defer.resolve(response);
    },
    (response) => {
      defer.reject(response);
    });
    return defer.promise;
  }

  /**
   * Gera a string com a URL para download de um documento
   *
   * @param response
   * @param downloadAction
   * @param stContrato
   * @param stNomePasta
   * @returns {string}
   */
  geraUrlDocumento(response, downloadAction, stContrato, stNomePasta) {
    let urlDocumentoContrato = this.geraUrlDownload(response, downloadAction);
    urlDocumentoContrato += '&st_upload=' + stContrato + '&st_nomepasta=' + stNomePasta;
    return urlDocumentoContrato;
  }

  /**
   * Gera a url para download do documento de certificado digital
   * @param idMatricula
   * @param idDocumentoDigitalMatricula
   * @returns {Promise}
   */
  geraUrlDownloadCertificadoDigital(idMatricula, idDocumentoDigitalMatricula) {
    return this.getAuth('certificado-digital')
      .then((response) => {
        return this.geraUrlDownload(response, 'certificado-digital')
          + '&id_matricula=' + idMatricula
          + '&id_documentodigitalmatricula=' + idDocumentoDigitalMatricula;
      });
  }

  /**
   * Gera a url para exibição do modelo de documento
   * @param idMatricula
   * @param idTextoSistema
   * @param draft
   * @param papel
   * @returns {Promise}
   */
  geraUrlModeloDocumento(idMatricula, idTextoSistema, draft, papel) {
    return this.getAuth('modelo-documento')
      .then((response) => {
        return this.geraUrlDownload(response, 'modelo-documento')
          + '&id_textosistema=' + idTextoSistema
          + '&id_matricula=' + idMatricula
          + '&draft=' + draft
          + '&papel=' + papel;
      });
  }

}

export default DownloadService;
