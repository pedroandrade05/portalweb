import RestService from './RestService';

class EsquemaConfiguracaoService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService, CursoAlunoService, EsquemaConfiguracaoConstante) {
    'ngInject';

    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'esquema-configuracao';

    this.CursoAlunoService = CursoAlunoService;
    this.EsquemaConfiguracaoConstante = EsquemaConfiguracaoConstante;
  }

  isEsquema(idMatricula, idEsquemaConfiguracao) {
    const matriculaAtiva = this.CursoAlunoService.getStorageCursoAtivo(idMatricula);
    return +matriculaAtiva.id_esquemaconfiguracao === idEsquemaConfiguracao;
  }

  isGraduacao(idMatricula) {
    return this.isEsquema(idMatricula, this.EsquemaConfiguracaoConstante.GRADUACAO);
  }

  isPosGraduacao(idMatricula) {
    return this.isEsquema(idMatricula, this.EsquemaConfiguracaoConstante.POS_GRADUACAO);
  }

  isEscolaTecnicaSemestral(idMatricula) {
    return this.isEsquema(idMatricula, this.EsquemaConfiguracaoConstante.ESCOLA_TECNICA_SEMESTRAL);
  }

  isGraduacaoEnfermagem(idMatricula) {
    return this.isEsquema(idMatricula, this.EsquemaConfiguracaoConstante.GRADUACAO_ENFERMAGEM);
  }

  isConcurso(idMatricula){
    return this.isEsquema(idMatricula, this.EsquemaConfiguracaoConstante.CONCURSO);
  }

}

export default EsquemaConfiguracaoService;
