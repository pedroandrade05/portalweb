import RestService from './RestService';

class IndiqueGanheService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'indique-ganhe';

    this.addCustomService('criarAfiliado', CONFIG.urlApi + '/indique-ganhe/criar-afiliado', 'POST', {});
    this.addCustomService('salvarIndicado', CONFIG.urlApi + '/indique-ganhe/salvar-indicado', 'POST', {});
    this.addCustomService('getIndicacoes', CONFIG.urlApi + '/indique-ganhe/get-indicacoes', 'GET', {}, false);
    this.addCustomService('getConfirmacaoTermos', CONFIG.urlApi + '/indique-ganhe/get-confirmacao-termos', 'GET', {}, false);
    this.addCustomService('getRegulamento', CONFIG.urlApi + '/indique-ganhe/get-regulamento', 'GET', {}, false);
    this.addCustomService('getBlResgatarRenovacaoIG', CONFIG.urlApi + '/indique-ganhe/get-bl-resgatar-renovacao', 'GET', {}, false);
    this.addCustomService('getComoFunciona', CONFIG.urlApi + '/indique-ganhe/get-como-funciona', 'GET', {}, false);
    this.addCustomService('getParceiros', CONFIG.urlApi + '/indique-ganhe/get-parceiros', 'GET', {}, true);
    this.addCustomService('resgatarCupons', CONFIG.urlApi + '/indique-ganhe/resgatar-cupons', 'GET', {}, true);
  }

  criarAfiliado(id_matricula_afiliado) {
    const defer = this.q.defer();

    this.getResource().criarAfiliado({
      id_matricula: id_matricula_afiliado
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  salvarIndicado(codcontratoafiliacao, id_matricula, nome, telefone, ddd, email, linha_de_negocio) {
    const defer = this.q.defer();

    this.getResource().salvarIndicado({
      codcontratoafiliacao,
      id_matricula,
      nome,
      telefone,
      ddd,
      email,
      linha_de_negocio
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  getIndicacoes(idMatricula) {
    const defer = this.q.defer();

    this.getResource()
      .getIndicacoes({
        id_matricula: idMatricula
      }, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });

    return defer.promise;
  }

  getRegulamento(idMatricula) {
    const defer = this.q.defer();

    this.getResource()
      .getRegulamento({
        id_matricula: idMatricula
      }, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });

    return defer.promise;
  }

  getConfirmacaoTermos(idUsuario) {
    const defer = this.q.defer();

    this.getResource().getConfirmacaoTermos({
      id_usuario: idUsuario
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  getBlResgatarRenovacaoIG(idMatricula) {
    const defer = this.q.defer();

    this.getResource()
      .getBlResgatarRenovacaoIG({
        id_matricula: idMatricula
      }, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      }
    );

    return defer.promise;
  }


  getComoFunciona(idMatricula) {
    const defer = this.q.defer();

    this.getResource()
      .getComoFunciona({
        id_matricula: idMatricula
      }, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });

    return defer.promise;
  }

  resgatarCupons(idMatricula, idSituacao, nuValor, idParceiro) {
    const defer = this.q.defer();

    this.getResource()
      .resgatarCupons({
        id_matricula: idMatricula,
        id_situacao: idSituacao,
        nu_valor: nuValor,
        id_afiliadoindicacaoparceiro: idParceiro
      }, (response) => {
        defer.resolve(response);
      }, (error) => {
        defer.reject(error);
      });

    return defer.promise;
  }

  getParceiros() {
    const defer = this.q.defer();

    this.getResource().getParceiros({}, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

}

export default IndiqueGanheService;
