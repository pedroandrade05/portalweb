import RestService from './RestService';

/**
 * Classe MenuService
 */
class RenovacaoService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services

    super(CONFIG, $resource, $http, $q, LSService);

    this.api = 'renovacao';
    this.CONFIG = CONFIG;

    this.addCustomService('verificarRenovacao', CONFIG.urlApi + '/renovacao/verificar-renovacao', 'POST', {}, false);
    this.addCustomService('buscarDisciplinasRenovacao', CONFIG.urlApi + '/renovacao/buscar-disciplinas-renovacao', 'POST', {}, false);
    this.addCustomService('calcularContrato', CONFIG.urlApi + '/renovacao/calculo-contrato', 'POST', {}, false);
    this.addCustomService('salvarGrade', CONFIG.urlApi + '/grade-disciplina/salva-grade', 'POST', {}, false);
    this.addCustomService('buscarLancamentos', CONFIG.urlApi + '/renovacao/buscar-lancamentos', 'POST', {}, true);
  }

  /**
   * Verificar se o aluno pode renovar
   * @param idMatricula
   */
  verificarRenovacao(idMatricula) {
    const defer = this.q.defer();

    this.getResource().verificarRenovacao({
      id_matricula: idMatricula
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }


  /**
   * Buscar as disciplinas para a renovação
   * @param idVendaProduto
   * @param idUsuario
   * @param idProjetoPedagogico
   * @param idEntidade
   * @param idMatricula
   * @param idTurma
   */
  buscarDisciplinasRenovacao(idVendaProduto, idUsuario, idProjetoPedagogico, idEntidade, idMatricula, idTurma) {
    const defer = this.q.defer();

    this.getResource().buscarDisciplinasRenovacao({
      id_vendaproduto: idVendaProduto,
      id_usuario: idUsuario,
      id_projetopedagogico: idProjetoPedagogico,
      id_entidade: idEntidade,
      id_matricula: idMatricula,
      id_turma: idTurma
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  /**
   * Buscar as valor do contrato segundo as disciplinas selecionadas
   * @param params
   */
  calcularContrato(params) {
    const defer = this.q.defer();

    this.getResource().calcularContrato(params || {}, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  salvarGrade(disciplinasSelecionadas, modelo) {
    const defer = this.q.defer();

    this.getResource().salvarGrade({
      disciplinasSelecionadas: disciplinasSelecionadas,
      modelo: modelo
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

  buscarLancamentos(idVendaProduto, blEntrada) {
    const defer = this.q.defer();
    this.getResource().buscarLancamentos(
      {
        id_venda: idVendaProduto,
        bl_entrada: blEntrada
      },
      (response) => {
        defer.resolve(response);
      },
      (error) => {
        defer.reject(error);
      });

    return defer.promise;
  }

}

export default RenovacaoService;
