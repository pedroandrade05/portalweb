import RestService from './RestService';

/**
 *
 * Classe para tratar as Atividades Complementares
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 *
 */
class AtividadeComplementarService extends RestService {

  constructor(CONFIG, $resource, $http, $q, LSService, Upload) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);
    this.api = 'atividade-complementar';

    this.Upload = Upload;

    this.addCustomService('getHorasProjeto', CONFIG.urlApi + '/' + this.api + '/get-horas-projeto', 'GET', {}, false);
  }

  enviarAtividadeComplementar(data) {
    return this.Upload.upload({
      url: this.CONFIG.urlApi + '/atividade-complementar/create',
      data: data
    });
  }

  getHorasProjeto(idMatricula) {
    const defer = this.q.defer();

    this.getResource().getHorasProjeto({
      id_matricula: idMatricula
    }, (response) => {
      defer.resolve(response);
    }, (error) => {
      defer.reject(error);
    });

    return defer.promise;
  }

}

export default AtividadeComplementarService;
