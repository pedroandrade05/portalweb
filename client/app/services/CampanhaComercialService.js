import RestService from './RestService';

/**
 * Classe CampanhaComercialService
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 */
class CampanhaComercialService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
  */
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller
    this.api = 'campanha-comercial';

    this.addCustomService('count', CONFIG.urlApi + '/campanha-comercial/count', 'GET', {});

  }

}

export default CampanhaComercialService;
