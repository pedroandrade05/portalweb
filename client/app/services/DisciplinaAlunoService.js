import RestService from "./RestService";

/**
 * Classe DisciplinaAlunoService
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 */
class DisciplinaAlunoService extends RestService {

  /**
   * Metodo construtor
   * @param CONFIG
   * @param $resource
   * @param $http
   * @author Elcio Guimarães <elcioguimaraes@gmail.com>
   */
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
    super(CONFIG, $resource, $http, $q, LSService); //Entrega para a classe pai as factorys e services injetadas na classe controller

    //Parametro obrigatorio para utilizacao pela service RestService
    this.api = 'disciplina-aluno';
    this.LSService = LSService;
    this.$q = $q;

    this.addCustomService('listConteudoDisciplina', CONFIG.urlApi + '/disciplina-aluno/list-conteudo-disciplina', 'GET', {});

  }


  /**
   * Retorna todas as disciplinas de uma matrícula
   * Reescrevendo o getAll para usar o LSFactory
   * @param idMatricula - matrícula
   * @param blMoodle - buscar dados do moodle ou não?
   * @param clearCache - busca do cache ou da api ?
   * @returns {*}
   */
  getAll = (params, clearCache = false) => {
    //verifica se esses parametros foi passado ou seta o valor padrão
    const blMoodle = params.bl_moodle || false;
    const idMatricula = params.id_matricula || null;

    const token_name = this.api + '.get-all.' + idMatricula.toString() + '.' + blMoodle.toString();

    return this.$q((resolve, reject) => {
      const exist_token = this.LSService.get(token_name, true);
      if (exist_token && !clearCache) {
        resolve(exist_token);
      }
      else {
        this.getResource().getAll(params, (response) => {
          this.LSService.set(token_name, response, 1440);
          resolve(response);
        }, (error) => {
          reject(error);
        });
      }

    });

  }


  /**
   * Retorna os dados da disciplina
   * @param idSalaDeAula
   * @param idMatricula
   * @param idEntidade
   * @param blMoodle
   * @param blDadosDisciplina
   * @param clearCache
   * @returns {*}
   */
  retornarDisciplina = (idSalaDeAula, idMatricula, idEntidade, blMoodle = 1, blDadosDisciplina = 0, clearCache = false) => {
    return this.$q((resolve, reject) => {
      this.getAll({
        id_matricula: idMatricula,
        id_saladeaula: idSalaDeAula,
        id_entidade: idEntidade,
        bl_moodle: blMoodle,
        bl_disciplina: blDadosDisciplina
      }, clearCache).then((response) => {
        const disciplina = response[0];
        disciplina ? resolve(disciplina) : reject("Disciplina não encontrada");
      }, (error) => {
        reject(error);
      });
    });
  }

}

export default DisciplinaAlunoService;
