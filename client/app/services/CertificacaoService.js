import RestService from "./RestService";

/***
 * Class CertificacaoService
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class CertificacaoService extends RestService {
  constructor(CONFIG, $resource, $http, $q, LSService) {
    'ngInject';
    super(CONFIG, $resource, $http, $q, LSService);

    this.$q = $q;
    this.api = 'certificacao';
    this.addCustomService('detalhesCertificacaoMatricula', CONFIG.urlApi + '/matricula-certificacao', 'GET', {});
    this.addCustomService('buscarCertificacao', CONFIG.urlApi + '/certificacao', 'GET', {});
    this.addCustomService('confirmarCertificacao', CONFIG.urlApi + '/certificacao/confirmar', 'GET', {});
    this.addCustomService('getInformacoesCertificacao', CONFIG.urlApi + '/certificacao/get-informacoes-certificacao', 'GET', {});
  }

  /** @function detalhesCertificacaoMatricula
   * Retorna os dados na tb_matriculacertificacao baseando-se na matrícula. */

  detalhesCertificacaoMatricula(idMatricula) {
    const defer = this.$q.defer();
    this.getResource().detalhesCertificacaoMatricula({id_matricula: idMatricula}, (success) => {
      defer.resolve(success);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

  /**
   * Retorna informações sobre o estado da certificação
   * @param idMatricula
   * @returns {*}
   */
  buscarCertificacao(idMatricula) {
    const defer = this.q.defer();
    this.getResource().buscarCertificacao({id_matricula: idMatricula}, (success) => {
      defer.resolve(success);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

  /**
   * Realiza a confirmação dos dados para certificação
   * @param idMatricula
   * @returns {*}
   */
  confirmarCertificacao(idMatricula) {
    const defer = this.q.defer();
    this.getResource().confirmarCertificacao({id_matricula: idMatricula}, (success) => {
      defer.resolve(success);
    }, (error) => {
      defer.reject(error);
    });
    return defer.promise;
  }

  /**
   * Retorna informações sobre a certificação
   * @param idMatricula
   * @returns {*}
   */
  getInformacoesCertificacao(idMatricula) {
    const defer = this.q.defer();
    this.getResource()
      .getInformacoesCertificacao({id_matricula: idMatricula},
        (success) => {
          defer.resolve(success);
        },
        (error) => {
          defer.reject(error);
        });
    return defer.promise;
  }

}

export default CertificacaoService;
