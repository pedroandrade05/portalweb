const Utils = {
  removerAcento: (text) => {
    return text.toLowerCase()
      .replace(new RegExp('[ÁÀÂÃ]', 'gi'), 'a')
      .replace(new RegExp('[ÉÈÊ]', 'gi'), 'e')
      .replace(new RegExp('[ÍÌÎ]', 'gi'), 'i')
      .replace(new RegExp('[ÓÒÔÕ]', 'gi'), 'o')
      .replace(new RegExp('[ÚÙÛ]', 'gi'), 'u')
      .replace(new RegExp('[Ç]', 'gi'), 'c');
  },

  isEmailValid: (email) => {
    if (!angular.isString(email)) {
      return false;
    }
    return !!email.match(/^([a-zA-Z0-9_.-]+)@([a-zA-Z0-9_-]+)\.([a-zA-Z]{2,5})(\.[a-zA-Z]{2,5})?$/);
  },

  mask: (str, mask = '') => {
    let maskared = '';
    let sIndex = 0;

    for (let mIndex = 0; mIndex < mask.length; mIndex++) {
      if (!str[sIndex]) {
        break;
      }

      if (mask[mIndex] === '#') {
        maskared += str[sIndex];
        sIndex++;
      }
      else {
        maskared += mask[mIndex];
      }
    }

    return maskared;
  }

};

export default Utils;
