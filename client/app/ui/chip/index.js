import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Services from '../../services/Services';
import template from './template.html';
import './styles.scss';

class UIChipController {
  name = 'UIChip';

  constructor() {
    'ngInject';
  }
}

const uiChipModule = angular.module('uiChip', [
  uiRouter,
  Services.name
]).component('uiChip', {
  bindings: {
    title: '@',
    icon: '@',
    block: '<',
    borderStyle: '@'
  },
  template,
  restrict: 'E',
  transclude: true,
  controller: UIChipController,
  controllerAs: 'ui'
});

export default uiChipModule;
