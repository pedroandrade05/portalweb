import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Services from '../../services/Services';
import template from './template.html';
import './styles.scss';

class UIContainerController {
  name = 'UIContainer';

  constructor() {
    "ngInject";
  }
}

const uiContainerModule = angular.module('uiContainer', [
  uiRouter,
  Services.name
]).component('uiContainer', {
  bindings: {},
  template,
  restrict: 'E',
  transclude: true,
  controller: UIContainerController,
  controllerAs: 'ui'
});

export default uiContainerModule;
