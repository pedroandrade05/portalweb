import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Services from '../../services/Services';
import template from './template.html';
import './styles.scss';

class UICardController {
  name = 'UICard';

  constructor($window, $sce) {
    "ngInject";

    this.$window = $window;
    this.$sce = $sce;
    this.botaoVisivel = !this.button && (this.url || angular.isFunction(this.onClick));
  }

  acessarUrl() {

    if (angular.isFunction(this.onClick)){
      this.onClick();
      return;
    }

    const win = this.$window.open(this.url, '_blank');
    if (win) {
      win.focus();
    }
    else {
      this.toaster.notify('warning', '', 'É necessário permitir que o navegador abra uma nova aba.');
    }
  }


}

const uiCardModule = angular.module('uiCard', [
  uiRouter,
  Services.name
]).component('uiCard', {
  bindings: {
    text: '@',
    button: '@',
    url: '@',
    desabilitado: '<',
    img: '<',
    imgStyle: '@',
    onClick: '<'
  },
  template,
  restrict: 'E',
  transclude: true,
  controller: UICardController,
  controllerAs: 'ui'
});

export default uiCardModule;
