import angular from 'angular';

import Box from './box';
import Chip from './chip';
import Container from './container';
import Input from './input';
import Title from './title';
import Card from './card';

export default angular.module('app.ui', [
  Box.name,
  Chip.name,
  Container.name,
  Input.name,
  Title.name,
  Card.name
]);
