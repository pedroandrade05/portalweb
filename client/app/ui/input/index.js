import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Services from '../../services/Services';
import template from './template.html';
import './styles.scss';

class UIInputController {
  name = 'UIInput';

  constructor() {
    "ngInject";
  }
}

const uiInputModule = angular.module('uiInput', [
  uiRouter,
  Services.name
]).component('uiInput', {
  bindings: {
    label: '@'
  },
  template,
  restrict: 'E',
  transclude: true,
  controller: UIInputController,
  controllerAs: 'ui'
});

export default uiInputModule;
