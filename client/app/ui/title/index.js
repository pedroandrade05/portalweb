import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Services from '../../services/Services';
import template from './template.html';
import './styles.scss';

class UITitleController {
  name = 'UITitle';

  constructor() {
    "ngInject";
  }
}

const uiTitleModule = angular.module('uiTitle', [
  uiRouter,
  Services.name
]).component('uiTitle', {
  bindings: {
    title: '@',
    subtitle: '@',
    icon: '@',
    divider: '<'
  },
  template,
  restrict: 'E',
  controller: UITitleController,
  controllerAs: 'ui'
});

export default uiTitleModule;
