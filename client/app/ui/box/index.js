import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Services from '../../services/Services';
import template from './template.html';
import './styles.scss';

class UIBoxController {
  name = 'UIBox';

  constructor() {
    "ngInject";

    this.key = this.key || '_' + Math.random().toString(36).substr(2, 9);

    this.style = {};

    if (angular.isString(this.border)) {
      this.style.border = this.border;
    }

    if (angular.isObject(this.border)) {
      this.style = Object.assign(this.style, this.border);
    }
  }
}

const uiBoxModule = angular.module('uiBox', [
  uiRouter,
  Services.name
]).component('uiBox', {
  bindings: {
    key: '@',
    border: '<',
    title: '@',
    footer: '@',
    hiddenContent: '@',
    showMorePosition: '@'
  },
  template,
  restrict: 'E',
  transclude: true,
  controller: UIBoxController,
  controllerAs: 'ui'
});

export default uiBoxModule;
