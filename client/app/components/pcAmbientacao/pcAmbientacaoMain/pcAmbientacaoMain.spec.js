import PcAmbientacaoMainModule from './PcAmbientacaoMain';
import PcAmbientacaoMainController from './PcAmbientacaoMain.controller';
import PcAmbientacaoMainComponent from './pcAmbientacaoMain.component';
import PcAmbientacaoMainTemplate from './PcAmbientacaoMain.html';

describe('PcAmbientacaoMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcAmbientacaoMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcAmbientacaoMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcAmbientacaoMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcAmbientacaoMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcAmbientacaoMainController);
    });
  });
});
