import template from './pcAmbientacaoMain.html';
import controller from './pcAmbientacaoMain.controller';
import './pcAmbientacaoMain.scss';

const pcAmbientacaoMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcAmbientacaoMainComponent;
