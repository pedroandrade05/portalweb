class pcAmbientacaoMainController {
  constructor(NotifyService, AmbientacaoService, $state, $window, $sce) {
    'ngInject';

    this.NotifyService = NotifyService;
    this.AmbientacaoService = AmbientacaoService;

    this.params = $state.params;
    this.$window = $window;
    this.$sce = $sce;

    this.idMatricula = $state.params.idMatriculaAtiva;
    this.urlAmbientacao = null;

    this.getUrlAmbientacao();
  }

  getUrlAmbientacao() {
    this.AmbientacaoService.getUrlAmbientacao(this.idMatricula)
      .then((response) => {
        this.urlAmbientacao = this.$sce.trustAsResourceUrl(response.st_urlambientacao);
      });
  }
}

export default pcAmbientacaoMainController;
