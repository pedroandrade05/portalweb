import topArrow from "../../../../assets/img/icons/left-top-arrow.png";
import anexo from "../../../../assets/img/icons/anexo.png";
import removeImg from "../../../../assets/img/icons/img-remove.png";
import iconClose from "../../../../assets/img/icons/icon-close.png";

class PcChamadosShowController {
  constructor(CONFIG, OcorrenciaService, $state, $window, NotifyService, InteracaoService, DownloadService, $uibModal, $scope, $localStorage, $rootScope, ModalService) {
    'ngInject';

    this.storage = $localStorage;
    this.OcorrenciaService = OcorrenciaService;
    this.InteracaoService = InteracaoService;
    this.DownloadService = DownloadService;
    this.CONFIG = CONFIG;
    this.$window = $window;
    this.state = $state;
    this.rootScope = $rootScope;
    this.ModalService = ModalService;

    this.name = 'pcChamadosShow';

    this.vchamado = null;
    this.vinteracoes = [];
    this.resposta = false;
    this.reabrir = false;

    this.topArrow = topArrow;
    this.anexo = anexo;
    this.removeImg = removeImg;
    this.iconClose = iconClose;

    this.loading = false;
    this.st_tramiteInteracao = '';

    this.toaster = NotifyService;
    this.dialog = $uibModal;
    this.scope = $scope;

    const id_ocorrencia = $state.params.id_ocorrencia ? $state.params.id_ocorrencia : null;
    this.visualizarOcorrencia(id_ocorrencia);

  }

  visualizarOcorrencia = (id_ocorrencia) => {
    this.loading = true;

    this.OcorrenciaService.getResource().getOneBy({id: id_ocorrencia}
      , (response) => {
        this.vchamado = response;
        this.listaInteracoesOcorrencia(id_ocorrencia);
        this.loading = false;
      }, (error) => {
        this.loading = false;
        this.toaster.notify(error.data);
      });

  };

  abrirArquivo = (arquivo, nomePasta) => {
    const downloadAction = 'download-s3-direto';
    this.DownloadService.getAuth(downloadAction).then((auth) => {

      const urlDocumento = this.DownloadService.geraUrlDocumento(auth, downloadAction, arquivo, nomePasta);

      const win = this.$window.open(urlDocumento, '_blank');

      if (win) {
        win.focus();
      }
      else {
        this.toaster.notify('warning', '', 'É necessário permitir que o navegador abra uma nova aba.');
      }
    });
  }

  /** Lista as interações de um chamado**/
  listaInteracoesOcorrencia = (id_ocorrencia) => {
    if (id_ocorrencia) {
      this.loading = true;
      this.InteracaoService.getResource().getAll({id_ocorrencia}, (response) => {
        this.loading = false;

        //Verificando a posição 1, pois quando se cria uma ocorrência adicionando o anexo, sempre é criado um novo tramite para salva-lo.
        if (response) {
          const lastIndex = response.length-1;
          this.vchamado.st_upload = response[lastIndex].st_upload;
          this.vchamado.st_diruploadgeral = response[lastIndex].st_diruploadgeral;
        }

        this.vinteracoes = response;
        this.cont = 0;
        response.forEach((item, indice) => {

          if (!this.vinteracoes[indice].bl_lido && this.vinteracoes[indice].id_tramite) {
            this.InteracaoService.lerInteracao(this.vinteracoes[indice].id_tramite);
          }

          if ((item.id_usuario !== this.vchamado.id_usuariointeressado) && this.cont === 0) {
            this.vinteracoes[indice].bl_button = true;
            this.cont++;
          }
        });

        if (this.cont === 0) {
          this.vinteracoes[this.vinteracoes.length - 1].bl_button = true;
        }

        this.OcorrenciaService.numeroOcorrenciasInteressado(this.vchamado.id_matricula).then((response) => {
          this.rootScope.numeroOcorrenciasInteressado = response.ocorrencias;
        });

      }, (error) => {
        this.loading = false;
        this.toaster.notify(error.data);
      });
    }
  }

  /**
   Verifica se o usuario da ocorrencia é o mesmo logado
   Utilizada para verificar se pode reabrir um chamado
   **/
  isSameUserSession = (id_usuario) => {
    let answer = false;
    if (id_usuario) {
      if (this.storage.user_id === id_usuario) {
        answer = true;
      }
    }
    return answer;
  }

  close = () => {
    this.state.go('app.chamados.list');
  };

  /**
   * Remove um arquivo que havia sido adicionado.
   */
  removerArquivo = () => {
    this.files = null;
  }

  /**
   * Salva uma nova interação com anexo.
   * Recebe os dados a serem salvos e envia para Service, efetuando o cadastro da interaçao
   *
   * @returns {boolean}
   */
  saveInteracao = (ocorrencia) => {

    if (!this.st_tramiteInteracao) {
      this.toaster.notify('error', 'Preençha o campo com sua interação.');
      return false;
    }

    const tramite = {
      st_tramite: this.st_tramiteInteracao,
      id_entidade: ocorrencia.id_entidade,
      id_evolucao: ocorrencia.id_evolucao,
      id_usuariointeressado: ocorrencia.id_usuariointeressado,
      bl_visivel: true,
      bl_lido: true,
      file: this.files,
      id_ocorrencia: ocorrencia.id_ocorrencia
    };

    this.InteracaoService.novaInteracao(tramite).success((response) => {
      this.vinteracoes.unshift(response);
      this.removerArquivo();
      this.st_tramiteInteracao = '';
      this.resposta = false;
      this.OcorrenciaService.numeroOcorrenciasInteressado(this.vchamado.id_matricula).then((response) => {
        this.rootScope.numeroOcorrenciasInteressado = response.ocorrencias;
      });
    }).error((error) => {
      this.toaster.notify(error);
    });
  };

  /**
   * Recebe um arquivo e faz as verificaçoes necessarias para permitir o upload
   * caso fique fora do permitido limpa o campo para nao enviar o arquivo
   */
  verificacaoArquivo = () => {

    if (this.files) {
      var formatosPermitidos = ["application/msword", "application/pdf", "image/png", "image/jpg", "image/jpeg", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"];

      if (formatosPermitidos.indexOf(this.files.type) === -1) {
        this.toaster.notify('error', this.storage.user_shortname + this.MensagemPadrao.ARQUIVO_FORA_DO_FORMATO_PERMITIDO);
        this.files = {};
      }

      if (this.files.size > 10000000) {
        this.toaster.notify('error', this.storage.user_shortname + this.MensagemPadrao.ARQUIVO_TAMANHO);
        this.files = {};
      }
    }
  }

  saveReabrirChamado = (ocorrencia) => {
    if (!this.st_tramiteInteracao) {
      this.toaster.notify('error', 'Preençha o campo com sua interação.');
      return false;
    }

    this.loading = true;

    const tramite = {
      st_tramite: this.st_tramiteInteracao,
      id_entidade: ocorrencia.id_entidade,
      id_evolucao: ocorrencia.id_evolucao,
      id_situacao: ocorrencia.id_situacao,
      id_usuariointeressado: ocorrencia.id_usuariointeressado,
      bl_visivel: true,
      bl_lido: true,
      file: this.files,
      id_ocorrencia: ocorrencia.id_ocorrencia
    };

    this.OcorrenciaService.reabrirChamado(tramite).then(() => {
      this.avisoReaberturaOcorrencia(tramite.id_ocorrencia);
    }, (error) => {
      this.toaster.notify(error.data);
    }).finally(() => {
      this.loading = false;
    });
  };

  avisoReaberturaOcorrencia = () => {
    this.ModalService.show({
      title: 'Ocorrência reaberta com sucesso!',
      body: 'Um atendente vai te responder em breve.',
      confirmButton: 'Ok, fechar',
      cancelButton: null
    }).then(() => {
      this.state.go('app.chamados.list');
    });
  }

  avisoEncerramentoOcorrencia = () => {
    this.ModalService.show({
      title: 'Serviço de atenção',
      body: 'Deseja realmente encerrar a ocorrência?',
      confirmButton: 'Confirmar',
      cancelButton: 'Cancelar'
    }).then(() => {
      this.saveEncerrarChamado();
    }).catch(() => {
      this.ModalService.close();
    });
  }

  saveEncerrarChamado = () => {
    this.loading = true;

    const tramite = {
      id_ocorrencia: this.vchamado.id_ocorrencia,
      bl_lido: true
    };

    this.OcorrenciaService.encerrarChamado(tramite).then(() => {
      this.state.go('app.chamados.list');
    }, (error) => {
      this.toaster.notify(error.data);
    }).finally(() => {
      this.loading = false;
    });
  }
}

export default PcChamadosShowController;
