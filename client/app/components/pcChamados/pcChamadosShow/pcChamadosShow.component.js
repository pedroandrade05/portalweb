import template from './pcChamadosShow.html';
import controller from './pcChamadosShow.controller';
import './pcChamadosShow.scss';

const pcChamadosShowComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcChamadosShowComponent;
