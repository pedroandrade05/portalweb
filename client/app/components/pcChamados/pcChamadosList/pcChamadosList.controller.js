import topArrow from "../../../../assets/img/icons/top-arrow.png";
import rigthArrow from "../../../../assets/img/icons/rigth-arrow.png";
import atention from "../../../../assets/img/icons/atention.png";

class PcChamadosListController {

  constructor(CONFIG, OcorrenciaService, $state, $window, NotifyService, EvolucaoOcorrencia, SituacaoOcorrencia, $rootScope) {

    'ngInject';

    this.OcorrenciaService = OcorrenciaService;

    this.EvolucaoOcorrencia = EvolucaoOcorrencia;

    this.SituacaoOcorrencia = SituacaoOcorrencia;

    this.topArrow = topArrow;

    this.rigthArrow = rigthArrow;

    this.atention = atention;

    this.$state = $state;

    this.name = 'pcChamadosList';

    this.rootScope = $rootScope;

    this.loading = false;

    this.chamados = [];

    //Mostra mensagem de erro caso ocorra algum erro na requisição de buscas as ocorrencias
    this.toaster = NotifyService;

    //Chama o método de buscar as ocorrencias
    this.buscarOcorrencias();

    // Variável para marcar o id ativo
    this.activeId;
  }

   /**
   Busca as ocorrencias do usuário logado
   Listagem
   **/
  buscarOcorrencias = () => {
    this.loading = true;
    var id_matricula = this.$state.params.idMatriculaAtiva ? this.$state.params.idMatriculaAtiva : null;
    this.OcorrenciaService.getResource().getAll({id_matricula: id_matricula}, (response) => {
      response.forEach((item, indice) => {
        switch (item.tramite.id_evolucaoocorrencia) {
        case this.EvolucaoOcorrencia.AGUARDANDO_INTERESSADO: {
          response[indice].condicaoEvolucao = 'AGUARDANDO_INTERESSADO';
          break;
        }
        case this.EvolucaoOcorrencia.INTERESSADO_INTERAGIU: {
          response[indice].condicaoEvolucao = 'AGUARDANDO_ATENDENTE';
          break;
        }
        case this.EvolucaoOcorrencia.AGUARDANDO_ATENDENTE: {
          response[indice].condicaoEvolucao = 'AGUARDANDO_ATENDENTE';
          break;
        }
        case this.EvolucaoOcorrencia.REABERTURA_REALIZADA_INTERESSADO: {
          response[indice].condicaoEvolucao = 'AGUARDANDO_ATENDENTE';
          break;
        }
        case this.EvolucaoOcorrencia.AGUARDANDO_ATENDIMENTO: {
          response[indice].condicaoEvolucao = 'AGUARDANDO_ATENDIMENTO';
          break;
        }
        case this.EvolucaoOcorrencia.SENDO_ATENDIDO: {
          response[indice].condicaoEvolucao = 'AGUARDANDO_ATENDIMENTO';
          break;
        }
        default: {
          response[indice].condicaoEvolucao = 'DEFAULT';
          break;
        }
        }

        if (response[indice].id_situacao === this.SituacaoOcorrencia.ENCERRADA) {
          response[indice].condicaoEvolucao = 'ENCERRADO';
        }
      });

      this.OcorrenciaService.numeroOcorrenciasInteressado(id_matricula).then((response) => {
        this.rootScope.numeroOcorrenciasInteressado = response.ocorrencias;
      });
      this.loading = false;
      this.chamados = response;
    }, (error) => {
      this.loading = false;
      this.toaster.notify(error.data);
    });
  }

}

export default PcChamadosListController;
