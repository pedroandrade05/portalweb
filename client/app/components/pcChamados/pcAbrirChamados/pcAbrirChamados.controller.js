import removeImg from "../../../../assets/img/icons/img-remove.png";

class PcAbrirchamadosController {
  constructor(CONFIG, OcorrenciaService, $uibModal, DisciplinaAlunoService, $state, NotifyService, $scope, $document, $localStorage, $rootScope, AuthService, Upload, MensagemPadrao, TbSistema, ModalService) {
    'ngInject';

    this.name = 'pcAbrirChamados';
    this.state = $state;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.document = $document;
    this.storage = $localStorage;
    this.AuthService = AuthService;
    this.Upload = Upload;
    this.MensagemPadrao = MensagemPadrao;
    this.TbSistema= TbSistema;
    this.dialog = $uibModal;
    this.ModalService = ModalService;

    this.OcorrenciaService = OcorrenciaService;
    this.DisciplinaAlunoService = DisciplinaAlunoService;

    this.toaster = NotifyService;
    this.removeImg = removeImg;
    this.loading = false;

    this.ocorrencia = {};
    this.assuntosOcorrencia = [];
    this.subAssuntosOcorrencia = [];
    this.matriculaAluno = [];
    this.salasAluno = [];
    this.escolha = [];

    //carrega o inicio do formulario
    this.loadDataForm();

    this.arquivo_ocorrencia = null;
    this.enviandoArquivo = false;
  }

  /**
   * No portal WEB só é possivel ter uma matriculaAluno.
   * Carrega as salas e os assuntos de acordo com o id_matricula e id_entidade, respectivamente.
   */
  loadDataForm = () => {
    this.loading = true;
    this.ocorrencia = {};
    this.assuntosOcorrencia = [];
    this.subAssuntosOcorrencia = [];
    this.matriculaAluno = [];
    this.matriculaAluno = angular.fromJson(this.storage['matricula.' + this.state.params.idMatriculaAtiva]);
    this.ocorrencia.id_matricula = this.state.params.idMatriculaAtiva;
    this.ocorrencia.id_entidade = this.matriculaAluno.id_entidade;

    this.getSalaAulaAlunoByMatricula(this.matriculaAluno);
    this.getAssuntosOcorrencia();
  };

  /**
   ** Retorna salas de aula por matricula do aluno
   **/
  getSalaAulaAlunoByMatricula = (matricula) => {
    this.loading = true;

    this.DisciplinaAlunoService.getResource().getAll({
      id_entidade: matricula.id_entidade,
      id_matricula: matricula.id_matricula,
      id_projetopedagogico: matricula.id_curso,
      bl_moodle: 0,
      bl_disciplina: 0
    }, (disciplinas) => {
      this.salasAluno = disciplinas;
      this.loading = false;
    }, (error) => {
      this.loading = false;
      this.toaster.notify(error.data);
    });
  };

  /**
   ** Retorna assuntos da entidade
   **/
  getAssuntosOcorrencia = () => {
    this.loading = true;

    this.OcorrenciaService.getResource().assuntos({
      id_matricula: this.matriculaAluno.id_matricula,
      bl_interno: 0
    }, (assuntos) => {
      this.assuntosOcorrencia = assuntos;
      this.loading = false;
    }, (error) => {
      this.loading = false;
      this.toaster.notify(error.data);
    });
  };

  /**
   ** Busca subassuntos depois do change do combo de assuntos
   **/
  getSubassunto = () => {
    const id_assuntocopai = this.ocorrencia.id_assuntopai;

    if (!id_assuntocopai) {
      this.subAssuntosOcorrencia = [];
      return;
    }

    this.OcorrenciaService.getResource().assuntos({
      id_assuntocopai: id_assuntocopai,
      bl_interno: 0
    }, (subassuntos) => {
      this.subAssuntosOcorrencia = subassuntos;
    }, (error) => {
      this.toaster.notify(error.data);
    });
  };

  /**
   * Remove um arquivo que havia sido adicionado.
   */
  removerArquivo = () => {
    this.ocorrencia.files = null;
  }

  /**
   ** Retorna ao componente chamadosList.
   **/
  close = () => {
    this.state.go('app.chamados.list');
  };

  /**
   * Salva ocorrencia, parâmetros e arquivo anexado
   */
  save = () => {

    this.loading = true;
    this.ocorrencia.dt_ultimainteracao = new Date();
    this.ocorrencia.id_sistema = this.TbSistema.PortalAluno;
    this.ocorrencia.bl_lido = true;

    this.OcorrenciaService.create(this.ocorrencia).success((response) => {
      this.loading = false;
      this.avisoCriacaoOcorrencia(response.id_ocorrencia);
    }).error((error) => {
      this.loading = false;
      this.toaster.notify(error);
    });
  };

  /**
   * Recebe um arquivo e faz as verificaçoes necessarias para permitir o upload
   * caso fique fora do permitido limpa o campo para nao enviar o arquivo
   */
  verificacaoArquivo = () => {

    if (this.ocorrencia.files) {
      var formatosPermitidos = ["application/msword", "application/pdf", "image/png", "image/jpg", "image/jpeg", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"];

      if (formatosPermitidos.indexOf(this.ocorrencia.files.type) === -1) {
        this.toaster.notify('error', this.storage.user_shortname + this.MensagemPadrao.ARQUIVO_FORA_DO_FORMATO_PERMITIDO);
        this.ocorrencia.files = {};
      }

      if (this.ocorrencia.files.size > 15000000) {
        this.toaster.notify('error', this.storage.user_shortname + this.MensagemPadrao.ARQUIVO_TAMANHO);
        this.ocorrencia.files = {};
      }

    }
  }

  avisoCriacaoOcorrencia = (id_ocorrencia) => {
    this.ModalService.show({
      title: 'Ocorrência enviada com sucesso!',
      body: 'Um atendente vai te responder em breve.',
      confirmButton: 'Ok, fechar',
      cancelButton: null
    }).then(() => {
      this.state.go('app.chamados.show', {id_ocorrencia: id_ocorrencia});
    });
  }
}

export default PcAbrirchamadosController;
