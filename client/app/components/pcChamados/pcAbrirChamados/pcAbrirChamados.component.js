import template from './pcAbrirChamados.html';
import controller from './pcAbrirChamados.controller';
import './pcAbrirChamados.scss';

const pcAbrirChamadosComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcAbrirChamadosComponent;
