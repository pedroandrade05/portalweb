
class PcChamadosCabecalhoController {

  constructor(CONFIG, OcorrenciaService, $state, EsquemaConfiguracaoService) {

    'ngInject';

    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao($state.params.idMatriculaAtiva);
    this.OcorrenciaService = OcorrenciaService;
    this.state = $state;
    this.name = 'pcChamadosCabecalho';

  }
}

export default PcChamadosCabecalhoController;
