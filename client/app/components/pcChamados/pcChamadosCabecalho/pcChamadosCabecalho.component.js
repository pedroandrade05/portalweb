import template from './pcChamadosCabecalho.html';
import controller from './pcChamadosCabecalho.controller';
import './pcChamadosCabecalho.scss';

const pcChamadosCabecalhoComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcChamadosCabecalhoComponent;
