
class pcChamadosMainController {

  constructor(CONFIG, OcorrenciaService, $state) {

    'ngInject';

    this.OcorrenciaService = OcorrenciaService;
    this.state = $state;

    this.state.go('app.chamados.list');
  }
}

export default pcChamadosMainController;
