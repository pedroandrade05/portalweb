import template from './pcBrinquedotecaMain.html';
import controller from './pcBrinquedotecaMain.controller';
import './pcBrinquedotecaMain.scss';

const pcBrinquedotecaMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcBrinquedotecaMainComponent;
