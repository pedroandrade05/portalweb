import PcBrinquedotecaMainModule from './pcBrinquedotecaMain';
import PcBrinquedotecaMainController from './pcBrinquedotecaMain.controller';
import PcBrinquedotecaMainComponent from './pcBrinquedotecaMain.component';
import PcBrinquedotecaMainTemplate from './pcBrinquedotecaMain.html';

describe('PcBrinquedotecaMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcBrinquedotecaMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcBrinquedotecaMainController();
    };
  }));

  describe('Module', () => {

  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcBrinquedotecaMainComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcBrinquedotecaMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcBrinquedotecaMainController);
    });
  });
});
