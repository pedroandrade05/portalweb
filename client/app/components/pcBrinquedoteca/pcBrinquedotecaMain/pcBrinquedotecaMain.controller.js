class pcBrinquedotecaMainController {
  constructor(
    $state,
    $sce
  ) {
    'ngInject';

    this.params = $state.params;
    this.$sce = $sce;
  }
}

export default pcBrinquedotecaMainController;
