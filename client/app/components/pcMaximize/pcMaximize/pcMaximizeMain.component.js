import template from './pcMaximizeMain.html';
import controller from './pcMaximizeMain.controller';
import './pcMaximizeMain.scss';

const pcMaximizeMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcMaximizeMainComponent;
