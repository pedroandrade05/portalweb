class pcMaximizeMainController {
  constructor(NotifyService, MaximizeService, $state, $window, $sce) {
    'ngInject';

    this.NotifyService = NotifyService;
    this.MaximizeService = MaximizeService;

    this.params = $state.params;
    this.$window = $window;
    this.$sce = $sce;

    this.idMatricula = $state.params.idMatriculaAtiva;
    this.urlMaximize = null;

    this.prepareMaximizeUrl();
  }

  prepareMaximizeUrl() {
    this.MaximizeService.prepareUserLogin()
      .then((res) => {
        this.urlMaximize = this.$sce.trustAsResourceUrl(res);

        const win = this.$window.open(this.urlMaximize, '_blank');

        if (!win) {
          return this.NotifyService.notify('warning', '', 'É necessário permitir que o navegador abra uma nova aba.');
        }

        win.focus();
      })
      .catch((err) => {
        console.log(err);
      });
  }
}

export default pcMaximizeMainController;
