import template from './pcMensagensList.html';
import controller from './pcMensagensList.controller.js';
import './pcMensagensList.scss';

const pcMensagensListComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcMensagensListComponent;
