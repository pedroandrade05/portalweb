import PcMensagensListModule from './pcMensagensList';
import PcMensagensListController from './pcMensagensList.controller.js';
import PcMensagensListComponent from './pcMensagensList.component.js';
import PcMensagensListTemplate from './pcMensagensList.html';

describe('PcMensagensList', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcMensagensListModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcMensagensListController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

    // controller specs

  });

  describe('Template', () => {

    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcMensagensListComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcMensagensListTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcMensagensListController);
    });
  });
});
