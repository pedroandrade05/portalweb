const _ = require('lodash');

import loadingGif from '../../../../assets/img/loading.gif';

class PcMensagensListController {

  constructor(CONFIG, MensagemAgendamentoService, $state, NotifyService, $rootScope) {
    'ngInject';
    this.MensagemAgendamentoService = MensagemAgendamentoService;

    this.$state = $state;

    this.$rootScope = $rootScope;

    this.name = 'pcMensagensList';

    this.loadingGif = loadingGif;

    this.loading = false;

    this.mensagens = [];

    this.toaster = NotifyService;

    this.selected = 0;

    /**
     * Marca qual mensagem foi selecionada pelos links "anterior" e "próximo"
     */
    this.$rootScope.$on('navegarMensagemEvent', (event, data) => {
      this.selected = data;
    });

    this.buscarMensagens();

  }

  /**
   * Navega para a mensagem escolhida e à marca como lida.
   * @param event
   * @param mensagem
   * @param $index
     */
  visualizarMensagem = (event, mensagem, $index) => {
    this.selected = $index;
    event.preventDefault();
    this.$state.go('app.mensagens.show', {
      idMatriculaMensagem: mensagem.id_matricula,
      id_enviomensagem: mensagem.id_enviomensagem
    });
    this.MensagemAgendamentoService.getResource().updateEvolucao({
      id_enviomensagem: mensagem.id_enviomensagem,
      id_matricula: mensagem.id_matricula,
      id_evolucao: 43
    });
  }

  /**
   * Busca todas as mensagens
   */
  buscarMensagens = () => {
    const idMatricula = +this.$state.params.idMatriculaAtiva;
    const idEnvioMensagem = +this.$state.params.id_enviomensagem;

    this.loading = true;

    this.MensagemAgendamentoService.getResource().getAll({id_matricula: idMatricula},
      (response) => {
        this.loading = false;
        this.mensagens = _.orderBy(response, ['dt_enviar'], ['desc']);

        if (idEnvioMensagem && idMatricula) {
          this.selected = _.findIndex(this.mensagens, (o) => {
            return (o.id_enviomensagem === idEnvioMensagem && o.id_matricula === idMatricula);
          });
        }

        this.$rootScope.$broadcast('buscarMensagensFinishEvent', _.orderBy(response, ['dt_enviar'], ['desc']));
      }, (error) => {
        this.loading = false;
        this.toaster.notify(error.data);
      });

  }


}

export default PcMensagensListController;
