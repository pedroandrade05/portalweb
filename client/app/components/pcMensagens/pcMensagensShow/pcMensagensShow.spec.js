import PcMensagensShowModule from './pcMensagensShow';
import PcMensagensShowController from './pcMensagensShow.controller';
import PcMensagensShowComponent from './pcMensagensShow.component';
import PcMensagensShowTemplate from './pcMensagensShow.html';

describe('PcMensagensShow', () => {
  let $rootScope, $state, makeController;

  beforeEach(window.module(PcMensagensShowModule.name));
  beforeEach(inject((_$rootScope_,_$state_) => {
    $rootScope = _$rootScope_;
    $state = _$state_;
    makeController = () => {
      return new PcMensagensShowController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcMensagensShowComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcMensagensShowTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcMensagensShowController);
    });
  });
});
