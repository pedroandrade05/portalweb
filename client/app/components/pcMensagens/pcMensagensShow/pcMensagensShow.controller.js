import loadingGif from "../../../../assets/img/loading.gif";
import _ from 'lodash';

class PcMensagensShowController {
  constructor(CONFIG, MensagemAgendamentoService, $state, NotifyService, $rootScope) {
    'ngInject';

    this.MensagemAgendamentoService = MensagemAgendamentoService;
    this.name = 'pcMensagensShow';
    this.vmensagem = null;
    this.vmensagens = [];
    this.loadingGif = loadingGif;
    this.loading = false;
    this.toaster = NotifyService;
    this.indexActive = -1;
    this.$rootScope = $rootScope;
    this.$state = $state;
    const id_enviomensagem = $state.params.id_enviomensagem ? $state.params.id_enviomensagem : null;
    this.id_matricula = $state.params.idMatriculaMensagem ? $state.params.idMatriculaMensagem : null;
    if (id_enviomensagem) {
      this.buscarMensagem(id_enviomensagem);
      this.buscarMensagens();
    }
    else {
      this.buscarMensagens(true);
    }

  }

  /**
   * Seta qual índice do array corresponde à mensagem exibida
   */
  setIndexActive() {
    if (this.vmensagens.length > 0 && this.vmensagem) {
      this.indexActive = _.findIndex(this.vmensagens, (o) => {
        return (o.id_enviomensagem === this.vmensagem.id_enviomensagem && o.id_matricula === this.vmensagem.id_matricula);
      });
    }
  }

  /**
   * Método para permitir a navegação entre mensagens usando os links "anterior" e "próximo"
   * @param index
   */
  navegarMensagem(index) {
    if (this.vmensagens && this.vmensagens[index] && index > -1) {
      this.indexActive = index;
      this.vmensagem = this.vmensagens[index];
      this.$rootScope.$broadcast('navegarMensagemEvent', index);
      this.$state.go('app.mensagens.show', {
        id_enviomensagem: this.vmensagem.id_enviomensagem,
        idMatriculaMensagem: this.vmensagem.id_matricula
      }, {notify: false});
    }
    else {
      this.toaster.notify('error', '', 'Mensagem não encontrada!');
    }
  }

  /**
   * Retorna a mensagem atual em caso de acesso direto ou atualização da página
   * @param id_enviomensagem
   */
  buscarMensagem = (id_enviomensagem) => {
    this.loading = true;
    this.MensagemAgendamentoService.getResource().getOneBy({
      id_matricula: this.id_matricula,
      id_enviomensagem
    }, (response) => {
      this.vmensagem = response;
      this.setIndexActive();
      this.loading = false;
    }, (error) => {
      this.loading = false;
      this.toaster.notify(error.data);
    });
  }

  /**
   * Busca todas as mensagens e mostra a última mensagem na tela
   * @param setarMensagem
   */
  buscarMensagens = (setarMensagem = false) => {
    this.loading = true;
    this.MensagemAgendamentoService.getAll({id_matricula: this.id_matricula}).then((response) => {
      if (response.length > 0) {
        this.vmensagens = _.orderBy(response, ['dt_enviar'], ['desc']);

        if (setarMensagem)          {
          this.vmensagem = this.vmensagens[0];
        }

        this.setIndexActive();
      }
      this.loading = false;
    }, (error) => {
      this.toaster.notify(error.data);
    }).finally(() => {
      this.loading = false;
    });

  }

}

export default PcMensagensShowController;
