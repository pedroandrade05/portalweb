import template from './pcMensagensShow.html';
import controller from './pcMensagensShow.controller';
import './pcMensagensShow.scss';

const pcMensagensShowComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcMensagensShowComponent;
