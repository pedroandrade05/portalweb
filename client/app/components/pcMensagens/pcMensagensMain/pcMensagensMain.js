/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcMensagensMainComponent from './pcMensagensMain.component.js';

//Import do modulo de services do portal
import Services from '../../../services/Services';

//Modulo principal do componente
const pcMensagensMainModule = angular.module('pcMensagensMain', [
  uiRouter,
  Services.name //Injecao modulo services do portal
]).config(($stateProvider) => {
  "ngInject"; //Inject necessario para que o angular faca as injecoes as factorys e services

    //Definicao de rotas para o componente
  $stateProvider
        .state('app.mensagens', {
          url: '/:idMatriculaAtiva/avisos',
          views: {
            content: {
              template: '<pc-mensagens-main  class="component-maxheight scroll-wrapper"></pc-mensagens-main>'
            }
          }, onEnter (AuthService, $state) {
            const params = $state.params;

                //grava log de acesso
            AuthService.gravaLogAcesso($state.current.name, params);
          }
        });
})

    .component('pcMensagensMain', pcMensagensMainComponent);

export default pcMensagensMainModule;
