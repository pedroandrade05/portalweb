import template from './pcMensagensMain.html';
import controller from './pcMensagensMain.controller.js';
import './pcMensagensMain.scss';

const pcMensagensComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcMensagensComponent;
