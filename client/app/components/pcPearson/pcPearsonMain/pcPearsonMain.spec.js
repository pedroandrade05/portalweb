import PcPearsonMainModule from './pcPearsonMain';
import PcPearsonMainController from './pcPearsonMain.controller';
import PcPearsonMainComponent from './pcPearsonMain.component';
import PcPearsonMainTemplate from './pcPearsonMain.html';

describe('PcPearsonMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcPearsonMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcPearsonMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcPearsonMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcPearsonMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcPearsonMainController);
    });
  });
});
