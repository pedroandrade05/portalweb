/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcPearsonMainComponent from './pcPearsonMain.component';

//Import do modulo de services do portal
import Services from '../../../services/Services'; // Talvez você precise alterar este destino

//Modulo principal do componente
const pcPearsonMainModule = angular.module('pcPearsonMain', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])

  .config(($stateProvider) => {
    'ngInject';

    // Rotas
    $stateProvider
      .state('app.biblioteca-virtual', {
        url: '/:idMatriculaAtiva/biblioteca-virtual',

        views: {
          content: {
            template: '<pc-pearson-main></pc-pearson-main>'
          }
        },

        onEnter (AuthService, $state) {
          const params = $state.params;

          //grava log de acesso
          AuthService.gravaLogAcesso($state.current.name, params);
        }
      });
  })

  .component('pcPearsonMain', pcPearsonMainComponent);

export default pcPearsonMainModule;
