import template from './pcPearsonMain.html';
import controller from './pcPearsonMain.controller';
import './pcPearsonMain.scss';

const pcPearsonMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcPearsonMainComponent;
