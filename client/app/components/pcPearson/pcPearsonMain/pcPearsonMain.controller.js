import logoBibliotecaVirtual from '../../../../assets/img/bibliotecas/biblioteca-virtual.png';
import logoMinhaBiblioteca from '../../../../assets/img/bibliotecas/minha-biblioteca.png';
import loadingGif from "../../../../assets/img/loading.gif";

class PcPearsonMainController {
  constructor(
    /* Úteis */ $state, $window, $sce, $localStorage,
    /* Services */ NotifyService, PearsonService, CursoAlunoService, EsquemaConfiguracaoService,
    /* Constantes */ MensagemPadraoConstante
  ) {
    'ngInject';

    this.NotifyService = NotifyService;
    this.PearsonService = PearsonService;
    this.CursoAlunoService = CursoAlunoService;
    this.EsquemaConfiguracaoService = EsquemaConfiguracaoService;

    this.MensagemPadraoConstante = MensagemPadraoConstante;
    this.loadingGif = loadingGif;

    // Logo das bibliotecas
    this.logoBibliotecaVirtual = logoBibliotecaVirtual;
    this.logoMinhaBiblioteca = logoMinhaBiblioteca;

    this.params = $state.params;
    this.$window = $window;
    this.$sce = $sce;
    this.storage = $localStorage;
    this.isGraduacao = this.EsquemaConfiguracaoService.isGraduacao(this.params.idMatriculaAtiva);
    this.matriculaAtiva = this.CursoAlunoService.getStorageCursoAtivo(this.params.idMatriculaAtiva);

    this.loading = false;
    this.status = this.isGraduacao ? null : 'Buscando bilbioteca virtual...';
    this.url = null;
    this.integracao = {};

    if (this.isGraduacao) {
      // Lista de cards exibidos em tela
      this.cards = [
        {
          img: this.logoBibliotecaVirtual,
          text: 'Um acervo de mais de 8 mil eBooks com 15 anos de história e uma navegação intuitiva.',
          onClick: this.buscarIntegracao
        }
      ];

      if (!$localStorage.BibliotecasVirtuaisState) {
        $localStorage.BibliotecasVirtuaisState = "";
      }

      if (!$localStorage.MinhaBibliotecaState) {
        $localStorage.MinhaBibliotecaState = {};
      }

      this.state = {
        title: $localStorage.BibliotecasVirtuaisState,
        textoInformacoes: $localStorage.MinhaBibliotecaState.textoInformacoes
      };

      this.getInformacoes();
      this.getMostrarMinhaBiblioteca(this.matriculaAtiva.id_curso);
      return;
    }

    this.buscarIntegracao();
  }

  buscarIntegracao = () => {
    const params = {
      id_matricula: this.params.idMatriculaAtiva
    };

    this.PearsonService.getResource().getOneBy(params, (response) => {
      this.integracao = response;
      this.integracao.st_caminho = this.$sce.trustAsResourceUrl(this.integracao.st_caminho);

      this.status = 1;
      this.url = `${response.st_caminho}?login=${response.st_usuario}&token=${response.st_login}`;

      const win = this.$window.open(this.url, '_blank');

      if (win) {
        win.focus();
      }
      else {
        this.NotifyService.notify('warning', '', 'É necessário permitir que o navegador abra uma nova aba.');
      }
    }, (error) => {
      this.status = error.data.message;
      this.NotifyService.notify('error', '', this.status);
    });
  }

  getMostrarMinhaBiblioteca(id_projetopedagogico) {
    this.CursoAlunoService.getMostrarMinhaBiblioteca(id_projetopedagogico)
      .then((response) => {

        this.matriculaAtiva.bl_minhabiblioteca = response.bl_minhabiblioteca;
        this.state.title = response.bl_minhabiblioteca ? 'Bibliotecas Virtuais' : 'Biblioteca Virtual';
        this.storage.BibliotecasVirtuaisState = this.state.title;

        // Se a opção bl_minhabiblioteca estiver habilitada, exibe o card na tela
        if (response.bl_minhabiblioteca) {
          const minhaBiblioteca = {
            img: this.logoMinhaBiblioteca,
            text: 'Milhares de títulos técnicos, acadêmicos e científicos de diversas áreas do conhecimento.',
            onClick: this.abrirMinhaBiblioteca
          };
          this.cards.push(minhaBiblioteca);
        }
      });
  }

  getInformacoes() {

    this.loading = !this.state.textoInformacoes;

    return this.CursoAlunoService
      .getMensagemPadrao(
        this.MensagemPadraoConstante.BIBLIOTECAS_VIRTUAIS
      )
      .then((response) => {
        const json = response.toJSON();

        this.state.textoInformacoes = json;
        this.storage.MinhaBibliotecaState.textoInformacoes = json;
      }).finally(() => {
        this.loading = false;
      });
  }

  abrirMinhaBiblioteca() {
    this.$window.open('https://dliportal.zbra.com.br/Login.aspx?key=unyleya', '_blank');
  }
}

export default PcPearsonMainController;
