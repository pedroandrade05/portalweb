import PcCarteiraEstudantilMainModule from './pcCarteiraEstudantilMain';
import PcCarteiraEstudantilMainController from './pcCarteiraEstudantilMain.controller';
import PcCarteiraEstudantilMainComponent from './pcCarteiraEstudantilMain.component';
import PcCarteiraEstudantilMainTemplate from './pcCarteiraEstudantilMain.html';

describe('PcCarteiraEstudantilMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcCarteiraEstudantilMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcCarteiraEstudantilMainController();
    };
  }));

  describe('Module', () => {

  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcCarteiraEstudantilMainComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcCarteiraEstudantilMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcCarteiraEstudantilMainController);
    });
  });
});
