import loadingGif from "../../../../assets/img/loading.gif";
import banner from "../../../../assets/img/carteira-estudante.png";

class pcCarteiraEstudantilMainController {
  constructor(
    /* Úteis */ $sce, $http, $state,
    /* Services */ CursoAlunoService, LSService,
    /* Constantes */ EvolucaoMatriculaConstante
  ) {
    'ngInject';

    // Úteis
    this.$sce = $sce;
    this.$http = $http;
    this.state = $state;

    // Services
    this.LSService = LSService;
    this.CursoAlunoService = CursoAlunoService;

    // Dados Matrícula
    this.matriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);

    // Constantes
    this.link = 'https://documentomeiaentrada.com.br/cadastro/unyleya';

    // Imagem do banner
    this.banner = banner;

    // Inicia a variavel de loading
    this.loading = false;

    // Carrega a imagem do gif
    this.loadingGif = loadingGif;

    this.permissao = +this.matriculaAtiva.id_evolucao === EvolucaoMatriculaConstante.CURSANDO;
  }
}

export default pcCarteiraEstudantilMainController;
