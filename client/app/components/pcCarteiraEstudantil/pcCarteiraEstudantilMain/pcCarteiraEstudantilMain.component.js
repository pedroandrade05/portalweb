import template from './pcCarteiraEstudantilMain.html';
import controller from './pcCarteiraEstudantilMain.controller';
import './pcCarteiraEstudantilMain.scss';

const pcCarteiraEstudantilMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcCarteiraEstudantilMainComponent;
