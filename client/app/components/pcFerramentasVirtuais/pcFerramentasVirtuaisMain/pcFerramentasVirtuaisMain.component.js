import template from './pcFerramentasVirtuaisMain.html';
import controller from './pcFerramentasVirtuaisMain.controller';
import './pcFerramentasVirtuaisMain.scss';

const pcFerramentasVirtuaisMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'fv'
};

export default pcFerramentasVirtuaisMainComponent;
