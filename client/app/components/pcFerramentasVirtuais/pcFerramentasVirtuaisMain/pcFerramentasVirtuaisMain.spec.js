import PcFerramentasVirtuaisMainModule from './pcFerramentasVirtuaisMain';
import PcFerramentasVirtuaisMainController from './pcFerramentasVirtuaisMain.controller';
import PcFerramentasVirtuaisMainComponent from './pcFerramentasVirtuaisMain.component';
import PcFerramentasVirtuaisMainTemplate from './pcFerramentasVirtuaisMain.html';

describe('PcFerramentasVirtuaisMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcFerramentasVirtuaisMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcFerramentasVirtuaisMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcFerramentasVirtuaisMainComponent;

    it('includes the intended template',() => {
        expect(component.template).to.equal(PcFerramentasVirtuaisMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
        expect(component.controller).to.equal(PcFerramentasVirtuaisMainController);
    });
  });
});
