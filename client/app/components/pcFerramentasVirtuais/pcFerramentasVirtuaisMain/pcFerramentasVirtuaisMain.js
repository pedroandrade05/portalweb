/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcFerramentasVirtuaisMainComponent from './pcFerramentasVirtuaisMain.component';

//Import do modulo de services do portal
import Services from '../../../services/Services'; // Talvez você precise alterar este destino

//Modulo principal do componente
const pcFerramentasVirtuaisMainModule = angular.module('pcFerramentasVirtuaisMain', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])

  .config(($stateProvider) => {
    "ngInject"; //Inject necessario para que o angular faca as injecoes as factorys e services

    //Definindo rotas para componente.
    $stateProvider
      .state('app.ferramentas-virtuais', {
        url: '/:idMatriculaAtiva/ferramentas-virtuais',
        views: {
          content: {
            template: '<pc-ferramentas-virtuais-main></pc-ferramentas-virtuais-main>'
          }
        }
      });
  })

  .component('pcFerramentasVirtuaisMain', pcFerramentasVirtuaisMainComponent);

export default pcFerramentasVirtuaisMainModule;
