import logoCopiar from "../../../../assets/img/icons/Shape.svg";
import politicas365 from "../../../../assets/files/politicas365.html";
import alertify from "alertifyjs/build/alertify.min.js";
import loadingGif from "../../../../assets/img/loading.gif";

class PcFerramentasVirtuaisMainController {
  constructor(
    /* Úteis */ $state, $window, $sce, ModalService, $localStorage, EtapasIntegracaoOffice365, $rootScope,
    /* Services */ NotifyService, Office365Service) {
    'ngInject';

    //Services
    this.NotifyService = NotifyService;
    this.Office365Service = Office365Service;

    //Úteis
    this.params = $state.params;
    this.$window = $window;
    this.$sce = $sce;
    this.ModalService = ModalService;
    this.$localStorage = $localStorage;
    this.EtapasIntegracaoOffice365 = EtapasIntegracaoOffice365;
    this.$rootScope = $rootScope;
    this.politicas365 = politicas365;
    this.url = this.Office365Service.getUrlAutenticaUsuario();
    this.urlDominioAzureAD = this.Office365Service.getUrlDominioAzureAD();
    this.logoCopiar = logoCopiar;

    // Outros
    this.loading = true;
    this.loadingGif = loadingGif;
    this.idUsuario = $localStorage.user_id;
    this.idEntidade = $localStorage.user_entidade;
    this.stPassword = this.$rootScope.stPasswordOffice365 ? this.$rootScope.stPasswordOffice365 : null;
    this.showTermo = false;
    this.mostraLogin = false;
  }

  etapaUsuario = () => {
    this.etapaIntegracao = this.EtapasIntegracaoOffice365.RECUPERAR_DADOS_USUARIO;
    this.getUsuarioAzure();
  }

  atribuirGrupoUsuarioOffice365() {
    this.Office365Service.postAtribuirGrupoUsuario(this.idUsuario, this.idEntidade)
      .then(() => {
        this.etapaIntegracao = this.EtapasIntegracaoOffice365.EXIBIR_DADOS_USUARIO;
        this.showTermo = false;
        this.msgSucesso();
        this.loading = false;
      })
      .catch(() => {
        this.loading = false;
        this.NotifyService.notify('error', '', 'Houve um erro ao criar seu usuário!');
      });
  }

  criarUsuario() {
    this.loading = true;
    this.etapaIntegracao = this.EtapasIntegracaoOffice365.CRIAR_USUARIO;
    this.Office365Service.postCriarUsuario(this.idUsuario)
      .then(
        (response) => {
          this.stLoginAzure = response.mensagem.st_login;
          this.stPassword = response.mensagem.st_senha;
          this.$rootScope.stPasswordOffice365 = this.stPassword;
          this.atribuirGrupoUsuarioOffice365();
        })
      .catch(() => {
        this.loading = false;
        this.NotifyService.notify('error', '', 'Houve um erro ao criar seu usuário!');
      });
  }

  msgSucesso() {
    this.ModalService.show({
      title: 'Conta criada com sucesso!',
      body: 'Você criou sua conta UnyLeya!<br>Agora você pode acessar as ferramentas <br>virtuais disponíveis para você!'
    }).then(() => {
      return true;
    });
  }

  getUsuarioAzure() {
    this.loading = true;
    this.Office365Service.getLocalizaUsuario(this.idUsuario)
      .then((response) => {
        this.idUsuarioAzure = response.id_usuarioazure;
        this.stLoginAzure = response.st_loginazure;
        this.loading = false;
        this.etapaIntegracao = this.EtapasIntegracaoOffice365.EXIBIR_DADOS_USUARIO;
      })
      .catch(() => {
        this.loading = false;
        this.etapaIntegracao = this.EtapasIntegracaoOffice365.CRIAR_USUARIO;
      });
  }

  abrirTermo() {
    this.accordion = false;
    this.showTermo = !this.showTermo;
  }

  verificarUsuario() {
    this.loading = true;
    if (!this.idUsuarioAzure) {
      this.criarUsuario();
    }
  }

  copyToClipboard = (option) => {
    if (option === 0) {
      navigator.clipboard.writeText(this.stLoginAzure);
    }
    if (option === 1) {
      navigator.clipboard.writeText(this.stPassword);
    }
    this.NotifyService.notify('success', '', 'Copiado com sucesso!');
  }

  redefinirSenhaUsuario() {
    this.loading = true;
    this.Office365Service.redefinirSenha(this.idUsuario, this.idEntidade).then(
      (response) => {
        this.loading = false;
        alertify.success('Senha redefinida com sucesso! Acesse seu e-mail ' + response.st_email + ' e siga as intruções.', '20');
      })
      .catch(() => {
        this.loading = false;
        this.NotifyService.notify('warning', '', 'Houve um erro ao redefinir a senha.');
      });
  }

  confirmarRedefinirSenha() {
    if (confirm("Confirma iniciar a redefinição de sua senha?")) {
      this.redefinirSenhaUsuario();
    }
  }

  mostrarLogin() {
    this.mostraLogin = !this.mostraLogin;
  }
}

export default PcFerramentasVirtuaisMainController;
