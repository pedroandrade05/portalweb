import template from './pcCursosLivresMain.html';
import controller from './pcCursosLivresMain.controller';
import './pcCursosLivresMain.scss';

const pcCursosLivresMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcCursosLivresMainComponent;
