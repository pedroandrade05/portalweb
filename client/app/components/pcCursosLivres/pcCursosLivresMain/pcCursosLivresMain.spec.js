import PcCursosLivresMainModule from './pcCursosLivresMain';
import PcCursosLivresMainController from './pcCursosLivresMain.controller';
import PcCursosLivresMainComponent from './pcCursosLivresMain.component';
import PcCursosLivresMainTemplate from './pcCursosLivresMain.html';

describe('PcCursosLivresMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcCursosLivresMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcCursosLivresMainController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcCursosLivresMainComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcCursosLivresMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcCursosLivresMainController);
    });
  });
});
