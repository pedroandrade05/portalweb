import template from './pcAtividadeComplementarMain.html';
import controller from './pcAtividadeComplementarMain.controller';
import './pcAtividadeComplementarMain.scss';

const pcAtividadeComplementarMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcAtividadeComplementarMainComponent;
