/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcAtividadeComplementarMainComponent from './pcAtividadeComplementarMain.component';

//Import do modulo de services do portal
import Services from '../../../services/Services'; // Talvez você precise alterar este destino

//Modulo principal do componente
const pcAtividadeComplementarMainModule = angular.module('pcAtividadeComplementarMain', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])

  .config(($stateProvider) => {
    "ngInject";

    // Routing
    $stateProvider
      .state('app.atividade-complementar', {
        url: '/:idMatriculaAtiva/atividade-complementar',
        views: {
          content: {
            template: '<pc-atividade-complementar-main></pc-atividade-complementar-main>'
          }
        },
        onEnter(AuthService, $state) {
          const params = $state.params;

          // Gravar log de acesso
          AuthService.gravaLogAcesso($state.current.name, params);
        }
      });
  })

  .component('pcAtividadeComplementarMain', pcAtividadeComplementarMainComponent);

export default pcAtividadeComplementarMainModule;
