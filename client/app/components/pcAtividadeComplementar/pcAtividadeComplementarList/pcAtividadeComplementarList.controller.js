import loadingGif from '../../../../assets/img/loading.gif';

class PcAtividadeComplementarListController {
  constructor($state, $q, NotifyService, AtividadeComplementarService) {
    'ngInject';

    this.loadingGif = loadingGif;

    this.NotifyService = NotifyService;
    this.AtividadeComplementarService = AtividadeComplementarService;

    this.$state = $state;
    this.params = $state.params;
    this.atividades = [];
    this.nu_horasprojeto = null;
    this.nu_horasaluno = null;
    this.st_resumo = null;
    this.st_observacaoavaliador = null;
    this.st_nomecompletoanalise = null;

    this.loading = true;

    const promises = [
      this.buscarHorasProjeto(),
      this.buscarAtividades()
    ];

    $q.all(promises).then(() => {
      this.loading = false;
    });
  }

  buscarHorasProjeto = () => {
    return this.AtividadeComplementarService.getHorasProjeto(this.params.idMatriculaAtiva).then((response) => {
      this.nu_horasprojeto = response.nu_horasatividades || 0;
      this.somarHorasAluno();
    });
  }

  clickAtividadeObservacao = (atividade) => {
    this.st_resumo = atividade.st_resumo;
    this.st_observacaoavaliador = atividade.st_observacaoavaliador;
    this.st_nomecompletoanalise = atividade.st_nomecompletoanalise;
  }
  buscarAtividades = () => {
    const params = {
      id_matricula: this.params.idMatriculaAtiva
    };

    return this.AtividadeComplementarService.getResource().getAll(params, (response) => {
      this.atividades = response;
      this.somarHorasAluno();
    }, (error) => {
      const message = error && error.data && error.data.message ? error.data.message : 'Ocorreu um erro ao buscar as atividades.';
      this.NotifyService.notify('error', '', message);
    });
  }

  somarHorasAluno = () => {
    let total = 0;

    if (this.atividades.length && this.nu_horasprojeto !== null) {
      this.atividades.forEach((item) => {
        total += parseFloat(item.nu_horasconvalidada || 0);
      });
    }

    this.nu_horasaluno = total;

  }

  abrirForm = () => {

    this.$state.go('app.atividade-complementar-form', {idMatriculaAtiva: this.params.idMatriculaAtiva});
  }

}

export default PcAtividadeComplementarListController;
