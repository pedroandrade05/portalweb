/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcAtividadeComplementarFormComponent from './pcAtividadeComplementarForm.component';

//Import do modulo de services do portal
import Services from '../../../services/Services'; // Talvez você precise alterar este destino

//Modulo principal do componente
const pcAtividadeComplementarFormModule = angular.module('pcAtividadeComplementarForm', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])

  .config(($stateProvider) => {
    "ngInject";

    // Routing
    $stateProvider
      .state('app.atividade-complementar-form', {
        url: '/:idMatriculaAtiva/atividade-complementar/form',
        views: {
          content: {
            template: '<pc-atividade-complementar-form></pc-atividade-complementar-form>'
          }
        }
      });
  })

  .component('pcAtividadeComplementarForm', pcAtividadeComplementarFormComponent);

export default pcAtividadeComplementarFormModule;
