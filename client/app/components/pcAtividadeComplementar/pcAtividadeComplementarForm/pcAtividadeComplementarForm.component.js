import template from './pcAtividadeComplementarForm.html';
import controller from './pcAtividadeComplementarForm.controller';
import './pcAtividadeComplementarForm.scss';

const pcAtividadeComplementarFormComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcAtividadeComplementarFormComponent;
