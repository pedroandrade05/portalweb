import template from './pcGradeNotasDetalhamentoMain.html';
import controller from './pcGradeNotasDetalhamentoMain.controller';
import './pcGradeNotasDetalhamentoMain.scss';

const pcGradeNotasDetalhamentoMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcGradeNotasDetalhamentoMainComponent;
