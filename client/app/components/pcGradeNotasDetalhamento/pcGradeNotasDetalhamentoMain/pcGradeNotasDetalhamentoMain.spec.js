import PcGradeNotasDetalhamentoMainModule from './pcGradeNotasDetalhamentoMain';
import PcGradeNotasDetalhamentoMainController from './pcGradeNotasDetalhamentoMain.controller';
import PcGradeNotasDetalhamentoMainComponent from './pcGradeNotasDetalhamentoMain.component';
import PcGradeNotasDetalhamentoMainTemplate from './pcGradeNotasDetalhamentoMain.html';

describe('PcGradeNotasDetalhamentoMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcGradeNotasDetalhamentoMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcGradeNotasDetalhamentoMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Component', () => {
    // component/directive specs
    const component = PcGradeNotasDetalhamentoMainComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcGradeNotasDetalhamentoMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcGradeNotasDetalhamentoMainController);
    });
  });
});
