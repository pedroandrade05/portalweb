import loadingGif from '../../../../assets/img/loading.gif';
import iconeBanner from '../../../../assets/img/icone-banner.svg';
import iconeCupom from '../../../../assets/img/icone-indique-cupom.png';
import Utils from '../../../classes/utils';

class PcIndiqueGanheMainController {
  constructor(
    /* Úteis */ $sce, $state, $timeout, $window, $localStorage,
    /* Services */ CursoAlunoService, EsquemaConfiguracaoService, IndiqueGanheService, MenuService, ModalService, NotifyService, PortalEgressoService,
    /* Constantes */ EvolucaoMatriculaConstante, FuncionalidadeConstante, SituacaoConstante
  ) {
    'ngInject';

    const idMatricula = $state.params.idMatriculaAtiva;
    const isGraduacao = EsquemaConfiguracaoService.isGraduacao(idMatricula);
    const idUsuario = $localStorage.user_id;

    this.name = 'pcIndiqueGanhe';

    /* Úteis */
    this.$sce = $sce;
    this.$state = $state;
    this.$timeout = $timeout;
    this.$window = $window;

    // Services
    this.IndiqueGanheService = IndiqueGanheService;
    this.MenuService = MenuService;
    this.ModalService = ModalService;
    this.NotifyService = NotifyService;
    this.PortalEgressoService = PortalEgressoService;

    // Constantes
    this.EvolucaoMatriculaConstante = EvolucaoMatriculaConstante;
    this.SituacaoConstante = SituacaoConstante;
    this.FuncionalidadeConstante = FuncionalidadeConstante;

    this.matriculaAtiva = CursoAlunoService.getStorageCursoAtivo(idMatricula);

    // Imagens
    this.loadingGif = loadingGif;
    this.iconeBanner = iconeBanner;
    this.iconeCupom = iconeCupom;
    this.htmlIcone = `<img src="${iconeCupom}" style="width: 100px"/><br/><br/>`;

    this.indicacaoModel = {};

    // State
    this.state = {
      loading: true,
      hasPermission: false,
      idMatricula,
      idUsuario,
      isGraduacao,
      bl_resgatarRenovacaoIG: null,
      textoRegulamento: null,
      textoComoFunciona: null,
      termoAceito: null,
      parceiros: null,
      indicacoes: [],
      pendentes: [],
      prazoValePresenteDisponivel: 8,
      idSituacao: SituacaoConstante.AFILIADO_INDICACAO_PENDENTE,
      somatorias: {
        nu_recebido: 0,
        nu_resgatado: 0,
        nu_expirado: 0,
        nu_disponivel: 0
      },
      resgatar: {
        nu_valor: 0,
        id_situacao: null,
        id_afiliadoindicacaoparceiro: null
      }
    };

    // Definir valores padrão de acordo com o Esquema de Configuração
    this.setDefaultResgatar();

    // Buscar dados
    this.getAllData();

    // Verificar aluno egresso
    this.checkAlunoEgresso();
  }

  checkAlunoEgresso = async () => {
    this.state.isAlunoEgresso = await this.PortalEgressoService.isAlunoEgresso(this.matriculaAtiva.id_matricula);

  }

  getAllData() {
    // Verificar se possui permissão de acesso à funcionalidade
    this.MenuService.hasPermission(this.FuncionalidadeConstante.INDIQUE_GANHE).then(() => {
      this.state.hasPermission = true;

      this.getRegulamento();
      this.getComoFunciona();
      this.getIndicacoes();
      this.getConfirmacaoTermos();
      this.getParceiros();
      this.getBlResgatarRenovacaoIG(this.state.idMatricula);
    }).catch(() => {
      this.state.hasPermission = false;
      this.state.loading = false;
    });
  }

  getRegulamento() {
    return this.IndiqueGanheService
      .getRegulamento(this.state.idMatricula)
      .then((response) => {
        this.state.textoRegulamento = response.toJSON();
      });
  }

  getComoFunciona() {
    return this.IndiqueGanheService
      .getComoFunciona(this.state.idMatricula)
      .then((response) => {
        this.state.textoComoFunciona = response.toJSON();
      });
  }

  getIndicacoes() {
    return this.IndiqueGanheService
      .getIndicacoes(this.state.idMatricula)
      .then((response) => {
        this.state.loading = false;
        this.state.indicacoes = response.indicacoes;

        for (const attr in this.state.somatorias) {
          this.state.somatorias[attr] = response[attr] || 0;
        }
      });
  }

  getConfirmacaoTermos() {
    return this.IndiqueGanheService
      .getConfirmacaoTermos(this.state.idUsuario)
      .then((response) => {
        this.state.termoAceito = response.toJSON();
      });
  }

  getBlResgatarRenovacaoIG(idMatricula) {
    return this.IndiqueGanheService
      .getBlResgatarRenovacaoIG(idMatricula)
      .then((response) => {
        this.state.bl_resgatarRenovacaoIG = response.bl_resgatarRenovacaoIG;
      },() => {

      });
  }

  getParceiros() {
    return this.IndiqueGanheService.getParceiros().then((response) => {
      response.map((parceiro) => {
        parceiro.st_urlicone = require('../../../../assets/img/indique-ganhe/parceiros/' + parceiro.st_icone);
      });

      this.state.parceiros = response;
    }, () => {
      this.state.parceiros = [];
    });
  }

  getSumCupons(cupons, idSituacao = null) {
    if (idSituacao) {
      cupons = cupons.filter(({id_situacao}) => +id_situacao === +idSituacao);
    }

    return cupons
      .map(({nu_valor}) => +nu_valor)
      .reduce((a, b) => a + b, 0);
  }

  getLinhaDeNegocio() {
    const graduacao = 2;
    const posGraduacao = 3;
    const linha_de_negocio = [
      {id_linha_de_negocio: graduacao, st_linha_de_negocio: 'Graduação'},
      {id_linha_de_negocio: posGraduacao, st_linha_de_negocio: 'Pós-Graduação'}
    ];
    return linha_de_negocio[this.linha_de_negocio];
  }

  setDefaultResgatar() {
    this.state.resgatar.nu_valor = 0;
    this.state.resgatar.id_afiliadoindicacaoparceiro = null;
    this.state.resgatar.id_situacao = this.state.isGraduacao && this.state.bl_resgatarRenovacaoIG
      ? null
      : this.SituacaoConstante.AFILIADO_INDICACAO_CUPOM_RESGATADO_VALE;
  }

  maskTelefone(str = '') {
    const value = (str || '').replace(/\D/g, '');

    const mask = value.length > 10
      ? '(##) # ####-####'
      : '(##) ####-####';

    return Utils.mask(value, mask);
  }

  handleChangeTelefone() {
    this.indicacaoModel.telefone = this.maskTelefone(this.indicacaoModel.telefone);
  }

  handleChangeValorResgatar(multiplier = 1) {
    const {resgatar} = this.state;
    const saldoDisponivel = this.state.somatorias.nu_disponivel;

    resgatar.nu_valor += 100 * multiplier;

    // O valor deve ser múltiplo de 100
    if (resgatar.nu_valor > 100 && resgatar.nu_valor % 100 !== 0) {
      resgatar.nu_valor = Math.floor(resgatar.nu_valor / 100) * 100;
    }

    // Não pode ser maior que o saldo disponível
    resgatar.nu_valor = Math.min(saldoDisponivel, resgatar.nu_valor);

    // Não pode ser menor que zero
    resgatar.nu_valor = Math.max(0, resgatar.nu_valor);
  }

  showRegulamento() {
    const {textoRegulamento} = this.state;
    const html = textoRegulamento ? textoRegulamento.st_textosistemabody : '';

    this.ModalService.show({
      title: 'Regulamento',
      body: `<div class="font-default text-left" style="font-size: 14px">${html}</div>`,
      confirmButton: 'Ok, Fechar',
      cancelButton: false
    });
  }

  showTab(tabIndex, $event) {
    $event && $event.stopPropagation();

    this.$timeout(() => {
      const $el = this.$window.document.querySelector(`#pcIndiqueGanhe .uib-tab[index="${tabIndex}"] a`);
      const $target = angular.element($el);
      $target && $target.triggerHandler('click');
    });
  }

  salvarIndicacao() {
    if (this.state.isAlunoEgresso) {
      this.NotifyService.notify('warning', 'Para realizar uma nova indicação, acesse o Portal do Egresso.');
      return;
    }

    if (!this.validarCampos()) {
      this.NotifyService.notify('warning', 'Preencha todos os campos para indicar uma pessoa.');
      return false;
    }

    this.state.loading = true;

    this.IndiqueGanheService
      .criarAfiliado(this.matriculaAtiva.id_matricula)
      .then((response) => {
        const codcontratoafiliacao = response.dados.codcontratoafiliacao;

        this.IndiqueGanheService.salvarIndicado(
          codcontratoafiliacao,
          this.matriculaAtiva.id_matricula,
          this.indicacaoModel.nome,
          this.indicacaoModel.tel,
          this.indicacaoModel.ddd,
          this.indicacaoModel.email,
          this.getLinhaDeNegocio(),
        ).then(() => {
          this.getIndicacoes();

          this.ModalService.show({
            title: `${this.htmlIcone} Indicação enviada com sucesso!`,
            confirmButton: 'Ok, Fechar',
            cancelButton: false
          }).then(() => {
            // Resetar campos
            this.linha_de_negocio = null;
            this.indicacaoModel.nome = '';
            this.indicacaoModel.telefone = '';
            this.indicacaoModel.tel = '';
            this.indicacaoModel.ddd = '';
            this.indicacaoModel.email = '';
          });
        }, (error) => {
          const title = error.data.message.includes('já foi indicada')
            ? error.data.message
            : 'Não conseguimos concluir sua indicação!';

          this.ModalService.show({
            title: `${this.htmlIcone} ${title}`,
            confirmButton: 'Fechar',
            cancelButton: false
          });
        }).finally(() => {
          this.state.loading = false;
        });
      }, (error) => {
        this.state.loading = false;

        const title = error.data.message.includes('incorreto')
          ? error.data.message
          : 'Não conseguimos concluir sua indicação!';

        this.ModalService.show({
          title: `${this.htmlIcone} ${title}`,
          confirmButton: 'Fechar',
          cancelButton: false
        });
      });
  }

  validarCampos() {
    return this.validarNome() && this.validarEmail() && this.validarTelefone() && this.validarLinhaDeNegocio();
  }

  validarNome() {
    const nomes = this.indicacaoModel.nome.split(' ');

    if (nomes.length < 2) {
      this.NotifyService.notify('warning', 'Favor colocar nome e sobrenome.');
      return false;
    }

    for (let i = 0; i < nomes.length; i++) {
      const somenteLetras = nomes[i].match(/[A-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð'-]{1,40}/i);
      if (!somenteLetras || somenteLetras[0].length !== nomes[i].length) {
        this.NotifyService.notify('warning', 'Favor conferir se o campo nome não possui números ou pontuações.');
        return false;
      }
    }

    return true;
  }

  validarEmail() {
    if (!Utils.isEmailValid(this.indicacaoModel.email)) {
      this.NotifyService.notify('warning', 'Favor conferir se o e-mail está correto.');
      return false;
    }
    return true;
  }

  validarTelefone() {
    if (!this.indicacaoModel.telefone) {
      this.NotifyService.notify('warning', 'Favor conferir se o telefone está correto.');
      return false;
    }

    const telefone = (this.indicacaoModel.telefone || '').replace(/\D/g, '');

    if (telefone.length < 10) {
      this.NotifyService.notify('warning', 'Favor conferir se o telefone está correto.');
      return false;
    }

    this.indicacaoModel.ddd = telefone.substr(0, 2);
    this.indicacaoModel.tel = telefone.substr(2);

    return true;
  }

  validarLinhaDeNegocio() {
    if (!this.linha_de_negocio) {
      this.NotifyService.notify('warning', 'Favor selecionar o interesse da pessoa.');
      return false;
    }
    return true;
  }

  getValidacaoErro() {
    const nuMin = 100;
    const nuMax = this.state.somatorias.nu_disponivel;

    // O valor deve ser maior que o mínimo
    if (this.state.resgatar.nu_valor < nuMin) {
      return `O valor do resgate não pode ser menor que R$ ${nuMin},00.`;
    }

    // O valor não pode ser maior que o disponível
    if (this.state.resgatar.nu_valor > nuMax) {
      return `O valor do resgate não pode ser maior que R$ ${nuMax},00.`;
    }

    // O valor deve ser múltiplo de 100
    if (this.state.resgatar.nu_valor % 100 !== 0) {
      return `O valor do resgate deve ser múltiplo de ${nuMin}.`;
    }

    // Para vale presente, é necessário informar o parceiro
    if (
      +this.state.resgatar.id_situacao === +this.SituacaoConstante.AFILIADO_INDICACAO_CUPOM_RESGATADO_VALE
      && !this.state.resgatar.id_afiliadoindicacaoparceiro
    ) {
      return 'É necessário selecionar o parceiro.';
    }

    return null;
  }

  confirmarResgate() {
    this.state.loading = true;

    this.IndiqueGanheService.resgatarCupons(
      this.state.idMatricula,
      this.state.resgatar.id_situacao,
      this.state.resgatar.nu_valor,
      this.state.resgatar.id_afiliadoindicacaoparceiro
    ).then(
      () => {
        this.getIndicacoes();

        let title = 'O resgate do bônus foi realizado.';

        if (+this.state.resgatar.id_situacao === +this.SituacaoConstante.AFILIADO_INDICACAO_CUPOM_RESGATADO_VALE) {
          title += ` O bônus será liberado em até ${this.state.prazoValePresenteDisponivel} dias.`;
        }

        this.ModalService.show({
          title: `${this.htmlIcone} ${title}`,
          confirmButton: 'Ok, Fechar',
          cancelButton: false
        }).then(() => {
          // Exibir aba "Meus Bônus"
          this.showTab(2);

          // Resetar valores
          this.setDefaultResgatar();
        });
      },
      () => {
        this.ModalService.show({
          title: `${this.htmlIcone} Não foi possível realizar o resgate neste momento.`,
          confirmButton: 'Ok, Fechar',
          cancelButton: false
        }).then(() => {
          // Exibir aba "Meus Bônus"
          this.showTab(2);

          // Resetar valores
          this.setDefaultResgatar();
        });
      }
    ).finally(() => {
      this.state.loading = false;
    });
  }

}

export default PcIndiqueGanheMainController;
