import template from './pcIndiqueGanheMain.html';
import controller from './pcIndiqueGanheMain.controller';
import './pcIndiqueGanheMain.scss';

const pcIndiqueGanheMainComponent = {
  restrict: 'E',
  bindings: {
    popoverOptions : '='
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pcIndiqueGanheMainComponent;
