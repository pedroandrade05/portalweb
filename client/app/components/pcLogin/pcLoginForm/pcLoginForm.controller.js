import logo from '../../../../assets/img/logo-1.png';

class PcLoginFormController {
  /**
   * Metodo construtor
   * @param AuthService
   * @param $state
   * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
   */
  constructor(CONFIG, AuthService, $state, $window, NotifyService, $compile, $scope, $localStorage, MensagemPadrao) {
    'ngInject';

    this.name = 'pcLoginForm';
    this.MensagemPadrao = MensagemPadrao;

    // Limpar o storage
    if ($state.is('login')) {
      $localStorage.$reset();
    }

    //Variavel local AuthService para uso dos metodos da classe
    this.AuthService = AuthService;

    //Variavel local $state para uso dos metodos da classe
    this.state = $state;

    //Objeto de dados do formulario
    this.user = {
      login: '',
      senha: '',
      entidadeId: CONFIG.idEntidade
    };
    this.recover = {
      st_email: '',
      id_entidade: CONFIG.idEntidade
    };

    this.toaster = NotifyService;

    this.acessar = true;

    this.$scope = $scope;

    this.$compile = $compile;

    this.loginLoading = false;

    this.logoUrl = logo;

    this.validLogin = /^[0-9a-zA-Z@._-]+$/;

    this.textValidate = 'Não é permitido o uso de caracteres especiais, excessões para "@", "." e "_".';

  }


  doRecuperarSenha() {
    this.loginLoading = true;
    this.recover.st_email = this.user.login;

    this.AuthService.recuperarSenha(this.recover).then((response) => {
      if (response.type === 'success') {
        this.toaster.notify('success', '', this.MensagemPadrao.RECUPERA_SENHA_SUCESSO);
        this.acessar = true;
        this.recover.st_email = '';
      }
      else {
        this.toaster.notify('error', '', this.MensagemPadrao.USUARIO_INCORRETO);
      }
    }, () => {
      this.toaster.notify('error', '', this.MensagemPadrao.ERRO_REQUISICAO);
    }).finally(() => {
      this.loginLoading = false;
    });
  }


  /**
   * Funcao para autenticacao de usuario.
   * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
   */
  doLogin() {
    if (!this.user.login.match(this.validLogin)) {
      this.toaster.notify('error', '', this.textValidate);
      return false;
    }

    this.loginLoading = true;

    /**
     * Nesta funcao estou utilizando Arrow Functions "()=>{}", desta forma mesmo dentro do metodo callback ainda e
     * possivel utilizar o escopo this definido no objeto ou funcao pai.
     *
     * https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Functions/Arrow_functions
     */
    this.AuthService.login(this.user).then((response) => {
      if (response) {
        if (response.type === 'success') {
          //Esse aviso de logado com sucesso só é apresentado após a validação de cursos - alterado para selecionacurso
          //const aluno = this.AuthService.getUserCredential('user_name');
          this.state.go('login.selecionar-curso');
        }
        else {
          this.toaster.notify('error', '', response);
        }
      }
      else {
        this.toaster.notify('error', '', this.MensagemPadrao.ERRO_REQUISICAO);
      }
    }, (error) => {
      this.toaster.notify('error', '', error.data.message);
    }).finally(() => {
      this.loginLoading = false;
    });
  }
}

export default PcLoginFormController;
