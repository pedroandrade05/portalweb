import logo from '../../../../assets/img/logo.png';

class PcLoginDiretoController {
  constructor($localStorage, $state, AuthService, NotifyService) {
    'ngInject';

    this.name = 'vm';
    this.logoUrl = logo;
    this.state = $state;
    this.AuthService = AuthService;

    this.showErrorLogin = false;

    this.blGravaLog = this.state.params.blGravaLog
      , this.idUsuarioOriginal = this.state.params.idUsuarioOriginal
      , this.idEntidade = this.state.params.idEntidade
      , this.idUsuario = this.state.params.idUsuario
      , this.stUrlAcesso = this.state.params.stUrlAcesso;

    if (this.idUsuario) {
      // Se o usuário do parâmetro for diferente do salvo na sessão, resetar antes de realizar login direto
      if (+this.idUsuario !== +this.AuthService.getUserCredential('user_id')) {
        $localStorage.$reset();
      }
    }

    if (this.idUsuarioOriginal && this.idEntidade && this.idUsuario && this.stUrlAcesso) {
      this.AuthService.autenticacaoDireta(this.blGravaLog
        , this.idUsuarioOriginal
        , this.idEntidade
        , this.idUsuario
        , this.stUrlAcesso).then(
        (response) => {

          if (response) {

            if (response.type === 'success') {
              if (this.state.params.idSalaDeAula) {
                localStorage.setItem('idSalaDeAula', this.state.params.idSalaDeAula);
              }

              $state.go('login.selecionar-curso', {
                idCurso: this.state.params.idCurso,
                idMatricula: this.state.params.idMatricula
              });
            }
          }
          else {
            NotifyService.notify('error', '', this.MensagemPadrao.ERRO_REQUISICAO);
            this.showErrorLogin = true;
          }

        }
        , (error) => {
          NotifyService.notify(error.data);
          this.showErrorLogin = true;
        });
    }
    else {
      this.showErrorLogin = true;
    }
  }
}

export default PcLoginDiretoController;
