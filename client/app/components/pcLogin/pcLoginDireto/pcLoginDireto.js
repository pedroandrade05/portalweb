/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';
import angularLadda from "angular-ladda";

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcLoginDiretoComponent from './pcLoginDireto.component';

//Import do modulo de services do portal
import Services from '../../../services/Services'; // Talvez você precise alterar este destino

//Modulo principal do componente
const pcLoginDiretoModule = angular.module('pcLoginDireto', [
  uiRouter,
  angularLadda,
  Services.name //Injecao modulo services do portal
])
  .config(($stateProvider, $urlRouterProvider, laddaProvider) => {
    "ngInject"; //Inject necessario para que o angular faca as injecoes as factorys e services

    laddaProvider.setOption({ /* optional */
      style: 'expand-left',
      spinnerSize: 20,
      spinnerColor: '#ffffff'
    });

    //Definicao de rotas para o componente
    $stateProvider
      .state('login.direto', {
        url: '/direto/:blGravaLog/:idUsuarioOriginal/:idEntidade/:idUsuario/:stUrlAcesso/:idCurso/:idSalaDeAula/:idMatricula',
        params: {
          idCurso: {
            value: null,
            squash: true
          },
          idSalaDeAula: {
            value: null,
            squash: true
          },
          idMatricula: {
            value: null,
            squash: true
          }
        },
        views: {
          'login-view': {
            template: '<pc-login-direto></pc-login-direto>'
          }
        }
      });
  })
.component('pcLoginDireto', pcLoginDiretoComponent);

export default pcLoginDiretoModule;
