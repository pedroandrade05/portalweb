import PcLoginDiretoModule from './pcLoginDireto';
import PcLoginDiretoController from './pcLoginDireto.controller';
import PcLoginDiretoComponent from './pcLoginDireto.component';
import PcLoginDiretoTemplate from './pcLoginDireto.html';
import '../../../testConfigs/mocks/ServiceMocks';
import StateMocks from '../../../testConfigs/mocks/StateMocks';
describe('PcLoginDireto', () => {

  let $rootScope, $state ={},makeController;
  angular.mock.module("Services");

  beforeEach(window.module(PcLoginDiretoModule.name));

  beforeEach(inject((_$rootScope_, AuthService) => {
    $rootScope = _$rootScope_;
    $state.params = { blGravaLog: false, idUsuarioOriginal: 1, idEntidade: 14, idUsuario: 1, stUrlAcesso: "www.portal.com.br" };
    makeController = () => {
      return new PcLoginDiretoController($state, AuthService);
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Component', () => {
      // component/directive specs
    const component = PcLoginDiretoComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcLoginDiretoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcLoginDiretoController);
    });
  });
});
