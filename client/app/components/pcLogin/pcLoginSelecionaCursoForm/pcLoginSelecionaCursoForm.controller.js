import logo from '../../../../assets/img/logo-1.png';
import {isUndefined} from 'util';

class PcLoginSelecionaCursoFormController {

  constructor(
    CONFIG,
    $state,
    $filter,
    $window,
    AuthService,
    NotifyService,
    CursoService,
    CursoAlunoService,
    CertificacaoService,
    EvolucaoMatriculaConstante,
    MensagensInfoEvolucaoConstante,
    VendaService,
    OcorrenciaService,
    EntidadeConstante,
    $localStorage) {
    'ngInject';

    this.CONFIG = CONFIG;
    this.$window = $window;
    this.storage = $localStorage;
    this.name = 'pcLoginSelecionaCursoForm';
    this.state = $state;
    this.$filter = $filter;

    this.toaster = NotifyService;
    this.AuthService = AuthService;
    this.CursoService = CursoService;
    this.CursoAlunoService = CursoAlunoService;
    this.CertificacaoService = CertificacaoService;
    this.OcorrenciaService = OcorrenciaService;

    this.user_avatar = AuthService.getUserCredential('user_avatar');
    this.user_email = AuthService.getUserCredential('user_email');
    this.user_id = AuthService.getUserCredential('user_id');
    this.logoUrl = logo;
    this.loginLoading = true;
    this.EvolucaoMatriculaConstante = EvolucaoMatriculaConstante;
    this.EntidadeConstante = EntidadeConstante;
    this.MensagensInfoEvolucaoConstante = MensagensInfoEvolucaoConstante;
    this.VendaService = VendaService;
    this.possuiUmcurso = false;

    this.cursos = [];

    this.somenteCursoLivre = false;

    const name = this.AuthService.getUserCredential('user_name').split(' ');
    if (name.length > 1) {
      this.user_name = name[0] + ' ' + name[name.length - 1];
    }
    else {
      this.user_name = this.AuthService.getUserCredential('user_name');
    }

    if (!parseInt(this.AuthService.getUserCredential('user_entidade'))) {
      $state.go('login');
    }

    this.carregarCursos();
  }

  /**
   * Retorna os cursos do Aluno
   */
  carregarCursos() {
    this.CursoAlunoService.getResource().getAll({
      bl_novoportal: true,
      'id_entidade[]': this.CONFIG.idEntidade
    }, (response) => {

      if (response.length) {
        this.entidades = response;

        angular.forEach(response, (value) => {
          angular.forEach(value.cursos, (valuec) => {
            this.cursos.push(valuec);
          });
        });

        this.adicionaStatusCursos();

        this.storage.renovacao = null;
        this.storage.dadosRenovacao = null;

        const idMatriculaSelecionada = this.state.params.idMatricula;
        let idCursoSelecionado = this.state.params.idCurso;

        // const cursosLivres = this.cursos.filter((curso) => this.CursoService.isCursoLivre(curso));
        const cursosRegulares = this.cursos.filter((curso) => !this.CursoService.isCursoLivre(curso));

        if (cursosRegulares.length === 1 && !idCursoSelecionado) {
          idCursoSelecionado = cursosRegulares[0].id_curso;
        }

        if (idMatriculaSelecionada) {
          this.cursos = this.cursos.filter((curso) => {
            return +curso.id_matricula === +idMatriculaSelecionada;
          });
        }
        else if (idCursoSelecionado) {
          this.cursos = this.cursos.filter((curso) => {
            return +curso.id_curso === +idCursoSelecionado;
          });
        }

        /* Se o aluno possuir apenas um curso e tiver evolução CURSANDO ou CONCLUINTE,
        * seleciona automaticamente e o redireciona para a tela principal. */
        if (this.cursos.length === 1) {
          if (!this.cursos[0].loginPermitido) {
            this.toaster.notify('warning', '', this.cursos[0].st_mensagem);
            this.state.go('login');
            return;
          }

          //este possui um curso é para quando o um usuario que possui
          // somente um curso nao mostre o formulario de escolher curso
          this.possuiUmcurso = !this.possuiUmcurso;
          this.selecionarCurso(this.cursos[0]);
        }

        // Se não tiver o ID do curso SELECIONADO,
        // Verificar se possui SOMENTE CURSOS LIVRES (cursos de extensão)
        // E exibir a tela de seleção de cursos livres
        this.somenteCursoLivre = !idCursoSelecionado && !this.cursos.find((curso) => !this.CursoService.isCursoLivre(curso));

        this.loginLoading = false;
      }
      else {
        this.toaster.notify('warning', '', 'Nenhuma matrícula ativa encontrada no Portal do Aluno.');
        this.state.go('login');
      }
    }, () => {
      this.toaster.notify('error', '', 'Ocorreu um erro ao tentar fazer seu acesso, por favor, caso este erro persista entre em contato com sua entidade.');
      this.state.go('login');
    });
  }

  /**
   * Salva os dados do curso no storage
   * @param cursoObj
   */
  selecionarCurso(cursoObj) {
    this.storage.idEvolucao = cursoObj.id_evolucao;

    //Ao selecionar o curso,  itera as variáveis do localStorage e apaga o conteúdo daquelas relacionadas ao bot
    for (var key in localStorage) {
      if (key.indexOf('dt.session') >= 0) {
        localStorage.setItem(key, '');
      }
    }

    //Buscando e salvando venda da matrícula ativa
    this.VendaService.getVendaPorMatricula(cursoObj.id_matricula).then((response) => {
      this.VendaService.setStorageVendaPorMatricula(response);
    });

    //Só loga no curso caso a evolução permita login.
    if (cursoObj.loginPermitido) {
      //recupera o id da entidade setado no storage
      this.CursoAlunoService.setStorageCursoAtivo(cursoObj);

      //Atualiza o token de acordo com o curso que o aluno esta logado
      this.CursoAlunoService.getResource().updateEntidade({
        id_entidade: cursoObj.id_entidade
      }, (token) => {
        this.AuthService.destroyUserCredentials();
        this.AuthService.storeUserCredentials(token.token);

        var idSalaDeAula = +localStorage.getItem('idSalaDeAula');
        localStorage.removeItem('idSalaDeAula');

        // Só carregar a página após a atualização do token
        // Se possuir ID DA SALA no localstorage, então direcionar diretamente para ela

        if (idSalaDeAula) {
          this.state.go('app.disciplina', {
            idMatriculaAtiva: cursoObj.id_matricula,
            idSalaDeAula: idSalaDeAula
          });
        }
        else if (this.storage.bl_acessarcomo) {
          this.state.go('app.curso', {idMatriculaAtiva: cursoObj.id_matricula});
        }
        else {
          //Buscando por ocorrências pendentes POR-960
          this.OcorrenciaService.numeroOcorrenciasInteressado(cursoObj.id_matricula).then((response) => {
            if (parseInt(response.ocorrencias) > 0) {
              this.state.go('login.aviso-ocorrencias', {
                idMatriculaAtiva: cursoObj.id_matricula
              });
            }
            else {
              this.state.go('app.curso', {idMatriculaAtiva: cursoObj.id_matricula});
            }
          }).catch(() => {
            this.state.go('app.curso', {idMatriculaAtiva: cursoObj.id_matricula});
          });

        }
      }, (error) => {
        this.toaster.notify(error.data);
      });

      //Se o bot estiver presente na página, ao trocar de curso limpa ele e esconde
      localStorage.removeItem('dt.session');
      if (!isUndefined(this.$window.DTBOT)) {
        this.$window.DTBOT.hide();
      }
    }

  }

  /** Percorre a lista de cursos e, baseando-se na evolução seta-se mensagens de erro e a string com a classe
   * que desabilita o clique na seleção de cursos para evoluções com acesso impedido. */

  adicionaStatusCursos() {

    angular.forEach(this.cursos, (valor) => {

      // Casting necessário para o funcionamento do Switch e comparação type-safe.
      const id_evolucao = Number(valor.id_evolucao);

      /* Caso seja qualquer uma das outras evoluções, seta a mensagem de informação e a classe que desabilita
      o acesso ao portal para evoluções com acesso impedido. */

      valor.st_mensagem = null;
      valor.loginPermitido = 1;
      if (valor.id_entidade === this.EntidadeConstante.FACULDADE_UNYLEYA) {
        switch (id_evolucao) {
        case this.EvolucaoMatriculaConstante.BLOQUEADO:
          valor.st_mensagem = this.MensagensInfoEvolucaoConstante.BLOQUEADO;
          valor.loginPermitido = 1;
          break;
        case this.EvolucaoMatriculaConstante.TRANCADO:
          valor.st_mensagem = this.MensagensInfoEvolucaoConstante.TRANCADO;
          valor.loginPermitido = 1;
          break;
        }
      }

      if (id_evolucao === this.EvolucaoMatriculaConstante.CANCELADO) {
        valor.st_mensagem = this.MensagensInfoEvolucaoConstante.PERFIL_DESATIVADO;
        valor.loginPermitido = 0;
      }
      if (id_evolucao === this.EvolucaoMatriculaConstante.ANULADO
        || id_evolucao === this.EvolucaoMatriculaConstante.DECURSO_DE_PRAZO
        || id_evolucao === this.EvolucaoMatriculaConstante.TRANSFERIDO_DE_ENTIDADE) {
        valor.st_mensagem = this.MensagensInfoEvolucaoConstante.ANULADO;
        valor.loginPermitido = 0;
      }
    });
  }

  /* Redireciona para a tela de login. Utilizado no botão "voltar" da tela de seleção de cursos. */
  redirecionaTelaLogin = () => {
    this.AuthService.destroyUserCredentials();
    this.state.go('login');
  };

}

export default PcLoginSelecionaCursoFormController;
