import PcLoginSelecionaCursoFormModule from './pcLoginSelecionaCursoForm';
import PcLoginSelecionaCursoFormController from './pcLoginSelecionaCursoForm.controller.js';
import PcLoginSelecionaCursoFormComponent from './pcLoginSelecionaCursoForm.component.js';
import PcLoginSelecionaCursoFormTemplate from './pcLoginSelecionaCursoForm.html';
import "../../../testConfigs/mocks/ServiceMocks";

describe('PcLoginSelecionaCursoForm', () => {
  let $rootScope, makeController, $state = {};
  angular.mock.module("Services");

  beforeEach(window.module(PcLoginSelecionaCursoFormModule.name));
  beforeEach(inject((_$rootScope_, CursoAluno, AuthService) => {
    $rootScope = _$rootScope_;
    $state.go = (to) => to;

    makeController = () => {
      return new PcLoginSelecionaCursoFormController(CursoAluno, AuthService,$state);
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Component', () => {
      // component/directive specs
    const component = PcLoginSelecionaCursoFormComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcLoginSelecionaCursoFormTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcLoginSelecionaCursoFormController);
    });
  });
});
