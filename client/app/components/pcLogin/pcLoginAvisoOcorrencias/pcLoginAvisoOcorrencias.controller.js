class PcLoginAvisoOcorrenciasController {
  constructor($state, OcorrenciaService) {
    'ngInject';
    this.state = $state;
    this.name = 'pcLoginAvisoOcorrencias';

    // Services
    this.OcorrenciaService = OcorrenciaService;

    this.idMatriculaAtiva = this.state.params.idMatriculaAtiva;
    this.numOcorrencias = null;
    this.checkboxCiente = false;

    this.getOcorrencias();
  }

  /**
   * Buscando número de ocorrências previamente salvo no localStorage
   */
  getOcorrencias() {
    if (!this.idMatriculaAtiva) {
      this.state.go('login');
    }
    else {
      this.numOcorrencias = this.OcorrenciaService.getStorageNumeroOcorrencias();
      if (!this.numOcorrencias) {
        this.state.go("app.curso", {idMatriculaAtiva: this.idMatriculaAtiva});
      }
    }
  }

  /**
   * Chama a Api registra ciencia
   * Não é necessário esperar um response
   */
  registraCiencia(){
    this.OcorrenciaService.registraCienciaOcorrencia(this.idMatriculaAtiva);
  }

}

export default PcLoginAvisoOcorrenciasController;
