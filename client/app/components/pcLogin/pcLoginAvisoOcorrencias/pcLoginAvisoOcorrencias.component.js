import template from './pcLoginAvisoOcorrencias.html';
import controller from './pcLoginAvisoOcorrencias.controller';
import './pcLoginAvisoOcorrencias.scss';

const pcLoginAvisoOcorrenciasComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcLoginAvisoOcorrenciasComponent;
