/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcLoginAvisoOcorrenciasComponent from './pcLoginAvisoOcorrencias.component';

//Import do modulo de services do portal
import Services from '../../../services/Services'; // Talvez você precise alterar este destino

//Modulo principal do componente
const pcLoginAvisoOcorrenciasModule = angular.module('pcLoginAvisoOcorrencias', [
  uiRouter,
  Services.name //Injecao modulo services do portal
]).config(($stateProvider) => {
  "ngInject"; //Inject necessario para que o angular faca as injecoes as factorys e services

  //Definicao de rotas para o componente
  $stateProvider
    .state('login.aviso-ocorrencias', {
      url: '/:idMatriculaAtiva/aviso-ocorrencias',
      views: {
        'login-view': {
          template: '<pc-login-aviso-ocorrencias></pc-login-aviso-ocorrencias>'
        }
      },
      onEnter(AuthService, $state) {
        if (!AuthService.isAuthenticated()) {
          $state.go('login');
        }
      }
    })
  ;
})

  .component('pcLoginAvisoOcorrencias', pcLoginAvisoOcorrenciasComponent);

export default pcLoginAvisoOcorrenciasModule;
