import loadingGif from '../../../../assets/img/loading.gif';

class PcCalendarioAcademicoMainController {
  constructor(
    /* Úteis */$state,
    /* Services */ CursoService
    /* Constantes */
  ) {
    'ngInject';

    this.name = 'pcCalendarioAcademicoMain';

    /* Úteis */
    this.state = $state;

    /* Services */
    this.CursoService = CursoService;

    this.loadingGif = loadingGif;
    this.loading = true;
    this.calendarioAcademico = [];

    // Dados da matrícula
    this.matricula = CursoService.getStorageMatriculaAtivo(this.state.params.idMatriculaAtiva);

    this.getCalendarioAcademico(this.matricula.id_entidade);
  }

  getCalendarioAcademico(id_entidade) {
    this.CursoService.getCalendarioAcademico(id_entidade).then(
      (success) => {
        this.calendarioAcademico = success;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }
}

export default PcCalendarioAcademicoMainController;
