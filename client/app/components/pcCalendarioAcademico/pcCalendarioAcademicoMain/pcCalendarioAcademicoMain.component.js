import template from './pcCalendarioAcademicoMain.html';
import controller from './pcCalendarioAcademicoMain.controller';
import './pcCalendarioAcademicoMain.scss';

const pcCalendarioAcademicoMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcCalendarioAcademicoMainComponent;
