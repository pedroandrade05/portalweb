import template from './pcPlannerMain.html';
import controller from './pcPlannerMain.controller';
import './pcPlannerMain.scss';

const pcPlannerMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcPlannerMainComponent;
