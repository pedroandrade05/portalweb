class pcPlannerMainController {
  constructor(NotifyService, PlannerService, $state, $window, $sce, $localStorage) {
    'ngInject';

    this.NotifyService = NotifyService;
    this.PlannerService = PlannerService;

    this.params = $state.params;
    this.$window = $window;
    this.$sce = $sce;

    this.idMatricula = $state.params.idMatriculaAtiva;
    this.urlPlanner = null;

    this.name = $localStorage.user_name;

    this.abrirPlanner();
  }

  abrirPlanner() {

    this.PlannerService.getUrlPlanner(this.idMatricula).then(
      (response) => {
        this.urlPlanner = this.$sce.trustAsResourceUrl(response.st_caminhocompleto);

      });
  }
}

export default pcPlannerMainController;
