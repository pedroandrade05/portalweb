import PcPlannerMainModule from './PcPlannerMain';
import PcPlannerMainController from './PcPlanneroMain.controller';
import PcPlannerMainComponent from './pcPlannerMain.component';
import PcPlannerMainTemplate from './PcPlannerMain.html';

describe('PcPlannerMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcPlannerMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcPlannerMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcPlannerMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcPlannerMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcPlannerMainController);
    });
  });
});
