import template from './pcPraticasJuridicasMain.html';
import controller from './pcPraticasJuridicasMain.controller';
import './pcPraticasJuridicasMain.scss';

const pcPraticasJuridicasMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcPraticasJuridicasMainComponent;
