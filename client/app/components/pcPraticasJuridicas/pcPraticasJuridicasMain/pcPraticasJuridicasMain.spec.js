import pcPraticasJuridicasMainModule from './pcPraticasJuridicasMain';
import pcPraticasJuridicasMainController from './pcPraticasJuridicasMain.controller';
import pcPraticasJuridicasMainComponent from './pcPraticasJuridicasMain.component';
import pcPraticasJuridicasMainTemplate from './pcPraticasJuridicasMain.html';

describe('pcPraticasJuridicasMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(pcPraticasJuridicasMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new pcPraticasJuridicasMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = pcPraticasJuridicasMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(pcPraticasJuridicasMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(pcPraticasJuridicasMainController);
    });
  });
});
