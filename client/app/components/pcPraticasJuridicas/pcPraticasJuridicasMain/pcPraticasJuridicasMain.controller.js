import logoNPJDigital from '../../../../assets/img/praticas_juridicas/NPJ_Digital.png';
import loadingGif from "../../../../assets/img/loading.gif";

class pcPraticasJuridicasMainController {
  constructor(
    /* Úteis */ $state, $window, $sce,
    /* Services */ CursoService, CursoAlunoService, NotifyService,
    /* Constantes */ TbSistema, MensagemPadraoConstante
  ) {
    'ngInject';

    /* Úteis */
    this.$state = $state;
    this.$window = $window;
    this.$sce = $sce;

    /* Services */
    this.CursoService = CursoService;
    this.NotifyService = NotifyService;

    /* Constantes */
    this.Sistema = TbSistema;
    this.MensagemPadraoConstante = MensagemPadraoConstante;
    this.loadingGif = loadingGif;

    this.idMatriculaAtiva = this.$state.params.idMatriculaAtiva;
    this.matricula = CursoAlunoService.getStorageCursoAtivo(this.idMatriculaAtiva);
    this.loading = true;
    this.cards = [];
    this.informacoes = "";
    this.getInformacoes();
    this.getPraticasJuridicas(this.idMatriculaAtiva);
  }

  getInformacoes() {

    return this.CursoService
      .getMensagemPadrao(
        this.MensagemPadraoConstante.NUCLEO_PRATICAS_JURIDICAS_INFORMACOES,
        this.matricula.id_entidade
      )
      .then((response) => {
        this.informacoes = response.st_textosistemabody;
      });
  }

  getPraticasJuridicas(id_matricula) {

    this.CursoService.getNPJDigital(id_matricula)
      .then(
        (success) => {
          if (success) {
            var img = logoNPJDigital;
            var button = "";
            var url = success.st_conexao;

            if (success.bl_acesso) {
              button = '<form action="' + success.st_conexao + '" method="post" target="_blank">' +
                '<input name="username" type="hidden" value="' + success.st_login + '">' +
                '<input name="password" type="hidden" value="' + success.st_acesso + '">' +
                '<button class="btn btn-brand btn-md" type="submit">Acesse agora</button>' +
                '</form>';
            }

            this.cards.push({
              img: img,
              text: success.st_titulo,
              url: url,
              desabilitado: !success.bl_acesso,
              button: button
            });
          }
        },
        (error) => {
          this.NotifyService.notify(error.data);
        }
      )
      .finally(() => {
        this.loading = false;
      });
  }
}

export default pcPraticasJuridicasMainController;
