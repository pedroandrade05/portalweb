import loadingGif from '../../../../assets/img/loading.gif';

class PcCronogramaMainController {
  constructor(
    /* Úteis */ $state, $localStorage, $rootScope, $window,
    /* Services */ CronogramaService, NotifyService, CursoService
    /* Constantes */
  ) {
    'ngInject';
    this.state = $state;
    this.window =  $window;
    this.scope = $rootScope;
    this.loadingGif = loadingGif;
    this.$localStorage = $localStorage;

    this.CronogramaService = CronogramaService;
    this.NotifyService = NotifyService;
    this.CursoService = CursoService;

    // Dados da matrícula
    this.matricula = CursoService.getStorageMatriculaAtivo(this.state.params.idMatriculaAtiva);
    this.calendarioAcademico = null;

    this.buscarCronograma();
    this.getCalendarioAcademico(this.matricula.id_entidade);
  }

  buscarCronograma() {
    this.loading = true;
    this.CronogramaService.buscarCronograma(
      this.state.params.idMatriculaAtiva
    ).then(
      (sucesso) => {
        this.loading = false;
        if (sucesso.length !== 0) {
          this.cronograma = {};
          sucesso.forEach((element) => {
            this.cronograma[element.st_modulo] = { dados: null, titulo: null };
            this.cronograma[element.st_modulo].dados = angular.fromJson(element.dados);
            this.cronograma[element.st_modulo].titulo = element.st_modulo;
          });

          return true;
        }

        this.cronograma = false;
      },
      (erro) => {
        this.cronograma = false;
        this.loading = false;
        this.NotifyService.notify('error', '', 'Não foi possível carregar o cronograma!');
        console.warn(erro);
      }
    );
  }

  /** Verifica se há calendario Academico para a entidade.
   * @param {string} id_entidade - ID da entidade.
   * */

  getCalendarioAcademico(id_entidade) {
    this.CursoService.getCalendarioAcademico(id_entidade).then((success) => {
      this.calendarioAcademico = success;
    });
  }

  /**
   * Redireciona para a tela de Calendário Acadêmico
   */
  redirecionaCalendarioAcademico = () => {
    try {
      if (this.calendarioAcademico.length > 1){
        this.state.go("app.calendario-academico", {
          idMatriculaAtiva: this.matricula.id_matricula
        });
      }
      else {
        this.window.open(this.calendarioAcademico[0].st_urlupload);
      }

    }
    catch (e) {
      this.toaster.notify('error', 'Ops', 'Erro ao tentar redirecionar para tela de Calendário Acadêmico.');
      console.error(e);
    }
  }
}

export default PcCronogramaMainController;
