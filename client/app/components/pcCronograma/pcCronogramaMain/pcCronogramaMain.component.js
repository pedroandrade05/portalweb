import template from './pcCronogramaMain.html';
import controller from './pcCronogramaMain.controller';
import './pcCronogramaMain.scss';

const pcCronogramaMainComponent = {
  restrict: 'E',
  bindings: {
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pcCronogramaMainComponent;
