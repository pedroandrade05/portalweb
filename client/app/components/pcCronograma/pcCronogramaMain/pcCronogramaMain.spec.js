import PcCronogramaMainModule from './pcCronogramaMain';
import PcCronogramaMainController from './pcCronogramaMain.controller';
import PcCronogramaMainComponent from './pcCronogramaMain.component';
import PcCronogramaMainTemplate from './pcCronogramaMain.html';

describe('PcCronogramaMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcCronogramaMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcCronogramaMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcCronogramaMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcCronogramaMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcCronogramaMainController);
    });
  });
});
