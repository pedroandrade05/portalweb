import PcPergamumMainModule from './pcPergamumMain';
import PcPergamumMainController from './pcPergamumMain.controller';
import PcPergamumMainComponent from './pcPergamumMain.component';
import PcPergamumMainTemplate from './pcPergamumMain.html';

describe('PcPergamumMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcPergamumMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcPergamumMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcPergamumMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcPergamumMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcPergamumMainController);
    });
  });
});
