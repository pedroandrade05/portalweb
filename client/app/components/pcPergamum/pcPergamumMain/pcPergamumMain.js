/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcPergamumMainComponent from './pcPergamumMain.component';

//Import do modulo de services do portal
import Services from '../../../services/Services'; // Talvez você precise alterar este destino

//Modulo principal do componente
const pcPergamumMainModule = angular.module('pcPergamumMain', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])

  .config(($stateProvider) => {
    'ngInject';

    // Rotas
    $stateProvider
      .state('app.biblioteca-polo', {
        url: '/:idMatriculaAtiva/biblioteca-polo',

        views: {
          content: {
            template: '<pc-pergamum-main></pc-pergamum-main>'
          }
        },

        onEnter (AuthService, $state) {
          const params = $state.params;

          //grava log de acesso
          AuthService.gravaLogAcesso($state.current.name, params);
        }
      });
  })

  .component('pcPergamumMain', pcPergamumMainComponent);

export default pcPergamumMainModule;
