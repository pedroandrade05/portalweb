class PcHistoricoDisciplinasMainController {
  constructor(
    /* Úteis */ $state
    /* Services */
    /* Constantes */
  ) {
    'ngInject';

    // Úteis
    this.$state = $state;
    this.name = 'pcHistoricoDisciplinasMain';
  }

  showMenu() {
    return [
      'app.historico-disciplinas',
      'app.historico-disciplinas-form'
    ].indexOf(this.$state.current.name) !== -1;
  }

}

export default PcHistoricoDisciplinasMainController;
