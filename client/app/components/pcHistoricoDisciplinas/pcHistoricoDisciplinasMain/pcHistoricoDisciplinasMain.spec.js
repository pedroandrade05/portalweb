import PcHistoricoDisciplinasMainModule from './pcHistoricoDisciplinasMain'
import PcHistoricoDisciplinasMainController from './pcHistoricoDisciplinasMain.controller';
import PcHistoricoDisciplinasMainComponent from './pcHistoricoDisciplinasMain.component';
import PcHistoricoDisciplinasMainTemplate from './pcHistoricoDisciplinasMain.html';

describe('PcHistoricoDisciplinasMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcHistoricoDisciplinasMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcHistoricoDisciplinasMainController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
      let component = PcHistoricoDisciplinasMainComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(PcHistoricoDisciplinasMainTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(PcHistoricoDisciplinasMainController);
      });
  });
});
