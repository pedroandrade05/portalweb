import template from './pcHistoricoDisciplinasMain.html';
import controller from './pcHistoricoDisciplinasMain.controller';
import './pcHistoricoDisciplinasMain.scss';

const pcHistoricoDisciplinasMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcHistoricoDisciplinasMainComponent;
