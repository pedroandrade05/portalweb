class PcHistoricoDisciplinaMainController {
  constructor(
    /* Utils */ $scope, $state,
    /* Services */ CronogramaService, NotasService, NotifyService,
    /* Constants */ EvolucaoDisciplinaConstante, SituacaoConstante
  ) {
    'ngInject';

    // Utils
    this.$scope = $scope;
    this.$state = $state;

    // Services
    this.CronogramaService = CronogramaService;
    this.NotasService = NotasService;
    this.NotifyService = NotifyService;

    // Constants
    this.EvolucaoDisciplinaConstante = EvolucaoDisciplinaConstante;
    this.SituacaoConstante = SituacaoConstante;

    // Outros
    this.loading = true;
    this.historicodisciplina = null;
    this.idMatricula = $state.params.idMatriculaAtiva;

    this.getCronograma();
  }

  getCronograma = () => {
    this.CronogramaService.buscarCronograma(
      this.idMatricula
    ).then((response) => {
      this.historicodisciplina = {};

      if (response.length !== 0) {
        response.forEach((item) => {
          this.historicodisciplina[item.st_modulo] = {
            titulo: item.st_modulo,
            dados: this.sortDisciplinas(angular.fromJson(item.dados))
          };
        });
      }
    }).catch(() => {
      this.NotifyService.notify('error', '', 'Não foi possível carregar o histórico!');
    }).finally(() => {
      this.loading = false;
    });
  };

  getNotas = async (disciplina) => {
    disciplina.expanded = !disciplina.expanded;

    if (disciplina.notas) {
      return;
    }

    disciplina.loading = true;

    let response = {};

    try {
      response = await this.NotasService.getGradeNotasDetalhada(this.idMatricula, disciplina.id_disciplina);
      response = response.toJSON() || {};
    }
    catch (err) {
      // Não escapar erro pro navegador
    }

    const modulo = Object.values(response).shift();
    disciplina.notas = [];

    if (modulo && modulo.disciplinas && modulo.disciplinas[0]) {
      disciplina.notas = modulo.disciplinas[0].ar_notasmoodle || [];
    }

    disciplina.loading = false;
    this.$scope.$apply();
  };

  getOrdem = (disciplina) => {
    // Disciplinas isentas devem vir no final
    if (this.isDisciplinaIsenta(disciplina)) {
      return 6;
    }

    return {
      [this.EvolucaoDisciplinaConstante.APROVADO]: 1,
      [this.EvolucaoDisciplinaConstante.INSATISFATORIO]: 2,
      [this.EvolucaoDisciplinaConstante.CURSANDO]: 3,
      [this.EvolucaoDisciplinaConstante.TRANCADO]: 4,
      [this.EvolucaoDisciplinaConstante.NAO_ALOCADO]: 5
    }[disciplina.id_evolucao] || 5;
  };

  sortDisciplinas = (disciplinas) => {
    disciplinas.sort((a, b) => {
      return this.getOrdem(a) - this.getOrdem(b);
    });

    return disciplinas;
  };

  isDisciplinaIsenta = (disciplina) => {
    return +disciplina.id_situacao === this.SituacaoConstante.APROVEITAMENTO_DISCIPLINA;
  };

  isDisciplinaAtiva = (disciplina) => {
    return [
      this.EvolucaoDisciplinaConstante.CURSANDO,
      this.EvolucaoDisciplinaConstante.APROVADO,
      this.EvolucaoDisciplinaConstante.INSATISFATORIO
    ].includes(+disciplina.id_evolucao);
  };

  getStatus = (disciplina) => {
    if (this.isDisciplinaIsenta(disciplina)) {
      return 'ISENTA';
    }

    let status = (disciplina.st_statusdisciplina || disciplina.st_evolucao || '').toUpperCase();

    if (status === 'NÃO INICIADA') {
      status = 'A CURSAR';
    }

    return status;
  };
}

export default PcHistoricoDisciplinaMainController;
