import template from './pcHistoricoDisciplinasForm.html';
import controller from './pcHistoricoDisciplinasForm.controller';
import './pcHistoricoDisciplinasForm.scss';

const pcHistoricoDisciplinasFormComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcHistoricoDisciplinasFormComponent;
