import PcHistoricoDisciplinasFormModule from './pcHistoricoDisciplinasForm';
import PcHistoricoDisciplinasFormController from './pcHistoricoDisciplinasForm.controller';
import PcHistoricoDisciplinasFormComponent from './pcHistoricoDisciplinasForm.component';
import PcHistoricoDisciplinasFormTemplate from './pcHistoricoDisciplinasForm.html';

describe('PcHistoricoDisciplinasForm', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcHistoricoDisciplinasFormModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcHistoricoDisciplinasFormController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Component', () => {
    // component/directive specs
    const component = PcHistoricoDisciplinasFormComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcHistoricoDisciplinasFormTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcHistoricoDisciplinasFormController);
    });
  });
});
