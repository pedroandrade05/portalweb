class PcHistoricoDisciplinasMenuController {
  constructor(
    /* Úteis */ $state, $localStorage, $scope, $sce, $document, $window,
    /* Services */ DocumentoDigitalService, EsquemaConfiguracaoService, DownloadService, CursoAlunoService
    /* Constantes */) {
    'ngInject';
    this.name = 'pcHistoricoDisciplinasMenu';
    this.$state = $state;
    this.$scope = $scope;
    this.$sce = $sce;
    this.$document = $document;
    this.$window = $window;
    this.DocumentoDigitalService = DocumentoDigitalService;
    this.$localStorage = $localStorage;
    this.DownloadService = DownloadService;
    this.idMatricula = $state.params.idMatriculaAtiva;
    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao(this.idMatricula);
    this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);

    //armazena a url da relação de disciplinas
    this.relacaodisciplinas = '';
  }

  /**
   * Gera a Url e injeta no iframe para efetuar o download da Relação de Disciplinas.
   */
  gerarRelacaoDisciplinas = () => {

    const idMatricula = this.idMatricula;
    const idEntidade = this.MatriculaAtiva.id_entidade;
    const downloadAction = 'relacao-disciplinas';

    const urlDownload = this.DownloadService.geraUrlDownloadRelacaoDisciplinas(downloadAction, idMatricula, idEntidade);
    const win = this.$window.open(urlDownload, '_blank');

    if (win) {
      win.focus();
    }
    else {
      this.toaster.notify('warning', '', 'É necessário permitir que o navegador abra uma nova aba.');
    }

  }

}

export default PcHistoricoDisciplinasMenuController;
