import template from './pcHistoricoDisciplinasMenu.html';
import controller from './pcHistoricoDisciplinasMenu.controller';
import './pcHistoricoDisciplinasMenu.scss';

const pcHistoricoDisciplinasMenuComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcHistoricoDisciplinasMenuComponent;
