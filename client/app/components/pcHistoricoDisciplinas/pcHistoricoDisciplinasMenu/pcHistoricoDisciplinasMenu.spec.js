import PcHistoricoDisciplinasMenuModule from './pcHistoricoDisciplinasMenu';
import PcHistoricoDisciplinasMenuController from './pcHistoricoDisciplinasMenu.controller';
import PcHistoricoDisciplinasMenuComponent from './pcHistoricoDisciplinasMenu.component';
import PcHistoricoDisciplinasMenuTemplate from './pcHistoricoDisciplinasMenu.html';

describe('PcHistoricoDisciplinasMenu', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcHistoricoDisciplinasMenuModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcHistoricoDisciplinasMenuController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
      let component = PcHistoricoDisciplinasMenuComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(PcHistoricoDisciplinasMenuTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(PcHistoricoDisciplinasMenuController);
      });
  });
});
