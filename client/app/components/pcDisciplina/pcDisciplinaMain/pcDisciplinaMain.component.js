import template from './pcDisciplinaMain.html';
import controller from './pcDisciplinaMain.controller';
import './pcDisciplinaMain.scss';

const pcDisciplinaMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcDisciplinaMainComponent;
