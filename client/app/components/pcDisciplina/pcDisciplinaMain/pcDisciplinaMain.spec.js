import PcDisciplinaMainModule from './pcDisciplinaMain';
import PcDisciplinaMainController from './pcDisciplinaMain.controller';
import PcDisciplinaMainComponent from './pcDisciplinaMain.component';
import PcDisciplinaMainTemplate from './pcDisciplinaMain.html';

describe('PcDisciplinaMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcDisciplinaMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcDisciplinaMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcDisciplinaMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcDisciplinaMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcDisciplinaMainController);
    });
  });
});
