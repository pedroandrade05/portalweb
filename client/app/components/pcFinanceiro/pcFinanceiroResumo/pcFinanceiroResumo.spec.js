import pcFinanceiroResumoModule from './pcFinanceiroResumo';
import pcFinanceiroResumoController from './pcFinanceiroResumo.controller';
import pcFinanceiroResumoComponent from './pcFinanceiroResumo.component';
import pcFinanceiroResumoTemplate from './pcFinanceiroResumo.html';

describe('pcFinanceiroResumo', () => {
  let $rootScope, makeController;

  beforeEach(window.module(pcFinanceiroResumoModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new pcFinanceiroResumoController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = pcFinanceiroResumoComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(pcFinanceiroResumoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(pcFinanceiroResumoController);
    });
  });
});
