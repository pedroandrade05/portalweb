import template from './pcFinanceiroResumo.html';
import controller from './pcFinanceiroResumo.controller';
import './pcFinanceiroResumo.scss';

const pcFinanceiroResumoComponent = {
  restrict: 'E',
  bindings: {
    doRefresh: '=',
    lista: '<',
    resumo: '='
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pcFinanceiroResumoComponent;
