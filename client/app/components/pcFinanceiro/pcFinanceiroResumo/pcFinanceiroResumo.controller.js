class pcFinanceiroResumoController {
  constructor(
    /* Úteis */ $state, $scope, $filter,
    /* Services */ FinanceiroService, NotifyService,
    /* Constantes */ StatusLancamentoFinanceiro
  ) {
    'ngInject';

    /* Úteis */
    this.name = 'pcFinanceiroResumo';
    this.state = $state;
    this.$scope = $scope;
    this.$filter = $filter;

    /* Services */
    this.FinanceiroService = FinanceiroService;
    this.toaster = NotifyService;

    /* Constantes */
    this.StatusLancamento = StatusLancamentoFinanceiro;

    this.contratos = {};

    this.loading = true;
    this.lancamentos = this.lista;

    this.buscarContratos(this.lancamentos.lancamentosMes, this.StatusLancamento.LANCAMENTOSMES);
    this.buscarContratos(this.lancamentos.lancamentosVencidas, this.StatusLancamento.LANCAMENTOSVENCIDAS);
    this.buscarContratos(this.lancamentos.lancamentosFuturos, this.StatusLancamento.LANCAMENTOSFUTUROS);
    this.buscarContratos(this.lancamentos.lancamentosPagos, this.StatusLancamento.LANCAMENTOSPAGOS);
    this.loading = false;
  }

  buscarContratos = (lancamentos, idSituacao) => {
    this.loading = true;

    var arrLancamento = lancamentos.cursos.concat(lancamentos.outrosProdutos);
    var contratos = [];

    angular.forEach(this.contratos, (contrato) => {
      contratos[contrato.id_venda] = contrato;
    });

    angular.forEach(arrLancamento, (lancamento) => {
      var id_venda = lancamento.id_venda;
      lancamento.id_situacao = idSituacao;
      if (!contratos[id_venda]) {
        contratos[id_venda] = {
          id_venda: id_venda,
          id_meiopagamento: [],
          st_loja: lancamento.st_urlloja,
          st_produto: lancamento.st_produto,
          lancamentos: [],
          valorContrato: null,
          formaPagamento:[],
          situacoes: {
            1: {valor: 0, quantidade: 0},
            2: {valor: 0, quantidade: 0},
            3: {valor: 0, quantidade: 0},
            4: {valor: 0, quantidade: 0}
          },
          grafico: {}
        };
      }

      if (!contratos[id_venda].formaPagamento.find((valor) => {
        return valor === lancamento.st_meiopagamento;
      })) {
        contratos[id_venda].formaPagamento.push(lancamento.st_meiopagamento);
        contratos[id_venda].id_meiopagamento.push(lancamento.id_meiopagamento);
      }
      lancamento.nu_parcela = +lancamento.nu_parcela;
      contratos[id_venda].situacoes[idSituacao].quantidade++;
      contratos[id_venda].situacoes[idSituacao].valor += parseFloat(lancamento.nu_valor.replace(',','.'));
      contratos[id_venda].valorContrato += parseFloat(lancamento.nu_valor.replace(',','.'));
      contratos[id_venda].lancamentos.push(lancamento);
      contratos[id_venda].grafico = this.getDadosGrafico(contratos[id_venda].situacoes);
    });
    this.contratos = Object.values(contratos);
  }

  /**
   ** Retorna ao componente financeiroForm.
   **/
  close = () => {
    this.resumo = false;
  }

  getDadosGrafico = (situacao) => {
    var retorno = {labels:[], data:[], cores:[], options: {}};
    var cores = [];
    for (var [key, value] of Object.entries(situacao)) {
      retorno.labels.push(this.$filter('getStatusLancamentoFinanceiro')(key));
      retorno.data.push(value.quantidade);

      var cor = this.$filter('getCorStatusLancamentoFinanceiro')(key);
      retorno.cores.push({
        backgroundColor: cor,
        borderColor: cor,
        pointBackgroundColor: cor,
        pointBorderColor: cor,
        pointHoverBackgroundColor: cor,
        pointHoverBorderColor: cor
      });
      cores.push(cor);
    }
    retorno.options = {
      animation: false,
      cutoutPercentage: 70,
      tooltips: {
        enabled: false
      },
      plugins: {
        labels: {
          render: 'percentage',
          fontColor: cores,
          position: 'outside',
          fontSize: 16,
          fontFamily: '"Ubuntu"',
          textMargin: 10,
          outsidePadding: 20
        }
      }
    };
    return retorno;
  }


}

export default pcFinanceiroResumoController;
