/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from "angular";
import uiRouter from "angular-ui-router";
import pcFinanceiroResumoComponent from "./pcFinanceiroResumo.component";
import Services from "../../../services/Services";

//Import modulo de rotas angular
//Import componente do modulo
//Import do modulo de services do portal

//Modulo principal do componente
const pcFinanceiroResumoModule = angular.module('pcFinanceiroResumo', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])
  .config(($stateProvider) => {
    "ngInject"; //Inject necessario para que o angular faca as injecoes as factorys e services

    //Definicao de rotas para o componente
    $stateProvider
      .state('app.financeiro.resumo', {
        url: '/resumo',
        views: {
          'financeiro-view': {
            template: '<pc-financeiro-resumo></pc-financeiro-resumo>'
          }
        }
      });
  })
  .component('pcFinanceiroResumo', pcFinanceiroResumoComponent);

export default pcFinanceiroResumoModule;
