import template from './pcFinanceiroCabecalho.html';
import controller from './pcFinanceiroCabecalho.controller';
import './pcFinanceiroCabecalho.scss';

const pcFinanceiroCabecalhoComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcFinanceiroCabecalhoComponent;
