
class pcFinanceiroCabecalhoController {

  constructor(CONFIG, $state, EsquemaConfiguracaoService) {

    'ngInject';

    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao($state.params.idMatriculaAtiva);
    this.state = $state;
    this.name = 'pcFinanceiroCabecalho';

  }
}

export default pcFinanceiroCabecalhoController;
