/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferência para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcFinanceiroCabecalhoComponent from '../pcFinanceiroCabecalho/pcFinanceiroCabecalho.component';

//Import do modulo de services do portal
import Services from '../../../services/Services';

//Modulo principal do componente
const pcFinanceiroCabecalhoModule = angular.module('pcFinanceiroCabecalho', [
  uiRouter,
  Services.name //Injecão modulo services do portal
]).config(($stateProvider) => {
  "ngInject"; //Inject necessário para que o angular faça as injeções as factorys e services
  $stateProvider;
}).component('pcFinanceiroCabecalho', pcFinanceiroCabecalhoComponent);

export default pcFinanceiroCabecalhoModule;
