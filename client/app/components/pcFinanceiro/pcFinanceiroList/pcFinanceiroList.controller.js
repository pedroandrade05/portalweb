class PcFinanceiroListController {
  constructor(
    /* Úteis */ $filter, $window,
    /* Services */
    /* Constantes */) {
    'ngInject';
    this.name = 'pcFinanceiroList';

    this.lancamentos =  this.parcelas;

    this.$filter = $filter;

    this.$window = $window;

    this.filter = {};

    this.orderby = +this.idsituacao === 4 ? ['-id_venda', '-dt_quitado', '+dt_vencimento'] : '+dt_vencimento';

  }

  getFiltroVenda(lancamento) {
    return (+lancamento.id_venda === +this.filter.id_venda || !this.filter.id_venda);
  }

  alterarRecorrente = () => {
    var id_venda = null;
    var st_loja = null;
    angular.forEach(this.parcelas.cursos, (lancamento) => {
      if (lancamento.id_meiopagamento === 11 && lancamento.bl_quitado === false) {
        if (+lancamento.id_venda === +this.filter.id_venda) {
          id_venda = lancamento.id_venda;
          st_loja = lancamento.st_urlloja;
        }
        else if (!this.filter.id_venda) {
          id_venda = lancamento.id_venda;
          st_loja = lancamento.st_urlloja;
        }
      }
    });
    const popupWin = this.$window.open(`${st_loja}/pagamento/mudar-cartao-recorrente/id_venda/${id_venda}`);
    if (!popupWin) {
      this.showMsgPopup = 'Você precisa liberar os popups';
    }
  };

  validaRecorrente = (bl_recorrente) => {
    this.bl_recorrente = false;
    angular.forEach(this.parcelas.cursos, (lancamento) => {
      if (+lancamento.id_venda === +this.filter.id_venda) {
        if (lancamento.bl_recorrente === true) {
          this.bl_recorrente = true;
        }
      }
      else if (!this.filter.id_venda) {
        if (lancamento.bl_recorrente === true) {
          this.bl_recorrente = true;
        }
      }
    });
    bl_recorrente = this.bl_recorrente;

    return bl_recorrente;
  };
}

export default PcFinanceiroListController;
