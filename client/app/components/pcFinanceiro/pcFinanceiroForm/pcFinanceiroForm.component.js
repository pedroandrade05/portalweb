import template from './pcFinanceiroForm.html';
import controller from './pcFinanceiroForm.controller';
import './pcFinanceiroForm.scss';

const pcFinanceiroFormComponent = {
  restrict: 'E',
  bindings: {
    id_lancamento: '=?bind'
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pcFinanceiroFormComponent;
