/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcFinanceiroFormComponent from './pcFinanceiroForm.component';

//Import do modulo de services do portal
import Services from '../../../services/Services';

//Modulo principal do componente
const pcFinanceiroFormModule = angular.module('pcFinanceiroForm', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])

  .config(($stateProvider) => {
    "ngInject"; //Inject necessario para que o angular faca as injecoes as factorys e services

    //Definicao de rotas para o componente
    $stateProvider
      .state('app.financeiro.form', {
        url: '/form',
        views: {
          'financeiro-view': {
            template: '<pc-financeiro-form></pc-financeiro-form>'
          }
        }
      });
  })

.component('pcFinanceiroForm', pcFinanceiroFormComponent);

export default pcFinanceiroFormModule;
