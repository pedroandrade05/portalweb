import loadingGif from "../../../../assets/img/loading.gif";
import logoQueroPago from "../../../../assets/img/logo-quero-pago.svg";

class PcFinanceiroFormController {
  constructor(
    /* Úteis */ $state, CONFIG, $localStorage, $window, $uibModal, $rootScope, $filter,
    /* Services */ DownloadService, FinanceiroService, NotifyService, ModalService,
    /* Constantes */ Afiliado, MeioPagamentoConstante, OrigemBoleto, StatusLancamentoConstante, TipoProduto) {
    'ngInject';

    /* Úteis */
    this.$state = $state;
    this.CONFIG = CONFIG;
    this.storage = $localStorage;
    this.$window = $window;
    this.dialog = $uibModal;
    this.$rootScope = $rootScope;
    this.$filter = $filter;

    /* Services */
    this.DownloadService = DownloadService;
    this.FinanceiroService = FinanceiroService;
    this.ModalService = ModalService;

    /* Constantes */
    this.Afiliado = Afiliado;
    this.OrigemBoleto = OrigemBoleto;
    this.MeioPagamentoConstant = MeioPagamentoConstante;
    this.StatusLancamentoConstante = StatusLancamentoConstante;
    this.TipoProduto = TipoProduto;
    this.logoQueroPago = logoQueroPago;

    this.name = 'pcFinanceiroForm';
    this.loadingGif = loadingGif;
    this.toaster = NotifyService;
    this.lancamentos = null;
    this.loaded = false;
    this.loading = false;
    this.boleto = null;
    this.id_lancamento = this.id_lancamento ? this.id_lancamento : ($state.params.id_lancamento ? $state.params.id_lancamento : null);
    this.buscarLancamentos();
    this.labelBtGerarBoleto = 'Gerar Boleto';
    this.showMsgPopup = null;

    this.lancamentosQueroPago = [];
    this.st_produtosQueroPago = "";
    this.lancamentosMes = {
      cursos: [],
      outrosProdutos: [],
      vendas: []
    };

    this.lancamentosPagos = {
      cursos: [],
      outrosProdutos: [],
      vendas: []
    };
    this.lancamentosVencidas = {
      cursos: [],
      outrosProdutos: [],
      vendas: []
    };
    this.lancamentosFuturos = {
      cursos: [],
      outrosProdutos: [],
      vendas: []
    };

    // O telefone com espaços para mostrar no HTML
    this.phone = '0800 291 7878';

    // O link concatenando o DDI 55 e o telefone, removendo os espaços com regexp
    this.whatsAppLink = `https://api.whatsapp.com/send?1=pt_BR&phone=55${this.phone.replace(/\D/g, '')}`;

    this.resumo = false;
    this.lancamentosResumo = [];
  }

  /* Retorna os lançamentos para serem exibidos */

  buscarLancamentos = () => {
    this.loading = true;

    this.FinanceiroService.getVendasUser().then((response) => {
      this.lancamentos = response;
    }, (error) => {
      this.toaster.notify(error.data);
    }).finally(() => {
      this.loading = false;
      var dataAtual = new Date();
      var vendas = [];
      this.lancamentos = this.$filter('orderBy')(this.lancamentos,'id_lancamento');

      this.lancamentos.map((lancamento) => {

        if (!vendas[lancamento.id_venda]){
          vendas[lancamento.id_venda] = [0,0];
        }
        const original = lancamento.id_acordo === null;
        var nu_parcela = vendas[lancamento.id_venda][original];
        vendas[lancamento.id_venda][original] = !nu_parcela ? 1 : (nu_parcela + 1);
        lancamento.nu_parcela = vendas[lancamento.id_venda][original];
      });

      angular.forEach(this.lancamentos, (lancamento) => {


        //Lançamentos de cursos QUERO_PAGO não serão exibidos
        if (lancamento.id_afiliado === this.Afiliado.QUERO_PAGO) {

          //Monta string com cursos Quero Pago
          if (this.lancamentosQueroPago.indexOf(lancamento.id_venda) < 0) {
            this.lancamentosQueroPago.push(lancamento.id_venda);
            this.st_produtosQueroPago += this.st_produtosQueroPago === "" ? lancamento.st_curso : ", " + lancamento.st_curso;
          }

          return;
        }

        let hasCurso = false;
        let st_produto = "";
        const original = lancamento.id_acordo === null;
        lancamento.nu_parcelas = vendas[lancamento.id_venda][original];

        if (lancamento.produtos) {
          lancamento.produtos.map((produto) => {

            //Definir o nome a ser exibido no lançamento
            let st_textoNomeCurso;
            st_textoNomeCurso = produto.st_tituloexibicao;
            if (produto.st_tituloexibicao === null || produto.st_tituloexibicao === "") {
              st_textoNomeCurso = produto.st_produto;
            }

            st_produto = st_produto !== "" ? st_produto + ", " + st_textoNomeCurso : st_textoNomeCurso;

            if (+produto.id_tipoproduto === this.TipoProduto.PROJETO_PEDAGOGICO) {
              hasCurso = true;
            }

          });
        }
        lancamento.st_produto = st_produto;

        var dt_vencimento = (new Date(lancamento.dt_vencimento + ' 00:00:00'));

        if (dt_vencimento.getMonth() === dataAtual.getMonth() &&
          dt_vencimento.getFullYear() === dataAtual.getFullYear() &&
          dt_vencimento.getDate() >= dataAtual.getDate()
          && !lancamento.bl_quitado && lancamento.id_meiopagamento !== this.MeioPagamentoConstant.CARTAO_CREDITO) {
          this.separarLancamentos(lancamento, this.lancamentosMes, hasCurso, hasCurso ? "VENCE EM BREVE" : "PENDENTE");
        }
        else if (lancamento.bl_quitado || (lancamento.id_meiopagamento === this.MeioPagamentoConstant.CARTAO_CREDITO && !lancamento.bl_quitado)) {
          this.separarLancamentos(lancamento, this.lancamentosPagos, hasCurso, "PAGA");
        }
        else if ((new Date(lancamento.dt_vencimento)) < dataAtual) {
          lancamento.st_statuslancamento = "VENCIDA";
          if (hasCurso) {
            this.separarLancamentos(lancamento, this.lancamentosVencidas, hasCurso, "VENCIDA");
          }
        }
        else {
          this.separarLancamentos(lancamento, this.lancamentosFuturos, hasCurso, hasCurso ? "A VENCER" : "PENDENTE");
        }
      });
      this.abrirPortalRenegociacao();
    });

  };

  separarLancamentos = (lancamento, destino, hasCurso, st_statuslancamento ) => {

    lancamento.st_statuslancamento = st_statuslancamento;

    if (hasCurso) {
      destino.cursos.push(lancamento);
      if (!destino.vendas.includes(lancamento.id_venda)) {
        destino.vendas.push(lancamento.id_venda);
      }
    }
    else {
      destino.outrosProdutos.push(lancamento);
      if (!destino.vendas.includes(lancamento.id_venda)) {
        destino.vendas.push(lancamento.id_venda);
      }
    }

  }

  abrirPortalQueroPago = () => {
    this.$window.open('https://queropago.com.br/entrar');
  };

  abrirPortalRenegociacao = () => {
    if (this.lancamentosVencidas.cursos.length >= 3) {
      this.ModalService.show({
        title: 'Renegocie suas dívidas',
        body: 'Você tem contrato com pendência financeira. Resolva sua situação de forma exclusiva no nosso Portal de Negociação',
        confirmButton: 'Acessar',
        cancelButton: 'Fechar'
      }).then((result) => {
        if (result) {
          const popupWin = this.$window.open('https://renegociacao.unyleya.com.br/', '_blank');
          if (!popupWin) {
            this.showMsgPopup = 'Você precisa liberar os popup';
          }
        }
      });
    }
  }

  abrirResumoFinanceiro = () => {
    this.lancamentosResumo = {
      lancamentosMes: this.lancamentosMes,
      lancamentosPagos: this.lancamentosPagos,
      lancamentosVencidas: this.lancamentosVencidas,
      lancamentosFuturos: this.lancamentosFuturos
    };
    this.resumo = true;

  }
}

export default PcFinanceiroFormController;
