import PcFinanceiroFormModule from './pcFinanceiroForm';
import PcFinanceiroFormController from './pcFinanceiroForm.controller';
import PcFinanceiroFormComponent from './pcFinanceiroForm.component';
import PcFinanceiroFormTemplate from './pcFinanceiroForm.html';

describe('PcFinanceiroForm', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcFinanceiroFormModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcFinanceiroFormController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcFinanceiroFormComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcFinanceiroFormTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcFinanceiroFormController);
    });
  });
});
