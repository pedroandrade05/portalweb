import PcFinanceiroMainModule from './pcFinanceiroMain';
import PcFinanceiroMainController from './pcFinanceiroMain.controller';
import PcFinanceiroMainComponent from './pcFinanceiroMain.component';
import PcFinanceiroMainTemplate from './pcFinanceiroMain.html';

describe('PcFinanceiroMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcFinanceiroMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcFinanceiroMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcFinanceiroMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcFinanceiroMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcFinanceiroMainController);
    });
  });
});
