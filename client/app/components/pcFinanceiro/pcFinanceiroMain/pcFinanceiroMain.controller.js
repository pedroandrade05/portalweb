class PcFinanceiroMainController {
  constructor(CONFIG, $state) {
    'ngInject';

    this.state = $state;
    var that = this;
    this.$doCheck = function () {

      // Caso chamar o app.financeiro carregara o app.financeiro.form
      if ( that.state.current.name === 'app.financeiro') {
        that.state.go('app.financeiro.form');
      }
    };

  }
}

export default PcFinanceiroMainController;
