// Padrão definido em http://confluence.unyleya.com.br/pages/viewpage.action?spaceKey=DEV&title=Desenvolvimento+Novo+Portal
import angular from "angular";
import Layout from "./pcLayout/pcLayout";
import Login from "./pcLogin/pcLoginMain/pcLoginMain";
import LoginForm from "./pcLogin/pcLoginForm/pcLoginForm";
import LoginDireto from "./pcLogin/pcLoginDireto/pcLoginDireto";
import LoginSelecionaCursoForm from "./pcLogin/pcLoginSelecionaCursoForm/pcLoginSelecionaCursoForm";
import LoginAvisoOcorrenciasModule from "./pcLogin/pcLoginAvisoOcorrencias/pcLoginAvisoOcorrencias";

// Mensagens
import MensagensMain from "./pcMensagens/pcMensagensMain/pcMensagensMain";
import MensagensList from "./pcMensagens/pcMensagensList/pcMensagensList";
import MensagensShow from "./pcMensagens/pcMensagensShow/pcMensagensShow";

// Visão geral do curso
import Curso from "./pcCurso/pcCursoMain/pcCursoMain";
import CursoDisciplinaList from "./pcCurso/pcCursoDisciplinaList/pcCursoDisciplinaList";

// Financeiro
import FinanceiroMain from "./pcFinanceiro/pcFinanceiroMain/pcFinanceiroMain";
import Financeirolist from "./pcFinanceiro/pcFinanceiroList/pcFinanceiroList";
import FinanceiroForm from "./pcFinanceiro/pcFinanceiroForm/pcFinanceiroForm";
import FinanceiroResumo from "./pcFinanceiro/pcFinanceiroResumo/pcFinanceiroResumo";
import FinanceiroCabecalho from "./pcFinanceiro/pcFinanceiroCabecalho/pcFinanceiroCabecalho";

//Meus dados
import MeusDadosMain from "./pcMeusDados/pcMeusDadosMain/pcMeusDadosMain";
import MeusDadosForm from "./pcMeusDados/pcMeusDadosForm/pcMeusDadosForm";

//Ocorrencias [Menu Chamados]/ [Title]Atendimento
import ChamadosMain from "./pcChamados/pcChamadosMain/pcChamadosMain";
import ChamadosList from "./pcChamados/pcChamadosList/pcChamadosList";
import ChamadosShow from "./pcChamados/pcChamadosShow/pcChamadosShow";
import ChamadosCabecalho from "./pcChamados/pcChamadosCabecalho/pcChamadosCabecalho";
import AbrirChamados from "./pcChamados/pcAbrirChamados/pcAbrirChamados";

//Promoções
import PromocoesMain from "./pcPromocoes/pcPromocoesMain/pcPromocoesMain";
import PromocoesList from "./pcPromocoes/pcPromocoesList/pcPromocoesList";
import PromocoesShow from "./pcPromocoes/pcPromocoesShow/pcPromocoesShow";

//Modulo, uma das telas do Moodle
import ModuloMain from './pcModulo/pcModuloMain/pcModuloMain';

// Atividade Complementar
import AtividadeComplementarMain from './pcAtividadeComplementar/pcAtividadeComplementarMain/pcAtividadeComplementarMain';
import AtividadeComplementarList from './pcAtividadeComplementar/pcAtividadeComplementarList/pcAtividadeComplementarList';
import AtividadeComplementarForm from './pcAtividadeComplementar/pcAtividadeComplementarForm/pcAtividadeComplementarForm';

//Disciplina
import DisciplinaMain from './pcDisciplina/pcDisciplinaMain/pcDisciplinaMain';

//Cronograma
import CronogramaMain from './pcCronograma/pcCronogramaMain/pcCronogramaMain';

//Renovacao
import RenovacaoMain from './pcRenovacao/pcRenovacaoMain/pcRenovacaoMain';
import RenovacaoDisciplinas from './pcRenovacao/pcRenovacaoDisciplinas/pcRenovacaoDisciplinas';
import RenovacaoTabela from './pcRenovacao/pcRenovacaoTabela/pcRenovacaoTabela';
import RenovacaoConfirmacao from './pcRenovacao/pcRenovacaoConfirmacao/pcRenovacaoConfirmacao';

//Provas
import ProvasMain from './pcProvas/pcProvasMain/pcProvasMain';

//Solicitação de Documentos
import DocumentosMain from './pcDocumentos/pcDocumentosMain/pcDocumentosMain';
import DocumentosAcompanhar from './pcDocumentos/pcDocumentosAcompanhar/pcDocumentosAcompanhar';
import DocumentosSolicitar from './pcDocumentos/pcDocumentosSolicitar/pcDocumentosSolicitar';
import DocumentosMeus from './pcDocumentos/pcDocumentosMeus/pcDocumentosMeus';
import DocumentosMenu from './pcDocumentos/pcDocumentosMenu/pcDocumentosMenu';
import DocumentosConfirmar from './pcDocumentos/pcDocumentosConfirmar/pcDocumentosConfirmar';
import DocumentosInfo from './pcDocumentos/pcDocumentosInfo/pcDocumentosInfo';
import DocumentosPessoais from './pcDocumentos/pcDocumentosPessoais/pcDocumentosPessoais';

// Cursos de Extensão (Cursos Livres)
import CursosLivresMain from './pcCursosLivres/pcCursosLivresMain/pcCursosLivresMain';

//Grade Notas
import GradeNotasDetalhamentoMain from './pcGradeNotasDetalhamento/pcGradeNotasDetalhamentoMain/pcGradeNotasDetalhamentoMain';
import GradeNotasMain from './pcGradeNotas/pcGradeNotasMain/pcGradeNotasMain';
import GradeNotasForm from './pcGradeNotas/pcGradeNotasForm/pcGradeNotasForm';

//Historico Disciplinas
import HistoricoDisciplinasMain from './pcHistoricoDisciplinas/pcHistoricoDisciplinasMain/pcHistoricoDisciplinasMain';
import HistoricoDisciplinasForm from './pcHistoricoDisciplinas/pcHistoricoDisciplinasForm/pcHistoricoDisciplinasForm';
import HistoricoDisciplinasMenu from './pcHistoricoDisciplinas/pcHistoricoDisciplinasMenu/pcHistoricoDisciplinasMenu';

//Meus Documentos
import MeusDocumentosMain from './pcMeusDocumentos/pcMeusDocumentosMain/pcMeusDocumentosMain';

//Biblioteca Virtual Pearson
import PearsonMain from './pcPearson/pcPearsonMain/pcPearsonMain';

//Ferramentas Virtuais
import FerramentasVirtuaisMain from './pcFerramentasVirtuais/pcFerramentasVirtuaisMain/pcFerramentasVirtuaisMain';
import Office365Main from './pcOffice365/pcOffice365Main/pcOffice365Main';
import UnylabMain from './pcUnylab/pcUnylabMain/pcUnylabMain';

//Biblioteca Polo Pergamum
import PergamumMain from './pcPergamum/pcPergamumMain/pcPergamumMain';

//Ambientacao
import AmbientacaoMain from './pcAmbientacao/pcAmbientacaoMain/pcAmbientacaoMain';

//Maximize
import MaximizeMain from './pcMaximize/pcMaximize/pcMaximizeMain';

//Brinquedoteca
import BrinquedotecaMain from './pcBrinquedoteca/pcBrinquedotecaMain/pcBrinquedotecaMain';

//Provas Online
import ProvaOnlineMain from './pcProvasOnline/pcProvasOnlineMain/pcProvasOnlineMain';

//Provas Online Escola de Saúde
import ProvasOnlineEscolaSaudeMain from './pcProvasOnlineEscolaSaude/pcProvasOnlineEscolaSaudeMain/pcProvasOnlineEscolaSaudeMain';

//Carteira Estudantil
import CarteiraEstudantilMain from './pcCarteiraEstudantil/pcCarteiraEstudantilMain/pcCarteiraEstudantilMain';

//Clube do Livro
import ClubeLivroMain from './pcClubeLivro/pcClubeLivroMain/pcClubeLivroMain';

//Programa Indique e Ganhe
import IndiqueGanheMain from './pcIndiqueGanhe/pcIndiqueGanheMain/pcIndiqueGanheMain';

//Paginas Externas
import ExternoMain from './pcExterno/pcExternoMain/pcExternoMain';

//Modal Confirm
import PcModalConfirm from './pcModal/pcModalConfirm/pcModalConfirm';

//Modal Video Tutorial
import playerTutoriaisModal from '../directives/tutoriaisFuncionalidades/playerTutoriaisModal/playerTutoriaisModal';

//Meus Documentos
import CalendarioAcademicoMain from './pcCalendarioAcademico/pcCalendarioAcademicoMain/pcCalendarioAcademicoMain';

//Certificação
import CertificacaoMain from './pcCertificacao/pcCertificacaoMain/pcCertificacaoMain';
import CertificacaoCabecalho from './pcCertificacao/pcCertificacaoCabecalho/pcCertificacaoCabecalho';
import CertificacaoList from './pcCertificacao/pcCertificacaoList/pcCertificacaoList';
import CertificacaoConferir from './pcCertificacao/pcCertificacaoConferir/pcCertificacaoConferir';

//Praticas Juridicas
import PraticasJuridicasMain from './pcPraticasJuridicas/pcPraticasJuridicasMain/pcPraticasJuridicasMain';

export default angular.module('app.components', [
  Login.name,
  LoginForm.name,
  LoginDireto.name,
  LoginSelecionaCursoForm.name,
  LoginAvisoOcorrenciasModule.name,
  MensagensMain.name,
  MensagensList.name,
  MensagensShow.name,
  Curso.name,
  CursoDisciplinaList.name,

  FinanceiroMain.name,
  Financeirolist.name,
  FinanceiroForm.name,
  FinanceiroResumo.name,
  FinanceiroCabecalho.name,

  Layout.name,
  MeusDadosMain.name,
  MeusDadosForm.name,
  ChamadosMain.name,
  ChamadosList.name,
  ChamadosShow.name,
  ChamadosCabecalho.name,
  AbrirChamados.name,

  PromocoesMain.name,
  PromocoesList.name,
  PromocoesShow.name,

  RenovacaoTabela.name,
  RenovacaoMain.name,
  RenovacaoDisciplinas.name,
  RenovacaoConfirmacao.name,

  ProvasMain.name,

  DocumentosMain.name,
  DocumentosAcompanhar.name,
  DocumentosSolicitar.name,
  DocumentosMeus.name,
  DocumentosMenu.name,
  DocumentosConfirmar.name,
  DocumentosInfo.name,
  DocumentosPessoais.name,


  CronogramaMain.name,

  ModuloMain.name,
  AtividadeComplementarMain.name,
  AtividadeComplementarList.name,
  AtividadeComplementarForm.name,
  GradeNotasDetalhamentoMain.name,
  GradeNotasMain.name,
  GradeNotasForm.name,
  HistoricoDisciplinasMain.name,
  HistoricoDisciplinasForm.name,
  HistoricoDisciplinasMenu.name,
  DisciplinaMain.name,
  PearsonMain.name,
  PergamumMain.name,

  AmbientacaoMain.name,
  BrinquedotecaMain.name,
  ProvaOnlineMain.name,
  ProvasOnlineEscolaSaudeMain.name,
  ClubeLivroMain.name,
  CarteiraEstudantilMain.name,

  ExternoMain.name,

  IndiqueGanheMain.name,

  CursosLivresMain.name,

  MeusDocumentosMain.name,
  PcModalConfirm.name,

  FerramentasVirtuaisMain.name,
  Office365Main.name,
  UnylabMain.name,

  playerTutoriaisModal.name,
  CalendarioAcademicoMain.name,

  CertificacaoMain.name,
  CertificacaoCabecalho.name,
  CertificacaoList.name,
  CertificacaoConferir.name,

  MaximizeMain.name,

  PraticasJuridicasMain.name

]);
