import template from './pcProvasOnlineEscolaSaudeMain.html';
import controller from './pcProvasOnlineEscolaSaudeMain.controller';
import './pcProvasOnlineEscolaSaudeMain.scss';

const pcProvasOnlineEscolaSaudeMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcProvasOnlineEscolaSaudeMainComponent;
