import PcProvasOnlineEscolaSaudeMainModule from './pcProvasOnlineEscolaSaudeMain';
import PcProvasOnlineEscolaSaudeMainController from './pcProvasOnlineEscolaSaudeMain.controller';
import PcProvasOnlineEscolaSaudeMainComponent from './pcProvasOnlineEscolaSaudeMain.component';
import PcProvasOnlineEscolaSaudeMainTemplate from './pcProvasOnlineEscolaSaudeMain.html';

describe('PcProvasOnlineEscolaSaudeMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcProvasOnlineEscolaSaudeMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcProvasOnlineEscolaSaudeMainController();
    };
  }));

  describe('Module', () => {

  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcProvasOnlineEscolaSaudeMainComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcProvasOnlineEscolaSaudeMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcProvasOnlineEscolaSaudeMainController);
    });
  });
});
