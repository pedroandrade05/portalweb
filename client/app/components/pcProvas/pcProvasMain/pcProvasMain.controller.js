import loadingGif from '../../../../assets/img/loading.gif';

class PcProvasMainController {

  constructor(
    /* Úteis */ CONFIG, $q, $sce, $state, $window, $location, $localStorage, $anchorScroll,
    /* Services */ NotifyService, PessoaService, CursoAlunoService, AgendamentoService,
    /* Constantes */ SituacaoConstante, TipoAvaliacaoConstante, EvolucaoDisciplinaConstante, EsquemaConfiguracaoConstante
  ) {
    'ngInject';

    if (!$state.params.submenu) {
      $state.params.submenu = 'atuais';
    }

    this.CONFIG = CONFIG;

    // Úteis
    this.$q = $q;
    this.$sce = $sce;
    this.$state = $state;
    this.$window = $window;
    this.$location = $location;
    this.$localStorage = $localStorage;
    this.$anchorScroll = $anchorScroll;

    // Services
    this.NotifyService = NotifyService;
    this.PessoaService = PessoaService;
    this.CursoAlunoService = CursoAlunoService;
    this.AgendamentoService = AgendamentoService;

    // Constantes
    this.SituacaoConstante = SituacaoConstante;
    this.TipoAvaliacaoConstante = TipoAvaliacaoConstante;
    this.EvolucaoDisciplinaConstante = EvolucaoDisciplinaConstante;
    this.EsquemaConfiguracaoConstante = EsquemaConfiguracaoConstante;

    // Outros
    this.loading = true;
    this.loadingGif = loadingGif;
    this.idUsuario = $localStorage.user_id;
    this.idEntidade = $localStorage.user_entidade;
    this.idMatricula = $state.params.idMatriculaAtiva;

    this.informacoes = {};
    this.submenu = $state.params.submenu;
    this.idsExibidos = ($state.params.idsExibidos || '').split(',').filter((id) => Number(id)).map((id) => Number(id));
    this.matricula = this.CursoAlunoService.getStorageCursoAtivo(this.idMatricula);
    this.isGraduacao = +this.matricula.id_esquemaconfiguracao === +this.EsquemaConfiguracaoConstante.GRADUACAO;

    // Padrões
    this.pessoa = {};
    this.grupos = [];
    this.agendamentos = [];
    this.aplicacoes = [];

    // Requests
    const requests = [
      this.getInformacoes()
    ];

    if (this.submenu !== 'informacoes') {
      requests.push(this.getDadosPessoais());
      requests.push(this.getAgendamentos());
    }

    $q.all(requests).finally(() => {
      this.loading = false;
    });
  }

  isAgendado(avaliacao) {
    return [
      this.SituacaoConstante.AGENDAMENTO_AGENDADO,
      this.SituacaoConstante.AGENDAMENTO_REAGENDADO
    ].indexOf(+avaliacao.id_situacao) !== -1;
  }

  isExibido(avaliacao) {
    return this.idsExibidos.indexOf(avaliacao.id_avaliacaoagendamento) !== -1;
  }

  isProvaRealizada(avaliacao) {
    if (avaliacao.dt_aplicacao) {
      const dtAtual = this.getDateISO();
      const dtFim = this.getDateISO(avaliacao.dt_aplicacao);

      if (dtAtual > dtFim) {
        return true;
      }
    }

    if (avaliacao.nu_presenca !== null) {
      return true;
    }

    return false;
  }

  /**
   * Converter uma string ou Date em formato de data ISO String
   * @param date
   * @param fixTimeZone
   * @returns string
   */
  getDateISO(date = null, fixTimeZone = false) {
    if (!date) {
      date = new Date();
    }

    if (date instanceof Date) {
      date = date.toISOString();
    }

    date = date.substr(0, 10).split('/').reverse().join('-');

    if (fixTimeZone) {
      // o 12:00:00 é o horário médio para não ter problema com TimeZone
      date += ' 12:00:00';
    }

    return date;
  }

  /**
   * Preparar o objeto de agendamento com algumas variáveis úteis para usar em condições e também definir os grupos
   * @param agendamento
   */
  getModelAgendamento(agendamento) {
    let grupo = 'disciplina';

    if (+agendamento.id_tipoavaliacao === this.TipoAvaliacaoConstante.AVALIACAO_SEMESTRAL ||
      +agendamento.id_tipoavaliacao === this.TipoAvaliacaoConstante.AVALIACAO_SEMESTRAL_ONLINE) {
      grupo = 'semestral';
    }

    if ([
      this.TipoAvaliacaoConstante.AVALIACAO_RECUPERACAO,
      this.TipoAvaliacaoConstante.AVALIACAO_ONLINE_RECUPERACAO,
      this.TipoAvaliacaoConstante.AVALIACAO_SEMESTRAL_RECUPERACAO,
      this.TipoAvaliacaoConstante.AVALIACAO_SEMESTRAL_ONLINE_RECUPERACAO
    ].indexOf(+agendamento.id_tipoavaliacao) !== -1) {
      grupo = 'final';
    }

    // RECUPERAÇÃO da Pos Graduação
    if (agendamento.id_tipodeavaliacao) {
      grupo = 'final';
    }

    let dtInicio;
    let dtFim;

    if (agendamento.dt_aplicacaoinicio) {
      dtInicio = agendamento.dt_aplicacaoinicio;
    }

    if (agendamento.dt_aplicacao) {
      dtFim = agendamento.dt_aplicacao;

      if (!dtInicio) {
        dtInicio = dtFim;
      }
    }

    dtInicio = dtInicio ? this.getDateISO(dtInicio) : null;
    dtFim = dtFim ? this.getDateISO(dtFim) : null;

    const dtAtual = this.getDateISO();
    const dtAlteracaoLimite = this.getDateISO(agendamento.dt_alteracaolimite);

    agendamento.st_grupo = grupo;
    agendamento.bl_agendado = this.isAgendado(agendamento);
    agendamento.bl_provarealizada = this.isProvaRealizada(agendamento);
    agendamento.bl_podereagendar = !agendamento.bl_provarealizada && agendamento.nu_presenca === null && dtAlteracaoLimite >= dtAtual;
    agendamento.bl_datainiciada = dtInicio ? dtAtual >= dtInicio : false;
    agendamento.bl_datafinalizada = dtAtual > dtFim;

    return agendamento;
  }

  getDadosPessoais() {
    return this.PessoaService
      .getDadosPessoa(
        this.$localStorage.user_id,
        this.$localStorage.user_entidade
      ).then((response) => {
        this.pessoa = response;
      });
  }

  getInformacoes() {
    return this.AgendamentoService.getInformacoes(
      this.idMatricula
    ).then(
      (response) => {
        this.informacoes = response.toJSON();
      }
    );
  }

  getAgendamentos() {
    this.agendamentos = [];

    return this.AgendamentoService.getAll({
      id_matricula: this.idMatricula
    }).then(
      (response) => {
        response.forEach((agendamento) => {
          this.agendamentos.push(this.getModelAgendamento(agendamento));
        });

        this.getGrupos();
      }
    );
  }

  getGrupos() {
    this.gruposOrdem = ['-nu_ordem', '-id_grupo'];

    let attrValue = 'id_modulo';
    let attrLabel = 'st_modulo';

    let grupos = {};

    if (this.submenu === 'atuais') {
      attrValue = 'st_grupo';
      attrLabel = 'st_grupo';

      // Em provas do período, os grupos são fixos
      grupos = {
        disciplina: {
          st_grupo: this.isGraduacao ? 'Provas das disciplinas' : 'Provas globais',
          avaliacoes: []
        },
        semestral: {
          st_grupo: 'Provas Semestrais',
          avaliacoes: []
        },
        final: {
          st_grupo: this.isGraduacao ? 'Provas Finais' : 'Provas de recuperação',
          avaliacoes: []
        }
      };
    }

    this.agendamentos.forEach((agendamento) => {
      const index = agendamento[attrValue];
      const label = agendamento[attrLabel];

      const isCursando = +agendamento.id_evolucaodisciplina === +this.EvolucaoDisciplinaConstante.CURSANDO;
      const isAgendamentoAtivo = !agendamento.bl_provarealizada;

      // Provas passadas: exibir somente avaliações de disciplinas que não esteja mais cursando e que não tenha agendamento ativo
      if (this.submenu === 'passadas') {
        if (isCursando || isAgendamentoAtivo) {
          return false;
        }
      }

      // Provas atuais: somente avaliações de disciplinas cursando ou que tenha algum agendamento ativo
      if (this.submenu === 'atuais') {
        if (!isCursando && !isAgendamentoAtivo) {
          return false;
        }
      }

      if (!grupos[index]) {
        grupos[index] = {
          id_grupo: index,
          st_grupo: label,
          nu_ordem: agendamento.nu_ordemmodulo,
          avaliacoes: []
        };
      }

      grupos[index].avaliacoes.push(agendamento);
    });

    // Remover provas semestrais quando não for graduação
    if (!this.isGraduacao) {
      delete grupos.semestral;
    }

    this.grupos = Object.values(grupos);
  }

  getUrlComprovante(avaliacao) {
    return this.CONFIG.urlG2 + 'paper/agendamento?id_avaliacaoagendamento=' + avaliacao.id_avaliacaoagendamento;
  }

  getLocalProva(avaliacao) {
    return [
      avaliacao.st_endereco, avaliacao.nu_numero,
      avaliacao.st_complemento, avaliacao.st_aplicadorprova,
      avaliacao.st_bairro, avaliacao.st_cidade, avaliacao.sg_uf
    ].filter((item) => item).join(' - ').replace(' -', ', ');
  }

  getAplicacoes(avaliacao) {
    this.aplicacoes = [];

    const sgUf = avaliacao.sg_uf || this.pessoa.sg_uf;
    const idMunicipio = avaliacao.id_municipio || this.pessoa.id_municipio;

    if (!sgUf || !idMunicipio) {
      this.NotifyService.notify('warning', '', 'Antes de agendar a avaliação, verifique o seu endereço.');
    }

    return this.AgendamentoService
      .getAvaliacaoAplicacoes(
        this.idMatricula,
        avaliacao.id_avaliacao,
        sgUf,
        idMunicipio,
        avaliacao.id_disciplina
      )
      .then(
        (response) => {
          this.aplicacoes = response;
          return response;
        },
        (error) => {
          const type = error.status === 404 ? 'warning' : 'error';
          this.NotifyService.notify(type, '', error.data.message);
        }
      );
  }

  getCalendarDayClass(data) {
    const mode = data.mode;

    if (mode === 'day') {
      if (this.possuiAplicacao(data.date)) {
        return 'datepicker-aplicacao';
      }

      if (this.getDateISO(data.date) === this.getDateISO()) {
        return 'datepicker-today';
      }
    }

    return '';
  }

  gotoAnchor(avaliacao) {
    if (!this.anchored && this.idsExibidos.indexOf(avaliacao.id_avaliacaoagendamento) !== -1) {
      avaliacao.expanded = true;
      avaliacao.highlighted = false;

      // Temporariamente definir o hash para rolar a página
      this.$location.hash('av' + avaliacao.id_avaliacaoagendamento);
      this.$anchorScroll();
      this.anchored = true;

      // Remover hash
      setTimeout(() => this.$location.hash(''), 1);
    }
  }

  toggleAvaliacao(avaliacao) {
    avaliacao.expanded = !avaliacao.expanded;
    avaliacao.highlighted = true;

    const idAgendamento = avaliacao.id_avaliacaoagendamento;

    if (avaliacao.expanded) {
      this.idsExibidos.indexOf(idAgendamento) === -1 && this.idsExibidos.push(idAgendamento);
    }

    if (!avaliacao.expanded) {
      var index = this.idsExibidos.indexOf(idAgendamento);

      if (index !== -1) {
        this.idsExibidos.splice(index, 1);
      }
    }

    this.$state.go('app.provas', {
      submenu: this.submenu,
      idsExibidos: this.idsExibidos.join(',')
    }, {
      notify: false,
      inherit: true
    });

  }

  checkDisabled(data) {
    return !this.possuiAplicacao(data.date);
  }

  possuiAplicacao(date) {
    let response = false;

    const dateToCheck = this.getDateISO(date);

    this.aplicacoes.forEach((aplicacao) => {
      const dtAplicacao = aplicacao.dt_aplicacaoinicio || aplicacao.dt_aplicacao;

      if (dateToCheck === this.getDateISO(dtAplicacao)) {
        response = true;
      }
    });

    return response;
  }

  mostrarAlteracaoAgendamento(avaliacao) {
    if (avaliacao.loadingCalendario || avaliacao.expandedDate) {
      return true;
    }

    avaliacao.id_avaliacaoaplicacao = null;
    avaliacao.loadingCalendario = true;

    this.getAplicacoes(avaliacao).then(
      () => {
        this.configCalendario(avaliacao);
        avaliacao.expandedDate = true;
        avaliacao.loadingCalendario = false;
      }
    );
  }

  configCalendario(avaliacao) {
    // Resetar valores
    this.calendarDate = new Date();
    avaliacao.hr_inicionova = null;
    avaliacao.horarios = [];
    avaliacao.locais = [];
    avaliacao.id_avaliacaoaplicacao = null;

    if (this.possuiAplicacao(this.calendarDate)) {
      this.verificarHorarios(avaliacao, this.calendarDate);
    }

    this.calendarOptions = {
      minDate: new Date(),
      showWeeks: false,
      customClass: this.getCalendarDayClass.bind(this),
      dateDisabled: this.checkDisabled.bind(this)
    };
  }

  verificarHorarios(avaliacao, date) {
    // Resetar valores
    avaliacao.hr_inicionova = null;
    avaliacao.horarios = [];
    avaliacao.locais = [];
    avaliacao.id_avaliacaoaplicacao = null;

    if (!this.possuiAplicacao(date)) {
      return false;
    }

    const dateToCheck = this.getDateISO(date);

    this.aplicacoes.forEach((aplicacao) => {
      const dtAplicacao = aplicacao.dt_aplicacaoinicio || aplicacao.dt_aplicacao;

      if (dateToCheck === this.getDateISO(dtAplicacao) && avaliacao.horarios.indexOf(aplicacao.hr_inicio) === -1) {
        avaliacao.horarios.push(aplicacao.hr_inicio);
      }
    });
  }

  verificarLocais(avaliacao, date, hour) {
    // Resetar valores
    avaliacao.locais = [];
    avaliacao.id_avaliacaoaplicacao = null;

    if (!this.possuiAplicacao(date)) {
      return false;
    }

    const dateToCheck = this.getDateISO(date);

    avaliacao.locais = this.aplicacoes.filter((aplicacao) => {
      const dtAplicacao = aplicacao.dt_aplicacaoinicio || aplicacao.dt_aplicacao;
      return dateToCheck === this.getDateISO(dtAplicacao) && aplicacao.hr_inicio === hour;
    });
  }

  toggleBotaoProva(avaliacao) {
    // É necessário existir a informação de integração
    if (!avaliacao.cod_prova) {
      return false;
    }

    // A situação precisa ser AGENDADO ou REAGENDADO
    if (!this.isAgendado(avaliacao)) {
      return false;
    }

    // Só deve ser exibido se o período de aplicação de prova já iniciou
    if (!avaliacao.bl_datainiciada) {
      return false;
    }

    return true;
  }

  abrirAvaliacaoMaximize = (avaliacao) => {
    const $win = this.$window.open(
      this.CONFIG.urlG2 + 'default/redirecionar/autenticar-maximize?id_usuario=' + this.idUsuario + '&id_entidade=' + this.idEntidade
    );

    if (!$win) {
      this.NotifyService.notify('warning', '', 'O carregamento da avaliação em uma nova aba foi bloqueada pelo navegador.');
    }

    if (avaliacao.st_linkexternoavaliacao) {
      setTimeout(() => {
        $win.location.href = avaliacao.st_linkexternoavaliacao;
      }, 1500);
    }
  };

  salvarAgendamento(avaliacao) {
    if (avaliacao.loadingConfirmar) {
      return false;
    }

    if (!avaliacao.id_avaliacaoaplicacao) {
      this.NotifyService.notify('warning', '', 'Por favor, selecione a data/horário e local para realizar a prova.');
      return false;
    }

    avaliacao.loadingConfirmar = true;

    return this.AgendamentoService.getResource()
      .create(avaliacao)
      .$promise.then(
        (response) => {
          angular.extend(avaliacao, this.getModelAgendamento(response.toJSON()));

          // Resetar estados (carregamento e exibição)
          avaliacao.loadingConfirmar = false;
          avaliacao.expandedDate = false;

          this.NotifyService.notify('success', '', 'Sua avaliação foi agendada!');
        },
        (error) => {
          avaliacao.loadingConfirmar = false;
          this.NotifyService.notify('error', '', error.data.message);
        }
      );
  }

}

export default PcProvasMainController;
