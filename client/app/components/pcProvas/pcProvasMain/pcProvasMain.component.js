import template from './pcProvasMain.html';
import controller from './pcProvasMain.controller';
import './pcProvasMain.scss';

const pcProvasMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcProvasMainComponent;
