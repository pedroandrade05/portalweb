/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcProvasMainComponent from './pcProvasMain.component';

//Import do modulo de services do portal
import Services from '../../../services/Services';

//Modulo principal do componente
const pcProvasMainModule = angular.module('pcProvasMain', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])
  .config(($stateProvider) => {
    "ngInject"; //Inject necessario para que o angular faca as injecoes as factorys e services

    //Definicao de rotas para o componente
    $stateProvider
      .state('app.provas', {
        url: '/:idMatriculaAtiva/provas/:submenu/:idsExibidos',
        params: {
          submenu: {
            value: null,
            squash: true
          },
          idsExibidos: {
            value: null,
            squash: true
          }
        },
        views: {
          content: {
            template: '<pc-provas-main class="component-maxheight scroll-wrapper"></pc-provas-main>'
          }
        }
      });
  })
  .component('pcProvasMain', pcProvasMainComponent);

export default pcProvasMainModule;
