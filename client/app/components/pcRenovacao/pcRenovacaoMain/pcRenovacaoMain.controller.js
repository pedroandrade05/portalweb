import estrela from '../../../../assets/img/estrela_amarela.svg';
import loadingGif from '../../../../assets/img/loading.gif';

class PcRenovacaoMainController {

  constructor(
    /* Úteis */ $state, $localStorage, $scope, $rootScope, $filter,
    /* Services */ RenovacaoService, NotifyService,
    /* Constantes */ TipoOfertaConstante
  ) {
    "ngInject";

    // Úteis
    this.$state = $state;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$localStorage = $localStorage;
    this.$filter = $filter;

    // Services
    this.RenovacaoService = RenovacaoService;
    this.NotifyService = NotifyService;

    // Constantes
    this.TipoOfertaConstante = TipoOfertaConstante;

    // Arquivos
    this.estrela = estrela;
    this.loading = loadingGif;

    // Dados
    this.arrayBotao = {0: 'Mostrar detalhes', 1: 'Fechar detalhes'};
    this.detalhes = {tipo: 1, texto: this.arrayBotao[1], classe: 'arrowUp'};
    this.confirmacao = false;

    // Tratamento dos dados
    this.info = {
      dados: angular.fromJson(this.$state.params.dados) || {},
      renovacao: {},
      contrato: {},
      salasAgrupadas: [],
      salasDisponiveis: [],
      salasSelecionadas: [],
      primeiraOfertaPadrao: null,
      ultimaOfertaPadrao: null
    };


    this.buscarDisciplinasRenovacao();

    const renovacaoContratoListener = this.$rootScope.$on('calcularContrato', (e, salasSelecionadas, callback= null) => {
      this.loadingContrato = true;

      this.info.salasSelecionadas = salasSelecionadas || [];

      this.RenovacaoService
        .calcularContrato({
          id_campanhacomercial: this.info.renovacao.id_campanhacomercial,
          id_campanhapontualidade: this.info.renovacao.id_campanhapontualidade,
          id_entidade: this.info.renovacao.id_entidade,
          id_matricula: this.info.renovacao.id_matricula,
          id_produto: this.info.renovacao.id_produto,
          id_projetopedagogico: this.info.renovacao.id_projetopedagogico,
          id_venda: this.info.renovacao.id_venda,
          id_disciplina: this.info.salasSelecionadas.map((sala) => sala.id_disciplina)
        })
        .then(
          (response) => {
            this.loadingContrato = false;

            this.info.contrato = response || {};
            this.info.contrato.id_meiopagamento = 1;
          },
          () => {
            this.NotifyService.notify('error', '', 'Não foi possível calcular o contrato!');
          },
        )
        .finally(() => {
          if (angular.isFunction(callback)) {
            callback();
          }
        });
    });

    this.$scope.$on('$destroy', renovacaoContratoListener);
  }

  redefinirDisciplinas() {
    this.$rootScope.$broadcast('redefinirDisciplinas');
  }

  async buscarDisciplinasRenovacao() {
    this.processando = true;

    if (!this.info.dados || !this.info.dados.id_usuario) {
      /*eslint-disable */
      try {
        this.info.dados = await this.RenovacaoService.verificarRenovacao(this.$state.params.idMatriculaAtiva);
      } catch (e) {
        this.NotifyService.notify('error', 'Houve um erro de comunicação ao tentar recuperar os dados de renovação.');
      }
      /*eslint-enable */
    }

    // Se não está apto OU a venda já foi confirmada, não buscar os dados para a renovação
    if (!this.info.dados || !this.info.dados.bl_aptorenovacao || +this.info.dados.id_evolucaovenda !== 9) {
      this.processando = false;
      return false;
    }

    this.RenovacaoService.buscarDisciplinasRenovacao(
      this.info.dados.id_vendaproduto,
      this.info.dados.id_usuario,
      this.info.dados.id_projetopedagogico,
      this.info.dados.id_entidadematricula,
      this.$state.params.idMatriculaAtiva,
      this.info.dados.id_turma
    ).then(
      (response) => {
        this.processando = false;

        if (
          angular.isDefined(response.bl_renovacao) && response.bl_renovacao === false
          && angular.isDefined(response.dt_renovacao) && response.dt_renovacao
        ) {
          this.info.dados.dt_renovacao = response.dt_renovacao;
          this.mensagemProximaRenovacao = true;
          return true;
        }

        if (angular.isDefined(response.dadosUsuario) && angular.isDefined(response.dadosUsuario.id_disciplina)) {
          response.dadosUsuario.id_entidade = this.info.dados.id_entidade;

          this.info.renovacao = response.dadosUsuario;
          this.info.salasAgrupadas = response.disciplinasDisponiveis;

          Object.values(this.info.salasAgrupadas).forEach((tipos) => {
            const ofertas = this.$filter('orderBy')(tipos.ofertas, 'dt_abertura');
            Object.values(ofertas).forEach((oferta) => {
              if (oferta.id_tipooferta === this.TipoOfertaConstante.PADRAO) {
                if (!this.info.primeiraOfertaPadrao) {
                  this.info.primeiraOfertaPadrao = oferta;
                }
                this.info.ultimaOfertaPadrao = oferta;
              }
            });
          });
        }
        else {
          this.NotifyService.notify('error', '', 'Não foi possível carregar as disciplinas da renovação!');
        }
      },
      (erro) => {
        this.processando = false;
        this.NotifyService.notify('error', '', erro.data.message);
      },
    );
  }

  continuar() {
    // a const minDisciplinas se refere a quantidade minima da disciplina de oferta padrao
    const minDisciplinas = this.info.renovacao.nu_mindisciplinas;
    const nuDisciplinasPadrao = this.info.renovacao.disciplinas.filter((item) => +item.id_tipooferta === this.TipoOfertaConstante.PADRAO).length;
    const nuPadraoSelecionadas = this.info.salasSelecionadas.filter((item) => +item.id_tipooferta === this.TipoOfertaConstante.PADRAO).length;

    if (nuDisciplinasPadrao >= minDisciplinas && nuPadraoSelecionadas < minDisciplinas) {
      const message = +minDisciplinas === 1 ? 'disciplina padrão' : 'disciplinas padrões';
      this.NotifyService.notify('warning', 'Selecione no mínimo ' + minDisciplinas + ' ' + message + ' para continuar');
      return false;
    }

    if (this.info.salasSelecionadas.length <= 0) {
      this.NotifyService.notify('warning', 'Selecione no mínimo 1 disciplina para continuar');
      return false;
    }

    this.confirmacao = true;
  }

  mostrarDetalhes() {
    this.detalhes.tipo = !this.detalhes.tipo;
    this.detalhes.texto = this.arrayBotao[this.detalhes.tipo ? 1 : 0];
    this.detalhes.classe = this.detalhes.tipo ? 'arrowUp' : 'arrowDown';
  }

  contarSelecionadas(idTipoOferta = this.TipoOfertaConstante.PADRAO) {
    return this.info.salasSelecionadas.filter((item) => +item.id_tipooferta === +idTipoOferta).length;
  }

}

export default PcRenovacaoMainController;
