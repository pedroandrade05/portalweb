import PcRenovacaoMainModule from './pcRenovacaoMain';
import PcRenovacaoMainController from './pcRenovacaoMain.controller';
import PcRenovacaoMainComponent from './pcRenovacaoMain.component';
import PcRenovacaoMainTemplate from './pcRenovacaoMain.html';

describe('PcRenovacaoMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcRenovacaoMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcRenovacaoMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcRenovacaoMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcRenovacaoMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcRenovacaoMainController);
    });
  });
});
