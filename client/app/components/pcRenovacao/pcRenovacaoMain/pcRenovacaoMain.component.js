import template from './pcRenovacaoMain.html';
import controller from './pcRenovacaoMain.controller';
import './pcRenovacaoMain.scss';

const pcRenovacaoMainComponent = {
  restrict: 'E',
  bindings: {
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pcRenovacaoMainComponent;
