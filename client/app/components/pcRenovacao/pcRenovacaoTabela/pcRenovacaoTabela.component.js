import template from './pcRenovacaoTabela.html';
import controller from './pcRenovacaoTabela.controller';
import './pcRenovacaoTabela.scss';

const pcRenovacaoTabelaComponent = {
  restrict: 'E',
  bindings: {
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pcRenovacaoTabelaComponent;
