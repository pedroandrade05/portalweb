class PcRenovacaoTabelaController {

  constructor(/* Úteis */ $scope, $rootScope,
              /* Constantes */ TipoOfertaConstante) {
    'ngInject';

    this.$scope = $scope;
    this.$rootScope = $rootScope;

    // Constantes
    this.TipoOfertaConstante = TipoOfertaConstante;

    this.semestres = [];
    this.ofertas = [];
    this.salasSelecionadas = [];

    this.$rootScope.$on('tabela', (e, salasDisponiveis) => {
      // Definir headers da tabela
      salasDisponiveis.forEach((sala) => {
        if (this.semestres.indexOf(sala.st_modulo) === -1) {
          this.semestres.push(sala.st_modulo);
        }

        if (sala.id_tipooferta !== this.TipoOfertaConstante.PERENE && this.ofertas.indexOf(sala.dt_abertura) === -1) {
          this.ofertas.push(sala.dt_abertura);
        }
      });

      this.semestres.sort();
      this.ofertas.sort();
    });

    const renovacaoTabelaListener = this.$rootScope.$on('salasSelecionadas', (e, disciplinas) => {
      this.salasSelecionadas = disciplinas;
    });

    this.$scope.$on('$destroy', renovacaoTabelaListener);
  }
}

export default PcRenovacaoTabelaController;
