import PcRenovacaoTabelaModule from './pcRenovacaoTabela';
import PcRenovacaoTabelaController from './pcRenovacaoTabela.controller';
import PcRenovacaoTabelaComponent from './pcRenovacaoTabela.component';
import PcRenovacaoTabelaTemplate from './pcRenovacaoTabela.html';

describe('PcRenovacaoTabela', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcRenovacaoTabelaModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcRenovacaoTabelaController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcRenovacaoTabelaComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcRenovacaoTabelaTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcRenovacaoTabelaController);
    });
  });
});
