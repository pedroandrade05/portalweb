class PcRenovacaoDisciplinasController {

  constructor(
    /* Úteis */ $localStorage, $scope, $state, $rootScope, $filter,
    /* Services */ CursoAlunoService, NotifyService,
    /* Constantes */ EsquemaConfiguracaoConstante, TipoOfertaConstante
  ) {
    "ngInject";

    // Úteis
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$filter = $filter;
    this.$localStorage = $localStorage;

    // Services
    this.CursoAlunoService = CursoAlunoService;
    this.NotifyService = NotifyService;

    // Constantes
    this.TipoOfertaConstante = TipoOfertaConstante;
    this.EsquemaConfiguracaoConstante = EsquemaConfiguracaoConstante;

    // Dados Matrícula
    this.idMatricula = $state.params.idMatriculaAtiva;
    this.cursoAtivo = this.CursoAlunoService.getStorageCursoAtivo(this.idMatricula);

    // Informações herdadas de pcRenovacaoMain
    this.info = angular.fromJson(this.dados);

    // Preparar as salas e selecionar as padrões
    this.prepararSalas();

    // Agrupar salas por semestre/período
    const salaspormodulo = this.periodos = this.$filter('orderBy')(this.info.salasDisponiveis, 'st_modulo');
    this.periodos = this.$filter('groupBy')(salaspormodulo, 'st_modulo');

    // Remover todas as disciplinas
    const renovacaoRedefinirListener = this.$rootScope.$on('redefinirDisciplinas', () => {
      // Desamarcar todas as disciplinas
      this.info.salasSelecionadas = [];

      // Esvaziar o array de disciplinas padrão para não selecionar novamente
      this.info.renovacao.disciplinas = [];

      // Executar rotina para selecionar disciplinas obrigattórias e ajustar alguns dados
      this.prepararSalas();
    });

    this.$scope.$on('$destroy', renovacaoRedefinirListener);
  }

  prepararSalas() {
    // Preparar salas
    let salasDisponiveis = [];

    // Agrupar salas num único array
    Object.values(this.info.salasAgrupadas).forEach((tipos) => {
      Object.values(tipos.salas).forEach((modulo) => {
        // Remover salas aprovadas ou cursando
        const naoCursadas = modulo.filter((item) => [12, 13].indexOf(item.id_evolucao) === -1);
        salasDisponiveis = salasDisponiveis.concat(naoCursadas);
      });
    });

    // Ordenar as salas
    this.info.salasDisponiveis = this.$filter('orderBy')(salasDisponiveis, 'dt_abertura');

    // Primeiramente verificar disciplinas obrigatórias
    this.info.salasDisponiveis.forEach((sala) => {
      sala.st_tipooferta = this.$filter('getNomeOferta')(sala.id_tipooferta);

      // Se a disciplina ainda não foi cursada
      // E não possui nenhuma outra sala selecionada para a mesma disciplina
      // Então selecionar sala
      if (sala.bl_obrigatorio && !this.isDisciplinaCursada(sala) && !this.isDisciplinaSelecionada(sala)) {
        this.toggleSala(sala, false);
      }
    });

    // Selecionar disciplinas padrão caso já não tenha uma sala da mesma disciplina selecionada (anteriormente na seleção das obrigatórias)
    if (this.info.renovacao.disciplinas) {
      this.info.renovacao.disciplinas.forEach((sala) => {
        if (!this.isDisciplinaSelecionada(sala)) {
          this.toggleSala(sala, false);
        }
      });
    }

    // Emitir eventos para os outros componentes
    this.emitEvents();
  }

  toggleSala(item, emitEvents = true) {
    let current = this.isSalaSelecionada(item);

    if (current) {
      // Remover
      this.info.salasSelecionadas.splice(this.info.salasSelecionadas.indexOf(current), 1);
    }
    else {
      // Buscar informações da sala
      current = this.info.salasDisponiveis.find((sala) => +sala.id_saladeaula === +item.id_saladeaula && +sala.id_disciplina === +item.id_disciplina);

      if (current) {
        // @history GII-11302
        // Se a disciplina selecionada for de oferta PADRÃO
        if (+current.id_tipooferta === this.TipoOfertaConstante.PADRAO) {
          const nuPadraoSelecionadas = this.info.salasSelecionadas.filter((item) => +item.id_tipooferta === this.TipoOfertaConstante.PADRAO).length;

          // @history GII-11302
          // Verificar a quantidade limite de disciplinas desconsiderando ofertas estendidas
          if (nuPadraoSelecionadas >= this.info.renovacao.nu_maxdisciplinas) {
            this.NotifyService.notify('warning', 'Somente é permitido selecionar o máximo de ' + this.info.renovacao.nu_maxdisciplinas + ' disciplinas padrões.');
            current.checked = false;
            return false;
          }
        }

        // @history GII-11506
        // Permitir somente uma disciplina por oferta na Escola de Saúde (conferir id da oferta e data)
        // @history POR-843
        // Permitir no máximo duas disciplinas por oferta na graduação (conferir somente id da oferta )
        const nuDisOfertaSelecionada = this.info.salasSelecionadas.filter((item) => +item.id_periodoletivo === +current.id_periodoletivo).length;
        const nuDataOfertaSelecionada = this.info.salasSelecionadas.filter((item) => item.dt_abertura === current.dt_abertura).length;

        let nuMaxDiasPorOferta = 2;
        let nuMaxDataPorOferta = Number.POSITIVE_INFINITY;

        if ([
          this.EsquemaConfiguracaoConstante.ESCOLA_TECNICA_SEMESTRAL,
          this.EsquemaConfiguracaoConstante.ESCOLA_TECNICA_PADRAO_POS
        ].includes(this.cursoAtivo.id_esquemaconfiguracao)) {
          nuMaxDiasPorOferta = 1;
          nuMaxDataPorOferta = 1;
        }

        if (!current.bl_obrigatorio && (nuDisOfertaSelecionada >= nuMaxDiasPorOferta || nuDataOfertaSelecionada >= nuMaxDataPorOferta)) {
          const dtFormatada = this.$filter('asDate')(current.dt_abertura, 'dd/MM/yyyy');

          this.NotifyService.notify('warning', 'O máximo de ' + nuMaxDiasPorOferta + ' disciplinas para a data ' + dtFormatada + ' já foi selecionado.');
          current.checked = false;
          return false;
        }

        // @history GII-12018
        // Somente é permitido uma disciplina optativa
        if (current.bl_optativo) {
          const optativa = this.getOptativaSelecionada();

          if (optativa) {
            this.NotifyService.notify('warning', 'Não é possível cursar mais de uma disciplina optativa.');
            current.checked = false;
            return false;
          }
        }

        // Adicionar
        this.info.salasSelecionadas.push(current);
      }
    }

    // Reordenar as disciplinas selecionadas
    this.info.salasDisponiveis = this.$filter('orderBy')(this.info.salasDisponiveis, 'dt_abertura');

    // Não permitir alterar disciplinas obrigatórias
    // Não permitir selecionar disciplinas já cursadas/cursando
    // Desabilitar outras salas da mesma disciplina
    this.info.salasDisponiveis.forEach((sala) => {
      sala.checked = !!this.isSalaSelecionada(sala);

      // Aluno já cursou/está cursando a disciplina
      const bl_cursada = this.isDisciplinaCursada(sala);

      // A disciplina já tem uma outra sala selecionada que não seja esta em questão
      const bl_selecionada = this.isDisciplinaSelecionada(sala) && !sala.checked;

      // A disciplina é obrigatória para o período
      const bl_obrigatorio = sala.bl_obrigatorio && !bl_cursada;

      sala.disabled = bl_cursada || bl_selecionada || bl_obrigatorio;
    });

    if (emitEvents) {
      this.emitEvents();
    }
  }

  emitEvents() {
    this.$rootScope.$emit('calcularContrato', this.info.salasSelecionadas);
    this.$rootScope.$emit('salasSelecionadas', this.info.salasSelecionadas);
    this.$rootScope.$broadcast('tabela', this.info.salasDisponiveis);
  }

  isDisciplinaCursada(item) {
    return [
      'Cursando', 'Aprovado', 'Aprovada', 'Prova Final', 'Em Prova Final'
    ].indexOf(item.st_status) !== -1;
  }

  isDisciplinaSelecionada(item) {
    return this.info.salasSelecionadas.find((disciplina) => {
      return +disciplina.id_disciplina === +item.id_disciplina;
    });
  }

  // Verificar se a sala está seleciona, importante se atentar que uma disciplina pode ter várias salas
  isSalaSelecionada(item) {
    return this.info.salasSelecionadas.find((disciplina) => {
      return this.isDisciplinaSelecionada(item) && +disciplina.id_saladeaula === +item.id_saladeaula;
    });
  }

  getOptativaSelecionada() {
    const optativas = this.info.salasDisponiveis.filter((sala) => sala.bl_optativo);

    let selecionada = optativas.find((optativa) => this.isDisciplinaCursada(optativa));

    if (!selecionada) {
      selecionada = this.info.salasSelecionadas.find((sala) => sala.bl_optativo);
    }

    return selecionada;
  }

}

export default PcRenovacaoDisciplinasController;
