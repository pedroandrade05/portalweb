import PcRenovacaoDisciplinasModule from './pcRenovacaoDisciplinas';
import PcRenovacaoDisciplinasController from './pcRenovacaoDisciplinas.controller';
import PcRenovacaoDisciplinasComponent from './pcRenovacaoDisciplinas.component';
import PcRenovacaoDisciplinasTemplate from './pcRenovacaoDisciplinas.html';

describe('PcRenovacaoDisciplinas', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcRenovacaoDisciplinasModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcRenovacaoDisciplinasController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcRenovacaoDisciplinasComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcRenovacaoDisciplinasTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcRenovacaoDisciplinasController);
    });
  });
});
