import template from './pcRenovacaoDisciplinas.html';
import controller from './pcRenovacaoDisciplinas.controller';
import './pcRenovacaoDisciplinas.scss';

const pcRenovacaoDisciplinasComponent = {
  restrict: 'E',
  bindings: {
    dados: '@',
    selecionadas:'='
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pcRenovacaoDisciplinasComponent;
