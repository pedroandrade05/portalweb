import template from './pcRenovacaoConfirmacao.html';
import controller from './pcRenovacaoConfirmacao.controller';
import './pcRenovacaoConfirmacao.scss';

const pcRenovacaoConfirmacaoComponent = {
  restrict: 'E',
  bindings: {
    confirmacao: '=',
    selecionadas: '<',
    valorcontrato: '<',
    dados: '<'
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pcRenovacaoConfirmacaoComponent;
