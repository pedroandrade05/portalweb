import arrow from '../../../../assets/img/arrowD.png';
import loadingGif from '../../../../assets/img/loading.gif';

class PcRenovacaoConfirmacaoController {

  constructor(
    /* Úteis */ $state, $localStorage, $window, $document, $uibModal, $rootScope,
    /* Services */ EsquemaConfiguracaoService, IndiqueGanheService, MenuService, NotifyService, RenovacaoService,
    /* Constantes */ FuncionalidadeConstante, MeioPagamentoConstante, SituacaoConstante, TipoOfertaConstante
  ) {
    'ngInject';

    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao($state.params.idMatriculaAtiva);

    // Úteis
    this.$state = $state;
    this.$window = $window;
    this.$document = $document;
    this.$localStorage = $localStorage;
    this.modal = $uibModal;
    this.$rootScope = $rootScope;

    // Services
    this.IndiqueGanheService = IndiqueGanheService;
    this.MenuService = MenuService;
    this.NotifyService = NotifyService;
    this.RenovacaoService = RenovacaoService;

    // Constantes
    this.MeioPagamentoConstante = MeioPagamentoConstante;
    this.SituacaoConstante = SituacaoConstante;
    this.TipoOfertaConstante = TipoOfertaConstante;

    // Arquivos
    this.arrow = arrow;
    this.loading = loadingGif;

    // Dados
    this.meiosPagamento = [
      {value: this.MeioPagamentoConstante.CARTAO_CREDITO, text: 'Cartão de crédito'},
      {value: this.MeioPagamentoConstante.BOLETO, text: 'Boleto'},
      {value: this.MeioPagamentoConstante.CARTAO_RECORRENTE, text: 'Cartão recorrente'}
    ];

    // Informações herdadas de pcRenovacaoMain
    this.info = angular.fromJson(this.dados);
    this.linkPagamento = this.info.renovacao.id_usuario === this.info.renovacao.id_usuariocadastro;
    this.lancamento = '';

    // Indique e Ganhe
    this.showIndiqueGanhe = null;
    this.indicacoes = null;
    this.saldoIndiqueGanhe = null;
    this.valorResgatar = 0;

    // Verificar se possui permissão para o Indique e Ganhe
    this.MenuService.hasPermission(FuncionalidadeConstante.INDIQUE_GANHE).then(() => {
      this.showIndiqueGanhe = true;
      this.getIndicacoesIndiqueGanhe();
    }).catch(() => {
      this.showIndiqueGanhe = false;
    });
  }

  voltar() {
    this.confirmacao = false;
  }

  confirmarContrato() {
    this.processando = true;

    const data = {
      ...this.info.contrato,
      id_venda: this.info.renovacao.id_venda,
      id_entidade: this.info.renovacao.id_entidade,
      id_usuario: this.info.renovacao.id_usuario,
      st_nomecompleto: this.info.renovacao.st_nomecompleto
    };

    const disciplinasSelecionadas = this.info.salasSelecionadas.map((sala) => ({
      id_disciplina: sala.id_disciplina,
      st_disciplina: sala.st_disciplina,
      id_saladeaula: sala.id_saladeaula,
      st_saladeaula: sala.st_saladeaula
    }));

    this.RenovacaoService.salvarGrade(disciplinasSelecionadas, data).then(
      () => {
        this.processando = false;
        this.buscarLancamentos();
        var mensagem = {
          title: 'Matrícula em disciplinas para o próximo semestre concluída com sucesso!',
          title2: '',
          okText: 'OK, fechar',
          question: 'Clique em fechar e você irá ser redirecionado para a página Financeiro.',
          showCancelButton: false,
          cancelText: ''
        };

        if (+this.info.contrato.id_meiopagamento === +this.MeioPagamentoConstante.CARTAO_CREDITO ||
          +this.info.contrato.id_meiopagamento === +this.MeioPagamentoConstante.CARTAO_RECORRENTE) {
          mensagem.title = 'Confirme a renovação';
          mensagem.title2 = 'Verifique com atenção aos detalhes da renovação.';
          mensagem.okText = 'Confirmar';
          mensagem.question = 'Para continuar com a renovação selecione a opção \'Confirmar\' e informe os dados para pagamento, com a segurança do nosso parceiro PAGAR-ME.';
          mensagem.showCancelButton = true;
          mensagem.cancelText = 'Voltar';
        }

        this.questionModal = this.modal.open({
          animation: true,
          component: 'pcModalConfirm',
          resolve: {
            data: () => {
              return {
                title: mensagem.title,
                title2: mensagem.title2,
                question: mensagem.question,
                okText: mensagem.okText,
                showCancelButton: mensagem.showCancelButton,
                cancelText: mensagem.cancelText,
                renovacao: true
              };
            }
          }
        }).result.then((result) => {
          if (result) {
            if (+this.info.contrato.id_meiopagamento === +this.MeioPagamentoConstante.CARTAO_CREDITO ||
              +this.info.contrato.id_meiopagamento === +this.MeioPagamentoConstante.CARTAO_RECORRENTE) {
              this.abrirLinkPagamento(
                this.info.renovacao.st_linkpagamento + this.info.renovacao.id_venda + (this.lancamento ? '&lancamento=' + this.lancamento : '')
              );
            }
            else {
              setTimeout(() => {
                this.$state.go('app.financeiro', {idMatriculaAtiva: this.$state.params.idMatriculaAtiva});
              }, 1000);
            }
          }
        });

      },
      () => {
        this.processando = false;
        this.NotifyService.notify('error', 'Houve um erro ao salvar a grade, tenta novamente mais tarde. Se o problema persistir, entre em contato com o suporte.');
      }
    );
  }

  getIndicacoesIndiqueGanhe() {
    return this.IndiqueGanheService
      .getIndicacoes(this.$state.params.idMatriculaAtiva)
      .then((response) => {
        this.indicacoes = response.indicacoes;
        this.saldoIndiqueGanhe = response.nu_disponivel;
      });
  }

  handleChangeValorResgatar(multiplier = 1) {
    this.valorResgatar += 100 * multiplier;

    // O valor deve ser múltiplo de 100
    if (this.valorResgatar > 100 && this.valorResgatar % 100 !== 0) {
      this.valorResgatar = Math.floor(this.valorResgatar / 100) * 100;
    }

    // O valor final não pode ser maior que o limite para a renovação
    if (this.info.contrato.nu_descontoindiqueganhepontualidade + this.valorResgatar > this.info.contrato.nu_maxdescontoindiqueganhepontualidade) {
      this.valorResgatar = this.info.contrato.nu_maxdescontoindiqueganhepontualidade - this.info.contrato.nu_descontoindiqueganhepontualidade;
    }

    // Não pode ser maior que o saldo disponível
    this.valorResgatar = Math.min(this.saldoIndiqueGanhe, this.valorResgatar);

    // Não pode ser menor que zero
    this.valorResgatar = Math.max(0, this.valorResgatar);
  }

  confirmarResgateIndiqueGanhe() {
    this.processando = true;

    this.IndiqueGanheService.resgatarCupons(
      this.$state.params.idMatriculaAtiva,
      this.SituacaoConstante.AFILIADO_INDICACAO_CUPOM_RESGATADO_RENOVACAO,
      this.valorResgatar
    ).then(
      () => {
        this.$rootScope.$emit('calcularContrato', this.info.salasSelecionadas, () => {
          this.indicacoes = null;
          this.saldoIndiqueGanhe = null;
          this.valorResgatar = 0;
          this.processando = false;
          this.NotifyService.notify('success', 'O resgate do bônus foi realizado.');

          this.getIndicacoesIndiqueGanhe();
        });
      },
      () => {
        this.processando = false;
        this.NotifyService.notify('error', 'Não foi possível realizar o resgate neste momento.');
      }
    );
  }

  async buscarLancamentos() {
    this.RenovacaoService.buscarLancamentos(this.info.renovacao.id_venda, true).then(
      (response) => {
        this.lancamento = '';
        if (response.length) {
          this.lancamento = response[0].id_lancamento.id_lancamento;
        }
      },
      () => {
        this.lancamento = '';
      }
    );
  }

  abrirLinkPagamento(link) {
    localStorage.setItem('linkExterno', link);
    this.$state.go('app.externo', {
      idMatriculaAtiva: this.$state.params.idMatriculaAtiva
    });
  }
}

export default PcRenovacaoConfirmacaoController;
