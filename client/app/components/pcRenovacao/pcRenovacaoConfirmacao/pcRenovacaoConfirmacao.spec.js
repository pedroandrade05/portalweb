import PcRenovacaoConfirmacaoModule from './pcRenovacaoConfirmacao';
import PcRenovacaoConfirmacaoController from './pcRenovacaoConfirmacao.controller';
import PcRenovacaoConfirmacaoComponent from './pcRenovacaoConfirmacao.component';
import PcRenovacaoConfirmacaoTemplate from './pcRenovacaoConfirmacao.html';

describe('PcRenovacaoConfirmacao', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcRenovacaoConfirmacaoModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcRenovacaoConfirmacaoController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcRenovacaoConfirmacaoComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcRenovacaoConfirmacaoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcRenovacaoConfirmacaoController);
    });
  });
});
