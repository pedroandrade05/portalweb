import PcMeusDadosFormModule from './pcMeusDadosForm';
import PcMeusDadosFormController from './pcMeusDadosForm.controller';
import PcMeusDadosFormComponent from './pcMeusDadosForm.component';
import PcMeusDadosFormTemplate from './pcMeusDadosForm.html';

describe('PcMeusDadosForm', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcMeusDadosFormModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcMeusDadosFormController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcMeusDadosFormComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcMeusDadosFormTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcMeusDadosFormController);
    });
  });
});
