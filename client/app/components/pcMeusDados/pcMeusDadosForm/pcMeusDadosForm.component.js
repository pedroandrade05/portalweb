import template from './pcMeusDadosForm.html';
import controller from './pcMeusDadosForm.controller';
import './pcMeusDadosForm.scss';

const pcMeusDadosFormComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcMeusDadosFormComponent;
