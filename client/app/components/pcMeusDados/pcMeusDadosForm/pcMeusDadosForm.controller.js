
class PcMeusDadosFormController {
  constructor(
    CONFIG,
    TipoEnderecoConstante,
    PessoaService,
    AuthService,
    NotifyService,
    CursoAlunoService,
    $state,
    $localStorage,
    EnderecoService,
    DownloadService,
    $q,
    MensagemPadrao,
    $rootScope,
    $scope,
    $uibModal,
    Upload
  ) {
    'ngInject';

    this.name = 'pcMeusDadosForm';
    this.TipoEndereco = TipoEnderecoConstante;
    this.PessoaService = PessoaService;
    this.AuthService = AuthService;
    this.CursoAlunoService = CursoAlunoService;
    this.toaster = NotifyService;
    this.DownloadService = DownloadService;
    this.dadosUsuario = {};
    this.dadosUsuarioEndereco = {};
    this.meusdadosLoading = false;
    this.state = $state;
    this.storage = $localStorage;
    this.MensagemPadrao = MensagemPadrao;
    this.$q = $q;
    this.EnderecoService = EnderecoService;
    this.paises = [];
    this.ufs = [];
    this.municipios = [];
    this.ufsCorrespondencia = [];
    this.municipiosCorrespondencia = [];
    this.$rootScope = $rootScope;
    this.$uibModal = $uibModal;
    this.$scope = $scope;
    this.tipoTelefone = [];

    this.modal = null;
    this.croppedDataUrl = null;
    this.picFile = null;
    this.Upload = Upload;
    this.CONFIG = CONFIG;
    this.st_urlavatar = null;
    this.uploading = false;

    this.stUrlAvatar = null;
    this.downloadAction = 'download-s3-direto';

    //Variaveis para validar exibição do botão de salvar dados cadastrais
    this.modelUserAlterado = -1;
    this.modelUserEndereco = -1;

    //recupera o parametro da matricula ativa
    const idMatricula = this.state.params.idMatriculaAtiva;
    this.cursoAtivo = this.CursoAlunoService.getStorageCursoAtivo(idMatricula);

    //define o id usuario
    this.idUsuario = $localStorage.user_id;
    const idEntidade = this.cursoAtivo.id_entidade;

    this.user = {
      senha: '',
      confirmaSenha: '',
      idEntidade
    };

    this.buscaDadosUsuario(this.idUsuario, idEntidade);
    this.buscaPessoaEndereco(this.idUsuario, this.TipoEndereco);

    /**
     * Se tiver alteração nos models this.dadosUsuario ou this.dadosUsuarioEndereco mostra o botão para edição
     * A variavel que controla a exibição do botão é feia com incremento porque ela é setada
     * também quando o model é carregado.
     * Assim, ela só deve exibir depois da segunda interação
     */
    this.$rootScope.$watchCollection(() => {
      return this.dadosUsuario;
    }, () => {
      this.modelUserAlterado++;
    });

    this.$rootScope.$watchCollection(() => {
      return this.dadosUsuarioEndereco;
    }, () => {
      this.modelUserEndereco++;
    });
  }

  openAvatarModal(){

    this.modal = this.$uibModal.open({
      animation: true,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'userAvatarModal.html',
      scope: this.$scope
    });

    this.modal.closed.then(() => {
      this.croppedDataUrl = null;
      this.picFile = null;
    });
  }

  uploadCancel(){

    this.modal.close();

  }

  startUpload(dataUrl, name){

    this.uploading = true;
    var error = false;
    var arquivoImagem = this.Upload.dataUrltoBlob(dataUrl, name);
    var formatosPermitidos = ["image/jpg", "image/jpeg", "image/png",'image/jpeg', 'image/pjpeg', 'image/x-png'];

    if (formatosPermitidos.indexOf(arquivoImagem.type) === -1) {
      this.toaster.notify('error', 'Ops!',  this.MensagemPadrao.ARQUIVO_FORA_DO_FORMATO_PERMITIDO_IMAGEM);
      this.st_urlavatar = null;
      this.$rootScope.wrapper.dadosUsuario.st_urlavatar = null;
      error = true;
    }

    if (arquivoImagem.size > 500000) {
      this.toaster.notify('error', 'Ops!', this.MensagemPadrao.ARQUIVO_TAMANHO_IMAGEM_500KB);
      this.st_urlavatar = null;
      this.$rootScope.wrapper.dadosUsuario.st_urlavatar = null;
      this.uploading = false;
      this.modal.close();
      error = true;
    }

    if (error === false) {
      this.Upload.upload({
        url: this.CONFIG.urlApi  +  '/usuario/upload-avatar/id/' + this.idUsuario,
        data: {
          file: this.Upload.dataUrltoBlob(dataUrl, name)
        }
      }).then((response) => {
        setTimeout(() => {
          this.st_urlavatar = response.data.mensagem[0];
          this.$rootScope.wrapper.dadosUsuario.st_urlavatar = this.st_urlavatar;
          this.toaster.notify('success', 'OK!', 'Imagem salva com sucesso!');
          this.uploading = false;
          this.modal.close();
        });
      }, (response) => {
        if (response.status > 0) {
          this.toaster.notify('error', 'Ops!', response.data.message);
        }
        this.uploading = false;
      }, (evt) => {
        this.progress = parseInt(100.0 * evt.loaded / evt.total);
      });
    }
  }

  /**
   * Função que busca dos dados do usuário
   * @param idUsuario
   * @param idEntidade
   */
  buscaDadosUsuario = (idUsuario, idEntidade) => {
    if (idUsuario) {
      this.PessoaService.getDadosPessoa(idUsuario, idEntidade).then((response) => {
        response.telefone = response.nu_ddd + response.nu_telefone;
        angular.extend(this.dadosUsuario, response);

        if (this.dadosUsuario.st_urlavatar) {
          this.st_urlavatar = this.dadosUsuario.st_urlavatar;
        }

        this.preencheCombos(this.TipoEndereco.RESIDENCIAL);

        if (this.st_urlavatar) {
          this.DownloadService.getAuth(this.downloadAction).then((auth) => {
            this.stUrlAvatar = this.DownloadService.geraUrlDocumento(auth, this.downloadAction, this.st_urlavatar, 'usuario');
          });
        }

      }, (error) => {
        this.toaster.notify(error.data);
      });
    }
  }

  /**
   * Função que busca o endereço do usuário, baseado no id_usuario e no id_tipoendereco
   * @param idUsuario
   * @param TipoEndereco
   */
  buscaPessoaEndereco = (idUsuario, TipoEndereco) => {
    if (idUsuario) {
      this.PessoaService.getVwPessoaEndereco(idUsuario, TipoEndereco.CORRESPONDENCIA).then((response) => {
        this.dadosUsuarioEndereco = response;
      }, (error) => {
        this.toaster.notify(error.data);
      }).finally(() => {
        this.preencheCombos(TipoEndereco.CORRESPONDENCIA);
      });
    }
  }

  alteraSenha() {
    if (this.user.senha !== this.user.confirmaSenha) {
      this.toaster.notify('error', '', 'As senhas não são iguais, por favor digite novamente.');
      return false;
    }

    if (this.user.senha === '') {
      this.toaster.notify('error', '', 'Preencha o campo SENHA.');
      return false;
    }

    if (this.user.confirmaSenha === '') {
      this.toaster.notify('error', '', 'Preencha o campo de CONFIRMAR SENHA.');
      return false;
    }

    if (this.user.senha.length < 6){
      this.toaster.notify('error', '', 'A senha deve conter no mínimo 6 caracteres.');
      return false;
    }

    if (/[^a-zA-Z0-9\.]/g.test(this.user.senha)) {
      this.toaster.notify('error', '', 'A senha não pode conter caracteres especiais.');
      return false;
    }

    this.meusdadosLoading = true;

    const idUsuario = this.storage.user_id;
    const stSenha = this.user.senha;
    const idEntidade = this.user.idEntidade;

    this.AuthService.alteraSenha(idUsuario, stSenha, idEntidade).then((response) => {
      if (response.tipo === 1) {
        this.toaster.notify('success', '',  this.storage.user_shortname + this.MensagemPadrao.SENHA_ALTERADA_SUCESSO);
        this.user.confirmaSenha = '';
        this.user.senha = '';
      }
      else {
        this.toaster.notify('error', '', response.text);
      }
      this.meusdadosLoading = false;
    }, (error) => {
      this.meusdadosLoading = false;
      this.toaster.notify(error.data);
    });

  }


  /**
   * Preenche combos de endereço
   * @param idTipoEndereco
   */
  preencheCombos = (idTipoEndereco) => {
    const promises = [
      this.buscarPaises()
      , this.buscarUfs(idTipoEndereco)
      , this.buscarMunicipios(idTipoEndereco)
      , this.buscarTipoTelefone()
    ];

    this.$q.all(promises).then(() => {
      this.loading = false;
    });
  }

  /**
   * Busca endereço de acordo com o CEP digitado
   * Preenche os models
   * Limpa o model em caso de CEP não encontrado
   *
   * @param idTipoEndereco
   * @returns {boolean}
   */
  buscaCep = (idTipoEndereco) => {
    if (idTipoEndereco === this.TipoEndereco.RESIDENCIAL) {
      if (this.dadosUsuario.st_cep && this.dadosUsuario.st_cep.replace(/\D/g).length === 8) {
        this.EnderecoService.getByCep(this.dadosUsuario.st_cep).then((response) => {
          delete response.id_pais;
          delete response.id_endereco;

          angular.extend(this.dadosUsuario, response);
          this.buscarMunicipios(idTipoEndereco);

        }, (error) => {
          this.dadosUsuario.st_endereco = '';
          this.dadosUsuario.st_bairro = '';
          this.dadosUsuario.sg_uf = '';
          this.dadosUsuario.nu_numero = '';
          this.dadosUsuario.id_municipio = '';
          this.toaster.notify('warning', '', error.data.message);
        });
      }
    }
    else {
      if (this.dadosUsuarioEndereco.st_cep && this.dadosUsuarioEndereco.st_cep.replace(/\D/g).length === 8) {
        this.EnderecoService.getByCep(this.dadosUsuarioEndereco.st_cep).then((response) => {
          delete response.id_pais;

          angular.extend(this.dadosUsuarioEndereco, response);
          this.buscarMunicipios(idTipoEndereco);

        }, (error) => {
          this.dadosUsuarioEndereco.st_endereco = '';
          this.dadosUsuarioEndereco.st_bairro = '';
          this.dadosUsuarioEndereco.sg_uf = '';
          this.dadosUsuarioEndereco.nu_numero = '';
          this.dadosUsuarioEndereco.id_municipio = '';
          this.toaster.notify('warning', '', error.data.message);
        });
      }
    }
    return false;
  }


  /**
   * Preenche combo de Paises
   * @returns {*}
   */
  buscarPaises = () => {
    return this.EnderecoService.getPaises().then((response) => {
      this.paises = response;
    });
  }

  /**
   * Preenche combo de estado
   * @param idTipoEndereco
   * @returns {*}
   */
  buscarUfs = (idTipoEndereco) => {
    return this.EnderecoService.getUfs().then((response) => {
      if (idTipoEndereco === this.TipoEndereco.RESIDENCIAL ){
        this.ufs = response;
      }
      else {
        this.ufsCorrespondencia = response;
      }
    });
  }

  /**
   * Preenche combo de municipios
   * O st_uf é obrigatorio
   * @param idTipoEndereco
   * @returns {*}
   */
  buscarMunicipios = (idTipoEndereco) => {
    const sg_uf = (idTipoEndereco === this.TipoEndereco.RESIDENCIAL ? this.dadosUsuario.sg_uf : this.dadosUsuarioEndereco.sg_uf) ;
    if ( angular.isDefined(sg_uf ) ) {
      return this.EnderecoService.getMunicipios(sg_uf).then((response) => {
        if (idTipoEndereco === this.TipoEndereco.RESIDENCIAL ){
          this.municipios = response;
        }
        else {
          this.municipiosCorrespondencia = response;
        }
      });
    }

  }

  /**
   * Preenche lista Tipo de telefone
   * @returns {*}
   */
  buscarTipoTelefone = () => {
    return this.EnderecoService.getTipoTelefone().then((response) => {
      this.tipoTelefone = response;
    });
  }

  /**
   * Salva Endereço Residencial / Endereço Corresondencia / Telefone Padrão
   */
  salvarDados = () => {

    let bl_error = false;

    this.meusdadosLoading = true;

    //Completa os campos do endereço padrão
    this.dadosUsuario.id_usuario = this.storage.user_id;
    this.dadosUsuario.id_tipoendereco = this.TipoEndereco.RESIDENCIAL;
    this.dadosUsuario.bl_padrao = true;
    this.dadosUsuario.id_entidade = this.cursoAtivo.id_entidade;

    if ((+this.dadosUsuario.id_tipotelefone === 1 || +this.dadosUsuario.id_tipotelefone === 2)
      && this.dadosUsuario.telefone.length !== 10) {
      this.toaster.notify('error', '', 'Telefone deve possuir DDD + 8 digitos ');
      return this.meusdadosLoading = false;
    }
    else if (+this.dadosUsuario.id_tipotelefone === 3 && this.dadosUsuario.telefone.length !== 11) {
      this.toaster.notify('error', '', 'Celular deve possuir DDD + 9 digitos');
      return this.meusdadosLoading = false;
    }

    //Salva endereço residencial
    if (this.dadosUsuario.id_endereco) {
      this.EnderecoService.getResource().update(this.dadosUsuario.id_endereco, this.dadosUsuario ).$promise.then((responseUser) => {
        this.dadosUsuario.id_endereco = responseUser.id_endereco;
      }, (error) => {
        bl_error = true;
        this.toaster.notify('error', '', error.data.message);
      }).finally(() => {
        this.meusdadosLoading = false;
      });
    }
    else {
      this.EnderecoService.getResource().create(this.dadosUsuario).$promise.then((responseUser) => {
        this.dadosUsuario.id_endereco = responseUser.id_endereco;
      }, (error) => {
        bl_error = true;
        this.toaster.notify('error', '', error.data.message);
      }).finally(() => {
        this.meusdadosLoading = false;
      });
    }

    /** Se tiver alteração ou algum campo for preenchido do endereçoç de correpondencia, salva **/
    if (this.validaEnderecoCorrespondencia()) {
      this.meusdadosLoading = true;

      //Completa os campos do endereço correspondencia
      this.dadosUsuarioEndereco.id_usuario = this.storage.user_id;
      this.dadosUsuarioEndereco.id_tipoendereco = this.TipoEndereco.CORRESPONDENCIA;
      this.dadosUsuarioEndereco.bl_padrao = true;
      this.dadosUsuarioEndereco.id_entidade = this.cursoAtivo.id_entidade;

      //Salva endereço correspondencia
      if (this.dadosUsuarioEndereco.id_endereco) {
        this.EnderecoService.getResource().update(this.dadosUsuarioEndereco.id_endereco,  this.dadosUsuarioEndereco).$promise.then((responseUserAddress) => {
          this.dadosUsuarioEndereco.id_endereco = responseUserAddress.id_endereco;
        }, (error) => {
          bl_error = true;
          this.toaster.notify('error', '', error.data.message);
        }).finally(() => {
          this.meusdadosLoading = false;
        });
      }
      else {
        this.EnderecoService.getResource().create(this.dadosUsuarioEndereco).$promise.then((responseUserAddress) => {
          this.dadosUsuarioEndereco.id_endereco = responseUserAddress.id_endereco;
        }, (error) => {
          bl_error = true;
          this.toaster.notify('error', '', error.data.message);
        }).finally(() => {
          this.meusdadosLoading = false;
        });
      }

    }

    if (!bl_error) {
      this.toaster.notify('success', '', (this.MensagemPadrao.DADOS_CADASTRAIS_SUCESSO));
    }

  }


  /**
   * Valida o Endereço de correspondência
   * Não é obrigatorio o preenchimento
   * Se preencher 1 campo, exige que todos os outros sejam preenchidos
   * @returns {boolean}
   */
  validaEnderecoCorrespondencia = () => {
    let validado = false;
    if (this.dadosUsuarioEndereco.st_cep !== null || this.dadosUsuarioEndereco.st_endereco !== null
      || this.dadosUsuarioEndereco.nu_numero !== null || this.dadosUsuarioEndereco.st_bairro !== null
      || this.dadosUsuarioEndereco.id_municipio !== null || this.dadosUsuarioEndereco.sg_uf !== null
      || this.dadosUsuarioEndereco.id_pais !== null) {

      if (this.dadosUsuarioEndereco.st_cep === null) {
        this.toaster.notify('warning', '', 'Endereço correspondência: Preencha o CEP corretamente.');
      }
      else if (this.dadosUsuarioEndereco.st_endereco === null || this.dadosUsuarioEndereco.st_endereco.trim() === '') {
        this.toaster.notify('warning', '', 'Endereço correspondência: Preencha o endereço corretamente.');
      }
      else if (this.dadosUsuarioEndereco.nu_numero === null) {
        this.toaster.notify('warning', '', 'Endereço correspondência: Preencha o número corretamente.');
      }
      else if (this.dadosUsuarioEndereco.st_bairro === null  || this.dadosUsuarioEndereco.st_bairro.trim() === '') {
        this.toaster.notify('warning', '', 'Endereço correspondência: Preencha o bairro corretamente.');
      }
      else if (this.dadosUsuarioEndereco.sg_uf === null) {
        this.toaster.notify('warning', '', 'Endereço correspondência: Escolha um estado.');
      }
      else if (this.dadosUsuarioEndereco.id_municipio === null) {
        this.toaster.notify('warning', '', 'Endereço correspondência: Escolha um município.');
      }
      else if (this.dadosUsuarioEndereco.id_pais === null) {
        this.toaster.notify('warning', '', 'Endereço correspondência: Escolha um país.');
      }
      else {
        validado = true;
      }
    }
    return validado;
  }
}

export default PcMeusDadosFormController;
