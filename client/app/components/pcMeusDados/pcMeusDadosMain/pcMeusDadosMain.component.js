import template from './pcMeusDadosMain.html';
import controller from './pcMeusDadosMain.controller';
import './pcMeusDadosMain.scss';

const pcMeusDadosMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcMeusDadosMainComponent;
