class PcExternoMainController {
  constructor($state, $sce) {
    'ngInject';
    this.name = 'pcExternoMain';
    this.state = $state;
    this.sce = $sce;

    this.link = localStorage.linkExterno;

    this.html = '<iframe class="pagina-externo" frameborder="0" allowtransparency="true" src="' + this.link + '"> </iframe>';

  }
}

export default PcExternoMainController;
