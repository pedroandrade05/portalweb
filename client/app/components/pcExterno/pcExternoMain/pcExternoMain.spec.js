import PcExternoMainModule from './pcExternoMain';
import PcExternoMainController from './pcExternoMain.controller';
import PcExternoMainComponent from './pcExternoMain.component';
import PcExternoMainTemplate from './pcExternoMain.html';

describe('PcExternoMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcExternoMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcExternoMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcExternoMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcExternoMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcExternoMainController);
    });
  });
});
