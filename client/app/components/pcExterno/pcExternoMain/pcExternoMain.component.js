import template from './pcExternoMain.html';
import controller from './pcExternoMain.controller';
import './pcExternoMain.scss';

const pcExternoMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcExternoMainComponent;
