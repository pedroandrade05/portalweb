import PcLayoutModule from './pcLayout';
import PcLayoutController from './pcLayout.controller';
import PcLayoutComponent from './pcLayout.component';
import PcLayoutTemplate from './pcLayout.html';

describe('PcLayout', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcLayoutModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcLayoutController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Component', () => {
      // component/directive specs
    const component = PcLayoutComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcLayoutTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcLayoutController);
    });
  });
});
