import template from './pcPromocoesMain.html';
import controller from './pcPromocoesMain.controller';
import './pcPromocoesMain.scss';

const pcPromocoesMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcPromocoesMainComponent;
