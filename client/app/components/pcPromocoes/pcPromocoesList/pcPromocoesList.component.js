import template from './pcPromocoesList.html';
import controller from './pcPromocoesList.controller';
import './pcPromocoesList.scss';

const pcPromocoesListComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcPromocoesListComponent;
