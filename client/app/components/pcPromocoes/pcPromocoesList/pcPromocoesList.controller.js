class PcPromocoesListController {

  constructor(CONFIG, CampanhaComercialService, $state, $q, NotifyService, $rootScope, CampanhaPublicitariaService, CursoAlunoService) {
    'ngInject';

    this.CampanhaComercialService = CampanhaComercialService;
    this.CampanhaPublicitariaService = CampanhaPublicitariaService;
    this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);

    this.$q = $q;
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.CONFIG = CONFIG;

    this.name = 'pcPromocoesList';
    this.promocoes = [];

    //Mostra mensagem de erro caso ocorra algum erro na requisição de buscas as campanhas
    this.toaster = NotifyService;

    this.activeId = `${$state.params.tipoCamanha || '0'}/${$state.params.idCampanha || '0'}`;

    //Chama o método de buscar as campanhas
    this.$q.all([
      this.buscarCampanhaComercial(true),
      this.buscarCampanhaPublicitaria()
    ]).finally(() => {
      let selecionada;

      // Verfiricar se a camapanha selecionada foi informada no URL
      if ($state.params.idCampanha) {
        selecionada = this.promocoes.find((campanha) => {
          return campanha.st_tipo === $state.params.tipoCamanha && +campanha.id_campanha === +$state.params.idCampanha;
        });
      }

      if (this.promocoes.length) {
        // Se não foi definido a campanha selecionada no URL, então exibir a primeira da busca
        if (!selecionada) {
          selecionada = this.promocoes[0];
        }

        this.visualizarCampanha(selecionada, true);
      }
    });

    this.listenerIdAtivo = this.$rootScope.$on('navegarPromocoesEvent', (event, data) => {
      this.activeId = `${data.st_tipo}/${data.id_campanha}`;
    });

    this.$rootScope.$on('$destroy', this.listenerIdAtivo);
  }

  count = () => {
    this.CampanhaComercialService.getResource().count({id_matricula: this.$state.params.idMatriculaAtiva, bl_portaldoaluno: 1}, (response) => {
      this.$rootScope.countPromocoes = response.total;
    });

  };

  /**
   Abre os detalhes de de uma campanha (show)
   **/
  visualizarCampanha(campanha, search = true) {
    this.activeId = `${campanha.st_tipo}/${campanha.id_campanha}`;
    this.count();
    if (search) {
      this.$state.go('app.promocoes.show', {
        campanha,
        tipoCamanha: campanha.st_tipo,
        idCampanha: campanha.id_campanha,
        promocoes: this.promocoes
      }, {reload: false});
    }
  }

  /**
   Busca as campanhas do usuário logado
   Listagem
   **/
  buscarCampanhaComercial() {
    return this.CampanhaComercialService.getResource().getAll({
      bl_portaldoaluno: 1,
      id_matricula: this.$state.params.idMatriculaAtiva,
      order: 'dt_inicio',
      direction: 'desc',
      'columns[]': ['id_campanhacomercial', 'st_campanhacomercial', 'st_descricao', 'st_imagem', 'st_link', 'dt_inicio', 'dt_fim', 'bl_visualizado']
    }, (response) => {
      response.forEach((campanha) => {
        this.promocoes.push({
          st_tipo: 'comercial',
          id_campanha: campanha.id_campanhacomercial,
          st_campanha: campanha.st_campanhacomercial,
          dt_inicio: campanha.dt_inicio,
          dt_fim: campanha.dt_fim,
          st_imagem: this.CONFIG.urlG2 + campanha.st_imagem,
          st_link: campanha.st_link,
          st_descricao: campanha.st_descricao,
          bl_visualizado: campanha.bl_visualizado
        });
      });
    }, (error) => {
      this.toaster.notify(error.data);
    });
  }

  /**
   Busca as campanhas da entidade logada
   Listagem
   **/
  buscarCampanhaPublicitaria() {
    return this.CampanhaPublicitariaService.getCampanhaPublicitaria(this.MatriculaAtiva.id_entidade).then((response) => {
      if (response.message) {
        this.toaster.notify('warning', response.message);
      }
      else {
        response.forEach((campanha) => {
          this.promocoes.push({
            st_tipo: 'publicitaria',
            id_campanha: campanha.id_campanhapublicitaria,
            st_campanha: campanha.st_campanhapublicitaria,
            dt_inicio: campanha.dt_inicio,
            dt_fim: campanha.dt_fim,
            st_imagem: campanha.st_urlimagem,
            st_descricao: campanha.st_conteudo
          });
        });
      }
    }, (error) => {
      this.toaster.notify(error.type, error.message);
    });
  }

}

export default PcPromocoesListController;
