import loadingGif from "../../../../assets/img/loading.gif";

class PcPromocoesShowController {
  constructor(CONFIG, CampanhaComercialService, $state, NotifyService, $rootScope, $scope, $localStorage) {
    'ngInject';

    this.storage = $localStorage;
    this.CampanhaComercialService = CampanhaComercialService;
    this.CONFIG = CONFIG;
    this.name = 'pcPromocoesShow';
    this.$state = $state;
    this.toaster = NotifyService;
    this.scope = $scope;
    this.indexActive = -1;
    this.$rootScope = $rootScope;

    this.loadingGif = loadingGif;
    this.loading = true;

    this.idMatricula = $state.params.idMatriculaAtiva;

    this.campanha = $state.params.campanha || {};
    this.promocoes = $state.params.promocoes || [];

    if (this.campanha || this.campanha.id_campanha) {
      this.loading = false;
    }

  }

  count = () => {
    this.CampanhaComercialService.getResource().count({id_matricula: this.idMatricula, bl_portaldoaluno: 1}, (response) => {
      this.$rootScope.countPromocoes = response.total;
    });
  };

  showNext() {
    const campanhaAtual = this.promocoes.find((item) => {
      return item.st_tipo === this.campanha.st_tipo && +item.id_campanha === +this.campanha.id_campanha;
    });

    return this.promocoes.indexOf(campanhaAtual) < this.promocoes.length - 1;
  }

  showPrev() {
    const campanhaAtual = this.promocoes.find((item) => {
      return item.st_tipo === this.campanha.st_tipo && +item.id_campanha === +this.campanha.id_campanha;
    });

    return this.promocoes.indexOf(campanhaAtual) > 0;
  }

  buscarCampanhaComercial() {
    return this.CampanhaComercialService.getResource().getAll({
      bl_portaldoaluno: 1,
      id_matricula: this.idMatricula,
      order: 'dt_inicio',
      direction: 'desc',
      'columns[]': ['id_campanhacomercial', 'st_campanhacomercial', 'st_descricao', 'st_imagem', 'st_link', 'dt_inicio', 'dt_fim']
    }, (response) => {
      this.promocoes = response;

      this.count();
    }, (error) => {
      this.toaster.notify(error.data);
    });
  }

  /**
   * Método para permitir a navegação entre mensagens usando os links "anterior" e "próximo"
   * @param index
   */
  navegarPromocao(campanha, index) {
    const campanhaAtual = this.promocoes.find((item) => {
      return item.st_tipo === campanha.st_tipo && +item.id_campanha === +campanha.id_campanha;
    });

    const indexAtual = this.promocoes.indexOf(campanhaAtual);
    const indexNovo = indexAtual + index;

    const campanhaNova = this.promocoes[indexNovo];

    if (!campanhaNova) {
      return this.toaster.notify('error', '', 'Promoção não encontrada!');
    }

    this.campanha = campanhaNova;
    this.$rootScope.$broadcast('navegarPromocoesEvent', this.campanha);
    this.$state.go('app.promocoes.show', {idCampanha: this.campanha.id_campanha}, {notify: false});
  }

}

export default PcPromocoesShowController;
