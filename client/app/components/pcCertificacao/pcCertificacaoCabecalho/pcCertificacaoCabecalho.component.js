import template from './pcCertificacaoCabecalho.html';
import controller from './pcCertificacaoCabecalho.controller';
import './pcCertificacaoCabecalho.scss';

const pcCertificacaoCabecalhoComponent = {
  restrict: 'E',
  bindings: {title: '@', subtitle : '@', showBtn: '@'},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcCertificacaoCabecalhoComponent;
