import PcCertificacaoCabecalhoModule from './pcCertificacaoCabecalho'
import PcCertificacaoCabecalhoController from './pcCertificacaoCabecalho.controller';
import PcCertificacaoCabecalhoComponent from './pcCertificacaoCabecalho.component';
import PcCertificacaoCabecalhoTemplate from './pcCertificacaoCabecalho.html';

describe('PcCertificacaoCabecalho', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcCertificacaoCabecalhoModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcCertificacaoCabecalhoController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
      let component = PcCertificacaoCabecalhoComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(PcCertificacaoCabecalhoTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(PcCertificacaoCabecalhoController);
      });
  });
});
