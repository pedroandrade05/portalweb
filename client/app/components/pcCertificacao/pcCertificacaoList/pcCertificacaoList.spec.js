import PcCertificacaoListModule from './pcCertificacaoList'
import PcCertificacaoListController from './pcCertificacaoList.controller';
import PcCertificacaoListComponent from './pcCertificacaoList.component';
import PcCertificacaoListTemplate from './pcCertificacaoList.html';

describe('PcCertificacaoList', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcCertificacaoListModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcCertificacaoListController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
      let component = PcCertificacaoListComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(PcCertificacaoListTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(PcCertificacaoListController);
      });
  });
});
