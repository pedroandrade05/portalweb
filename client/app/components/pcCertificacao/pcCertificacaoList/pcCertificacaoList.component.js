import template from './pcCertificacaoList.html';
import controller from './pcCertificacaoList.controller';
import './pcCertificacaoList.scss';

const pcCertificacaoListComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcCertificacaoListComponent;
