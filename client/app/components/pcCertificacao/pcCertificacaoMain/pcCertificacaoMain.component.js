import template from './pcCertificacaoMain.html';
import controller from './pcCertificacaoMain.controller';
import './pcCertificacaoMain.scss';

const pcCertificacaoMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcCertificacaoMainComponent;
