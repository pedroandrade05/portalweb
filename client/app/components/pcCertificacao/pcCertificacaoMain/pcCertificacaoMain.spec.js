import PcCertificacaoMainModule from './pcCertificacaoMain'
import PcCertificacaoMainController from './pcCertificacaoMain.controller';
import PcCertificacaoMainComponent from './pcCertificacaoMain.component';
import PcCertificacaoMainTemplate from './pcCertificacaoMain.html';

describe('PcCertificacaoMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcCertificacaoMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcCertificacaoMainController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
      let component = PcCertificacaoMainComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(PcCertificacaoMainTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(PcCertificacaoMainController);
      });
  });
});
