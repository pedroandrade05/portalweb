class PcCertificacaoMainController {
  constructor($state) {
    "ngInject";
    this.name = 'pcCertificacaoMain';
    this.state = $state;
    this.state.go('app.certificacao.list');
  }
}

export default PcCertificacaoMainController;
