import PcCertificacaoConferirModule from './pcCertificacaoConferir'
import PcCertificacaoConferirController from './pcCertificacaoConferir.controller';
import PcCertificacaoConferirComponent from './pcCertificacaoConferir.component';
import PcCertificacaoConferirTemplate from './pcCertificacaoConferir.html';

describe('PcCertificacaoConferir', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcCertificacaoConferirModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcCertificacaoConferirController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
      let component = PcCertificacaoConferirComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(PcCertificacaoConferirTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(PcCertificacaoConferirController);
      });
  });
});
