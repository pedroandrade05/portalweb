import template from './pcCertificacaoConferir.html';
import controller from './pcCertificacaoConferir.controller';
import './pcCertificacaoConferir.scss';

const pcCertificacaoConferirComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcCertificacaoConferirComponent;
