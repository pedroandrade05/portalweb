import loadingGif from "../../../../assets/img/loading.gif";

class pcClubeLivroMainController {
  constructor(
    $sce,
    $http,
    $state,
    NotifyService,
    ClubeLivroService,
    LSService
  ) {
    'ngInject';

    this.$sce = $sce;
    this.$http = $http;
    this.state = $state;

    this.NotifyService = NotifyService;
    this.ClubeLivroService = ClubeLivroService;
    this.LSService = LSService;

    this.matriculaAtiva = this.ClubeLivroService.getStorageMatricula($state.params.idMatriculaAtiva);
    this.urlClubeLivro = null;

    //inicia a variavel de loading
    this.loading = false;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;

    this.abrirClubeLivro();
  }

  /**
   * Chamar a pagina da ClubeLivro buscando a url do back end
   */
  abrirClubeLivro() {
    this.ClubeLivroService.getUrl()
      .then((success) => {

        this.st_urlClubeLivro = success.st_url;

        if (this.st_urlClubeLivro) {
          this.ClubeLivroService.getTokenMoodle(this.matriculaAtiva.id_matricula, this.matriculaAtiva.id_entidade)
            .then((success) => {
              const urlClubeLivro = success.loginurl + '&wantsurl=' + this.st_urlClubeLivro;
              this.urlClubeLivro = this.$sce.trustAsResourceUrl(urlClubeLivro);
            }, (error) => {
              this.NotifyService.notify(error.data);
            });
        }
      }, (error) => {
        this.NotifyService.notify(error.data);
      });
  }
}

export default pcClubeLivroMainController;
