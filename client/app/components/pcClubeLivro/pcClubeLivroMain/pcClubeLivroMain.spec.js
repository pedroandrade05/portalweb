import PcClubeLivroMainModule from './pcClubeLivroMain';
import PcClubeLivroMainController from './pcClubeLivroMain.controller';
import PcClubeLivroMainComponent from './pcClubeLivroMain.component';
import PcClubeLivroMainTemplate from './pcClubeLivroMain.html';

describe('PcClubeLivroMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcClubeLivroMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcClubeLivroMainController();
    };
  }));

  describe('Module', () => {

  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcClubeLivroMainComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcClubeLivroMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcClubeLivroMainController);
    });
  });
});
