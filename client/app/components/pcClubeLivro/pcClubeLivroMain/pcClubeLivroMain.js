/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcClubeLivroMainComponent from './pcClubeLivroMain.component';

//Import do modulo de services do portal
import Services from '../../../services/Services'; // Talvez você precise alterar este destino

//Modulo principal do componente
const pcClubeLivroMainModule = angular.module('pcClubeLivroMain', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])

  .config(($stateProvider) => {
    'ngInject';

    // Rotas
    $stateProvider
      .state('app.clube-do-livro', {
        url: '/:idMatriculaAtiva/clube-do-livro',

        views: {
          content: {
            template: '<pc-clube-livro-main></pc-clube-livro-main>'
          }
        },

        onEnter(AuthService, $state) {
          const params = $state.params;

          //grava log de acesso
          AuthService.gravaLogAcesso($state.current.name, params);
        }
      });
  })

  .component('pcClubeLivroMain', pcClubeLivroMainComponent);

export default pcClubeLivroMainModule;
