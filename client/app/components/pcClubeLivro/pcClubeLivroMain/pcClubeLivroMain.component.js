import template from './pcClubeLivroMain.html';
import controller from './pcClubeLivroMain.controller';
import './pcClubeLivroMain.scss';

const pcClubeLivroMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcClubeLivroMainComponent;
