class pcConteudoMainController {
  constructor(NotifyService, ConteudoService, $state, $window, $sce) {
    'ngInject';

    this.NotifyService = NotifyService;
    this.ConteudoService = ConteudoService;

    this.params = $state.params;
    this.$window = $window;
    this.$sce = $sce;

    this.idMatricula = $state.params.idMatriculaAtiva;
    this.urlConteudo = null;

    this.abrirConteudo();
  }

  abrirConteudo() {
    this.ConteudoService.retornaUrlPlayerConteudo(this.idMatricula)
      .then(
        (response) => {
          this.urlConteudo = this.$sce.trustAsResourceUrl(response.st_caminhocompleto);
        })
      .catch(() => {
        this.urlConteudo = false;
      });
  }
}

export default pcConteudoMainController;
