import template from './pcConteudoMain.html';
import controller from './pcConteudoMain.controller';
import './pcConteudoMain.scss';

const pcConteudoMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcConteudoMainComponent;
