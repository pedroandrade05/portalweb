import template from './pcMeusDocumentosMain.html';
import controller from './pcMeusDocumentosMain.controller';
import './pcMeusDocumentosMain.scss';

const pcMeusDocumentosMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcMeusDocumentosMainComponent;
