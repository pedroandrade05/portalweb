import loadingGif from "../../../../assets/img/loading.gif";
import logoCopiar from "../../../../assets/img/icons/Shape.svg";
import logoPowerpoint from "../../../../assets/img/icons/powerpoint_logo.svg";
import logoWord from "../../../../assets/img/icons/word_logo.svg";
import logoExcel from "../../../../assets/img/icons/excel_logo.svg";
import logoTeams from "../../../../assets/img/icons/teams_logo.svg";
import logoOneDrive from "../../../../assets/img/icons/OneDrive-logo-outlined.svg";
import logoFace from "../../../../assets/img/icons/HappyFace-logo-outlined.svg";
import logoCollab from "../../../../assets/img/icons/Colab-logo-outlined.svg";
import politicas365 from "../../../../assets/files/politicas365.html";
import alertify from "alertifyjs/build/alertify.min.js";
import thumb_acesso_conta from "../../../../assets/img/thumb_como_acessar_sua_conta.png";
import thumb_criar_conta from "../../../../assets/img/thumb_como_criar_sua_conta.png";
import thumb_recuperar_senha from "../../../../assets/img/thumb_como_recuperar_sua_senha.png";

class PcOffice365MainController {
  constructor(
    /* Úteis */ $state, $localStorage, $window, $sce, EtapasIntegracaoOffice365,
    /* Services */ NotifyService, Office365Service) {
    'ngInject';

    //Services
    this.NotifyService = NotifyService;
    this.Office365Service = Office365Service;

    //Úteis
    this.$sce = $sce;
    this.params = $state.params;
    this.$window = $window;
    this.$localStorage = $localStorage;
    this.EtapasIntegracaoOffice365 = EtapasIntegracaoOffice365;
    this.politicasDeUso = false;
    this.politicas365 = politicas365;
    this.url = this.Office365Service.getUrlAutenticaUsuario();
    this.urlDominioAzureAD = this.Office365Service.getUrlDominioAzureAD();
    this.logoCopiar = logoCopiar;
    this.logoPowerpoint = logoPowerpoint;
    this.logoWord = logoWord;
    this.logoExcel = logoExcel;
    this.logoTeams = logoTeams;
    this.logoOneDrive = logoOneDrive;
    this.logoFace = logoFace;
    this.logoCollab = logoCollab;

    // Outros
    this.loading = true;
    this.loadingGif = loadingGif;
    this.idUsuario = $localStorage.user_id;
    this.idEntidade = $localStorage.user_entidade;
    this.password = null;
    this.showTermo = false;
    this.mostraLogin = false;

    this.tutoriais = [
      {url: 'https://pvbps-sambavideos.akamaized.net/account/3237/4/2020-11-16/video/420e497b6e7625c73fc8a7e7cdbe57ce/Como_criar_sua_conta_no_Office_365_2.mp4', imagem: thumb_criar_conta, duracao: '2:46', titulo: 'Como criar a sua conta' ,descricao: ''},
      {url: 'https://pvbps-sambavideos.akamaized.net/account/3237/4/2020-11-16/video/60ba2ab81d2a52f42138fe6a6301b73f/Como_Acessar_o_Office_365_no_Portal_do_Aluno.mp4', imagem: thumb_acesso_conta, duracao: '2:22', titulo: 'Como acessar o office 365' ,descricao: ''},
      {url: 'https://pvbps-sambavideos.akamaized.net/account/3237/4/2020-11-18/video/18102e1a50226d40c88804f1016756a9/Como_Recuperar_seu_Login_ou_Senha_do_Office_365.mp4', imagem: thumb_recuperar_senha, duracao: '1:44', titulo: 'Como recuperar o login ou senha' ,descricao: ''}];
  }

  integrarOffice365 = (etapaIntegracao) => {
    switch (etapaIntegracao) {
    case this.EtapasIntegracaoOffice365.CRIAR_USUARIO: {
      this.criarUsuario();
      break;
    }
    case this.EtapasIntegracaoOffice365.EXIBIR_DADOS_USUARIO: {
      this.etapaIntegracao = this.EtapasIntegracaoOffice365.EXIBIR_DADOS_USUARIO;
      break;
    }
    case this.EtapasIntegracaoOffice365.RECUPERAR_DADOS_USUARIO: {
      this.etapaIntegracao = this.EtapasIntegracaoOffice365.RECUPERAR_DADOS_USUARIO;
      this.getUsuarioAzure();
      break;
    }
    }
  }

  criarUsuario() {
    this.loading = true;
    this.etapaIntegracao = this.EtapasIntegracaoOffice365.CRIAR_USUARIO;
    this.Office365Service.postCriarUsuario(this.idUsuario)
      .then(
        (response) => {
          this.loading = false;
          this.st_loginazure = response.mensagem.st_login;
          this.password = response.mensagem.st_senha;
          this.atribuirGrupoUsuario();
          this.showTermo = false;
        })
      .catch(() => {
        this.loading = false;
        this.NotifyService.notify('error', '', 'Houve um erro ao criar seu usuário!');
      });
  }

  atribuirGrupoUsuario() {
    this.loading = true;
    this.showTermo = false;
    this.Office365Service.postAtribuirGrupoUsuario(this.idUsuario, this.idEntidade, this.st_dominio = this.urlDominioAzureAD)
      .then(
        (response) => {
          this.loading = false;
          this.st_loginazure = response.mensagem.st_login;
          this.password = response.mensagem.st_senha;
          this.etapaIntegracao = this.EtapasIntegracaoOffice365.PRIMEIRO_ACESSO;
        })
      .catch(() => {
        this.loading = false;
        this.NotifyService.notify('error', '', 'Houve um erro ao criar seu usuário!');
      });
  }

  abrirLoginOffice365() {
    const win = this.$window.open(this.url, '_blank');
    if (win) {
      win.focus();
    }
    else {
      this.NotifyService.notify('warning', '', 'É necessário permitir que o navegador abra uma nova aba.');
    }
  }

  abrirTermo(){
    this.showTermo = !this.showTermo;
  }

  getUsuarioAzure(){
    this.loading = true;
    this.Office365Service.getLocalizaUsuario(this.idUsuario).then(
      (response) => {
        this.idUsuarioAzure = response.id_usuarioazure;
        if (response.hasOwnProperty("bl_licencaazure") && response.hasOwnProperty('id_usuarioazure')) {
          if (response.bl_licencaazure === '1') {
            this.st_loginazure = response.st_loginazure;
            this.loading = false;
            this.etapaIntegracao = this.EtapasIntegracaoOffice365.EXIBIR_DADOS_USUARIO;
          }
          else {
            this.loading = false;
            this.etapaIntegracao = this.EtapasIntegracaoOffice365.CRIAR_USUARIO;
          }
        }
        else {
          this.loading = false;
          this.NotifyService.notify('warning', '', 'Ocorreu algum erro ao buscar dados do seu usuário!');
        }
      })
      .catch(() => {
        this.loading = false;
        this.etapaIntegracao = this.EtapasIntegracaoOffice365.CRIAR_USUARIO;
      });
  }

  verificarUsuario(){
    this.loading = true;
    if (this.idUsuarioAzure){
      this.atribuirGrupoUsuario();
    }
    else {
      this.criarUsuario();
    }
  }

  redefinirSenhaUsuario() {
    this.loading = true;
    this.Office365Service.redefinirSenha(this.idUsuario, this.idEntidade).then(
      (response) => {
        this.loading = false;
        alertify.success('Senha redefinida com sucesso! Acesse seu e-mail ' + response.st_email + ' e siga as intruções.', '20');
      })
      .catch(() => {
        this.loading = false;
        this.NotifyService.notify('warning', '', 'Houve um erro ao redefinir a senha.');
      });
  }

  confirmarRedefinirSenha() {
    if (confirm("Confirma iniciar a redefinição de sua senha?")) {
      this.redefinirSenhaUsuario();
    }
  }

  mostrarLogin() {
    this.mostraLogin = !this.mostraLogin;
  }

  copyToClipboard = (option) => {
    if (option === 0) {
      navigator.clipboard.writeText(this.st_loginazure);
    }
    if (option === 1) {
      navigator.clipboard.writeText(this.password);
    }
    this.NotifyService.notify('success', '', 'Copiado com sucesso!');
  }
}

export default PcOffice365MainController;
