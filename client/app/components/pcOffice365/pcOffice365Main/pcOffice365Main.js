/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcOffice365MainComponent from './pcOffice365Main.component';

//Import do modulo de services do portal
import Services from '../../../services/Services'; // Talvez você precise alterar este destino

//Modulo principal do componente
const pcOffice365MainModule = angular.module('pcOffice365Main', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])

  .config(($stateProvider) => {
    'ngInject';

    // Rotas
    $stateProvider
      .state('app.office365', {
        url: '/:idMatriculaAtiva/office365',
        views: {
          content: {
            template: '<pc-office365-main></pc-office365-main>'
          }
        },

        onEnter (AuthService, $state) {
          const params = $state.params;

          //grava log de acesso
          AuthService.gravaLogAcesso($state.current.name, params);
        }
      });
  })

  .component('pcOffice365Main', pcOffice365MainComponent);

export default pcOffice365MainModule;
