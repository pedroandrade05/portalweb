import template from './pcOffice365Main.html';
import controller from './pcOffice365Main.controller';
import './pcOffice365Main.scss';

const pcOffice365MainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcOffice365MainComponent;
