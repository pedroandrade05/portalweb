import PcOffice365MainModule from './pcOffice365Main';
import PcOffice365MainController from './pcOffice365Main.controller';
import PcOffice365MainComponent from './pcOffice365Main.component';
import PcOffice365MainTemplate from './pcOffice365Main.html';

describe('PcOffice365Main', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcOffice365MainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcOffice365MainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcOffice365MainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcOffice365MainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcOffice365MainController);
    });
  });
});
