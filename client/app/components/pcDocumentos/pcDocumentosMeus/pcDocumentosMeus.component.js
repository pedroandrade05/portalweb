import template from './pcDocumentosMeus.html';
import controller from './pcDocumentosMeus.controller';
import './pcDocumentosMeus.scss';

const pcDocumentosMeusComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcDocumentosMeusComponent;
