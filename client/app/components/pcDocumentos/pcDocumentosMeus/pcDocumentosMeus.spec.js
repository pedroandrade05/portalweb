import PcDocumentosMeusModule from './pcDocumentosMeus';
import PcDocumentosMeusController from './pcDocumentosMeus.controller';
import PcDocumentosMeusComponent from './pcDocumentosMeus.component';
import PcDocumentosMeusTemplate from './pcDocumentosMeus.html';

describe('PcDocumentosMeus', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcDocumentosMeusModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcDocumentosMeusController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcDocumentosMeusComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcDocumentosMeusTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcDocumentosMeusController);
    });
  });
});
