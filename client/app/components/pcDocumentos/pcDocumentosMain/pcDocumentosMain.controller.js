// import loadingGif from '../../../../assets/img/loading.gif';

class PcDocumentosMainController {

  constructor(
    /* Úteis */ $state
    /* Services */
    /* Constantes */
  ) {
    'ngInject';

    // Úteis
    this.$state = $state;

  }

  showMenu() {
    return [
      'app.documentos',
      'app.documentos.acompanhar',
      'app.documentos.meus',
      'app.documentos.info',
      'app.documentos.pessoais'
    ].indexOf(this.$state.current.name) !== -1;
  }

}

export default PcDocumentosMainController;
