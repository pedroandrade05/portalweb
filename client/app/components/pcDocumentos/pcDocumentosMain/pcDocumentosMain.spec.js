import PcDocumentosMainModule from './pcDocumentosMain';
import PcDocumentosMainController from './pcDocumentosMain.controller';
import PcDocumentosMainComponent from './pcDocumentosMain.component';
import PcDocumentosMainTemplate from './pcDocumentosMain.html';

describe('PcDocumentosMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcDocumentosMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcDocumentosMainController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcDocumentosMainComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcDocumentosMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcDocumentosMainController);
    });
  });
});
