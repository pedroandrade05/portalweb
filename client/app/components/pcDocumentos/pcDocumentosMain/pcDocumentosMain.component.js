import template from './pcDocumentosMain.html';
import controller from './pcDocumentosMain.controller';
import './pcDocumentosMain.scss';

const pcDocumentosMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcDocumentosMainComponent;
