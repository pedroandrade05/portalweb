import template from './pcDocumentosPessoais.html';
import controller from './pcDocumentosPessoais.controller';
import './pcDocumentosPessoais.scss';

const pcDocumentosPessoaisComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcDocumentosPessoaisComponent;
