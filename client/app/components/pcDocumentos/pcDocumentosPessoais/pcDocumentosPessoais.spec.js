import PcDocumentosPessoaisModule from './pcDocumentosPessoais'
import PcDocumentosPessoaisController from './pcDocumentosPessoais.controller';
import PcDocumentosPessoaisComponent from './pcDocumentosPessoais.component';
import PcDocumentosPessoaisTemplate from './pcDocumentosPessoais.html';

describe('PcDocumentosPessoais', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcDocumentosPessoaisModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcDocumentosPessoaisController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
      let component = PcDocumentosPessoaisComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(PcDocumentosPessoaisTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(PcDocumentosPessoaisController);
      });
  });
});
