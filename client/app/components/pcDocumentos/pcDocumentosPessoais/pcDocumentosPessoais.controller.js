class PcDocumentosPessoaisController {
  constructor($rootScope, $scope, $state, EntidadeConstante, FuncionalidadeConstante, CursoAlunoService, EsquemaConfiguracaoService, MenuService) {
    'ngInject';
    this.name = 'pcDocumentosPessoais';
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.$state = $state;
    this.EntidadeConstante = EntidadeConstante;
    this.FuncionalidadeConstante = FuncionalidadeConstante;
    this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);
    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao(this.MatriculaAtiva.id_matricula);
    this.exibirDescricaoDocumentos = true;
    this.exibirMenuDocumentosPessoais = false;
    this.MenuService = MenuService;
    const exibirTopoListener = this.$rootScope.$on('toggleTopoDescricaoDocumentos', () => {
      this.exibirDescricaoDocumentos = !this.exibirDescricaoDocumentos;
    });
    this.$scope.$on('$destroy', exibirTopoListener);

    this.exibirDocumentosPessoais();
  }

  exibirDocumentosPessoais() {
    this.MenuService.hasPermission(this.FuncionalidadeConstante.DOCUMENTOS_PESSOAIS).then(() => {
      this.exibirMenuDocumentosPessoais = true;
    })
      .catch(() => {
      })
      .finally(() => {
        this.loading = false;
      });
  }
}

export default PcDocumentosPessoaisController;
