/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcDocumentosPessoaisComponent from './pcDocumentosPessoais.component';

//Import do modulo de services do portal
import Services from '../../../services/Services'; // Talvez você precise alterar este destino

//Modulo principal do componente
const pcDocumentosPessoaisModule = angular.module('pcDocumentosPessoais', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])
  .config(($stateProvider) => {
    "ngInject"; //Inject necessario para que o angular faca as injecoes as factorys e services

    //Definicao de rotas para o componente
    $stateProvider
      .state('app.documentos.pessoais', {
        url: '/pessoais',
        views: {
          content: {
            template: '<pc-documentos-pessoais class="scroll-wrapper"></pc-documentos-pessoais>'
          }
        }
      });
  })

  .component('pcDocumentosPessoais', pcDocumentosPessoaisComponent);

export default pcDocumentosPessoaisModule;
