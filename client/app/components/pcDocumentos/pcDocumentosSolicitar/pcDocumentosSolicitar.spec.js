import PcDocumentosSolicitarModule from './pcDocumentosSolicitar';
import PcDocumentosSolicitarController from './pcDocumentosSolicitar.controller';
import PcDocumentosSolicitarComponent from './pcDocumentosSolicitar.component';
import PcDocumentosSolicitarTemplate from './pcDocumentosSolicitar.html';

describe('PcDocumentosSolicitar', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcDocumentosSolicitarModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcDocumentosSolicitarController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcDocumentosSolicitarComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcDocumentosSolicitarTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcDocumentosSolicitarController);
    });
  });
});
