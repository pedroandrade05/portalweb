import loadingGif from '../../../../assets/img/loading.gif';

class PcDocumentosSolicitarController {

  constructor(
    /* Úteis */ CONFIG, $filter, $localStorage, $rootScope, $state, $window,
    /* Services */ NotifyService, DocumentoDigitalService, DownloadService,
    /* Constantes */ TextoSistemaConstante
  ) {
    'ngInject';

    // Úteis
    this.CONFIG = CONFIG;
    this.$filter = $filter;
    this.$localStorage = $localStorage;
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$window = $window;

    // Serviços
    this.NotifyService = NotifyService;
    this.DocumentoDigitalService = DocumentoDigitalService;
    this.DownloadService = DownloadService;

    // Arquivos
    this.loadingGif = loadingGif;

    // Constantes
    this.ModeloDocumentoConstantes = TextoSistemaConstante;

    // Dados
    this.loading = true;
    this.filter = {};
    this.idMatricula = $state.params.idMatriculaAtiva;

    this.documentos = this.$state.params.documentos || [];
    this.produtos = this.$state.params.produtos || [];
    this.selecionados = this.$state.params.selecionados || [];

    if (!this.produtos.length) {
      this.getProdutos();
    }

    this.getTiposDocumentos();
  }

  getProdutos() {
    return this.DocumentoDigitalService.getDocumentosDisponiveis(this.$localStorage.user_entidade, this.idMatricula)
      .then((response) => {
        // Definir o que foram selecionados previamente
        response.forEach((produto) => {
          const isSelecionado = this.selecionados.find((item) => +item.id_documentodigital === +produto.id_documentodigital);
          if (isSelecionado) {
            produto.checked = true;
          }
        });
        this.produtos = response;
        this.$localStorage.documentosProdutos = this.produtos;
      }).finally(() => {
        this.loading = false;
      });
  }

  getSelecionados() {
    return this.produtos.filter((item) => item.checked);
  }

  getTiposDocumentos() {
    return this.DocumentoDigitalService.getTipos().then(
      (response) => {
        this.tipos = response;
      },
      (err) => {
        this.NotifyService.notify('warning', '', err.text || 'Não foi possível buscar os tipos.');
      });
  }

  continuar() {
    const selecionados = this.getSelecionados();

    if (!selecionados.length) {
      this.NotifyService.notify('warning', '', 'Selecione um documento ou uma declaração');
      return false;
    }

    this.$state.go('app.documentos.confirmar', {produtos: this.produtos});
  }

  getValorTotal() {
    let valorTotal = 0;

    this.getSelecionados().forEach((produto) => {
      valorTotal += produto.checked ? +produto.nu_valor : 0;
    });

    return valorTotal;
  }

  getFiltroDocumento(documento) {
    const docProduto = this.$filter('removeSpecialChars')((documento.st_produto || '').toUpperCase());
    const filProduto = this.$filter('removeSpecialChars')((this.filter.st_produto || '').toUpperCase());

    return (docProduto.includes(filProduto) || !filProduto)
      && (+documento.id_documentodigitaltipo === +this.filter.id_documentodigitaltipo || !this.filter.id_documentodigitaltipo);
  }

  abrirModelo(produto) {
    if (!produto.id_textosistema) {
      return false;
    }
    this.DownloadService
      .geraUrlModeloDocumento(
        this.idMatricula,
        produto.id_textosistema,
        this.ModeloDocumentoConstantes.MODELO_DOCUMENTO_DRAFT,
        this.ModeloDocumentoConstantes.MODELO_DOCUMENTO_PAPEL,
      )
      .then((response) => {
        const $win = this.$window.open(response);

        if (!$win) {
          this.NotifyService.notify('warning', '', 'O seu navegador bloqueou o download do documento.');
        }
      }, (error) => {
        const type = error && error.status === 404 ? 'warning' : 'error';
        const message = error && error.data && error.data.message ? error.data.message : 'Não foi possível visualizar o modelo do documento.';
        this.NotifyService.notify(type, '', message);
      });

  }

  voltar() {
    this.$state.go('app.documentos.acompanhar', {
      documentos: this.documentos
    });
  }

}

export default PcDocumentosSolicitarController;
