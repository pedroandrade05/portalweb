import template from './pcDocumentosSolicitar.html';
import controller from './pcDocumentosSolicitar.controller';
import './pcDocumentosSolicitar.scss';

const pcDocumentosSolicitarComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcDocumentosSolicitarComponent;
