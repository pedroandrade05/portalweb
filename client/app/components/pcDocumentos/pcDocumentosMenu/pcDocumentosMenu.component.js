import template from './pcDocumentosMenu.html';
import controller from './pcDocumentosMenu.controller';
import './pcDocumentosMenu.scss';

const pcDocumentosMenuComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcDocumentosMenuComponent;
