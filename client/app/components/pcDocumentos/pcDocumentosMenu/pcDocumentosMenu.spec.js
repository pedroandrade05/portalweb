import PcDocumentosMenuModule from './pcDocumentosMenu';
import PcDocumentosMenuController from './pcDocumentosMenu.controller';
import PcDocumentosMenuComponent from './pcDocumentosMenu.component';
import PcDocumentosMenuTemplate from './pcDocumentosMenu.html';

describe('PcDocumentosMenu', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcDocumentosMenuModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcDocumentosMenuController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcDocumentosMenuComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcDocumentosMenuTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcDocumentosMenuController);
    });
  });
});
