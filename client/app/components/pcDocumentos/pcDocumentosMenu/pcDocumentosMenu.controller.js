import loadingGif from '../../../../assets/img/loading.gif';

class PcDocumentosMenuController {

  constructor(
    /* Úteis */ $state, $localStorage,
    /* Services */ DocumentoDigitalService, EsquemaConfiguracaoService, CursoAlunoService, MenuService,
    /* Constantes */ EntidadeConstante, FuncionalidadeConstante
  ) {
    'ngInject';

    this.$state = $state;
    this.DocumentoDigitalService = DocumentoDigitalService;
    this.MenuService = MenuService;
    this.$localStorage = $localStorage;
    this.loadingGif = loadingGif;
    this.loading = true;
    this.EntidadeConstante = EntidadeConstante;
    this.FuncionalidadeConstante = FuncionalidadeConstante;
    this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);

    this.idMatricula = $state.params.idMatriculaAtiva;
    this.permiteSolicitar = false;
    this.permiteAcompanhar = false;
    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao(this.idMatricula);
    this.exibirMenuDocumentosPessoais = false;
    this.getSolicitacoesDocumentosDisponiveis();
    this.getSolicitacoes();
    this.exibirDocumentosPessoais();
  }

  getSolicitacoesDocumentosDisponiveis() {

    this.DocumentoDigitalService.getDocumentosDisponiveis(this.$localStorage.user_entidade, this.idMatricula)
      .then((response) => {
        if (response) {
          this.permiteSolicitar = true;
        }
      });
  }

  exibirDocumentosPessoais() {
    this.MenuService.hasPermission(this.FuncionalidadeConstante.DOCUMENTOS_PESSOAIS).then(() => {
      this.exibirMenuDocumentosPessoais = true;
    })
      .catch(() => {
      })
      .finally(() => {
        this.loading = false;
      });
  }

  getSolicitacoes() {
    this.DocumentoDigitalService.getAll({
      id_matricula: this.idMatricula
    }).then((response) => {
      if (response) {
        this.permiteAcompanhar = true;
      }
    });
  }
}

export default PcDocumentosMenuController;
