import template from './pcDocumentosConfirmar.html';
import controller from './pcDocumentosConfirmar.controller';
import './pcDocumentosConfirmar.scss';

const pcDocumentosConfirmarComponent = {
  restrict: 'E',
  bindings: {
    selecionados: '@'
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pcDocumentosConfirmarComponent;
