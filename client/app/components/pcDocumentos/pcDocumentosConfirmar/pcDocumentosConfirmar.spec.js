import PcDocumentosConfirmarModule from './pcDocumentosConfirmar';
import PcDocumentosConfirmarController from './pcDocumentosConfirmar.controller';
import PcDocumentosConfirmarComponent from './pcDocumentosConfirmar.component';
import PcDocumentosConfirmarTemplate from './pcDocumentosConfirmar.html';

describe('PcDocumentosConfirmar', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcDocumentosConfirmarModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcDocumentosConfirmarController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcDocumentosConfirmarComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcDocumentosConfirmarTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcDocumentosConfirmarController);
    });
  });
});
