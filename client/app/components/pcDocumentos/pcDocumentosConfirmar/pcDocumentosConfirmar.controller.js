// import loadingGif from '../../../../assets/img/loading.gif';

class PcDocumentosConfirmarController {

  constructor(
    /* Úteis */ $localStorage, $state, $uibModal,
    /* Services */ DocumentoDigitalService, NotifyService,
    /* Constantes */ MeioPagamentoConstante
  ) {
    'ngInject';

    // Úteis
    this.$localStorage = $localStorage;
    this.$state = $state;
    this.$modal = $uibModal;

    // Serviços
    this.DocumentoDigitalService = DocumentoDigitalService;
    this.NotifyService = NotifyService;

    // Constantes
    this.MeioPagamentoConstante = MeioPagamentoConstante;

    // Dados
    this.produtos = $state.params.produtos || $localStorage.documentosProdutos || [];
    this.selecionados = this.produtos.filter((item) => item.checked);

    if (!this.selecionados.length) {
      return this.voltar();
    }

    // Calcular valor total
    this.valorTotal = 0;
    this.selecionados.forEach((produto) => {
      this.valorTotal += +produto.nu_valor;
    });

    this.meiosPagamento = [
      {value: MeioPagamentoConstante.CARTAO_CREDITO, text: 'Cartão de crédito'},
      {value: MeioPagamentoConstante.BOLETO, text: 'Boleto bancário'}
    ];

    this.data = {
      id_entidade: this.$localStorage.user_entidade,
      id_usuario: this.$localStorage.user_id,
      id_matricula: this.$state.params.idMatriculaAtiva,
      id_meiopagamento: MeioPagamentoConstante.CARTAO_CREDITO,
      nu_valorbruto: this.valorTotal,
      nu_valorliquido: this.valorTotal,
      produtos: []
    };
  }

  isGratuito() {
    return this.valorTotal <= 0;
  }

  voltar() {
    this.$state.go('app.documentos.solicitar', {
      produtos: this.produtos
    });
  }

  confirmar() {
    this.loadingConfirmar = true;

    const dtVencimento = new Date();
    dtVencimento.setDate(dtVencimento.getDate() + 2);

    this.data.produtos = this.selecionados.map((produto) => {
      return {
        id_produto: produto.id_produto,
        id_documentodigital: produto.id_documentodigital,
        nu_valor: produto.nu_valor
      };
    });

    if (this.isGratuito()) {
      this.data.id_meiopagamento = null;
    }

    return this.DocumentoDigitalService.getResource()
      .create(this.data).$promise
      .then(
        (response) => {
          this.venda = response;

          // Resetar o "carrinho"
          this.produtos.forEach((item) => item.checked = false);

          // Exibir mensagem
          this.msgSucesso();
        },
        (error) => {
          this.NotifyService.notify('error', '', error.data.message);
        }
      ).finally(() => {
        this.loadingConfirmar = false;
      });
  }

  msgSucesso() {
    this.$modal.open({
      animation: true,
      component: 'pcModalConfirm',
      resolve: {
        data: () => {
          return {
            title: 'Documentos solicitados com sucesso!',
            question: 'Para acompanhar a solicitação, acesse <b>Meus Documentos</b>.',
            okText: 'Ok, fechar',
            renovacao: true
          };
        }
      }
    }).result.then((response) => {
      if (response && this.isGratuito()) {
        this.$state.go('app.documentos');
      }
    });
  }

}

export default PcDocumentosConfirmarController;
