import template from './pcDocumentosAcompanhar.html';
import controller from './pcDocumentosAcompanhar.controller';
import './pcDocumentosAcompanhar.scss';

const pcDocumentosAcompanharComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcDocumentosAcompanharComponent;
