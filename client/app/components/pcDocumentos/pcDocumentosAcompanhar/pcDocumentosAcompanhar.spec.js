import PcDocumentosAcompanharModule from './pcDocumentosAcompanhar';
import PcDocumentosAcompanharController from './pcDocumentosAcompanhar.controller';
import PcDocumentosAcompanharComponent from './pcDocumentosAcompanhar.component';
import PcDocumentosAcompanharTemplate from './pcDocumentosAcompanhar.html';

describe('PcDocumentosAcompanhar', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcDocumentosAcompanharModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcDocumentosAcompanharController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcDocumentosAcompanharComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcDocumentosAcompanharTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcDocumentosAcompanharController);
    });
  });
});
