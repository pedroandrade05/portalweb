import loadingGif from '../../../../assets/img/loading.gif';

class PcDocumentosAcompanharController {

  constructor(
    /* Úteis */ $filter, $interval, $localStorage, $scope, $state, $window,
    /* Services */ DocumentoDigitalService, DownloadService, NotifyService, EsquemaConfiguracaoService, MenuService,
    /* Constantes */ FuncionalidadeConstante
  ) {
    'ngInject';

    // Úteis
    this.$filter = $filter;
    this.$scope = $scope;
    this.$state = $state;
    this.$window = $window;
    this.$localStorage = $localStorage;
    this.FuncionalidadeConstante = FuncionalidadeConstante;

    // Serviços
    this.DocumentoDigitalService = DocumentoDigitalService;
    this.DownloadService = DownloadService;
    this.NotifyService = NotifyService;
    this.MenuService = MenuService;

    // Arquivos e estados
    this.loading = true;
    this.loadingGif = loadingGif;
    this.loadingBaixar = {};

    // Dados
    this.filter = {};
    this.idMatricula = $state.params.idMatriculaAtiva;
    this.documentos = this.$state.params.documentos || [];
    this.permiteSolicitar = false;
    this.isGraduacao = EsquemaConfiguracaoService.isGraduacao(this.idMatricula);

    this.strings = {
      Liberado: {title: 'Liberados', color: 'success'},
      Pendente: {title: 'Pendentes', color: 'warning'},
      Expirado: {title: 'Expirados', color: 'danger'}
    };

    if (this.documentos.length) {
      this.getGrupos();
      this.loading = false;
    }

    this.getSolicitacoes();
    this.getTiposDocumentos();
    this.getSolicitacoesDocumentosDisponiveis();

    // Atualizar a tela constantemente
    const atualizarInterval = $interval(this.getSolicitacoes.bind(this), 10000);
    $scope.$on('$destroy', () => $interval.cancel(atualizarInterval));
  }

  getTiposDocumentos() {
    return this.DocumentoDigitalService.getTipos().then(
      (response) => {
        this.tipos = response;
      },
      (err) => {
        this.NotifyService.notify('warning', '', err.text || 'Não foi possível buscar os tipos.');
      });
  }

  getSolicitacoes() {
    this.DocumentoDigitalService.getAll({
      id_matricula: this.idMatricula
    }).then(
      (response) => {
        this.documentos = response;
      },
      () => {
        this.documentos = [];
        if (this.isGraduacao) {
          let rota = "app.documentos.meus";

          this.MenuService.hasPermission(this.FuncionalidadeConstante.DOCUMENTOS_PESSOAIS).then(() => {
            rota = "app.documentos.pessoais";
          }).finally(() => {
            this.$state.go(rota, {
              idMatriculaAtiva: this.idMatricula
            });
          });
        }
      }
    ).finally(() => {
      this.getGrupos();
      this.loading = false;
    });
  }

  getGrupos() {
    const filtered = this.$filter('groupBy')(this.documentos, 'st_status');

    const grupos = {};

    // Manter ordenado Liberado > Pendente > Expirado

    if (filtered.Liberado) {
      grupos.Liberado = filtered.Liberado;
    }

    if (filtered.Pendente) {
      grupos.Pendente = filtered.Pendente;
    }

    if (filtered.Expirado) {
      grupos.Expirado = filtered.Expirado;
    }

    this.grupos = grupos;
  }

  getFiltroGrupo(documentos) {
    return documentos.filter((documento) => this.getFiltroDocumento(documento)).length;
  }

  getFiltroDocumento(documento) {
    const docProduto = this.$filter('removeSpecialChars')((documento.st_produto || '').toUpperCase());
    const filProduto = this.$filter('removeSpecialChars')((this.filter.st_produto || '').toUpperCase());

    return (docProduto.includes(filProduto) || !filProduto)
      && (+documento.id_documentodigitaltipo === +this.filter.id_documentodigitaltipo || !this.filter.id_documentodigitaltipo);
  }

  getSolicitacoesDocumentosDisponiveis() {

    this.DocumentoDigitalService.getDocumentosDisponiveis(this.$localStorage.user_entidade, this.idMatricula)
      .then((response) => {
        if (response) {
          this.permiteSolicitar = true;
        }
      });
  }

  baixarDocumento(documento) {
    this.DownloadService
      .geraUrlDownloadDocumentoDigital(
        this.idMatricula,
        documento.id_documentodigital
      )
      .then((response) => {
        const $win = this.$window.open(response);

        if (!$win) {
          this.NotifyService.notify('warning', '', 'O seu navegador bloqueou o download do documento.');
        }
      }, (error) => {
        const type = error && error.status === 404 ? 'warning' : 'error';
        const message = error && error.data && error.data.message ? error.data.message : 'Não foi possível baixar o documento.';
        this.NotifyService.notify(type, '', message);
      });
  }

}

export default PcDocumentosAcompanharController;
