// import loadingGif from '../../../../assets/img/loading.gif';

import loadingGif from "../../../../assets/img/loading.gif";

class PcDocumentosInfoController {

  constructor(
    /* Úteis */ $state, $localStorage, $sce,
    /* Services */ AvisosService
    /* Constantes */
  ) {
    'ngInject';

    // Úteis
    this.$state = $state;
    this.$sce = $sce;

    // Services
    this.AvisosService = AvisosService;

    // Outros
    this.loading = true;
    this.loadingGif = loadingGif;
    this.idUsuario = $localStorage.user_id;
    this.idMatricula = $state.params.idMatriculaAtiva;

    this.informacoes = {};
    this.getInformacoes();
  }

  getInformacoes() {
    return this.AvisosService.getMensagemDocumentoDigital(
      this.idMatricula
    ).then(
      (response) => {
        this.loading = false;
        this.informacoes = response.toJSON();
      }, () => {
        this.loading = false;
      }
    );
  }

}

export default PcDocumentosInfoController;
