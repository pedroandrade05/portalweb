import template from './pcDocumentosInfo.html';
import controller from './pcDocumentosInfo.controller';
import './pcDocumentosInfo.scss';

const pcDocumentosInfoComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcDocumentosInfoComponent;
