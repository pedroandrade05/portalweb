import PcDocumentosInfoModule from './pcDocumentosInfo';
import PcDocumentosInfoController from './pcDocumentosInfo.controller';
import PcDocumentosInfoComponent from './pcDocumentosInfo.component';
import PcDocumentosInfoTemplate from './pcDocumentosInfo.html';

describe('PcDocumentosInfo', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcDocumentosInfoModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcDocumentosInfoController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcDocumentosInfoComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcDocumentosInfoTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcDocumentosInfoController);
    });
  });
});
