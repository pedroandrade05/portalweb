import logoCopiar from "../../../../assets/img/icons/Shape.svg";
import loadingGif from "../../../../assets/img/loading.gif";
import logoConttabil from "../../../../assets/img/unylab/logoConttabil.png";
import logoAutoCad from "../../../../assets/img/unylab/logoAutoCad.png";
import logoMaya3d from "../../../../assets/img/unylab/logoMaya3d.png";
import logoAxureRp from "../../../../assets/img/unylab/logoAxureRp.png";
import politicas365 from "../../../../assets/files/politicas365.html";
import politicasUnylab from "../../../../assets/files/politicasUnylab.html";
import alertify from "alertifyjs/build/alertify.min.js";
import Guacamole from "guacamole-common-js";
import thumb_criar_conta from "../../../../assets/img/thumb_como_criar_sua_conta.png";
import thumb_acesso_conta from "../../../../assets/img/thumb_como_acessar_sua_conta.png";
import thumb_recuperar_senha from "../../../../assets/img/thumb_como_recuperar_sua_senha.png";

class PcUnylabMainController {
  constructor(
    /* Úteis */ $state, $window, $sce, ModalService, $localStorage, CONFIG, EtapasIntegracaoOffice365, $rootScope,
    /* Services */ NotifyService, Office365Service, UnylabService) {
    'ngInject';

    //Services
    this.NotifyService = NotifyService;
    this.Office365Service = Office365Service;
    this.UnylabService = UnylabService;

    //Úteis
    this.$state = $state;
    this.params = $state.params;
    this.$window = $window;
    this.$sce = $sce;
    this.ModalService = ModalService;
    this.$localStorage = $localStorage;
    this.CONFIG = CONFIG;
    this.EtapasIntegracaoUnylab = EtapasIntegracaoOffice365;
    this.$rootScope = $rootScope;
    this.url = this.Office365Service.getUrlAutenticaUsuario();
    this.urlDominioAzureAD = this.Office365Service.getUrlDominioAzureAD();

    // Logos
    this.logoCopiar = logoCopiar;
    this.loadingGif = loadingGif;
    this.logoConttabil = logoConttabil;
    this.logoAutoCad = logoAutoCad;
    this.logoMaya3d = logoMaya3d;
    this.logoAxureRp = logoAxureRp;

    //Outros
    this.idUsuario = $localStorage.user_id;
    this.idEntidade = $localStorage.user_entidade;
    this.stLogin = $localStorage.login;
    this.loadingUL = true;

    // Office 365
    this.showTermoOffice365 = false;
    this.stLoginAzure = null;
    this.blLicencaazure = 0;
    this.stPassword = this.$rootScope.stPasswordOffice365 ? this.$rootScope.stPasswordOffice365 : null;
    this.politicas365 = politicas365;

    // Unylab
    this.showTermoUnylab = false;
    this.blLicencaunylab = 0;
    this.politicasUnylab = politicasUnylab;
    this.nuToken = 0;
    this.nuConexoes = 0;
    this.nuConexoesDisponiveis = 0;
    this.idConexaoDisponivel = null;
    this.mostraLogin = false;

    this.tutoriais = [
      {url: 'https://pvbps-sambavideos.akamaized.net/account/3237/4/2020-11-16/video/420e497b6e7625c73fc8a7e7cdbe57ce/Como_criar_sua_conta_no_Office_365_2.mp4', imagem: thumb_criar_conta, duracao: '2:46', titulo: 'Como criar a sua conta' ,descricao: ''},
      {url: 'https://pvbps-sambavideos.akamaized.net/account/3237/4/2020-11-16/video/60ba2ab81d2a52f42138fe6a6301b73f/Como_Acessar_o_Office_365_no_Portal_do_Aluno.mp4', imagem: thumb_acesso_conta, duracao: '2:22', titulo: 'Como acessar o office 365' ,descricao: ''},
      {url: 'https://pvbps-sambavideos.akamaized.net/account/3237/4/2020-11-18/video/18102e1a50226d40c88804f1016756a9/Como_Recuperar_seu_Login_ou_Senha_do_Office_365.mp4', imagem: thumb_recuperar_senha, duracao: '1:44', titulo: 'Como recuperar o login ou senha' ,descricao: ''}];
  }

  etapaUsuarioUnylab() {
    this.etapaIntegracao = this.EtapasIntegracaoUnylab.RECUPERAR_DADOS_USUARIO;
    this.getUsuario();
  }

  getUsuario() {
    this.loading = true;
    this.Office365Service.getLocalizaUsuario(this.idUsuario).then(
      (response) => {
        if (response.st_loginazure && response.id_usuarioazure) {
          this.stLoginAzure = response.st_loginazure;
          this.idUsuarioAzure = response.id_usuarioazure;

          if (response.bl_licencaunylab === '1') {
            this.blLicencaunylab = response.bl_licencaunylab;
            this.loading = false;
            this.etapaIntegracao = this.EtapasIntegracaoUnylab.EXIBIR_DADOS_USUARIO;
            this.retornaConexoesGruposUnylab();
          }
          else {
            this.loading = false;
            this.etapaIntegracao = this.EtapasIntegracaoUnylab.PRIMEIRO_ACESSO_UNYLAB;
          }
        }
        else {
          this.loading = false;
          this.etapaIntegracao = this.EtapasIntegracaoUnylab.CRIAR_USUARIO;
        }
      })
      .catch(() => {
        this.loading = false;
        this.etapaIntegracao = this.EtapasIntegracaoUnylab.CRIAR_USUARIO;
      });
  }

  retornaConexoesGruposUnylab() {
    return this.UnylabService.postConexoesGrupos(this.idEntidade)
      .then((response) => {
        this.nuConexoes = response.nu_conexoes;
        this.nuConexoesDisponiveis = response.nu_conexoesdisponiveis;
        this.idConexaoDisponivel = response.id_conexaodisponivel;
      })
      .catch((error) => {
        this.NotifyService.notify('warning', '', error || 'Não foi possível buscar os dados das conexões.');
      });
  }

  verificarUsuarioOffice365() {
    this.loading = true;
    if (this.idUsuarioAzure) {
      this.loading = true;
      this.atribuirGrupoUsuarioOffice365();
    }
    else {
      this.criarUsuarioOffice365();
    }
  }

  atribuirGrupoUsuarioOffice365() {
    this.Office365Service.postAtribuirGrupoUsuario(this.idUsuario, this.idEntidade)
      .then(() => {
        this.etapaIntegracao = this.EtapasIntegracaoUnylab.PRIMEIRO_ACESSO_UNYLAB;
        this.showTermoOffice365 = false;
        this.msgSucesso();
        this.loading = false;
      })
      .catch(() => {
        this.loading = false;
        this.NotifyService.notify('error', '', 'Houve um erro ao criar seu usuário!');
      });
  }

  criarUsuarioOffice365() {
    this.loading = true;
    this.Office365Service.postCriarUsuario(this.idUsuario)
      .then(
        (response) => {
          this.stLoginAzure = response.mensagem.st_login;
          this.stPassword = response.mensagem.st_senha;
          this.$rootScope.stPasswordOffice365 = this.stPassword;
          this.atribuirGrupoUsuarioOffice365();
        })
      .catch(() => {
        this.loading = false;
        this.NotifyService.notify('error', '', 'Houve um erro ao criar seu usuário!');
      });
  }

  msgSucesso() {
    this.ModalService.show({
      title: 'Conta criada com sucesso!',
      body: 'Você criou sua conta UnyLeya!<br>Agora você pode acessar as ferramentas <br>virtuais disponíveis para você!'
    }).then(() => {
      return true;
    });
  }

  abrirTermoOffice365() {
    this.accordion = false;
    this.showTermoOffice365 = !this.showTermoOffice365;
  }

  abrirTermoUnylab() {
    this.accordion = false;
    if (this.etapaIntegracao === this.EtapasIntegracaoUnylab.PRIMEIRO_ACESSO_UNYLAB) {
      this.etapaIntegracao = this.EtapasIntegracaoUnylab.CRIAR_USUARIO_UNYLAB;
    }
    this.showTermoUnylab = !this.showTermoUnylab;
  }

  verificarUsuarioUnylab() {
    this.loading = true;
    if (!this.blLicencaunylab) {
      this.criarUsuarioUnylab();
    }
    this.loading = false;
  }

  criarUsuarioUnylab() {
    this.etapaIntegracao = this.EtapasIntegracaoUnylab.CRIAR_USUARIO_UNYLAB;
    this.UnylabService.postCriarUsuarioUnylab(this.idEntidade, this.stLoginAzure)
      .then((response) => {
        this.UnylabService.postAtribuirUsuarioGrupo( this.idEntidade, response.data.username)
          .then(() => {
          })
          .catch(() => {
            this.loading = false;
            this.NotifyService.notify('error', 'Erro', 'Houve um erro ao atribuir o usuário a um grupo da Unylab!');
          });

        this.UnylabService.postAlterarLicencaUnylab(this.idUsuario, 1)
          .then(() => {
            this.blLicencaunylab = 1;
            this.retornaConexoesGruposUnylab();
            this.etapaIntegracao = this.EtapasIntegracaoUnylab.EXIBIR_DADOS_USUARIO;
          })
          .catch(() => {
            this.loading = false;
            this.NotifyService.notify('error', 'Erro', 'Erro ao tentar alterar a licença Unylab do usário!');
          });
      })
      .catch(() => {
        this.loading = false;
        this.NotifyService.notify('error', '', 'Houve um erro ao criar seu usuário!');
      });
    this.showTermoUnylab = false;
  }

  copyToClipboard = (option) => {
    if (option === 0) {
      navigator.clipboard.writeText(this.st_loginazure);
    }
    if (option === 1) {
      navigator.clipboard.writeText(this.st_password);
    }
    this.NotifyService.notify('success', '', 'Copiado com sucesso!');
  }

  redefinirSenhaUsuario() {
    this.loading = true;
    this.Office365Service.redefinirSenha(this.idUsuario, this.idEntidade).then(
      (response) => {
        this.loading = false;
        alertify.success('Senha redefinida com sucesso! Acesse seu e-mail ' + response.st_email + ' e siga as intruções.', '20');
      })
      .catch(() => {
        this.loading = false;
        this.NotifyService.notify('warning', '', 'Houve um erro ao redefinir a senha.');
      });
  }

  mostrarLogin() {
    this.mostraLogin = !this.mostraLogin;
  }

  confirmarRedefinirSenha() {
    if (confirm("Confirma iniciar a redefinição de sua senha?")) {
      this.redefinirSenhaUsuario();
    }
  }

  abrirConexaoTunnel() {
    this.UnylabService.postTokenUnylab(this.stLogin)
      .then((response) => {

        this.nuToken = response.token;

        if (this.idConexaoDisponivel === null) {
          this.retornaConexoesGruposUnylab()
            .catch((error) => {
              this.loading = false;
              this.NotifyService.notify('error', error, 'Não foi possível retornar uma conexão disponível!');
              return false;
            });
        }

        const $win = this.$window.open('about:blank');

        $win.document.title = 'Unylab - Laboratório virtual';

        const $style = $win.document.createElement('style');
        $style.innerHTML = 'body{margin:0}';
        $win.document.head.appendChild($style);

        const client = new Guacamole.Client(
          new Guacamole.HTTPTunnel(`${this.CONFIG.urlApiUnylab}/tunnel`)
        );

        // Desconectar ao fechar
        $win.onunload = function () {
          client.disconnect();
        };

        const pcDiplayUnylab = $win.document.body; //this.$window.document.querySelector("#pcDisplayUnylab");
        pcDiplayUnylab.appendChild(client.getDisplay().getElement());

        // Connect
        client.connect('token=' + this.nuToken
          + '&GUAC_DATA_SOURCE=mysql'
          + '&GUAC_ID=' + this.idConexaoDisponivel
          + '&GUAC_TYPE=c'
          + '&GUAC_WIDTH=' + this.$window.innerWidth
          + '&GUAC_HEIGHT=' + this.$window.innerHeight
          + '&GUAC_DPI=96'
          + '&GUAC_TIMEZONE=America/Sao_Paulo'
          + '&GUAC_AUDIO=audio/L8'
          + '&GUAC_AUDIO=audio/L16'
          + '&GUAC_IMAGE=image/jpeg'
          + '&GUAC_IMAGE=image/png'
          + '&GUAC_IMAGE=image/webp');

        const mouse = new Guacamole.Mouse(client.getDisplay().getElement());

        mouse.onmousedown = mouse.onmouseup = mouse.onmousemove = (mouseState) => {
          client.sendMouseState(mouseState);
        };

        const keyboard = new Guacamole.Keyboard($win.document);

        keyboard.onkeydown = (keysym) => {
          client.sendKeyEvent(1, keysym);
          return false;
        };

        keyboard.onkeyup = (keysym) => {
          client.sendKeyEvent(0, keysym);
          return false;
        };
      }).catch(() => {
        this.loading = false;
        this.NotifyService.notify('error', '', 'Houve um erro ao criar seu usuário!');
      });
  }
}

export default PcUnylabMainController;
