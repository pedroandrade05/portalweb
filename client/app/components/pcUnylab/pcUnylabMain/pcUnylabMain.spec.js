import PcUnylabMainModule from './pcUnylabMain';
import PcUnylabMainController from './pcUnylabMain.controller';
import PcUnylabMainComponent from './pcUnylabMain.component';
import PcUnylabMainTemplate from './pcUnylabMain.html';

describe('PcUnylabMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcUnylabMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcUnylabMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcUnylabMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcUnylabMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcUnylabMainController);
    });
  });
});
