import template from './pcUnylabMain.html';
import controller from './pcUnylabMain.controller';
import './pcUnylabMain.scss';

const pcUnylabMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'uyl'
};

export default pcUnylabMainComponent;
