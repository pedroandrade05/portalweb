import templateModal from "../../../widgets/primeiro-acesso/pwPrimeiroAcesso/pwPrimeiroAcesso.html";
import modalPrimeiroAcessoController from "../../../widgets/primeiro-acesso/pwPrimeiroAcesso/pwPrimeiroAcesso.controller";

class PcCursoMainController {
  constructor(CursoAlunoService, $state, $uibModal, $scope, $rootScope, $localStorage, MensagemPadrao) {
    'ngInject';
    this.name = 'pcCursoMain';
    this.$rootScope = $rootScope;
    this.modal = $uibModal;
    this.CursoAlunoService = CursoAlunoService;
    this.storage = $localStorage;
    this.mensagemPadrao = MensagemPadrao;

    //variavel do scope para alterar a visualizacao dos states com 3 colunas ou duas colunas e duas linhas
    this.toggleState = false;

    //cria uma variavel no escopo para a matricula ativa
    this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);

    //verifica se é pra redirecionar o usuário para a tela de primeiro acesso
    if (this.MatriculaAtiva.bl_primeiroacesso ) {
      this.modal.open({
        template: templateModal,
        controller: modalPrimeiroAcessoController,
        controllerAs: 'pwpa',
        size: 'lg',
        scope: $scope,
        resolve: {
          primeiroacesso: () => {
            return this.MatriculaAtiva;
          }
        }
      });
    }

    if (this.storage.bl_avisoacessarcomo) {
      this.storage.bl_avisoacessarcomo = false;

      this.modal.open({
        animation: true,
        component: 'pcModalConfirm',
        resolve: {
          data: () => {
            return {
              alerta: true,
              title: "ATENÇÃO!",
              question: this.mensagemPadrao.ALERTA_ACESSAR_COMO,
              showCancelButton: false,
              okText: "Ok"
            };
          }
        }
      });
    }

    /**
     * Evento que atualiza Storage do Curso Ativo
     * @constructor
     */
    this.$rootScope.$on('atualizaStorage', () => {
      this.CursoAlunoService.removeStorageCursoAtivo(this.MatriculaAtiva.id_matricula);
      this.CursoAlunoService.verificaMatriculaAluno(this.MatriculaAtiva.id_matricula);
      this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo($state.params.idMatriculaAtiva);
    });

  }
}

export default PcCursoMainController;
