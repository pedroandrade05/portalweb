import template from './pcCursoMain.html';
import controller from './pcCursoMain.controller.js';
import './pcCursoMain.scss';

const pcCursoMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcCursoMainComponent;
