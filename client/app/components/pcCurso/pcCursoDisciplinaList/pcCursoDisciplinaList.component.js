import template from './pcCursoDisciplinaList.html';
import controller from './pcCursoDisciplinaList.controller';
import './pcCursoDisciplinaList.scss';

const pcCursoDisciplinaListComponent = {
  restrict: 'E',
  bindings: { limit: '=' },
  template,
  controller,
  controllerAs: 'vm'
};

export default pcCursoDisciplinaListComponent;
