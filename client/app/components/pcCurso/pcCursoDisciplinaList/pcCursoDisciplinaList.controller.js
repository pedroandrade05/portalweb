class PcCursoDisciplinaListController {
  constructor(CONFIG, DisciplinaAlunoService, $state) {
    'ngInject';
    this.DisciplinaAlunoService = DisciplinaAlunoService;
    this.CONFIG = CONFIG;
    this.params = $state.params;
    this.disciplinas = [];
    this.buscarDisciplinas();
  }
  buscarDisciplinas = () => {

    const params = {
      id_entidade: this.CONFIG.idEntidade,
      id_matricula: this.params.id_matricula,

      // id_projetopedagogico: params.idProjeto,
      id_disciplina: this.params.id_disciplina,
      bl_moodle: false
    };

    this.loading = true;
    this.DisciplinaAlunoService.getResource().getAll(params, (response) => {
      this.loading = false;
      this.disciplinas = response;
    }, (error) => {
      this.loading = false;
      this.toaster.notify(error.data);
    });
  }

}

export default PcCursoDisciplinaListController;
