import PcGradeNotasFormModule from './pcGradeNotasForm';
import PcGradeNotasFormController from './pcGradeNotasForm.controller';
import PcGradeNotasFormComponent from './pcGradeNotasForm.component';
import PcGradeNotasFormTemplate from './pcGradeNotasForm.html';

describe('PcGradeNotasForm', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcGradeNotasFormModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcGradeNotasFormController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcGradeNotasFormComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcGradeNotasFormTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcGradeNotasFormController);
    });
  });
});
