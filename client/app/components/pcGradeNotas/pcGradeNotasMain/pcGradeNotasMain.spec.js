import PcGradeNotasMainModule from './pcGradeNotasMain';
import PcGradeNotasMainController from './pcGradeNotasMain.controller';
import PcGradeNotasMainComponent from './pcGradeNotasMain.component';
import PcGradeNotasMainTemplate from './pcGradeNotasMain.html';

describe('PcGradeNotasMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcGradeNotasMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcGradeNotasMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });


  describe('Component', () => {
      // component/directive specs
    const component = PcGradeNotasMainComponent;

    it('includes the intended template',() => {
        expect(component.template).to.equal(PcGradeNotasMainTemplate);
      });

    it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

    it('invokes the right controller', () => {
        expect(component.controller).to.equal(PcGradeNotasMainController);
      });
  });
});
