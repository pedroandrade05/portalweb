import template from './pcGradeNotasMain.html';
import controller from './pcGradeNotasMain.controller';
import './pcGradeNotasMain.scss';

const pcGradeNotasMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcGradeNotasMainComponent;
