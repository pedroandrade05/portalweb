import template from './pcModuloMain.html';
import controller from './pcModuloMain.controller.js';
import './pcModuloMain.scss';

const pcModuloMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcModuloMainComponent;
