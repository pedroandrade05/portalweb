import PcModuloMainModule from './pcModuloMain';
import PcModuloMainController from './pcModuloMain.controller.js';
import PcModuloMainComponent from './pcModuloMain.component.js';
import PcModuloMainTemplate from './pcModuloMain.html';

describe('PcModuloMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcModuloMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcModuloMainController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {

  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PcModuloMainComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PcModuloMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcModuloMainController);
    });
  });
});
