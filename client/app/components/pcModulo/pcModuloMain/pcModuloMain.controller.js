import loadingGif from "../../../../assets/img/loading.gif";

class PcModuloMainController {
  constructor(CursoAlunoService, DisciplinaAlunoService, MoodleService, NotifyService, $state, $q, $sce) {
    'ngInject';

    this.DisciplinaAlunoService = DisciplinaAlunoService;
    this.MoodleService = MoodleService;
    this.NotifyService = NotifyService;

    this.$q = $q;
    this.$sce = $sce;

    this.idMatricula = $state.params.idMatriculaAtiva; // Matrícula atual do Aluno
    this.idModulo = $state.params.idModulo; // Id do Módulo no Moodle
    this.idCurso = $state.params.idCurso; // Id do Curso no Moodle
    this.idSaladeaula = $state.params.idSaladeaula; // Sala de aula no G2
    this.modulo = [];
    this.modulourl = null;
    this.fullscreen = false;

    //inicia a variavel de loading
    this.loading = false;

    //carrega a imagem do gif
    this.loadingGif = loadingGif;

    this.MatriculaAtiva = CursoAlunoService.getStorageCursoAtivo(this.idMatricula);
    this.sala = this.sala || $state.params.sala;

    if (!this.sala) {
      this.buscarDadosSala();
    }

    if (this.idMatricula && this.idModulo && this.idCurso && this.idSaladeaula) {
      this.CourseContents(this.idMatricula, this.idCurso, this.idModulo, this.idSaladeaula).then(
        (success) => {
          this.modulo = success;

          if (success.url) {
            this.modulourl = $sce.trustAsResourceUrl(success.loginurl + '&wantsurl=' + success.url);
          }
          else {
            this.modulourl = $sce.trustAsResourceUrl(success.loginurl);
          }

        },
        () => {

          // falta definir o que fazer ao receber um erro
        }
      );
    }
  }

  buscarDadosSala() {
    if (this.MatriculaAtiva.id_matricula && this.idSaladeaula) {
      this.DisciplinaAlunoService
        .retornarDisciplina(
          this.idSaladeaula,
          this.MatriculaAtiva.id_matricula,
          this.MatriculaAtiva.id_entidade,
          1,
          1,
          true
        )
        .then((response) => {
          this.sala = response;
        }, (error) => {
          this.NotifyService.notify('error', error);
        });
    }
  }

  /**
   * Retorna o conteúdo do curso do Moodle e verifica se é igual ao solicitado
   * @param idMatricula - Matrícula atual do Aluno
   * @param idCurso - Id do Módulo no Moodle
   * @param idModulo - Id do Curso no Moodle
   * @param idSaladeaula - Id da sala de aula no G2
   * @returns {*}
   * @constructor
   */
  CourseContents = (idMatricula, idCurso, idModulo, idSaladeaula) => {

    return this.$q((resolve, reject) => {

      // Passando o clearCache como true para resolver problema de usuario acessando em dois navegadores
      // @todo: implementar lógica de verificação
      this.MoodleService.getTokenAcesso(idMatricula, idSaladeaula, true).then((sucess) => {
        this.loading = true;
        this.MoodleService.getCourseContents(idMatricula, idCurso, idSaladeaula).then((contents) => {
          angular.forEach(contents, (value) => {
            angular.forEach(value.modules, (mvalue) => {
              if (parseInt(mvalue.id) === parseInt(idModulo)) {
                mvalue.loginurl = sucess.data.loginurl;
                resolve(mvalue);
              }
            });
          });
        }, (error) => {
          reject(error);
        }).finally(() => {
          this.loading = false;
        });
      }, (error) => {
        reject(error);
      });

    });


  }

}

export default PcModuloMainController;
