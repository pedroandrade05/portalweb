import PcProvasMainModule from './pcProvasMain';
import PcProvasOnlineMainController from './pcProvasMain.controller';
import PcProvasMainComponent from './pcProvasMain.component';
import PcProvasMainTemplate from './pcProvasMain.html';

describe('PcProvasMain', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PcProvasMainModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PcProvasOnlineMainController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Template', () => {

  });

  describe('Component', () => {
    // component/directive specs
    const component = PcProvasMainComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(PcProvasMainTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PcProvasOnlineMainController);
    });
  });
});
