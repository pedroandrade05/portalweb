import loadingGif from '../../../../assets/img/loading.gif';

class PcProvasOnlineMainController {

  constructor(
    /* Úteis */ CONFIG, $q, $sce, $state, $window, $location, $localStorage, $anchorScroll,
    /* Services */ NotifyService, CursoAlunoService, AgendamentoService, AplicacaoProvaOnlineService, EntidadeConstante,
    /* Constantes */ SituacaoConstante
  ) {
    'ngInject';

    if (!$state.params.submenu) {
      $state.params.submenu = 'atuais';
    }

    this.CONFIG = CONFIG;

    // Úteis
    this.$q = $q;
    this.$sce = $sce;
    this.$state = $state;
    this.$window = $window;
    this.$location = $location;
    this.$localStorage = $localStorage;
    this.$anchorScroll = $anchorScroll;

    // Services
    this.NotifyService = NotifyService;
    this.CursoAlunoService = CursoAlunoService;
    this.AplicacaoProvaOnlineService = AplicacaoProvaOnlineService;
    this.AgendamentoService = AgendamentoService;

    // Constantes
    this.SituacaoConstante = SituacaoConstante;
    this.EntidadeConstante = EntidadeConstante;

    // Outros
    this.loading = true;
    this.loadingGif = loadingGif;
    this.idUsuario = $localStorage.user_id;
    this.idMatricula = $state.params.idMatriculaAtiva;

    this.submenu = $state.params.submenu;
    this.idsExibidos = ($state.params.idsExibidos || '').split(',').filter((id) => Number(id)).map((id) => Number(id));
    this.matricula = this.CursoAlunoService.getStorageCursoAtivo(this.idMatricula);

    this.EntidadeFaculdadeUnyleya = +this.idEntidade === +this.EntidadeConstante.FACULDADE_UNYLEYA;
    this.EntidadePosGraduacao = +this.idEntidade === +this.EntidadeConstante.POS_GRADUACAO;
    this.EntidadeEstrategia = +this.idEntidade === +this.EntidadeConstante.ESTRATEGIA;

    // Padrões
    this.informacoes = {};
    this.grupos = [];
    this.aplicacoes = [];

    // Requests
    const requests = [
      this.getInformacoes()
    ];

    if (this.submenu !== 'informacoes') {
      requests.push(this.getAplicacoes());
    }

    $q.all(requests).finally(() => {
      this.loading = false;
    });
  }

  /**
   * Verifica se a aplicação está aberta
   * @param aplicacao
   * @returns {boolean}
   */
  isExibido(aplicacao) {
    return this.idsExibidos.indexOf(aplicacao.id_aplicacaoprovaonline) !== -1;
  }

  /**
   * Informa se o periodo da prova está expirado
   * @param aplicacao
   * @returns {boolean}
   */
  isProvaFinalizada(aplicacao) {
    const dtAtual = this.getDateISO();
    const dtFim = this.getDateISO(aplicacao.dt_termino);
    return !(+aplicacao.id_situacao === this.SituacaoConstante.APLICACAO_PROVA_ONLINE_AGUARDANDO_ALUNO && dtAtual > dtFim);
  }

  /**
   * Converter uma string ou Date em formato de data ISO String
   * @param date
   * @param fixTimeZone
   * @returns string
   */
  getDateISO(date = null, fixTimeZone = false) {
    if (!date) {
      date = new Date();
    }

    if (date instanceof Date) {
      date = date.toISOString();
    }

    date = date.substr(0, 10).split('/').reverse().join('-');

    if (fixTimeZone) {
      // o 12:00:00 é o horário médio para não ter problema com TimeZone
      date += ' 12:00:00';
    }

    return date;
  }

  /**
   * Busca informações relacionadas a prova (Mensagem Padrão)
   * @returns {*}
   */
  getInformacoes() {
    return this.AgendamentoService.getInformacoes(
      this.idMatricula
    ).then(
      (response) => {
        this.informacoes = response.toJSON();
      }
    );
  }

  //Busca Aplicações disponíveis para o aluno
  getAplicacoes() {
    this.aplicacoes = [];
    return this.AplicacaoProvaOnlineService.getAplicacoes(
      this.idMatricula
    ).then(
      (response) => {
        response.forEach((aplicacao) => {
          aplicacao.st_grupo = 'final';
          this.aplicacoes.push(aplicacao);
        });

        if (!this.aplicacoes.length) {
          this.submenu = 'informacoes';
        }

        this.getGrupos();
      }
    );
  }

  getGrupos() {
    this.gruposOrdem = ['-nu_ordem', '-id_grupo'];

    let attrValue = 'id_modulo';
    let attrLabel = 'st_modulo';

    let grupos = {};

    attrValue = 'st_grupo';
    attrLabel = 'st_grupo';

    grupos = {
      final: {
        st_grupo: 'Provas Finais',
        avaliacoes: []
      }
    };

    this.aplicacoes.forEach((aplicacao) => {
      const index = aplicacao[attrValue];
      const label = aplicacao[attrLabel];

      switch (aplicacao.id_situacao) {
      case this.SituacaoConstante.APLICACAO_PROVA_ONLINE_AGUARDANDO_ALUNO:
        aplicacao.st_situacaoprova = 'Prova liberada';
        aplicacao.bl_expandedetalhes = true;
        break;

      case this.SituacaoConstante.APLICACAO_PROVA_ONLINE_NOTA_LANCADA:
        aplicacao.st_situacaoprova = 'Prova realizada';
        aplicacao.bl_expandedetalhes = true;
        break;

      case this.SituacaoConstante.APLICACAO_PROVA_ONLINE_ALUNO_AUSENTE:
        aplicacao.st_situacaoprova = 'Período para realização expirado';
        aplicacao.bl_expandedetalhes = false;
        break;
      }

      if ((this.submenu === 'passadas' && this.isProvaFinalizada(aplicacao))
        || (this.submenu === 'atuais' && !this.isProvaFinalizada(aplicacao))) {
        return;
      }

      if (!grupos[index]) {
        grupos[index] = {
          id_grupo: index,
          st_grupo: label,
          nu_ordem: aplicacao.nu_ordemmodulo,
          avaliacoes: []
        };
      }

      grupos[index].avaliacoes.push(aplicacao);
    });

    this.grupos = Object.values(grupos);
  }

  gotoAnchor(avaliacao) {
    if (!this.anchored && this.idsExibidos.indexOf(avaliacao.id_aplicacaoprovaonline) !== -1) {
      avaliacao.expanded = true;
      avaliacao.highlighted = false;

      // Temporariamente definir o hash para rolar a página
      this.$location.hash('av' + avaliacao.id_aplicacaoprovaonline);
      this.$anchorScroll();
      this.anchored = true;

      // Remover hash
      setTimeout(() => this.$location.hash(''), 1);
    }
  }

  toggleAvaliacao(avaliacao) {
    avaliacao.expanded = !avaliacao.expanded;
    avaliacao.highlighted = true;

    const idAplicacao = avaliacao.id_aplicacaoprovaonline;

    if (avaliacao.expanded) {
      this.idsExibidos.indexOf(idAplicacao) === -1 && this.idsExibidos.push(idAplicacao);
    }

    if (!avaliacao.expanded) {
      var index = this.idsExibidos.indexOf(idAplicacao);

      if (index !== -1) {
        this.idsExibidos.splice(index, 1);
      }
    }

    this.$state.go('app.provasOnline', {
      submenu: this.submenu,
      idsExibidos: this.idsExibidos.join(',')
    }, {
      notify: false,
      inherit: true
    });

  }

  /**
   * Verifica se aplicação pode ser acessada pelo aluno
   * @param aplicacao
   * @returns {boolean}
   */
  acessaProvaMaximize(aplicacao) {
    // É necessário existir a informação de integração
    if (!aplicacao.st_codprova) {
      return false;
    }

    if (+aplicacao.id_situacao !== this.SituacaoConstante.APLICACAO_PROVA_ONLINE_AGUARDANDO_ALUNO) {
      return false;
    }

    return true;
  }

  /**
   * Abre a avaliação da Maximize
   * @param aplicacao
   */
  abrirAvaliacaoMaximize = (aplicacao) => {

    var stUrl = 'default/redirecionar/autenticar-maximize-pos?id_usuario=';

    const $win = this.$window.open(
      this.CONFIG.urlG2 + stUrl + this.idUsuario
    );

    if (!$win) {
      this.NotifyService.notify('warning', '', 'O carregamento da avaliação em uma nova aba foi bloqueada pelo navegador.');
    }

    if (aplicacao.st_linkexternoavaliacao) {
      setTimeout(() => {
        $win.location.href = aplicacao.st_linkexternoavaliacao;
      }, 1500);
    }
  };

}

export default PcProvasOnlineMainController;
