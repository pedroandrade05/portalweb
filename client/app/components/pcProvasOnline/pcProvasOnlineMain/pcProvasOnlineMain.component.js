import template from './pcProvasOnlineMain.html';
import controller from './pcProvasOnlineMain.controller';
import './pcProvasOnlineMain.scss';

const pcProvasOnlineMainComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pcProvasOnlineMainComponent;
