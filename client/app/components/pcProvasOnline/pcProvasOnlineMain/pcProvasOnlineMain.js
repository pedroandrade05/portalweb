
//Import do angular
import angular from 'angular';

//Import modulo de rotas angular
import uiRouter from 'angular-ui-router';

//Import componente do modulo
import pcProvasOnlineMainComponent from './pcProvasOnlineMain.component';

//Import do modulo de services do portal
import Services from '../../../services/Services';

//Modulo principal do componente
const pcProvasMainModule = angular.module('pcProvasOnlineMain', [
  uiRouter,
  Services.name //Injecao modulo services do portal
])
  .config(($stateProvider) => {
    "ngInject"; //Inject necessario para que o angular faca as injecoes as factorys e services

    //Definicao de rotas para o componente
    $stateProvider
      .state('app.provasOnline', {
        url: '/:idMatriculaAtiva/provasOnline/:submenu/:idsExibidos',
        params: {
          submenu: {
            value: null,
            squash: true
          },
          idsExibidos: {
            value: null,
            squash: true
          }
        },
        views: {
          content: {
            template: '<pc-provas-online-main class="component-maxheight scroll-wrapper"></pc-provas-online-main>'
          }
        }
      });
  })
  .component('pcProvasOnlineMain', pcProvasOnlineMainComponent);

export default pcProvasMainModule;
