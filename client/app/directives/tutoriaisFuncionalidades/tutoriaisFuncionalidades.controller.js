/**
 Este diretiva controla a exibicao dos video tutoriais das funcionalidades. Basta somente injetar em algum
 lugar do html a seguinte tag: <tutoriais-funcionalidades tutoriais="dados"></tutoriais-funcionalidades>
 e passar um objeto json na variavel tutoriais da tag contendo a seguinte estrutura:
 [{url: '', imagem: '', duracao: '', titulo: '' ,descricao: ''}],
 **/

class TutoriaisFuncionalidadesController {
  constructor($window, $scope, $document, $uibModal) {
    'ngInject';

    this.$window = $window;
    this.$scope = $scope;
    this.$document = $document;
    this.videosRecuperados = [];
    this.modal = $uibModal;

    this.recuperaVideosDeUrl();
  }

  recuperaVideosDeUrl() {
    this.videosRecuperados = this.tutoriais;
  }

  abrirModal(item) {
    this.modal.open({
      animation: true,
      component: 'playerTutoriaisModal',
      resolve: {
        data: () => {
          return {
            url: item.url,
            titulo: item.titulo
          };
        }
      }
    });
  }
}

export default TutoriaisFuncionalidadesController;
