/**
 * O bloco de scripts abaixo foi gerado automaticamente pelo componente generator,
 * caso tenha necessidade de criar um componente favor dar preferencia para uso deste componente.
 */

//Import do angular
import angular from 'angular';

//Import componente do modulo
import playerTutoriaisModalComponent from './playerTutoriaisModal.component';


//Modulo principal do componente
const playerTutoriaisModal = angular.module('playerTutoriaisModal', ['ui.bootstrap'] )
  .component('playerTutoriaisModal', playerTutoriaisModalComponent);

export default playerTutoriaisModal;
