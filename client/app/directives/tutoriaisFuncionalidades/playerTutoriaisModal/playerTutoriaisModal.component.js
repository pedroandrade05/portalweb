/**
  Este modulo é responsável por exibir o video dos tutoriais. Ele é chamado a partir da diretiva
  tutoriaisFuncionalidades
 **/
import template from './playerTutoriaisModal.html';

const playerTutoriaisModalComponent = {
  template,
  bindings: {
    modalInstance: "<",
    resolve: "<"
  },
  controller: function ($sce) {
    'ngInject';

    const $ctrl = this;

    $ctrl.url = $sce.trustAsResourceUrl($ctrl.resolve.data.url);
    $ctrl.titulo = $ctrl.resolve.data.titulo;

    $ctrl.close = function () {
      $ctrl.modalInstance.close(true);
    };

    $ctrl.cancel = function () {
      $ctrl.modalInstance.dismiss(false);
    };

  }
};

export default playerTutoriaisModalComponent;
