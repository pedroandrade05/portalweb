//Import do angular
import angular from 'angular';

//Import componente do modulo
import tutoriaisFuncionalidadesComponent from './tutoriaisFuncionalidades.component';

//Modulo principal do componente
const tutoriaisFuncionalidadesModulo = angular.module('tutoriaisFuncionalidades', []).component('tutoriaisFuncionalidades', tutoriaisFuncionalidadesComponent);

export default tutoriaisFuncionalidadesModulo;
