import controller from './tutoriaisFuncionalidades.controller';
import template from './tutoriaisFuncionalidades.html';
import './tutoriaisFuncionalidades.scss';

const tutoriaisFuncionalidadesComponent = {
  name: 'tutoriaisFuncionalidades',
  restrict: 'E',
  bindings: {
    tutoriais: '<'
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default tutoriaisFuncionalidadesComponent;
