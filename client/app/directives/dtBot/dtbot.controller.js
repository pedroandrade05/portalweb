import imagem_bot from '../../../assets/img/bot_unyleya.gif';
import avatar_bot from '../../../assets/img/avatar_bot.png';

class DtBotController {
  constructor($window, $scope, $document) {
    'ngInject';

    this.imagem_bot = imagem_bot;
    this.avatar_bot = avatar_bot;
    this.$window = $window;
    this.$scope = $scope;
    this.$document = $document;
    this.mostrarBot = 'hidden';
    this.mostrarTooltip = 'hidden';
    this.mostrarAvatar = 'hidden';

    $scope.$watch(() => {
      return this.url;
    }, (valor) => {
      if (angular.isDefined(valor) && valor !== '') {
        var scriptElem = angular.element('<script/>');
        scriptElem.attr('src', this.url);
        scriptElem.attr('id', 'dtbot-script');

        var body = this.$document.find('body');

        body.append(scriptElem);
      }
    });

    this.$document.ready(
      () => {
        this.checaBot();
      }
    );
  }

  checaBot() {
    /*eslint-disable */
    setTimeout(() => {
      if (document.querySelectorAll('div.dt-BOTfloater')[0] !== undefined) {
        this.mostrarBot = 'showBot';
        this.minimizar();
        this.$scope.$apply();
      }
      else {
        this.checaBot();
      }
    }, 1000);
    /*eslint-enable */
  }

  minimizar() {
    this.mostrarBot = this.mostrarBot === 'showBot' ? 'hidden' : 'showBot';
    this.mostrarAvatar = this.mostrarAvatar === 'showBot' ? 'hidden' : 'showBot';
  }

  abrir() {
    this.$window.DTBOT.start(this.bot, true);
    this.$window.DTBOT.show();
    this.minimizar();
    this.mostrar = 'hidden';
  }
}

export default DtBotController;
