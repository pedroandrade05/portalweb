//Import do angular
import angular from 'angular';

//Import componente do modulo
import dtBotComponent from './dtbot.component';

//Modulo principal do componente
const dtBotModule = angular.module('dtbot', []).component('dtbot', dtBotComponent);

export default dtBotModule;
