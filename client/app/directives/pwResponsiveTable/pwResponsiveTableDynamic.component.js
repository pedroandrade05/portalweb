
import './pwResponsiveTable.scss';

function updateTitle(td, th) {
  var title = th && th.textContent;
  if (title && (td.getAttribute('data-title-override') || !td.getAttribute('data-title'))) {
    td.setAttribute('data-title', title);
    td.setAttribute('data-title-override', title);
  }
}

function pwResponsiveTableDynamicComponent() {
  return {
    restrict: 'E',
    require: '?^^wtResponsiveTable',
    link (scope, element, attrs, tableCtrl) {
      if (!tableCtrl) {
        return;
      }
      if (!tableCtrl.contains(element[0])) {
        return;
      }

      setTimeout(() => {
        [].forEach.call(element[0].parentElement.children, (td) => {
          if (td.tagName !== 'TD') {
            return;
          }

          var th = tableCtrl.getHeader(td);
          updateTitle(td, th);
        });
      }, 0);
    }
  };
}

export default pwResponsiveTableDynamicComponent;
