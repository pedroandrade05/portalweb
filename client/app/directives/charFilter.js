// import angular from 'angular';
// let directivesModule = angular.module('app.directives', [])
//
//
// /**
// * Filtra o campo input para evitar caracteres inválidos, hoje ela é usada no Login, caso precise usar em outro lugar, favor fazer a evolução da mesma e atualizar no login
// */
// .directive('charFilter', function ($document) {
//   var validExpression = function (e) {
//
//     var vKey = 86;
//     var atualKey = (e.keyCode||e.which||e.charCode);
//     var char = String.fromCharCode(atualKey);
//
//     if(e.ctrlKey==true && atualKey == vKey){
//       $document.getElementsByTagName(e.target.tagName)[0].value = '';
//       e.preventDefault();
//     }
//
//     var checkChar = char.match(/^[0-9a-zA-Z@.]+$/);
//     if(!((checkChar && checkChar.length) || _.indexOf([8, 9,37,38,39,40], atualKey)>-1) || e.code == "Quote"){
//       e.preventDefault();
//     }
//   }
//   return {
//     link: function (scope, elm) {
//       elm.bind('keypress', validExpression);
//       elm.bind('keyup', validExpression);
//       elm.bind('blur', validExpression);
//     }
//   }
// });
//
// export default directivesModule;
