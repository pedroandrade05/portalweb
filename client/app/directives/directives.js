import angular from 'angular';

// Padrão definido em http://confluence.unyleya.com.br/pages/viewpage.action?spaceKey=DEV&title=Desenvolvimento+Novo+Portal
import pwChart from './pwChart/pwChart';
import pwResponsiveTable from './pwResponsiveTable/pwResponsiveTable';
import dtBot from './dtBot/dtbot';
import tutoriaisFuncionalidades from './tutoriaisFuncionalidades/tutoriaisFuncionalidades';

export default angular.module('app.directives', [
  pwChart.name,
  pwResponsiveTable.name,
  dtBot.name,
  tutoriaisFuncionalidades.name
]);
