import template from './pwChart.html';
import controller from './pwChart.controller';
import './pwChart.scss';

const pwChartComponent = {
  restrict: 'E',
  bindings: {
    mysize: '@',
    total: '=',
    completed: '=',
    color: '@',
    statuscomplete: '=statuscomplete',
    statussala: '=statussala'
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default pwChartComponent;
