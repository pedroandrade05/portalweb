class PwChartController {

  sizes = {
    small: 60,
    medium: 70,
    big: 85
  };

  constructor() {
    this.name = 'pwChart';

    // Calculando porcentagem completa de atividades
    this.percent = Math.round(this.completed / this.total * 100);

    // Configurar chart na primeira execução da Controller.
    this.configurarChart();
  }

  configurarChart() {
    this.strokeColor = '#4caf50';

    this.size = this.sizes[this.mysize] || this.sizes.medium;

    if (this.isEncerrado()) {
      this.percent = 100;
      this.strokeColor = '#7fa2d5';
    }

    if (this.isNaoIntegrado()) {
      this.percent = 100;
      this.strokeColor = '#ef6f0e';
    }

    // Calculando a área transparente do gráfico.
    const result = 100 - this.percent;

    this.labels = ['', '']; // Deixando labels vazias.
    this.data = [this.percent, result]; // Definindo dados do gráfico

    this.options = {
      animation: false,
      cutoutPercentage: 70,
      tooltips: {
        enabled: false
      },
      plugins: {
        labels: {
          render: 'label'
        }
      }
    };

    // Aplicando ao gráfico a cor selecionada baseada no status.
    this.colors = [
      {
        backgroundColor: this.strokeColor,
        borderColor: this.strokeColor,
        pointBackgroundColor: this.strokeColor,
        pointBorderColor: this.strokeColor,
        pointHoverBackgroundColor: this.strokeColor,
        pointHoverBorderColor: this.strokeColor
      },
      {
        backgroundColor: 'rgba(0,0,0,0)',
        borderColor: 'rgba(0,0,0,0)',
        pointBackgroundColor: 'rgba(0,0,0,0)',
        pointBorderColor: 'rgba(0,0,0,0)',
        pointHoverBackgroundColor: 'rgba(0,0,0,0)',
        pointHoverBorderColor: 'rgba(0,0,0,0)'
      }
    ];
  }

  isEncerrado() {
    return [2, 4].includes(+this.statussala);
  }

  isNaoIntegrado() {
    return (+this.statussala === 4);
  }

}

export default PwChartController;
