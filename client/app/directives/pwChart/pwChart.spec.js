import PwChartModule from './pwChart';
import PwChartController from './pwChart.controller';
import PwChartComponent from './pwChart.component';
import PwChartTemplate from './pwChart.html';

describe('PwChart', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PwChartModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PwChartController();
    };
  }));

  describe('Module', () => {

    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {

  });

  describe('Component', () => {
      // component/directive specs
    const component = PwChartComponent;

    it('includes the intended template',() => {
      expect(component.template).to.equal(PwChartTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(PwChartController);
    });
  });
});
