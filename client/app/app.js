// Libs
import angular from "angular";
import "angular-i18n/angular-locale_pt-br.js";
import uiRouter from "angular-ui-router";
import ngAnimate from "angular-animate";
import "jquery";
import hljs from "angular-highlightjs";
import "angular-input-masks";
import "angular-sanitize";
import "angular-material";
import "ngstorage/ngStorage.min.js";
import "alertifyjs/build/alertify.min.js";
import "alertifyjs/build/css/alertify.min.css";
import "alertifyjs/build/css/themes/default.min.css";
import "alertifyjs/build/css/themes/default.min.css";
import "angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css";

//Graficos
import "angular-ui-bootstrap";

// import Chart from 'chart.js/dist/Chart.bundle';
import Chart from "chart.js/dist/Chart.min.js";
import angularChart from "angular-chart.js/dist/angular-chart.min.js";
import 'chartjs-plugin-labels';

//Ladda
import "ladda/dist/spin.min";
import "ladda/dist/ladda.min";
import angularLadda from "angular-ladda";

//Lib ngFileUpload para upload de arquivos
import "ng-file-upload/dist/ng-file-upload.min.js";

import "ng-img-crop/compile/minified/ng-img-crop.js";

// Components
import Directives from "./directives/directives";
import Common from "./common/common";
import Components from "./components/components";
import Widgets from "./widgets/widgets";
import UI from "./ui/ui";
import AppComponent from "./app.component";

import Filters from "./filters/filters";

angular
  .module("app", [
    uiRouter,
    angularLadda,
    "ui.bootstrap",
    require("angular-input-masks"),
    require("angular-sanitize"),
    require("angular-bootstrap-calendar"),
    "ngStorage",
    hljs,
    angularChart,
    ngAnimate,
    "ngFileUpload",
    "ngImgCrop",
    Common.name,
    Components.name,
    Filters.name,
    Directives.name,
    Widgets.name,
    UI.name
  ])
  .config(($locationProvider, hljsServiceProvider) => {
    "ngInject";

    // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
    // #how-to-configure-your-server-to-work-with-html5mode
    $locationProvider.html5Mode(true).hashPrefix("!");
    hljsServiceProvider.setOptions({

      // replace tab with 4 spaces
      tabReplace: ""
    });

    // ChartJsProvider.setOptions({
    //     chartColors: ['#FF5252', '#FF8A80'],
    //     responsive: false
    //   });
    //
    //configs gerais do chart
    Chart.defaults.global.elements.arc.borderWidth = 0;
    Chart.defaults.global.elements.arc.backgroundColor = "rgba(0,0,0,0)";
    Chart.defaults.global.elements.arc.defaultColor = "rgba(0,0,0,0)";
  })
  .run(($window, $rootScope, $location, CONFIG) => {
    "ngInject";

    /*eslint angular/on-watch: 0*/
    //Config do analytics

    //Registra um evento ara quando a rota mudar
    $rootScope.$on("$stateChangeSuccess", () => {
      //pegao path e quebra usando a barra
      var urlToRegister = $location.path().split("/");

      // Pega o nome da rota que fica no terceiro parametro e usando para registrar
      $window.ga("send", "pageview", urlToRegister[2]);
    });
    $window.ga("create", CONFIG.analytics, "auto");
  })

  .component("app", AppComponent);
