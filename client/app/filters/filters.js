import angular from 'angular';

const filtersModule = angular.module('app.filters', [])
  .filter('capitalize', () => {
    return function(input) {

      // Precisa mesmo do parâmetro all ?

      const alwaysToLower = ["em", "de", "e", "é", "a", "o", "da", "do", "dos", "das", "na", "com", "por", "para"]; // adicionar aqui todas as palavras que normalmente não precisam de alterações
      const words = input.split(' ');
      const capitalized = words.map((word) => {
        if (alwaysToLower.indexOf(word.toLowerCase().trim()) > -1) {
          return word.toLowerCase();
        }
        return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
      });
      return capitalized.join(' ');
    };
  })
  .filter('removeHtmlTags', () => {
    return function(text) {
      let html = String(text).replace(/<style[^*]+style>/gm, '');
      html = String(html).replace(/<[^>]+>/gm, '');
      html = String(html).replace("&nbsp;", " ");
      return html.trim();
    };
  })
  .filter('asDate', ($filter) => {
    'ngInject';
    return function (input, format) {
      if (!input) {
        return '';
      }

      if (angular.isString(input)) {
        input = input.substr(0, 10);
      }

      return $filter('date')(input, format || 'dd/MM/yyyy HH:mm:ss');
    };
  })
  .filter('parseHtml', ($sce) => {
    'ngInject';
    return function(value) {
      if (!value) {
        return '';
      }
      return $sce.trustAsHtml(value);
    };
  })
  .filter('parseHtmlSemStyle', ($sce) => {
    'ngInject';
    return function(value) {
      if (!value) {
        return '';
      }
      const html = String(value).replace(/<style[^*]+style>/gm, '');
      return $sce.trustAsHtml(html);
    };
  })
  .filter('sliceString', () => {
    return function(value, wordwise, max, tail) {
      if (!value) {
        return '';
      }

      max = parseInt(max, 10);
      if (!max) {
        return value;
      }
      if (value.length <= max) {
        return value;
      }

      value = value.substr(0, max);
      if (wordwise) {
        const lastspace = value.lastIndexOf(' ');
        if (lastspace !== -1) {
          value = value.substr(0, lastspace);
        }
      }

      return value + (tail || '…');
    };
  })
  .filter('trim', () => {
    return function(text) {
      if (!angular.isString(text)) {
        return text;
      }
      const html = String(text).replace(/^\s+|\s+$/g, '');
      return html.trim();
    };
  })
  .filter('firstAndLast', () => {
    return function(text) {
      const words = text.split(" ");
      const name = words.shift() + " " + words.pop();
      return name;
    };
  })
  .filter("trust", ($sce) => {
    return function(htmlCode){
      return $sce.trustAsHtml(htmlCode);
    };
  })
  .filter("getCorStatus", () => {
    return function(cor){
      var retorno = '';
      switch (cor){
      case 'Não Cursado':
        retorno = 'azul';
        break;
      case 'Aprovado':
        retorno = 'verde';
        break;
      case 'Reprovado':
        retorno = 'vermelho';
        break;
      case 'Em Prova Final':
        retorno = 'amarelo';
        break;
      }
      return retorno;
    };
  })
  .filter('groupBy', ($parse) => {
    "ngInject";

    return function _groupBy(collection, property) {
      const getter = $parse(property);
      const result = {};

      let prop;

      angular.forEach(collection, (elm) => {
        prop = getter(elm);

        if (!result[prop]) {
          result[prop] = [];
        }

        result[prop].push(elm);
      });

      return result;
    };
  })
  .filter("getNomeOferta", () => {
    return function(id){
      var retorno = '';
      switch (id){
      case 1:
        retorno = 'Padrão';
        break;
      case 2:
      case 3: //Oferta Perene exibir como estendida
        retorno = 'Estendida';
        break;
      case 4:
        retorno = 'Excepcional';
        break;
      case 5:
        retorno =  'Optativa';
        break;
      }
      return retorno;
    };
  })
  .filter('removeSpecialChars', () => {
    return function (input) {
      return input
        .replace(/[áàâãäª]/g, 'a')
        .replace(/[ÁÀÂÃÄ]/g, 'A')
        .replace(/[éèêë]/g, 'e')
        .replace(/[ÉÈÊË]/g, 'E')
        .replace(/[íìîï]/g, 'i')
        .replace(/[ÍÌÎÏ]/g, 'I')
        .replace(/[óòôõöº]/g, 'o')
        .replace(/[ÓÒÔÕÖ]/g, 'O')
        .replace(/[úùûü]/g, 'u')
        .replace(/[ÚÙÛÜ]/g, 'U')
        .replace(/ç/g, 'c')
        .replace(/Ç/g, 'C')
        .replace(/ñ/g, 'n')
        .replace(/Ñ/g, 'N')
        .replace(/–/g, '-')
        .replace(/[’‘‹›‚]/g, ' ')
        .replace(/[“”«»„]/g, ' ')
        .replace(/ /, ' ');
    };
  })
  .filter('asDateMinus', ($filter) => {
    'ngInject';
    return function(input, format, minus) {
      if (input === null || angular.isUndefined(input)) {
        return "";
      }

      var data = new Date(input.replace(/-/g, "/").split(" ").splice(0,1));
      var dia = angular.isDefined(minus) ? data.getDate() - minus : data.getDate();
      return $filter('date')(data.setDate(dia), format ? format : 'dd/MM/yyyy HH:mm:ss');
    };
  })
  .filter('asDateHour', ($filter) => {
    'ngInject';
    return function (input, format) {
      if (!input) {
        return '';
      }

      return $filter('date')(new Date(input), format || 'dd/MM/yyyy HH:mm:ss');
    };
  })
  .filter('asCPF', () => {
    return function (input) {
      if (!input) {
        return '';
      }
      let str = input + '';
      str = str.replace(/\D/g, '');
      str = str.replace(/(\d{3})(\d)/, "$1.$2");
      str = str.replace(/(\d{3})(\d)/, "$1.$2");
      str = str.replace(/(\d{3})(\d{1,2})$/, "$1-$2");

      return str;
    };
  })
  .filter('asRG', () => {
    return function (input) {
      if (!input) {
        return '';
      }
      let str = input + '';
      str = str.replace(/\D/g, "");
      str = str.replace(/(\d{2})(\d{3})(\d{3})(\d)$/, "$1.$2.$3-$4");
      return str;
    };
  })
  .filter("getStatusLancamentoFinanceiro", () => {
    return function(id){
      var retorno = '';
      switch (+id){
      case 1:
        retorno = 'VENCE EM BREVE';
        break;
      case 2:
        retorno = 'VENCIDA';
        break;
      case 3:
        retorno = 'A VENCER';
        break;
      case 4:
        retorno = 'PAGA';
        break;
      }
      return retorno;
    };
  })
  .filter("getCorStatusLancamentoFinanceiro", () => {
    return function(id){
      var retorno = '';
      switch (+id){
      case 1:
        retorno = '#000000';
        break;
      case 2:
        retorno = '#FA5A55';
        break;
      case 3:
        retorno = '#4097D6';
        break;
      case 4:
        retorno = '#28C968';
        break;
      }
      return retorno;
    };
  });

export default filtersModule;
